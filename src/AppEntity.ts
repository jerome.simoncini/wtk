import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { ASSERT, ExtensionMap, MESSAGE } from "./utils.js";

export class AppEntity {
    private __objectId: number | undefined = undefined;
    private _objectType: string = '';
    private _objectName: string = '';
    protected _$: any[] = [];
    protected _extensions: ExtensionMap = new ExtensionMap();
    protected _parent: AppEntity | undefined = undefined;
    protected _fakeRoot: boolean = false;
    private _module: AppEntity | undefined = undefined;

    constructor(id: number = 1) {
        this.setObjectId(id);
        this.setObjectType("AppEntity");
    }

    get __type__() {
        return this._objectType;
    }

    get __name__() {
        return this._objectName;
    }

    get _objectId(): number {                //
        return this.__objectId || 0;
    }

    private setObjectId(id: number) {            
        this.__objectId = id;
    }

    get extensions(): ExtensionMap {
        return ASSERT(this._extensions, MESSAGE(this, `extensions map undefined`));
    }

    get parent(): AppEntity | undefined {
        return this._parent;
    }

    get inode(): string {
        return this._fakeRoot || !this.parent ? `${Scope.ROOT}` : `${this.parent.inode}:${this._objectId}`;
    }

    get _inode(): string {
        return !this.parent ? `${Scope.ROOT}:${this._objectId}` : `${this.parent.inode}:${this._objectId}`;
    }

    get fakeRoot(): boolean {
        return this._fakeRoot;
    }

    get module(): AppEntity {        /////
        return ASSERT(this._module, MESSAGE(this, `app module undefined`));
    }

    getParent<T extends AppEntity>(parent_type: Constructor<T>): T | undefined {      ///////////
        return this._parent instanceof parent_type ? this.parent as T : undefined;
    }

    getModule<T extends AppEntity>(module_type: Constructor<T>): T | undefined {      ///////////
        return this._module instanceof module_type ? this.module as T : undefined;
    }

    setAppModule(module: AppEntity) {
        this._module = module;
    }

    setFakeRoot(enable: boolean) {
        this._fakeRoot = enable;
    }

    setParent(parent: AppEntity) {
        this._parent = parent;

        if(this.__name__== '') {
            this.setObjectName(this.inode);
        }
    }

    setObjectType(objectType: string) {
        this._objectType = objectType;
    }

    setObjectName(objectName: string) {
        this._objectName = objectName.replaceAll(' ','_');
    }

    setState = (name, value) => {
        this._$[name] = value;
    };

    $ = (name) => {
        return this._$[name];
    };

    getId(name: string) {
        return `${name}-${this._inode.replaceAll(':','-')}`;      ///////
    }

    pack<T extends AppEntity>(source: Constructor<T>): IPack<AppEntity> {
        return this._onPack(this);
    }

    unpack(pack: IPack<AppEntity>) {
        this._onUnpack(this);
    }

    onCreate(...args: any[]): any { }
    onDestroy(...args: any[]): any { }
    onInit(...args: any[]): any { }

    _onPack(entity: AppEntity): IPack<AppEntity> { return {
        source: AppEntity,
        data: {}
    }}

    _onUnpack(entity: AppEntity): void {}

    log(inode: string, level: LogLevel, message: string) {}
}

export enum LogLevel {
    INFO,
    WARNING,
    ERROR
}

export interface IPack<T extends AppEntity> {
    source: Constructor<T>,
    data: object
}