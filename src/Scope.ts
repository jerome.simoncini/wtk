enum SystemScope {
    ROOT,
    ////////////////////
    NAME = -9999,
    PATH,
    TYPE,
    INODE,
    ////////////////////
    MEMORY,
    ////////////////////
    FLAGS,
    ////////////////////
    SLOTS,
    STYLES,
    ////////////////////
    GLOBAL,
    LOCAL,
    ////////////////////
    LIST,
    ////////////////////
    ENTITY
}

type Scope = SystemScope
let Scope = { ...SystemScope }; //
export { Scope };