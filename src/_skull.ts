import { AppEntity } from "./AppEntity.js";
import { Constructor } from "./Constructor.js";
import { IAppEntity, IAppObject } from "./_gen_object.js";

export interface ISkull<S extends IAppEntity> {
    __name__?: string;
    settings?: S;
    onCreate?: (_: AppEntity, settings?: S) => void,
    onInit?: (_: AppEntity, settings?: S) => Promise<void>,
};

export const __skull__ = <S extends IAppEntity>(
    objectType: Constructor<AppEntity>,
    skull: ISkull<S> = {}): Constructor<AppEntity> => {
    return class extends objectType {
        onCreate(...args: any[]): void {
            if (skull.onCreate) {
                skull.onCreate(this, skull.settings);
            }
            super.onCreate(...args);
        }
        async onInit(...args: any[]) {
            if (skull.onInit) {
                await skull.onInit(this, skull.settings);
            }
            await super.onInit(...args);
        }
    } as Constructor<AppEntity>;
}
