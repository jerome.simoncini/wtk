import { WTK_GEN_MEMORY } from "./_gen_memory.js";
import { WTK_DB_LOGIC } from "./AppDB.js";
import { AppDbEventCode, EventType } from "./AppEvent.js";
import { AppObject } from "./AppObject.js";
import { StateObject } from "./AppState.js";
import { System } from "./events.js";
//import { LOGGER } from "./Logger.js";
import { IMemoryUpdate, MemoryEvent, MemoryPool, MemRef } from "./MemoryPool.js";
import { ModelGroupItem } from "./ModelGroup.js";
import { ASSERT, MESSAGE } from "./utils.js";
/*
const THIS = "App DB Store"
const DEBUG = LOGGER("App DB");
DEBUG.on();
*/

export enum RequestMethod {
    CREATE,
    READ,
    UPDATE,
    DELETE
}

export interface IAppDBRequest {
    method: RequestMethod;
    content?: any;    //
}

export interface IQueryResult {
    success: boolean;
    response?: any; //
}


export interface AppDBStoreDTO {
    key: string;
    data: string;
}
export class AppDBStoreDTO { }

export class AppDBStoreDTOSpec<T extends StateObject> extends AppDBStoreDTO {
    insert(key: string, object: T): void {
        this.key = key;
        this.data = object._serialize();
    }
}

export interface IQueryResultHandler {
    onSuccess: (...args: any[]) => any;
    onError?: (...args: any[]) => any;
}

class ClientQueryPool extends MemoryPool<number, IQueryResult> { }

const KEY_PATH: string = 'key';

export const _dbStoreCREATE = (_: WTK_DB_STORE_LOGIC): void => {                /////
    _.createLocalStore();
    _.setQueryPool(_._localStore.get(WTK_GEN_MEMORY(ClientQueryPool,{name:'db_store_memory'}), _.getGlobalStore()));
    _.startObserver([System.MEMORY_UPDATE]);
    _.register(System.MEMORY_UPDATE, MemoryEvent.WRITE, (params) => _.onQueryResult(params));
}

export class WTK_DB_STORE_LOGIC extends AppObject {
    private _nextKey: number = 1;
    private _db: WTK_DB_LOGIC | undefined = undefined;
    protected _name: string | undefined = undefined;   //
    protected _queryPool: ClientQueryPool | undefined = undefined;
    protected _queries: Map<MemRef<number, IQueryResult>, IQueryResultHandler> = new Map();

    setName(name: string) {
        this._name = name;
    }

    setDatabase(db: WTK_DB_LOGIC) {
        this._db = db;
    }

    setQueryPool(pool: ClientQueryPool) {
        this._queryPool = pool;
    }

    createStore(cnx: any) {
        ASSERT(this.name, MESSAGE(this,`(create store) name undefined`));
        const store = cnx.createObjectStore(this.name, { keyPath: KEY_PATH, autoIncrement: true });
        store.createIndex(KEY_PATH, KEY_PATH, { unique: true });
        return store;
    }

    onQueryResult(params: any) {
        const memoryUpdate = params as IMemoryUpdate;
        ASSERT(memoryUpdate, `(${this.constructor.name}) 'on query update' error`);
        const refKey = memoryUpdate.key;
        const poolId = memoryUpdate.poolId;
        //DEBUG.log(`(on query result) key=${refKey} poolId=${poolId}`);
        const ref = Array.from(this._queries.keys()).find(ref => ref.key.toString() == refKey);
        if (ref && poolId == this._queryPool?._objectId) {
            //DEBUG.log(`(on query result) success=${ref.value.success}`);
            const hdl = this._queries.get(ref)!;
            if (ref.value.success) {
                hdl.onSuccess(ref.value.response);
            }
            else if (hdl.onError) {
                hdl.onError(ref.value.response);
            }
        }
    }

    get name() {
        return ASSERT(this._name, `(${this.constructor.name}) store name undefined`);
    }

    get db() {
        return ASSERT(this._db, `(${this.constructor.name}) db undefined`);
    }

    get queryPool() {
        return ASSERT(this._queryPool, `(${this.constructor.name}) query pool undefined`)
    }

    private _genRef(hdl: IQueryResultHandler) {
        const ref = ASSERT(
            this.queryPool.createRef(this._nextKey++, MemRef) as MemRef<number, IQueryResult>,
            MESSAGE(this,`(create) ref undefined`)
        );
        this._queries.set(ref, hdl);
        return ref;
    }

    create<T extends ModelGroupItem>(key: string, data: T, hdl: IQueryResultHandler): MemRef<number, IQueryResult> {
        const ref = this._genRef(hdl);
        const dto: AppDBStoreDTOSpec<T> = new AppDBStoreDTOSpec<T>();
        dto.insert(key, data);
        this._create(dto, ref);
        return ref;
    }

    getAll(hdl: IQueryResultHandler): MemRef<number, IQueryResult> {
        const ref = this._genRef(hdl);
        this._getAll(ref);
        return ref;
    }

    delete(key: string, hdl: IQueryResultHandler): MemRef<number, IQueryResult> {
        const ref = this._genRef(hdl);
        this._delete(key, ref);
        return ref;
    }

    async _save<T extends ModelGroupItem>(key: string, data: T, hdl: IQueryResultHandler, index: string | null = null, indexValue: any = null) {        //  -> async
        //DEBUG.log("_SAVE: " + JSON.stringify(data));
        const useIndex = index && indexValue;
        ASSERT(useIndex && index, MESSAGE(this,`index undefined`)); //
        this._getOneByIndex(index!, indexValue, (result) => {
            if (result.success) {
                this.modify(result.success, data);
            }
            else {
                this.create(key, data, hdl);
            }
        });

    }

    modify<T extends StateObject>(success: boolean, data: T) {  //
    }

    _create<T extends ModelGroupItem>(dto: AppDBStoreDTOSpec<T>, ref: MemRef<number, IQueryResult>) {       //
        this.db.connect((event) => {
            const cnx = event.target.result;
            const txn = cnx.transaction(this.name, 'readwrite');
            const store = txn.objectStore(this.name);

            let query = store.put(dto);
            query.onsuccess = (event: any) => {
                console.log(`DB SUCCESS refKey=${ref.key}`);
                ref.setValue({
                    success: true,
                    response: {
                        type: EventType.INFO,
                        code: AppDbEventCode.DB_CREATE_SUCCESS,
                        data: {
                            message: "object created",
                            object: JSON.stringify(event.result)
                        }
                    }
                });
            }
            query.onerror = (event: any) => {
                ref.setValue({
                    success: false,
                    response: {
                        type: EventType.ERROR,
                        code: AppDbEventCode.DB_CREATE_ERROR,
                        data: {
                            message: "object create error",
                            error: event.target.error.name
                        }
                    }
                });
            }
            txn.oncomplete = () => cnx.close();
        });
    }

    _delete(key: any, ref: MemRef<number, IQueryResult>) {        //  -> async
        this.db.connect((event) => {
            const cnx = event.target.result;
            const txn = cnx.transaction(this.name, 'readwrite'); //
            const store = txn.objectStore(this.name);

            if (store) {
                //DEBUG.log("KEY PATH: " + store.keyPath);
                //DEBUG.log("DELETE KEY: " + key)

                let query = store.delete(key);

                query.onsuccess = function (event) {
                    //DEBUG.log("DELETED");
                    ref.setValue({ success: true });
                };

                query.onerror = function (event) {
                    ref.setValue({ success: false });
                    // notify error
                }
            }
            else {
                // notify error
            }

            txn.oncomplete = function () {      //
                cnx.close();                   //
            }
        });
    }

    _getAll(ref: MemRef<number, IQueryResult>) {
        this.db.connect((event) => {
            const cnx = event.target.result;
            const txn = cnx.transaction(this.name, 'readonly');
            const store = txn.objectStore(this.name);
            const index = store.index(KEY_PATH);
            if (index) {
                const query = index.getAll();
                query.onsuccess = () => {
                    //DEBUG.log(`(getAll) ${JSON.stringify(query.result)}`);
                    ref.setValue({ success: true, response: query.result ?? [] });
                }
                query.onerror = (event) => {
                    ref.setValue({ success: false, response: event.target.error.name });
                }
            }
            else {
                //DEBUG.log(`unknown index name '${KEY_PATH}`);
                // norify error
            }
            txn.oncomplete = function () {
                cnx.close();
            }
        });
    }

    _getOneByIndex(
        indexName: string,
        value: any,
        output: (result: IQueryResult) => void) {
        this.db.connect((event: any) => {
            const cnx = event.target.result;
            const txn = cnx.transaction(this.name, 'readonly');
            const store = txn.objectStore(this.name);
            const index = store.index(indexName);
            if (index) {
                let query = index.get(value);

                query.onsuccess = (event) => {
                    output({ success: query.result ?? null });
                };

                query.onerror = (event) => {
                    output({ success: false, response: event.target.error.name });
                }
            }
            txn.oncomplete = function () {
                cnx.close();
            };
        });
    }
}