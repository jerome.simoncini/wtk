import { Constructor } from "./Constructor.js";
import { WTK_HTTP_SERVER_LOGIC } from "./server/HttpServer.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";

const TYPE = "HttpServer";

export interface IHttpServer extends IAppObject {
    startHttpService?: () => void;
    startHttpsService?: () => void;
    readFile: (url: string) => Promise<BodyInit>;
};

export const WTK_GEN_HTTP_SERVER = (server: Constructor<WTK_HTTP_SERVER_LOGIC>, settings?: IHttpServer): Constructor<WTK_HTTP_SERVER_LOGIC> => {
    const _component = __skull__(server, {
        onCreate: _create_http_server_, 
        onInit: _init_http_server_, 
        settings: settings
    }) as Constructor<WTK_HTTP_SERVER_LOGIC>;
    return WTK_GEN_OBJECT(_component, settings as IAppObject) as Constructor<WTK_HTTP_SERVER_LOGIC>
};

const _create_http_server_ = (server: WTK_HTTP_SERVER_LOGIC, settings: IHttpServer): void => {
    if(settings.startHttpService) {
        server._startHttpService = settings.startHttpService;
    }
    if(settings.startHttpsService) {
        server._startHttpsService = settings.startHttpsService;
    }
    server._readFile = settings.readFile;
    server.createLocalStore();
    server.setObjectType(TYPE);
};

const _init_http_server_ = async (component: WTK_HTTP_SERVER_LOGIC, settings?: IHttpServer) => {
};