import { AppEntity } from "./AppEntity.js";
import { AppObject } from "./AppObject.js";
import { Constructor } from "./Constructor.js";

class ObjectFactory<T extends AppObject> extends AppObject {

    create(importType: Constructor<T>, ...args: any[]): T {
        const store = this.getGlobalStore();
        if (!store) {
            throw 'global store undefined'
        }
        const object = new importType(store._genId());
        object.setGlobalStore(store);
        object.setParent(this.parent||this);               ///////////////////////////////
        if(this.getModule(AppEntity)) {        //////
            object.setAppModule(this.module);
        }
        object.onCreate(...args);
        return object;
    }
}

export { ObjectFactory };