import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { WTK_CONTAINER_ID, WTK_CONTAINER_LOGIC } from "./AppContainer.js";
import { AppModule } from "./AppModule.js";
import { Constructor } from "./Constructor.js";
import { WTK_GEN_COMPONENT } from "./_gen_component.js";


export class WTK_OVERLAY_LOGIC extends WTK_CONTAINER_LOGIC {

    createLayer(id: WTK_CONTAINER_ID, layer: Constructor<WTK_COMPONENT_LOGIC>) {
        this.registerContent(WTK_GEN_COMPONENT(layer, {
            styles: ['wtk-overlay']
        }), id);
    }

    async close() {
        await this.clear();
    }

    async show(layer: WTK_CONTAINER_ID) {
        console.warn(layer);
        await this.setContent(layer)
            .then(() => {
                this.render()
            })
            .then(() => {

                //////////////////////////////
                const module = this.getModule(AppModule);
                if (module) {
                    console.warn(module);
                    console.warn(`width=${module.root.clientWidth} height=${module.root.clientHeight}`);
                    const content = this.getContent(layer);
                    console.warn(content)
                    if (content) {
                        try {
                            (<HTMLElement>content.root).style.width = module.root.clientWidth + 'px';
                            (<HTMLElement>content.root).style.height = module.root.clientHeight + 'px';
                        }
                        catch (error) {
                            console.error(error);
                        }
                    }
                    else {
                        // notify error
                    }
                }
                else {
                    // notify error
                }

            })
    }

}