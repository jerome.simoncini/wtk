import { IAppRoute, WTK_APP_ROUTER_LOGIC } from "./AppRouter.js";
import { _HTML_ } from "./Compiler.js";
import { Constructor } from "./Constructor.js";
import { IAppContainer, WTK_GEN_CONTAINER } from "./_gen_container.js";
import { __skull__ } from "./_skull.js";
import { WTK_CONTAINER_LOGIC } from "./AppContainer.js";
import { System } from "./events.js";

const TYPE = "AppRouter";

export interface IAppRouter extends IAppContainer {
    routes: IAppRoute[];
};

export const WTK_GEN_APP_ROUTER = (router: Constructor<WTK_APP_ROUTER_LOGIC>, settings: IAppRouter): Constructor<WTK_APP_ROUTER_LOGIC> => {
    const skull = __skull__(router, {
        onCreate: _create_app_router_, 
        onInit: _init_app_router_, 
        settings: settings
    }) as Constructor<WTK_CONTAINER_LOGIC>;
    return WTK_GEN_CONTAINER(skull, {
        ...settings
    }) as Constructor<WTK_APP_ROUTER_LOGIC>;
};

const _create_app_router_ = (router: WTK_APP_ROUTER_LOGIC, settings: IAppRouter): void => {
    router.addNotifierActions([      //////
        System.ROUTER_UPDATE,
        System.ROUTER_ERROR
    ]);
    router.setObjectType(TYPE)
};

const _init_app_router_ = async (router: WTK_APP_ROUTER_LOGIC, settings: IAppRouter): Promise<void> => {
    router.initRoutes(settings.routes);
    router.setCurrentPath(router.origin);    //////
};