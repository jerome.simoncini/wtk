import { Constructor } from "./Constructor.js";
import { ServerPage, ServerPageMeta } from "./server/ServerPage.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { WTK_COMPONENT_LOGIC } from "./AppComponent";

const TYPE = "ServerPage";

export interface IPageModule {
    name: string;
    path: string;
};

export interface IIndexPage extends IAppObject {
    css?: string[],
    metas?: ServerPageMeta[],
    ssr?: Constructor<WTK_COMPONENT_LOGIC>,
    module: IPageModule,
    root: string,
    path: string,
    cache?: boolean
};

export const WTK_GEN_INDEX = (page: Constructor<ServerPage>, settings: IIndexPage): Constructor<ServerPage> => {
    const skull = __skull__(page, {
        onCreate: _create_index_page_,
        onInit: _init_index_page_,
        settings: settings
    }) as Constructor<ServerPage>;
    return WTK_GEN_OBJECT(skull, settings) as Constructor<ServerPage>
};

const _create_index_page_ = (page: ServerPage, settings: IIndexPage): void => {
    if (settings.css) {
        page.importCSS(settings.css);
    }
    if (settings.metas) {
        page.setMetas(settings.metas);
    }
    if (settings.module) {
        page.setModule(settings.module);
    }
    if(settings.cache) {
        page.cache();
    }
    page.setRoot(settings.root);
    page.setRootPath(settings.path);   //
    page.setSSR(settings.ssr);
    page.setObjectType(TYPE);
};

const _init_index_page_ = async (page: ServerPage, settings: IIndexPage) => {

};