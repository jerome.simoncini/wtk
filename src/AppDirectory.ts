import { WTK_FILE_LOGIC } from "./AppFile.js";
import { WTK_FS_NODE_LOGIC } from "./AppFsNode.js";
import { WTK_SYMLINK_LOGIC } from "./AppSymLink.js";
import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { WFSNodeType, WTK_GEN_FS_NODE } from "./_gen_fs_node.js";
import { __skull__ } from "./_skull.js";


export class WTK_DIRECTORY_LOGIC extends WTK_FS_NODE_LOGIC {
    private _directories: Map<string, Constructor<WTK_DIRECTORY_LOGIC>> = new Map();
    private _files: Map<string, Constructor<WTK_FILE_LOGIC>> = new Map();
    private _links: Map<string, Constructor<WTK_SYMLINK_LOGIC>> = new Map();

    addDirectory(name: string): WTK_DIRECTORY_LOGIC {
        const d = WTK_GEN_FS_NODE(WTK_DIRECTORY_LOGIC, {
            name,
            type: WFSNodeType.DIRECTORY
        });
        this._directories.set(name, d);
        return this.getObjectStore(Scope.LOCAL).get(d);
    }

    removeDirectory(name: string) {
        const d = this._directories.get(name);
        if (d) {
            this.getObjectStore(Scope.LOCAL).release(d)
        }
    }

    addFile(name: string): WTK_FILE_LOGIC {
        const f = WTK_GEN_FS_NODE(WTK_FILE_LOGIC, {
            name,
            type: WFSNodeType.FILE
        });
        this._files.set(name, f);
        return this.getObjectStore(Scope.LOCAL).get(f);
    }

    removeFile(name: string) {
        const f = this._files.get(name);
        if (f) {
            this.getObjectStore(Scope.LOCAL).release(f)
        }
    }

    addLink(name: string, target: string): WTK_SYMLINK_LOGIC {
        const l = WTK_GEN_FS_NODE(WTK_SYMLINK_LOGIC, {
            name,
            type: WFSNodeType.LINK,
            target
        });
        return this.getObjectStore(Scope.LOCAL).get(l);
    }

    removeLink(name: string) {
        const l = this._links.get(name);
        if (l) {
            this.getObjectStore(Scope.LOCAL).release(l)
        }
    }

    hasDirectory(name: string): boolean {
        return this._directories.has(name);
    }

    hasFile(name: string): boolean {
        return this._files.has(name);
    }

    hasLink(name: string): boolean {
        return this._links.has(name);
    }

    getDirectory(name: string): WTK_DIRECTORY_LOGIC | undefined {
        const d = this._directories.get(name);
        return d ? this.getObjectStore(Scope.LOCAL).get(d) : undefined;
    }

    getFile(name: string): WTK_FILE_LOGIC | undefined {
        const f = this._files.get(name);
        return f ? this.getObjectStore(Scope.LOCAL).get(f) : undefined;
    }

    getLink(name: string): WTK_SYMLINK_LOGIC | undefined {
        const l = this._links.get(name);
        return l ? this.getObjectStore(Scope.LOCAL).get(l) : undefined;
    }
}
