import { WTK_FILE_LOGIC } from "./AppFile.js";
import { WTK_FS_NODE_LOGIC } from "./AppFsNode.js";
import { __skull__ } from "./_skull.js";
import { ASSERT, MESSAGE } from "./utils.js";


export class WTK_SYMLINK_LOGIC extends WTK_FS_NODE_LOGIC {
    private _target: string | undefined = undefined;

    bind(inode: string) {
        this._target = inode;
    }

    get(): WTK_FILE_LOGIC {
        return ASSERT(this._target, MESSAGE(this, `target undefined`));
    }
}
