import { AppEvent } from "./AppEvent.js";
import { Constructor } from "./Constructor.js";
import { ObjectStore } from "./ObjectStore.js";
import { ObserverNotifier } from "./ObserverNotifier.js";
import { Scope } from "./Scope.js";
import { MESSAGE } from "./utils.js";

class ObjectLoader extends ObserverNotifier {
    private _stores: Map<number,ObjectStore> = new Map();

    setGlobalStore(store: ObjectStore) {
        this.setObjectStore(store, Scope.GLOBAL);
    }

    createLocalStore() {
        const store = new ObjectStore();
        store.setParent(this);
        this.setObjectStore(store, Scope.LOCAL);
    }

    getGlobalStore(): ObjectStore|undefined {
        return this.getObjectStore(Scope.GLOBAL);
    }

    getLocalStore(): ObjectStore|undefined {
        return this.getObjectStore(Scope.LOCAL);
    }

    setObjectStore(store: ObjectStore, scope: Scope = Scope.GLOBAL){  
        this._stores.set(scope,store);
    }

    getObjectStore(scope: Scope = Scope.GLOBAL): ObjectStore {
        const store = this._stores.get(scope || Scope.GLOBAL);     ////////////////// 
        if (!store) {
            throw MESSAGE(this,`store undefined '${scope}'`);
        }
        return store;
    }

    ___<T extends ObjectLoader>(importType: Constructor<T>,...args:any[]){ 
        return this.getObjectStore(Scope.GLOBAL).get(importType, ...args); 
    }

    get _localStore(): ObjectStore {
        return this.getObjectStore(Scope.LOCAL);
    }

    onNotify(update: AppEvent): void {
        throw new Error("Method not implemented.");
    }
}

export { ObjectLoader };