import { IElementSetup, IService, ISlot, WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { AppEntity } from "./AppEntity.js";
import { AppModule } from "./AppModule.js";
import { AppState } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { ServerPageMeta } from "./server/ServerPage.js";

const TYPE = "AppComponent";

export interface IStylesheetProperty {
    name: string;
    className: string;
};

export interface IThemeProperty {
    style: number;
    classNames: string[]
};

interface IState {
    name: string,
    ctor: Constructor<AppState>,
    scope: Scope
};


export interface IAppComponent extends IAppObject {
    styles?: string[];
    slots?: ISlot[];
    setup?: IElementSetup[];
    stylesheet?: IStylesheetProperty[];
    theme?: IThemeProperty[];
    states?: IState[];
    services?: IService[];
    css?: string[];
    onShow?: (component: WTK_COMPONENT_LOGIC) => Promise<void>;
    onHide?: (component: WTK_COMPONENT_LOGIC) => Promise<void>;
    onReady?: (component: WTK_COMPONENT_LOGIC) => Promise<void>;
    onResize?: (component: WTK_COMPONENT_LOGIC, rect: DOMRect) => Promise<void>;
    onFirstRender?: (component: WTK_COMPONENT_LOGIC) => Promise<void>;
    onprerender?: (component: WTK_COMPONENT_LOGIC) => Promise<void>;
    onpostrender?: (component: WTK_COMPONENT_LOGIC) => Promise<void>;
    initSSR?: (component: WTK_COMPONENT_LOGIC, metas: ServerPageMeta[]) => Promise<void>;
    html?: (component: WTK_COMPONENT_LOGIC) => Promise<string>;
    ssr?: boolean;
    isModule?: boolean;
};


export const WTK_GEN_COMPONENT = (component: Constructor<WTK_COMPONENT_LOGIC>, settings?: IAppComponent): Constructor<WTK_COMPONENT_LOGIC> => {
    const _component = __skull__(component, {
        onCreate: _create_component_,
        onInit: _init_component_,
        settings: settings
    }) as Constructor<WTK_COMPONENT_LOGIC>;
    return WTK_GEN_OBJECT(_component, settings as IAppObject) as Constructor<WTK_COMPONENT_LOGIC>
};

const _create_component_ = (component: WTK_COMPONENT_LOGIC, settings?: IAppComponent): void => {

    component.setObjectType(TYPE);

    component.extensions.createSlots([
        'beforeOnReady', 'afterOnReady',
        'beforeOnShow', 'afterOnShow',
        'beforeOnHide', 'afterOnHide',
        'beforeOnRender', 'afterOnRender',
        'beforeInitSSR', 'afterInitSSR',
        'beforeFinishSSR', 'afterFinishSSR',
    ]);

    if (settings?.styles) {
        component.addStyles(settings.styles);
    }
    if (settings?.slots) {
        for (const slot of settings.slots) {
            component.createSlot(slot.name);
        }
    }
    if (settings?.onShow) {
        component._onShow = settings.onShow;
    }
    if (settings?.onHide) {
        component._onHide = settings.onHide;
    }
    if (settings?.onReady) {
        component._onReady = settings.onReady;
    }
    if (settings?.onResize) {
        component._onResize = settings.onResize;
    }
    if (settings?.onFirstRender) {
        component._onFirstRender = settings.onFirstRender;
    }
    if (settings?.initSSR) {
        component._initSSR = settings.initSSR;
    }
    if (settings?.onprerender) {
        component._onprerender = settings.onprerender;
    }
    if (settings?.onpostrender) {
        component._onpostrender = settings.onpostrender;
    }
    if (settings?.html) {
        component._html = settings.html;
    }
    component.setSSR(settings?.ssr ?? true);

    if (settings?.extensions) {
        console.error(settings.extensions);
        for (const extension of settings.extensions) {
            if (!component.extensions.add(extension.slot, extension.cmd)) {
                //  console.error(MESSAGE(component, `extension slot not found '${extension.slot}'`));  //
            }
            else {
                // console.warn(MESSAGE(component, `add extension slot '${extension.slot}'`));  //
            }
        }
    }

    if (settings?.setup) {                   /////////////////
        component._setup(settings.setup);
    }

    if(settings?.isModule) {
        component.setAppModule(component as AppModule);
        component.getObjectStore(Scope.LOCAL).setAppModule(component);
    }
    else if(component.getModule(AppEntity)){
        component.getObjectStore(Scope.LOCAL).setAppModule(component.getModule(AppEntity)!);
    }


    if (settings?.css) {
        component.initCSS(settings.css);
    }
};

const _init_component_ = async (component: WTK_COMPONENT_LOGIC, settings?: IAppComponent): Promise<void> => {
    if (settings?.slots) {
        for (const slot of settings.slots) {
            if (slot.content) {
                if (slot.scope == Scope.LOCAL) {
                    try {
                        component.getLocalStore()
                    } catch (e) {
                        component.createLocalStore();
                    }
                }
                const store = component.getObjectStore(slot.scope || Scope.GLOBAL);
                if (store) {
                    const access = slot.access ? component.getObjectStore(slot.access) : null;
                    const content = access ? store.get(slot.content, access) : store.get(slot.content);
                    component.mount(slot.name, content);
                }
                else {
                    throw `(${component.constructor.name})(_init_component_) store undefined '${slot.scope}'`;
                }
            }
        }
    }

    if (settings?.stylesheet) {
        for (const property of settings?.stylesheet) {
            component.setStylesheetProperty(property.name, property.className);
        }
    }

    if (settings?.theme) {
        for (const property of settings?.theme) {
            component.setThemeProperty(property.style, property.classNames);
        }
    }

    if (settings?.states) {
        for (const state of settings.states) {
            if (state.scope == Scope.LOCAL) {
                if (!component.getLocalStore()) {
                    component.createLocalStore();
                }
                component.loadState(state.name, state.ctor, component._localStore);
            }
            else {
                component.loadState(state.name, state.ctor);
            }
        }
    }

    if (settings?.services) {     //////////////////////////////
        component.setServices(settings.services);
    }
};
