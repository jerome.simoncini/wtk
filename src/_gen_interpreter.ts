import { Constructor } from "./Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { WTK_INTERPRETER_LOGIC } from "./services/script/Interpreter.js";


export interface IInterpreter extends IAppObject{
}


export const WTK_GEN_INTERPRETER = (interpreter: Constructor<WTK_INTERPRETER_LOGIC>, settings: IInterpreter): Constructor<WTK_INTERPRETER_LOGIC> => {
    const skull = __skull__(interpreter, {
        onCreate: _create_interpreter_,
        onInit: _init_interpreter_,
        settings: settings
    }
    ) as Constructor<WTK_INTERPRETER_LOGIC>;
    return WTK_GEN_OBJECT(skull, settings) as Constructor<WTK_INTERPRETER_LOGIC>
}

const _create_interpreter_ = (interpreter: WTK_INTERPRETER_LOGIC, settings: IInterpreter) => {

}

const _init_interpreter_ = async (interpreter: WTK_INTERPRETER_LOGIC, settings: IInterpreter) => {

}

