import { WTK_DB_LOGIC } from "./AppDB.js";
import { WTK_DB_STORE_LOGIC } from "./AppDBStore.js";
import { Constructor } from "./Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";

export interface IAppDB extends IAppObject {
    stores: Constructor<WTK_DB_STORE_LOGIC>[];
    dbName: string;
    dbVersion: number;
};

export interface IAppDBStore extends IAppObject {
    storeName: string;
};

export const WTK_GEN_DB = (settings: IAppDB): Constructor<WTK_DB_LOGIC> => {
    const skull = __skull__(WTK_DB_LOGIC, {
        onCreate: _create_app_db_,
        onInit: _init_app_db_,
        settings: settings
    }) as Constructor<WTK_DB_LOGIC>;
    return WTK_GEN_OBJECT(skull, {
        ...settings
    }) as Constructor<WTK_DB_LOGIC>
};

export const WTK_GEN_DB_STORE = (settings: IAppDBStore): Constructor<WTK_DB_STORE_LOGIC> => {
    const skull = __skull__(WTK_DB_STORE_LOGIC, {
        onCreate: _create_app_db_store_, 
        onInit: _init_app_db_store_, 
        settings: settings
    }) as Constructor<WTK_DB_STORE_LOGIC>;
    return WTK_GEN_OBJECT(skull,{
        ...settings
    }) as Constructor<WTK_DB_STORE_LOGIC>;
};

const _create_app_db_ = (_: WTK_DB_LOGIC, settings: IAppDB): void => {
    _.setStores(settings.stores);
    _.setDbName(settings.dbName);
    _.setDbVersion(settings.dbVersion);
};

const _init_app_db_ = async (_: WTK_DB_LOGIC, settings?: IAppDB) => { };

const _create_app_db_store_ = (_: WTK_DB_STORE_LOGIC, settings: IAppDBStore): void => {
    _.setName(settings.storeName);
};

const _init_app_db_store_ = async (_: WTK_DB_STORE_LOGIC, settings?: IAppDBStore) => { };