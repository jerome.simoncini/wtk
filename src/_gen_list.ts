import { Constructor } from "./Constructor.js";
import { __skull__ } from "./_skull.js";
import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { WTK_LIST_LOGIC } from "./AppList.js";
import { _HTML_ } from "./Compiler.js";

export interface IList extends IAppComponent {
    item: Constructor<WTK_COMPONENT_LOGIC>,
};

export const WTK_GEN_LIST = (list: Constructor<WTK_LIST_LOGIC>, settings: IList): Constructor<WTK_LIST_LOGIC> => {
    const skull = __skull__(list, {
        onCreate: _create_list_,
        onInit: _init_list_,
        settings: settings
    }) as Constructor<WTK_COMPONENT_LOGIC>;
    return WTK_GEN_COMPONENT(skull, {
        html : (_: WTK_LIST_LOGIC) => {
            return _HTML_(`{{ safe | slots }}`, _);
        },
        onCreate: (_: WTK_LIST_LOGIC): void => {
            _.setState('slots', '');
        },
        onprerender: async (_: WTK_LIST_LOGIC) => {
            if(_.items != undefined) {
                _.build();
            }
            else {
                console.error('[WTK_LIST] (onprerender) items undefined')
                // notify error
            }
        },
        ...settings
    }) as Constructor<WTK_LIST_LOGIC>
};

const _create_list_ = (list: WTK_LIST_LOGIC, settings: IList): void => {
    list.setItemCtor(settings.item);
};

const _init_list_ = async (list: WTK_LIST_LOGIC, settings: IList) => {};