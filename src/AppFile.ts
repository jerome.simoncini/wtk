import { WTK_FS_NODE_LOGIC } from "./AppFsNode.js";
import { __skull__ } from "./_skull.js";


export class WTK_FILE_LOGIC extends WTK_FS_NODE_LOGIC {
    private _content: string[] = [];

    writeLines(lines: string[]) {
        lines.map(l => this.writeLine(l));
    }

    writeLine(s: string) {
        this._content.push(s);
    }

    clear() {
        this._content = [];
    }

    get content(): string[] {
        return this._content;
    }
}
