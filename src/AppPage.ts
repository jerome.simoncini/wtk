import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { WTK_APP_ROUTER_LOGIC } from "./AppRouter.js";
import { ASSERT } from "./utils.js";

export class WTK_APP_PAGE_LOGIC extends WTK_COMPONENT_LOGIC {
    protected _router: WTK_APP_ROUTER_LOGIC|undefined = undefined;
    protected _path: string|undefined = undefined;
    protected _origin: string|undefined = undefined;

    setRouter(router: WTK_APP_ROUTER_LOGIC) {
        this._router = router;
    }

    setPath(path: string) {
        this._path = path;
    }

    setOrigin(origin: string) {
        this._origin = origin;
    }

    get router(): WTK_APP_ROUTER_LOGIC {
        return ASSERT(this._router, `router undefined`)
    }

    get path(): string {
        return ASSERT(this._path, `(${this.constructor.name}) path undefined`);
    }

    get origin(): string { 
        return ASSERT(this._origin, `(${this.constructor.name}) origin undefined`);
    }
}
