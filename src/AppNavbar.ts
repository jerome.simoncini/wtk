import { WTK_APP_ROUTER_LOGIC } from "./AppRouter.js";
import { IElementSetup, WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { _HTML_ } from "./Compiler.js";
import { ASSERT } from "./utils.js";
import { Constructor } from "./Constructor.js";

export enum NavbarPosition {
    LEFT,
    CENTER,
    RIGHT,
}

export interface INavbarItem {
    id: string;
    label: string;
    position: NavbarPosition;
    destination: string;
    component?: Constructor<WTK_COMPONENT_LOGIC>;
    hidden?: boolean;
}

export class WTK_NAVBAR_LOGIC extends WTK_COMPONENT_LOGIC {
    private _items: INavbarItem[] = [];
    private _itemSetup: IElementSetup[] = [];
    private _router: WTK_APP_ROUTER_LOGIC | undefined = undefined;
    private _selected: INavbarItem | undefined = undefined;
    private _default: string | undefined = undefined;

    setRouter(router: WTK_APP_ROUTER_LOGIC) {
        this._router = router;
    }

    setDefault(itemId: string) {
        this._default = itemId;
    }

    addItem(item: INavbarItem) {
        this._items.push(item);
        this._itemSetup.push({
            name: item.id,
            selector: { tagname: true, value: item.id },
            actions: [{
                event: 'click',
                cmd: (_: WTK_NAVBAR_LOGIC): void => {
                    _.select(item.id);
                }
            }]
        })
    }

    getItem(id: string) {
        return this._items.find(item => item.id == id);
    }

    removeItem(id: string) {
        this._items = this._items.filter(item => item.id != id)
        this._itemSetup = this._itemSetup.filter(item => item.name != id);
    }

    clearItems() {
        this._items = [];
        this._itemSetup = [];
    }

    reset() {
        this._selected = undefined;
    }

    setup(items: IElementSetup[]) {
        this._setup(items);
    }

    async select(path: string) {
        await this.syncPath(path);
        await this.router.navigate(this._selected!.destination);   //
    }

    async syncState() {
        if (this._selected) {
            await this.select(this._selected.id);
        }
    }

    async syncPath(path: string) {
        this._selected = this._items.find(item => item.id == path) ||
            ASSERT(this._items.find(item => item.id == this.default),
                `[NAVBAR ${this._objectId}] unknown item '${this.default}'`);
        await this.render();
    }

    get items() {
        return this._items;
    }

    get itemSetup() {
        return this._itemSetup;
    }

    get router() {
        return ASSERT(this._router, `[Navbar '${this._objectId}'] router undefined`)
    }

    get selected() {
        return this._selected;
    }

    get default() {
        return ASSERT(this._default, `[Navbar '${this._objectId}'] default id undefined`)
    }
}
