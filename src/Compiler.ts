import { LOGGER } from "./Logger.js";
import { STYLE_MAP_SYMBOL } from "./StyleMap.js";
import { ASSERT, escapeHTML } from './utils.js';

const DEBUG = LOGGER("Compiler");
DEBUG.off();

const _ = (id) => {
    return document.querySelector(id);   
}
export const __ = (e, a, c) => e?.addEventListener(a, c);         ///////////////////////////////
export const _HTML_ = (template, component) => Compiler.compile(template, component);

/***************************************************************************************/

export class Compiler {
    static _slots = (data, component, template) => {
        const s = data.split('|');
        const c = s.shift().trim().split(' ');
        const tagname = component.getTagname(c.shift());
        const htmlProps = c.join(' ');
        const componentProps = s.join('|').trim();
        if (tagname) {
            if (componentProps?.length) {
                //console.warn(componentProps);
                const slot = component._slots.get(tagname);
                slot?.setProps(JSON.parse(componentProps));
            }
            return `<${tagname} ${htmlProps}></${tagname}>`;
        }
        else throw Error(`[TEMPLATE COMPILE ERROR] tagname undefined ${tagname}, template=${template}`);
    }
    static _brackets = (itp, component, template) => {
        DEBUG.log(itp);
        itp = itp.split('|') || itp;
        DEBUG.log(itp);
        const [inp, unsafe] = ((itp.length > 1) && (itp[0].trim() == Compiler.__safe__))
            ? [itp[1], false]
            : [itp[0], true];
        DEBUG.log(inp);
        const data = component.$(inp.trim());
        DEBUG.log(data);
        const compiled = unsafe ? escapeHTML(data) : data;
        if (compiled !== undefined) {
            return compiled;
        }
        else throw Error(`[TEMPLATE COMPILE ERROR] unknown name "${inp.trim()}", template=${template}`);
    };
    static _snails = (data, component, template) => {
        const s = data.split(/:/);
        const e = s[0].trim();
        const d = Compiler.directives.find((_d) => e.startsWith(_d.cmd));
        return d ? d.hdl(e, s, component, template) : "";
    };
    static _style = (data, component, template) => {
        return `class="${data.split(STYLE_MAP_SYMBOL).map(style => component.stylesheet.get(style) || '').join(' ')}" `;
    };
    static _id = (data, component, template) => {
        return `id=${component.getId(data)} `;
    };
    static __brackets__ = /{{(.*?)}}/;
    static __snails__ = /<@(.*?)@>/;
    static __slots__ = /<<(.*?)>>/;
    static __style__ = /--(.*?) /;
    static __id__ = /@(.*?) /;
    static __disable__ = /\\/;       ///////////////////
    /*---------------------------------------------------------*/
    static _for_hdl = (e, s, c, t) => {
        const loop = e.slice(Compiler.__for__.cmd.length);
        if (loop) {
            const input = loop.split(/ in /, 2);
            const array = c.$(input[1].trim());
            if (Array.isArray(array))
                return Compiler._foreach(input[0].trim(), array, s[1].trim());
            else
                throw Error(`[TEMPLATE COMPILE ERROR] ${input[1]} is not an array, template=${t}`);
        }
        return "";
    }
    static _foreach = (item, array, s) => {
        let o: any = [];
        for (let obj of array) {
            o.push(s.replace(item, obj));
        }
        return o.join("");
    };
    static __for__ = { cmd: "for", hdl: Compiler._for_hdl };
    /*---------------------------------------------------------*/
    static _if_hdl = (e, s, c, t) => {
        e = e.slice(Compiler.__if__.cmd.length);
        const r = Function("return " + e);
        return (() => r() ? c.$(s[1]?.trim()) : c.$(s[2]?.trim()))() || "";
    }
    static __if__ = { cmd: "if", hdl: Compiler._if_hdl };
    /*---------------------------------------------------------*/
    static __safe__ = "safe";
    static __escape__ = "escape";
    static delimiters = [Compiler.__brackets__,
    Compiler.__snails__,
    Compiler.__slots__,
    Compiler.__style__,
    Compiler.__id__];
    static directives = [Compiler.__for__, Compiler.__if__];

    static compiler = [{ delimiter: Compiler.__brackets__, compile: Compiler._brackets },
    { delimiter: Compiler.__snails__, compile: Compiler._snails },
    { delimiter: Compiler.__slots__, compile: Compiler._slots },
    { delimiter: Compiler.__style__, compile: Compiler._style },
    { delimiter: Compiler.__id__, compile: Compiler._id },];

    static compile = (template, component) => {
        for (const delimiter of Compiler.delimiters) {
            let result: RegExpExecArray | null = null;
            let output: string[] = [];
            do {
                result = delimiter.exec(template);
                if (result) {
                    if (result.index > 0) {
                        output.push(template.substring(0, result.index));
                    }
                    const handler = Compiler.compiler.find((hdl) => hdl.delimiter.toString() == delimiter.toString());
                    if (handler !== undefined) {
                        //console.warn(result[1]);
                        const skip = Compiler.__disable__.exec(result[1].split(' ')[0]);
                        if (skip) {
                            if (delimiter == Compiler.__id__) {
                                output.push('@'+result[1].replace(Compiler.__disable__,''));
                            }
                        }
                        else {
                            output.push(handler.compile(result[1], component, template));
                        }
                    }
                    else throw Error(`[TEMPLATE COMPILE ERROR] unknown delimiter ${delimiter}, template=${template}`);

                    template = template.slice(result.index + result[0].length);
                }
                else {
                    output.push(template);
                }
            } while (result);
            template = output.join("");
        }
        return template;
    }
}

export { _ };