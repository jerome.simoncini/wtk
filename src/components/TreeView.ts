import { _HTML_ } from "../Compiler.js";
import { ASSERT } from "../utils.js";
import { WTK_GEN_COMPONENT } from "../_gen_component.js";
import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { Constructor } from "../Constructor.js";

const NODE_ID = 'node';
const CONTENT = 'content';

export const _treeViewNodeHTML = (_: WTK_TREE_VIEW_NODE_LOGIC) => {
    return _HTML_(`<div style="border-radius: 5px; border-bottom: solid 1px grey; background-color: darkslategrey; color: lightgrey; padding-left: 5px;">
                     <span @${NODE_ID} >${_.symbol}</span>
                     <span style="font-weight: bold">${_.label}</span>
                   </div>
                   << ${CONTENT} >>`, _);
}

export const _treeViewNodeTOGGLE = (_: WTK_TREE_VIEW_NODE_LOGIC): void => {
    _.toggle();
    _.render();
}

export const _treeViewNodeSETUP = [
    {
        name: NODE_ID,
        selector: { id: true, value: NODE_ID },
        actions: [{
            event: 'click',
            cmd: _treeViewNodeTOGGLE
        }]
    }
]

export class WTK_TREE_VIEW_NODE_LOGIC extends WTK_COMPONENT_LOGIC {
    private _label: string | undefined = undefined;
    private _content: WTK_COMPONENT_LOGIC | undefined = undefined;
    private _showContent: boolean = false;
    symbol: string = ' (+) ';

    setLabel(label: string) {
        this._label = label
    }

    setContent(content: WTK_COMPONENT_LOGIC) {
        this._content = content;
    }

    get content() {
        return ASSERT(this._content, `tree view node content undefined [${this.label}]]`)
    }

    get label() {
        return ASSERT(this._label, `tree view node label undefined`)
    }

    get showContent() {
        return this._showContent;
    }

    toggle() {
        this._showContent = !this._showContent;

        if (this.showContent) {
            this.mount(CONTENT, this.content);
            this.symbol = ' (-) ';
        }
        else {
            this.umount(CONTENT);
            this.symbol = ' (+) ';
        }
    }
}



//-------------------------------------------------------------


const tnode = WTK_GEN_COMPONENT(WTK_TREE_VIEW_NODE_LOGIC, {
    setup: _treeViewNodeSETUP,
    html: _treeViewNodeHTML
}) as Constructor<WTK_TREE_VIEW_NODE_LOGIC>;

export class WTK_TREE_VIEW_LOGIC extends WTK_COMPONENT_LOGIC {
    private _nodes: Map<string, WTK_COMPONENT_LOGIC> = new Map();

    addNode(label: string, content: WTK_COMPONENT_LOGIC) {
        this._nodes.set(label, content);
    }

    removeNode(label: string) {
        this._nodes.delete(label);
    }

    build() {
        const nodes: string[] = [];
        for (const [label, content] of this._nodes) {
            const _ = class extends tnode { };
            const cnode = this._localStore.get(_);
            cnode.setLabel(label);
            cnode.setContent(content);
            cnode.createSlot(CONTENT);
            const slotname = `${NODE_ID}-${cnode._objectId}`;
            nodes.push(`<< ${slotname} >>`);
            this.createSlot(slotname);
            this.mount(slotname, cnode);
        }
        this.setState('nodes', nodes);
    }
}

export const WtkTreeView = WTK_GEN_COMPONENT(WTK_TREE_VIEW_LOGIC, {
    onprerender: async (_: WTK_TREE_VIEW_LOGIC) => {
        _.build();
    },
    html: (_: WTK_TREE_VIEW_LOGIC) => {
        return _HTML_(`<div style="padding: 5px;">
                          <@ for node in nodes : node @>
                       </div>`, _);
    },
    createLocalStore: true
})


