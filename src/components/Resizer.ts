import { AppWindow, APP_WINDOW_LOGIC } from "../AppWindow.js";
import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { ASSERT } from "../utils.js";
import { WTK_GEN_COMPONENT } from "../_gen_component.js";
import { _HTML_ } from "../Compiler.js";


export class WTK_RESIZER_LOGIC extends WTK_COMPONENT_LOGIC {
    private _main: WTK_COMPONENT_LOGIC|undefined = undefined;
    private _replica: WTK_COMPONENT_LOGIC|undefined = undefined;
    private _resize: boolean = false;

    init(main: WTK_COMPONENT_LOGIC, replica: WTK_COMPONENT_LOGIC) {
        this._main = main;
        this._replica = replica;
    }

    startResize() {
        console.error('[START RESIZE]')
        this._resize = true;
    }

    stopResize() {
        console.error('[STOP RESIZE]')
        this._resize = false;
    }

    onHorizontalResize(mousePosX: number, mousePosY: number) {   //
        console.error('[HORIZONTAL RESIZE]')
        const rcbr = this.replica.root.getBoundingClientRect();
        const rw = rcbr.right - rcbr.left;
        const scaleRatio = (mousePosX - rcbr.left)/rw;
        const rflex = getComputedStyle(this.replica.root).flex.split(' ');
        const mflex = getComputedStyle(this.main.root).flex.split(' ');
        const rf = +rflex[0] * scaleRatio;  //
        const mf = 1 - rf;
        console.error(`rw=${rw} rflex0=${rflex[0]} scale=${scaleRatio} replica=${rf}  main=${mf}`)
        this.main.root.style.flex = [mf, mflex[1], mflex[2]].join(' ');
        this.replica.root.style.flex = [rf, rflex[1], rflex[2]].join(' ');

        this.main.onResize();  
        this.replica.onResize();  
    }

    get main() {
        return ASSERT(this._main, `main component undefined`);
    }

    get replica() {
        return ASSERT(this._replica, `replica component undefined`);
    }

    get resize(): boolean {
        return this._resize;
    }
}

//----------------------------------------------------------------------------------------//

const RESIZER = "resizer";
const MOUSE_UP = "wtk-resizer-mu"; 
const MOUSE_MOVE = "wtk-resizer-move"; 

export const WtkHorizontalResizer = WTK_GEN_COMPONENT(WTK_RESIZER_LOGIC, {   //
    html: (_: WTK_COMPONENT_LOGIC) => {
        return _HTML_(`<div @${RESIZER} style="flex: 1"></div>`,_);
    },
    styles: [
        'resizer'
    ],
    setup: [
        {
            name: RESIZER,
            selector: { id: true, value: RESIZER },
            actions: [{
                event: "mousedown",
                cmd:  (_: WTK_RESIZER_LOGIC): void => {
                    _.startResize();
                }
            }]
        }
    ],
    onShow: async (_: WTK_RESIZER_LOGIC) => {
        (<APP_WINDOW_LOGIC>_.___(AppWindow)).addEventListener(_.getId(MOUSE_MOVE), 'mousemove', (e)=>{
            if(_.resize) {
                const mx = (<MouseEvent>e).clientX;
                const my = (<MouseEvent>e).clientY;
                _.onHorizontalResize(mx, my);  //
                e.stopPropagation();
                e.preventDefault();
            }
        });
        (<APP_WINDOW_LOGIC>_.___(AppWindow)).addEventListener(_.getId(MOUSE_UP), 'mouseup', (e)=>{
            if(_.resize) {
               _.stopResize();
               e.stopPropagation();
               e.preventDefault();
            }
        });
    },
    onHide: async (_: WTK_RESIZER_LOGIC) => {
        (<APP_WINDOW_LOGIC>_.___(AppWindow)).removeEventListener(_.getId(MOUSE_MOVE));
        (<APP_WINDOW_LOGIC>_.___(AppWindow)).removeEventListener(_.getId(MOUSE_UP));
    },
    createLocalStore: true
})