import { WTK_GEN_COMPONENT } from "../_gen_component.js";
import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { _HTML_ } from "../Compiler.js";

export enum NavbarItemStyle {
    BASIC,
    SELECTED
}

export const __navbarItemTHEME_default = [
    { style: NavbarItemStyle.BASIC, classNames: ['navbar-item'] },
    { style: NavbarItemStyle.SELECTED, classNames: ['navbar-item', 'navbar-item-selected'] },
];

export const __navbarItemSTYLESHEET = [
    { name: 'navbar-item', className: 'wtk-navbar-item' },
    { name: 'navbar-item-selected', className: 'wtk-navbar-item-selected' }
];

export class WTK_NAVBAR_ITEM_LOGIC extends WTK_COMPONENT_LOGIC {
    label: string;
    selected: boolean;
}

export const __navbarItemHTML = (_: WTK_NAVBAR_ITEM_LOGIC) => {
    const style = _.selected ? NavbarItemStyle.SELECTED : NavbarItemStyle.BASIC;

    return _HTML_(`<span --${_.stylemap.use(style)} >
                     ${_.label}
                   </span>`, _);
}

export const NavbarItem = WTK_GEN_COMPONENT(WTK_NAVBAR_ITEM_LOGIC, {
    html: __navbarItemHTML,
    stylesheet: __navbarItemSTYLESHEET,
    theme: __navbarItemTHEME_default
});