import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { _HTML_ } from "../Compiler.js";

class DropdownEntry { }

class DropdownOption {
    value?: any;
    html: string;
    handler: any;
}

class DropdownValues extends DropdownEntry {
    value: any;
    html: string;
}

class DropdownGroup extends DropdownEntry {
    options: DropdownValues[];
}

const WTK_DROPDOWN = "wtk-dropdown";
const WTK_DROPDOWN_ID = "dropdown";


export class WTK_DROPDOWN_LOGIC extends WTK_COMPONENT_LOGIC {
    private _options: DropdownEntry[] = [];
    private handlers: Map<string, any> = new Map();
    protected values: Map<string, number> = new Map();

    buildContent(): void {
        throw "not implemented"
    }

    addOption = (option: DropdownOption) => {
        const values = this._buildValues(option);
        this._options.push(values);
        this.values.set(values.html, values.value);
        this.handlers.set(values.value, option.handler);
    }

    addOptionGroup = (options: DropdownOption[]) => {
        let optGroup = new DropdownGroup();
        optGroup.options = [];
        options.forEach(option => {
            const values = this._buildValues(option);
            optGroup.options.push(values);
            this.values.set(values.html, values.value);
            this.handlers.set(values.value, option.handler);
        })
        this._options.push(optGroup);
    }

    clearContent = () => {
        this._options = [];
    }

    handleInput() {
        const handler: any = this.handlers.get(this.$(WTK_DROPDOWN_ID));
        if (handler) {
            handler();
        }
    }

    getValue(): string | undefined {
        return this.getElement(WTK_DROPDOWN_ID).value;
    }

    _select(optionText: string): boolean {
        const select: boolean = this.values.has(optionText);
        if (!select) {
            throw `(${this.constructor.name})(select error) value not found "${optionText}"`;
        }
        this.getElement(WTK_DROPDOWN_ID).value = optionText;
        return select;
    }

    private _buildValues(option: DropdownOption) {
        let values = new DropdownValues();
        values.value = option.value || option.html;
        values.html = option.html;
        return values;
    }

    static _buildOption(option: DropdownValues) {
        return `<option value="${option.value}">${option.html}</option>`;
    }

    static _buildGroup(group: DropdownGroup) {
        let groupHTML: string[] = [];
        groupHTML.push("<optgroup>");
        group.options.forEach((o: DropdownValues) => {
            groupHTML.push(WTK_DROPDOWN_LOGIC._buildOption(<DropdownValues>o));
        });
        groupHTML.push("</optgroup>");
        return groupHTML.join("");
    }

    get options() {
        return this._options;
    }
}

const onInput = (_: WTK_DROPDOWN_LOGIC): void => {
    _.handleInput();
}

export const __dropdownSETUP = [
    {
        name: WTK_DROPDOWN_ID,
        selector: { id: true, value: WTK_DROPDOWN_ID },
        actions: [{
            event: "input",
            cmd: onInput
        }],
        auto: true
    }
];

export const __dropdownINIT = async (_: WTK_DROPDOWN_LOGIC) => {
    _.clearContent();
    _.buildContent();
}

export const __dropdownHTML = (_: WTK_COMPONENT_LOGIC) => {
    return _HTML_(`<div class="${WTK_DROPDOWN}">
                 <select id=${_.getId(WTK_DROPDOWN_ID)}>
                    <@ for option in options : option @>
                 </select>
              </div>`, _);
}

export const __dropdownBUILDOPTIONS = async (_: WTK_DROPDOWN_LOGIC) => {
    let optionsHTML: string[] = [];
    _.options.forEach(option => {
        if (option instanceof DropdownValues) {
            optionsHTML.push(WTK_DROPDOWN_LOGIC._buildOption(<DropdownValues>option));
        }
        else if (option instanceof DropdownGroup) {
            optionsHTML.push(WTK_DROPDOWN_LOGIC._buildGroup(<DropdownGroup>option));
        }
    });
    _.setState('options', optionsHTML);
}
