////deprecated
import { _HTML_, __ } from "../Compiler.js";
import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { ASSERT, escapeHTML } from "../utils.js";
import { LOGGER } from "../Logger.js";

const THIS = "Item Table";
const DEBUG = LOGGER(THIS);
DEBUG.on();

interface TableItem {
    column: number;
    label: string;
    parentNode?: any;  //
    flex?: number;  //
}

class ItemTable extends WTK_COMPONENT_LOGIC{
    headersdata: TableItem[] = [];
    contentdata: TableItem[][] = [[]];
   // resize: any = null;
    selected: TableItem|null = null;

    _init = () => {
        this.contentdata.forEach(content=>content.sort((a,b)=>a.column-b.column));
        this.headersdata.sort((a,b)=>a.column-b.column);
    }

    protected async onprerender() {
        DEBUG.log("onprerender");
        this._init();
        this._buildHeaders();
        this._buildContent();
    }

    html = () => {
        return _HTML_(`<div>
                         <div class="header-row" style="color: darkslategray">
                            <@ for header in headers : header @>
                         </div>
                         <div class="table-content">
                            <@ for item in content : item @>
                         </div>
                       </div>`, this);
    }

    async onpostrender() {
       DEBUG.log("onpostrender");
       
       ASSERT(document, `${this.constructor.name}: (onpostender) server side`);

       const [ separators, rows ] = [ document.querySelectorAll('.separator'), 
                                      document.querySelectorAll('.table-row') ];

      // separators.forEach( separator => __( separator, 'mousedown', (e) => this._startResize(e) ) );
       
       rows.forEach( row => __( row, 'click', (e) => this._toggleSelect(e.target) ) ); 

     //  this._updateOffsets(document.querySelectorAll('.table-header'));
     //  this._updateOffsets(document.querySelectorAll('.table-item'));

       //__( window, 'mouseup', (e)=> { if(this.resize) this._stopResize(e); } );
       //__( window, 'mousemove', (e)=> { if(this.resize) this._updateResize(e.clientX); } );
    }

    setHeadersData(headers: TableItem[]) {
        this.headersdata = headers;
    }

    setContentData(content: TableItem[][]) {
        this.contentdata = content;
    }

    _buildHeaders = () => {
        let headers: string[] = [];

        for( const item of this.headersdata ) {
            headers.push(`<div class="table-header" data-column=${item.column}>${escapeHTML(item.label)}</div>
                          <div class="separator"></div>`);
        }

        this.setState('headers', headers);
    }

    _buildContent = () => {
        DEBUG.log("build content");
        let content: string[] = [];
        DEBUG.log(JSON.stringify(this.contentdata));
        for(const item of this.contentdata ){
            DEBUG.log(JSON.stringify(item));
            content.push(`<div class="table-row">`);
            for(const obj of item){
                console.log(obj.label);
                content.push(`<div class="table-item" data-column="${obj.column}">${escapeHTML(obj.label)}</div>`)
            }
            content.push(`</div>`);
        }

        this.setState('content', content);
    }
/*
    _startResize = (e) => {
        const r = e.target.nextElementSibling;
        if(r){
            let p = e.target.previousElementSibling;
            if(p){
               this.resize = {};
               this.resize.position = 0;
               do{ 
                 if(p.innerHTML){
                    if(!this.resize.previous){
                       this.resize.w1 = p.getBoundingClientRect().width;
                       this.resize.previous = p;
                    } 
                    this.resize.position++;
                 }
                 p = p.previousSibling; 
               }while(p);
               this.resize.next = r;
               this.resize.w2 = r.getBoundingClientRect().width;
               this.resize.xstart = e.clientX;
            }
            e.preventDefault();
        }
    }

    _updateResize = (x) => {
        this.resize.x = x;
        const move = Number(this.resize.x-this.resize.xstart);
        this.resize.xstart = this.resize.x;
        const r = (this.resize.w1+move)/this.resize.w1;
        this._resizeItems(this.resize.previous, this.resize.next, r);
        this._updateOffsets(document.querySelectorAll('.table-item'));
    }

    _stopResize = (e) => {
        this.setState('resize', Object.assign({},this.resize));
        this.resize = undefined;
    }

    _resizeItems = (left, right, ratio) => {
        const f1 = getComputedStyle(left).flex.split(' ')[0];
        const f2 = getComputedStyle(right).flex.split(' ')[0];
        left.style.flex = parseInt(f1)*ratio;  //
        right.style.flex = parseInt(f2)/ratio;  //
        const item1 = this._getHeaderAtPos(this.resize.position);
        const item0 = this._getHeaderAtPos(this.resize.position-1);
        item0.flex = left.style.flex;
        item1.flex = right.style.flex;
    }
*/
    _getColumnId = (label) => {
        return ASSERT(this.headersdata.find( (item) => item.label==label ), `[TABLE CONTENT] unknown header label ${label}`)
                  .column;
    }

    _getHeaderAtPos = (pos) => {
        return ASSERT(this.headersdata.find( (item) => item.column==pos), `[TABLE CONTENT] unknown header at pos ${pos}`);
    }

    _getSelectedItem = (): any => {   //
       if( ! this.selected?.parentNode ) return undefined;

       let item: string[] = [];
       let current = this.selected.parentNode.firstElementChild;
       let i = 0;

       do{
          const label = this._getHeaderAtPos(i).label;
          const value = current.innerHTML;
          item[label] = value;
          current = current.nextElementSibling;
          i++;
       }while(current)

       return Object.assign({},item);
    }
/*
    _updateOffsets = (nodelist) => {
        const items: any[] = Array.from(nodelist);  //
        for( const item of items ){
            if(item.dataset.column){ 
               const header = this._getHeaderAtPos(item.dataset.column);
               item.style.flex = header.flex;
            }
        }
    }
*/
    _toggleSelect = (row) => {
        if( row !== this.selected ){
           this.selected?.parentNode?.classList.remove('row-selected');
           row.parentNode?.classList.add('row-selected');
           this.selected = row;
        }
        else{
           this.selected?.parentNode?.classList.remove('row-selected');
           this.selected = null;
        }
    }
}

export { ItemTable, TableItem };

