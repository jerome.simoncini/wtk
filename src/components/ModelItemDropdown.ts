import { AppStateService, IStateUpdate, ModelGroupItemState } from "../AppState.js";
import { Constructor } from "../Constructor.js";
import { ModelGroupItem } from "../ModelGroup.js";
import { WTK_DROPDOWN_LOGIC, __dropdownBUILDOPTIONS, __dropdownHTML, __dropdownINIT, __dropdownSETUP } from "./Dropdown.js";
import { ASSERT } from "../utils.js";

type StateType = ModelGroupItemState<ModelGroupItem>;

export class WTK_MODEL_ITEM_DROPDOWN_LOGIC extends WTK_DROPDOWN_LOGIC {
    private _values: string[] = [];
    private _syncEvent: number|undefined = undefined;
    private _stateType: Constructor<StateType> | undefined = undefined;
    private _stateServiceType: Constructor<AppStateService> | undefined = undefined;

    buildContent = (): void => {
        for (const value of this._values) {
            this.addOption({
                html: value,
                handler: () => {
                    this.onSelect(value)
                }
            });
        }
    }

    onSelect(value: string) {
        throw 'not implemented'
    }

    getItemValue(): string {
        throw "not implemented";
    }

    setValues(values: string[]) {
        this._values = values;
    }

    setModel(stateType: Constructor<StateType>, stateServiceType: Constructor<AppStateService>) {
        this._stateType = stateType;
        this._stateServiceType = stateServiceType;
    }

    setSyncEvent(event: number) {
        this._syncEvent = event;
    }

    syncState() {
        const value = this.getItemValue();
        if (value && value != this.getValue()) {
            if (!this._select(value)) {
                // notify error
            }
        }
    }

    get stateType() {
        return ASSERT(this._stateType, `(${this.constructor.name}) state type undefined`) as Constructor<StateType>;
    }

    get stateServiceType() {
        return ASSERT(this._stateServiceType, `(${this.constructor.name}) state service type undefined`) as Constructor<AppStateService>;
    }

    get syncEvent() {
        return ASSERT(this._syncEvent, `(${this.constructor.name}) sync event undefined`);
    } 
}

export const __modelItemDropdownSYNC = async (_: WTK_MODEL_ITEM_DROPDOWN_LOGIC) => {
    _.syncState();
}

export const __modelItemDropdownUPDATE = (_: WTK_MODEL_ITEM_DROPDOWN_LOGIC, update: IStateUpdate): void => {
    if(update.mask.has(_.syncEvent)) {
        __modelItemDropdownSYNC(_);
    }
}