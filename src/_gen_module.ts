import { IAppDebug, WTK_DEBUG_LOGIC, WTK_GEN_DEBUG } from "./AppDebug.js";
import { AppModule } from "./AppModule.js";
import { Constructor } from "./Constructor.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { __skull__ } from "./_skull.js";
import { __DEBUG__ } from "./utils.js";

const TYPE = "AppModule";

export interface IAppModule extends IAppComponent {
    js?: string[];                                        //
};

export const WTK_GEN_APP_MODULE = (settings: IAppModule|IAppDebug, debug: boolean = false): Constructor<AppModule> => {
    settings.isModule = true;
    const _ = debug ? class extends WTK_DEBUG_LOGIC {} : class extends AppModule {}
    const _module = __skull__(_, {
        onCreate: _create_app_module_,
        onInit: _init_app_module_,
        settings: settings
    }) as Constructor<AppModule>;
    return debug ?  WTK_GEN_DEBUG(WTK_DEBUG_LOGIC, {
        root: WTK_GEN_COMPONENT(_module, settings),
        module: settings,
        scale: (<IAppDebug>settings).scale || 0.7
    }) as Constructor<AppModule>
    : WTK_GEN_COMPONENT(_module, settings) as Constructor<AppModule>;
};

export const _create_app_module_ = (module: AppModule, settings?: IAppModule): void => {
    if(settings?.js) {
        module.importJS(settings.js)
    } 

    module.setObjectType(TYPE);
};

export const _init_app_module_ = async (module: AppModule, settings?: IAppModule) => {

};