import { AppObject } from "../AppObject.js";
import { Constructor } from "../Constructor.js";

const EMPTY_VALUE = '';       ////////

export interface IStateSlot {
    value: string;
    func: (v: any) => void;
}

export const StateSlots = (base: Constructor<AppObject>): Constructor<AppObject> => {
    return class extends base {
        private _stateSlots: Map<number, IStateSlot> = new Map();

        /////////////////////

        getStateSlot(slot: number): [string, (v: any) => void] {
            const state = this._stateSlots.get(slot);
            return (state ? [state.value, state.func] : [EMPTY_VALUE, () => { }]) as [string, (v: any) => void];
        }

        useState<T>(slot: number, value: T): void {
            const [s, f] = this.getStateSlot(slot);
            const v = JSON.stringify(value);                         ///////


            if (s != v) {
                console.warn(s);
                console.warn(v);
                try {
                    if(f.apply(this, [value])) {
                        this.updateStateSlot(slot, v);
                    }
                }
                catch (e) {
                    this.restoreStateSlot(slot);
                }
            }
        }

        restoreStateSlot(slot: number) {
            const [s, f] = this.getStateSlot(slot);
            if (s != EMPTY_VALUE) {                      ///////
                f.apply(this, [JSON.parse(s)]);
            }
        }

        createStateSlot<T>(slot: number, func: (v: T) => void, value: string = EMPTY_VALUE) {
            this._stateSlots.set(slot, { value, func })
        }

        updateStateSlot<T>(slot: number, value: string) {
            const [s, f] = this.getStateSlot(slot);
            this._stateSlots.set(slot, { value, func: f });
        }

    } as Constructor<AppObject>;
}