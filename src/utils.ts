import { AppEntity } from "./AppEntity.js";

export const __DEBUG__ = true;    //

const MASK_LIMIT = 30;

class Mask {
    private _mask: Map<number, number> = new Map();

    constructor(flags: number[] = []) {
        if (flags.length) {
            this.setBits(...flags);
        }
    }

    setBits(...bits: number[]) {
        [...bits].forEach((bit) => {
            const offset = Math.floor(bit / MASK_LIMIT);
            let mask = 0;
            if (this._mask.has(offset)) {
                mask = this._mask.get(offset)!;
            }
            this._mask.set(offset, mask |= 1 << (bit - offset * MASK_LIMIT));
        });
    }

    unset(bit: number) {   //
        if (this.has(bit)) {
            const offset = Math.floor(bit / MASK_LIMIT);
            let mask = 0;
            if (this._mask.has(offset)) {
                mask = this._mask.get(offset)!;
            }
            this._mask.set(offset, mask ^= 1 << (bit - offset * MASK_LIMIT));
        }
    }

    hasOne(...bits: number[]): boolean {
        for (const bit of bits) {
            const offset = Math.floor(bit / MASK_LIMIT);
            let mask = this._mask.get(offset);
            if (mask && (mask & (1 << (bit - offset * MASK_LIMIT)))) {
                return true;
            }
        }
        return false;
    }

    has(bit: number): boolean {
        const offset = Math.floor(bit / MASK_LIMIT);
        let mask = this._mask.get(offset);
        if (mask && (mask & (1 << (bit - offset * MASK_LIMIT)))) {
            return true;
        }
        return false;
    }

    copy(bits: number[]) {
        const flags: number[] = []
        bits.map(b => this.has(b) ? flags.push(b) : undefined);
        return new Mask(flags)
    }

    get values() {
        return Array.from(this._mask.values());
    }
}


class DoubleBuffer<T> {
    private _data: T | null = null;
    private __data: T | null = null;

    set(data: T) {
        this._data = data;
    }

    reset(data: T | null = null) {
        this._data = data;
    }

    _get() {
        return this._data;
    }

    update(fun: (data: T | null) => void) {
        fun(this._data);
    }

    swap() {
        this.__data = this._data;
    }

    get() {
        return this.__data;
    }
}


const SPECIAL_CHARS = [
    { character: '&', escape: '&amp' },
    { character: '<', escape: '&lt' },
    { character: '>', escape: '&gt' },
    { character: '"', escape: '&quot' },
    { character: "'", escape: '&#039' },
];
const escapeHTML = (data) => {
    for (const s of SPECIAL_CHARS) {
        data = data.replaceAll(s.character, s.escape);
    }
    return data;
};


export { DoubleBuffer, Mask, escapeHTML };

export const TEST = __DEBUG__ ? (_: boolean, message: string): any => {
    const status = _ ? "[OK]" : "[FAILED]";
    console.warn(`${status} ===> ${message}`)
} : (_: boolean, message: string): void => { };

export const ASSERT = (_: any, message: string): any => {
    if (_ == null || _ == undefined) {
        throw message;
    }
    return _;
};

export const MESSAGE = (_: AppEntity, message: string): string => {
    return `[${_.__type__}][${_.inode}](${_.__name__}): ${message}`;
};

export class HtmlArray extends Array<String> {
    get() {
        return this.join('');
    }
}

export interface IURLContent {
    protocol: string;
    host: string;
    port: string;
    path: string;
    origin: string;
    url?: string;
}

export class URLParser {
    static parse(url: string): IURLContent {
        const s0 = url.split('://')
        const s1 = s0[1].split('/');

        const protocol = s0[0];
        const [host, port]: string[] = s1.shift()!.split(':');
        const path = s1.slice(-1)[0]?.includes('.') ? '/' : `/${s1.shift() || ''}`;
        const origin: string = s1.length ? `/${s1.join('/')}` : '/';

        return { protocol, host, port, path, origin, url };
    }
}


//----------------------------------------------------------------------------------//


export class ExtensionMap {
    private _map: Map<string, ((_: AppEntity) => void)[]> = new Map();

    createSlots(slots: string[]) {
        for (const slot of slots) {
            this._map.set(slot, []);
        }
    }

    add(slot: string, cmd: (_: AppEntity) => void): boolean {
        const hasSlot = this._map.has(slot)
        if (hasSlot) {
            if (!this._map.get(slot)?.find(c => c == cmd)) {       //
                this._map.get(slot)!.push(cmd);
            }
        }
        return hasSlot;
    }

    execute(slot: string, caller: AppEntity) {
        const cmds = this._map.get(slot) || [];
        for (const cmd of cmds) {
            cmd(caller);
        }
    }

    list(): string[] {
        return Array.from(this._map.keys());
    }
}



//------------------------------------------------------------------------------//
export function debounce(func, timeout = 100) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
}