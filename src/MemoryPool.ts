import { AppObject } from "./AppObject.js";
import { Constructor } from "./Constructor.js";
import { ObjectFactory } from "./ObjectFactory.js";
import { Scope } from "./Scope.js";
import { System } from "./events.js";
import { ASSERT, MESSAGE } from "./utils.js";


export enum MemoryEvent {
    READ,
    WRITE,
    DELETE
}

export interface IMemoryUpdate {  
    poolId: number;
    key: string;
}


const MEM_REF = 'MemRef';
const STATIC_REF = 'StaticRef';

export abstract class Ref extends AppObject {
    abstract get key(): any;
    abstract get value(): any;
    abstract get poolId(): number;
}


export class MemRef<K = string, V = any> extends Ref {
    private _key: K | undefined = undefined;

    onCreate(key: K, memoryPool: MemoryPool) {
        this._key = key;
        this.setMemoryPool('_', memoryPool);
        this.setObjectType(MEM_REF);
        this.setObjectName(`mem-ref-${this._objectId}`);
    }

    onDestroy(...args: any[]) {
        this.stopObserver([System.MEMORY_UPDATE])
    }

    setValue(value: V) {
        if (this._key) {  //
            this.mem().set(this, value);
        }
        else {
            throw new Error(MESSAGE(this, `key undefined`));
        }
    }

    get value() {
        if (this._key) {
            return this.mem().get(this)
        }
        else {
            throw new Error(MESSAGE(this, `key undefined`));
        }
    }

    get key() {
        return ASSERT(this._key, MESSAGE(this, `key undefined`));
    }

    get poolId() {
        return this.mem()._objectId;
    }
}

export class StaticRef<V> extends Ref {
    private _value: V | null = null;

    constructor(value: V | undefined) {
        super();
        const v = value != undefined ? value : null;
        this.setValue(v);
    }

    setValue(value: V | null) {
        this._value = value;
        this.setObjectType(STATIC_REF);
        this.setObjectName(`static-ref-${this._objectId}`);
    }

    onCreate(...args: any[]) { }
    onDestroy(...args: any[]) { }
    async onInit(...args: any[]) { }

    get value(): V | null {
        return this._value;
    }

    get key() {
        return null;
    }

    get poolId(): number {
        return -1;
    }
}


export class RefFactory<K, V> extends ObjectFactory<MemRef<K, V>> { }

export class MemoryPool<K = any, V = any> extends AppObject {
    private _refs: Map<K, number> = new Map();
    private _data: Map<string, V> = new Map();
    private _refFactory: RefFactory<K, V> | undefined = undefined;
    private _refStore: Map<number, MemRef<K, V>> = new Map();

    createRefFactory() {
        if(!this.getObjectStore(Scope.LOCAL)) {   ////
            this.createLocalStore();
        }
        const refFactoryType = class extends RefFactory<K, V> { }
        this._refFactory = this._localStore.get(refFactoryType);
        if (this._refFactory == undefined) {
            throw MESSAGE(this, `ref factory undefined`);
        }
    }

    clearRefFactory() {
        for (const refId of this._refs.values()) { //
            const ref = this._refStore.get(refId);
            if (ref) {
                this.clearRef(ref);
            }
            else {
                // notify error
            }
        }
    }

    createRef<R extends MemRef<K, V>>(key: K, refType: Constructor<R>) {
        const ref = this._refFactory!.create(refType, key, this) as MemRef<K, V>;
        if (ref) {
            this._refs.set(key, ref._objectId);
            this._refStore.set(ref._objectId, ref);
            return ref;
        }
        else {
            throw new Error(MESSAGE(this,`(create ref ) error`));
        }
    }

    clearRef(ref: MemRef<K, V> | undefined) {
        if (ref?.key) {
            this.erase(ref.key);
            this._refs.delete(ref.key);
        }
        if (ref) {
            this._refStore.delete(ref._objectId);
        }
    }

    getRef(key: K) {
        const id = this._refs.get(key);
        let ref: MemRef<K, V> | null = null;
        if (id) {
            ref = this._refStore.get(id) as MemRef<K, V>;
        }
        return ref || undefined;
    }

    set(ref: MemRef<K, V>, value: V) {
        if (ref.key) {
            this.write(ref.key, value)
        }
        else {
            throw new Error(MESSAGE(this,`ref key undefined`));
        }
    }

    get(ref: MemRef<K, V>) {
        if (ref.key) {
            return this.read(ref.key);
        }
        else {
            throw new Error(MESSAGE(this,`ref key undefined`));
        }
    }


    clearKey(key: K) {  //
        this.erase(key);
    }


    write(key: K, value:V) {
        const keyStr = JSON.stringify(key); //
        this._data.set(`${keyStr}.${this._objectId}`, value);
        this.action(System.MEMORY_UPDATE, {
            action: MemoryEvent.WRITE,
            params: { poolId: this._objectId, key }    /////
        });
    }

    read(key: K) {
        const keyStr = JSON.stringify(key); //
        return this._data.get(`${keyStr}.${this._objectId}`);
    }

    erase(key: K): boolean {
        const keyStr = JSON.stringify(key); //
        //const h = __hash(keyStr, poolId);
        return this._data.delete(`${keyStr}.${this._objectId}`);
    }

    clear() {   //
        this._data = new Map();
    }

    get data() {  //
        return Array.from(this._data.entries());
    }

}


export class NumberRef extends MemRef<string, number> { };
export class StringRef extends MemRef<string, string> { };

