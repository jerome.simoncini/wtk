import { ASSERT } from "./utils.js";

export const STYLE_MAP_SYMBOL = ':'

export type Style = string[];

export class StyleMap extends Map<number, Style> {
    use(style: number): string {
        return ASSERT(this.get(style), `unknown style '${style}'`).join(STYLE_MAP_SYMBOL);
    }
}