import { WTK_DB_STORE_LOGIC } from "./AppDBStore.js";
import { AppObject } from "./AppObject.js";
import { Constructor } from "./Constructor.js";
import { LOGGER } from "./Logger.js";
import { ObjectStore } from "./ObjectStore.js";
import { ASSERT } from "./utils.js";

const DEBUG = LOGGER("App DB");
DEBUG.on();


export class WTK_DB_LOGIC extends AppObject {
    private _db_stores: Map<Constructor<WTK_DB_STORE_LOGIC>, WTK_DB_STORE_LOGIC> = new Map();
    private _dbName: string | undefined = undefined;
    private _dbVersion: number | undefined = undefined;

    setDbName(dbName: string) {
        this._dbName = dbName;
    }

    setDbVersion(dbVersion: number) {
        this._dbVersion = dbVersion;
    }

    setStores(stores: Constructor<WTK_DB_STORE_LOGIC>[]) {  //
        for (const store of stores) {
            const objectStore = new ObjectStore();
            const dbstore = objectStore.get(store) as WTK_DB_STORE_LOGIC;
            dbstore.setDatabase(this);
            this._db_stores.set(store, dbstore);
        }
    }

    connect(onsuccess: (event: any) => any) {
        DEBUG.log("CONNECT");

        const request = indexedDB.open(this.dbName, this.dbVersion);
        console.log(request);

        request.onerror = (event) => {
            DEBUG.error((<IDBRequest>event.target).error?.name);
        }

        request.onupgradeneeded = (event) => {
            ASSERT(window.indexedDB, `Your browser doesn't support IndexedDB`)
            const cnx = (<IDBRequest>event.target).result;
            if (cnx) {
                DEBUG.log("init");
                this.createStores(cnx);
            }
            else {
                // notify error
                DEBUG.log("init error");
            }
        }

        request.onsuccess = onsuccess;
    }

    createStores(cnx: any): void {
        for (const store of this._db_stores.values()) {
            store.createStore(cnx);
        }
    }

    getStore<S extends WTK_DB_STORE_LOGIC>(store: Constructor<S>) {
        return this._db_stores.get(store);
    }

    get dbName() {
        return ASSERT(this._dbName, `(${this.constructor.name}) db name undefined`);
    }

    get dbVersion() {
        return ASSERT(this._dbVersion, `(${this.constructor.name}) db version undefined`);
    }
}