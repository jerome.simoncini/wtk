import { WTK_APP_PAGE_LOGIC } from "./AppPage.js";
import { Constructor } from "./Constructor.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { __skull__ } from "./_skull.js";

const TYPE = "AppPage";

export interface IAppPage extends IAppComponent {
};

export const WTK_GEN_APP_PAGE = <P extends WTK_APP_PAGE_LOGIC>(page: Constructor<P>, settings?: IAppPage): Constructor<P> => {
    const skull = __skull__(page, {
        onCreate: _create_app_page_, 
        onInit: _init_app_page_, 
        settings: settings
    }) as Constructor<P>;
    return WTK_GEN_COMPONENT(skull, settings as IAppComponent) as Constructor<P>;
};

const _create_app_page_ = (page: WTK_APP_PAGE_LOGIC, settings?: IAppPage): void => {
    page.setObjectType(TYPE)
};

const _init_app_page_ = async (page: WTK_APP_PAGE_LOGIC, settings?: IAppPage) => {
};

//------------------------------------------------------------------------------------------------------------//


