import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { AppStateService } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { MemoryPool } from "./MemoryPool.js";
import { IModelGroupItemLogic, IModelSetup, WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC, ModelGroup, ModelGroupItem, ModelLogic, ModelType } from "./ModelGroup.js";
import { ModelProps } from "./ModelProps.js";
import { WTK_MODEL_GROUP_UI_SPEC_LOGIC, _modelSettingsHTML, _modelSettingsINIT, _modelSettingsPRERENDER } from "./settings/ModelGroupUI.js";
import { IPropsUI } from "./settings/PropsUI.js";
import { WtkUI, WTK_UI_FACTORY_LOGIC } from "./settings/UI.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { WTK_GEN_MEMORY } from "./_gen_memory.js";


export interface IModelGroup extends IAppObject {
    setup: IModelSetup;
};

export const WTK_GEN_MODEL_GROUP = (settings: IModelGroup): Constructor<ModelGroup> => {
    const _ = class extends ModelGroup { }
    const modelGroup = __skull__(_, {
        onCreate: _create_model_group_,
        settings: settings
    }) as Constructor<ModelGroup>;
    return WTK_GEN_OBJECT(modelGroup, settings) as Constructor<ModelGroup>
};

const _create_model_group_ = (_: ModelGroup, settings: IModelGroup): void => {
    _.setProps(settings.setup.items);
};

/***********************************/

export interface IModelItem extends IAppObject {
    group: Constructor<ModelGroup>
};

export const WTK_GEN_MODEL_GROUP_ITEM = (settings: IModelItem): Constructor<ModelGroupItem> => {
    const _ = class extends ModelGroupItem {
        onCreate(modelType: ModelType | undefined = undefined) {
            this.initProps(modelType);
        }
    }
    const modelGroupItem = __skull__(_, {
        onCreate: _create_model_group_item_,
        settings: settings
    }) as Constructor<ModelGroupItem>;
    return WTK_GEN_OBJECT(modelGroupItem, settings) as Constructor<ModelGroupItem>
};

const _create_model_group_item_ = (_: ModelGroupItem, settings: IModelItem): void => {
    _.setModelGroup(settings.group);
};

/***********************************/

export interface IModelFactory extends IAppObject {
    items: IModelGroupItemLogic<ModelProps>[]
};

export const WTK_GEN_MODEL_FACTORY = <L extends ModelLogic<P>, P extends ModelProps>(settings: IModelFactory): Constructor<WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<L, P>> => {
    const _ = class extends WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<L, P> { }
    const modelFactory = __skull__(_, {
        onCreate: _create_model_factory_,
        settings: settings
    }) as Constructor<WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<L,P>>;
    return WTK_GEN_OBJECT(modelFactory, settings) as Constructor<WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<L, P>>
};

const _create_model_factory_ = <L extends ModelLogic<P>, P extends ModelProps>(_: WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<L, P>, settings: IModelFactory): void => {
    _.createLocalStore();
    _.setLogic(settings.items);
};

/***********************************/

export const WTK_GEN_MODEL_PROPS = (): Constructor<ModelProps> => {
    const _ = class extends ModelProps { }
    const modelProps = __skull__(_, {
        onCreate: _create_model_props_
    }) as Constructor<ModelProps>;
    return WTK_GEN_OBJECT(modelProps) as Constructor<ModelProps>
};

const _create_model_props_ = (_: ModelProps): void => {       
    const memoryPool = WTK_GEN_MEMORY(MemoryPool, {
        name: "props memory"
    }) 
    _.setMemoryPool('_', _.___(memoryPool));              //////////////////
};

/***********************************/

export const WTK_GEN_MODEL_LOGIC = <P extends ModelProps>(props: Constructor<P>): Constructor<ModelLogic<P>> => {
    const _ = class extends ModelLogic<ModelProps> { }
    const modelLogic = __skull__(_) as Constructor<ModelLogic<P>>;
    return WTK_GEN_OBJECT(modelLogic) as Constructor<ModelLogic<P>>
};

/***********************************/

interface IModelItemProps extends IAppObject {
    props: IPropsUI[];
    uiFactory?: Constructor<WTK_UI_FACTORY_LOGIC>; //
    onInit?: (_: ModelProps) => Promise<void>;
    onUpdate?: (_: ModelProps, key: string, groupUI: WTK_COMPONENT_LOGIC) => void;
};

export const WTK_GEN_MODEL_ITEM_PROPS = (modelProps: Constructor<ModelProps>, settings: IModelItemProps): Constructor<ModelProps> => {
    const _ = class extends modelProps { }
    const modelItemProps = __skull__(_, {
        onCreate: _create_model_item_props_,
        settings: settings
    }) as Constructor<ModelProps>;
    return WTK_GEN_OBJECT(modelItemProps, settings) as Constructor<ModelProps>
};

const _create_model_item_props_ = (_: ModelProps, settings: IModelItemProps): void => {
    _.setProps(settings.props);
    _.setUIFactoryCtor(settings.uiFactory || WtkUI); //
};

/***********************************/

interface IModelItemLogic extends IAppObject {
    onInit?: (_: ModelLogic<ModelProps>) => Promise<void>;
    onUpdate?: (_: ModelLogic<ModelProps>) => void;
};

export const WTK_GEN_MODEL_ITEM_LOGIC = (modelLogic: Constructor<ModelLogic<ModelProps>>, settings: IModelItemLogic): Constructor<ModelLogic<ModelProps>> => {
    return WTK_GEN_OBJECT(modelLogic, settings) as Constructor<ModelLogic<ModelProps>>;
};

/***********************************/

interface IModelSettings extends IAppComponent {
    stateService: Constructor<AppStateService>;
    uiFactory?: Constructor<WTK_UI_FACTORY_LOGIC>;  //
};

const _modelSettingsOnUpdate = (_: WTK_MODEL_GROUP_UI_SPEC_LOGIC): void => {
    const state = _.stateService.getState();
    _.stateService.reset(state);
}

export const WTK_GEN_MODEL_SETTINGS = (settings: IModelSettings): Constructor<WTK_COMPONENT_LOGIC> => {
    const _ = class extends WTK_MODEL_GROUP_UI_SPEC_LOGIC { }
    const modelSettings = __skull__(_, {
        onCreate: _create_model_settings_,
        settings: settings
    }) as Constructor<WTK_COMPONENT_LOGIC>
    return WTK_GEN_COMPONENT(modelSettings, {
        onInit: settings.onInit || _modelSettingsINIT,
        onprerender: settings.onprerender || _modelSettingsPRERENDER,
        html: settings.html || _modelSettingsHTML,
        onUpdate: settings.onUpdate || _modelSettingsOnUpdate,   //
        createLocalStore: true,
    });
};

const _create_model_settings_ = (_: WTK_MODEL_GROUP_UI_SPEC_LOGIC, settings: IModelSettings): void => {
    _.setStateService(settings.stateService);
    _.setUIFactoryCtor(settings.uiFactory || WtkUI);  //
};