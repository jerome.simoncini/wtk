import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { LogLevel } from "./AppEntity.js";
import { AppNodeFlag } from "./AppComponent.js";
import { AppTree, WTK_APP_TREE_LOGIC } from "./AppTree.js";
import { AppWindow } from "./AppWindow.js";
import { Scope } from "./Scope.js";
import { WTK_APP_ROOT, WTK_SERVICE_WORKER_SCOPE, WTK_SERVICE_WORKER_SCRIPT, WTK_SSR } from "./server/settings.js";
import { __DEBUG__ } from "./utils.js";


export class AppModule extends WTK_COMPONENT_LOGIC {
    private _js_files: string[] = [];                /////////////////////////

    importJS(js: string[]) {                        /////////////////////////
        this._js_files = js;
    }

    async load(): Promise<AppModule> {
        for (const file of this._js_files) {
            await import(file);
        }
        return this;
    }

    async start() {
        const [root, ssr] = this.getMetas(WTK_APP_ROOT, WTK_SSR);

        this.setTagname(root);

        if (ssr === "false") {
            this.nodeflags.setBits(AppNodeFlag.MOUNTED);
            await this.render();
        }
        else if (document) {
            await this._finishSSR(root);
            await this._ready();
        }

        await this.startServiceWorker();

        //////////////
        this.___(AppWindow).addEventListener('AppModule_onResize', 'resize', (e: Event) => {
            this.onResize();
        });
    }

    startServiceWorker(): Promise<boolean> {
        const [sw_script, sw_scope] = this.getMetas(WTK_SERVICE_WORKER_SCRIPT, WTK_SERVICE_WORKER_SCOPE);

        if (!(sw_script && sw_scope) || !('serviceWorker' in navigator)) {
            // notify error
            return new Promise(resolve => false);
        }

        return navigator.serviceWorker.register(sw_script, {
            scope: sw_scope,
        }).then(() => {
            console.log(`service worker registered "script=${sw_script} scope=${sw_scope}`);
            return true
        });
    }


    get appTree(): WTK_APP_TREE_LOGIC {                                    ///
        return this.getObjectStore(Scope.LOCAL).get(AppTree);
    }

    log(inode: string, level: LogLevel, message: string) {
        this.appTree.log(inode, level, message);
    }
}
