import { WTK_FS_NODE_LOGIC } from "./AppFsNode.js";
import { WTK_SYMLINK_LOGIC } from "./AppSymLink.js";
import { Constructor } from "./Constructor.js";
import { IAppObject } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";



export enum WFSNodeType {
    DIRECTORY = "directory",
    FILE = "file",
    LINK = "link"
}


export interface IAppFsNode extends IAppObject {
    name: string;
    type: WFSNodeType;
    target?: string;
}

export interface IAppSymLink extends IAppFsNode {
}

export const WTK_GEN_FS_NODE = <T extends WTK_FS_NODE_LOGIC>(dir: Constructor<T>, settings: IAppFsNode): Constructor<T> => {
    return __skull__(dir, {
        onCreate: _create_fs_node_,
        onInit: _init_fs_node_,
        settings
    }) as Constructor<T>;
}

const _create_fs_node_ = (node: WTK_FS_NODE_LOGIC, settings: IAppFsNode): void => {
    node.setObjectName(settings.name);
    node.setObjectType(settings.type);

    if(settings.type == WFSNodeType.DIRECTORY) {
        node.createLocalStore();                 //
    }

    if(settings.type == WFSNodeType.LINK) {
        if(settings.target) {
            (<WTK_SYMLINK_LOGIC>node).bind(settings.target);
        }
        else {
            // error
        }
    }
}

const _init_fs_node_ = async (node: WTK_FS_NODE_LOGIC, settings: IAppFsNode) => {
}