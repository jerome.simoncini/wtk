import { AppObject } from "./AppObject.js";
import { Constructor } from "./Constructor";
import { WTK_GEN_OBJECT } from "./_gen_object.js";
import { ASSERT } from "./utils.js";

const _gen_fs_node_map_type = <T extends FsNode>(nodeType: Constructor<T>)=> class extends Map<string,T> {}

abstract class FsNode {
    private _root: string|undefined = undefined;
    private _name: string|undefined = undefined;

    constructor(root: string, name: string) {
        this._root = root;
        this._name = name;
    }

    protected get root() {
        return ASSERT(this._root, `fs node root undefined`);
    }

    protected get name() {
        return ASSERT(this._name, `fs node name undefined`);
    }

    protected set name(name: string) {
        this._name = name;
    }

    get path(): string{
       return this.root.concat(this.name);
    }
}

class FileNode extends FsNode {
    constructor(root: string, id: string) {
        super(root, id)
    }
}

type FsNodeMap = Map<string, FsNode>;

class FolderNode extends FsNode {
    private _subtree: Map<FsNode, FsNodeMap> = new Map(); 

    constructor(root: string, id: string) {
        super(root, id);
    }

    create<T extends FsNode>(nodeType: Constructor<T>, name: string) {
        if(!this.subtree.has(nodeType)) {
            const nodeMap = _gen_fs_node_map_type(nodeType);
            this.subtree.set(nodeType, new nodeMap())
        }
        this.subtree.get(nodeType).set(nodeType, new nodeType(this.path, name));
    }

    rename<T extends FsNode>(nodeType: Constructor<T>, name: string) {
        const node = ASSERT(this.subtree.get(nodeType)?.get(name), `node not found: ${nodeType.name} -> ${name}`);
        node.name = name;
    }

    remove<T extends FsNode>(nodeType: Constructor<T>, name: string) {
        const st = ASSERT(this.subtree.get(nodeType), `unknwon subtree '${nodeType}'`);
        st.remove(name);
    }

    private get subtree() {
        return ASSERT(this._subtree, `fs subtree undefined`);
    }
}


export class FILE_SYSTEM_LOGIC extends AppObject {
    private _root: FolderNode = new FolderNode('~','/');

    load(hash: string) {
       this.root.create(FileNode, 'file-test');
       this.root.create(FolderNode, 'folder-test/');
    }

    private get root() {
        return ASSERT(this._root, `fs root undefined`);
    }
}

export const FileSystem = WTK_GEN_OBJECT(FILE_SYSTEM_LOGIC, {

});