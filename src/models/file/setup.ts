import { WTK_GEN_MODEL_FACTORY, WTK_GEN_MODEL_GROUP, WTK_GEN_MODEL_GROUP_ITEM } from "../../_gen_model.js";
import { IModelSetup } from "../../ModelGroup.js";
import { AppDbFileLogic, AppDbFileProps, WTK_FILE_MODEL } from "./model.js";


export enum WtkFileType {
    APP_DB_FILE = "app_db_file"
}

export const WtkFileSetup: IModelSetup = 
{
    name: WTK_FILE_MODEL,
    items: [
        {
            type: WtkFileType.APP_DB_FILE,
            props: AppDbFileProps,
            logic: AppDbFileLogic,
        },
    ]
}

export const WtkFileGroup = WTK_GEN_MODEL_GROUP({
    setup: WtkFileSetup
});
export const WtkFileItem = WTK_GEN_MODEL_GROUP_ITEM({
    group: WtkFileGroup
});
export const WtkFileFactory = WTK_GEN_MODEL_FACTORY({
    items: WtkFileSetup.items
});
