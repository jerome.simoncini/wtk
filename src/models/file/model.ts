import { WTK_GEN_MODEL_ITEM_LOGIC, WTK_GEN_MODEL_ITEM_PROPS, WTK_GEN_MODEL_LOGIC, WTK_GEN_MODEL_PROPS } from "../../_gen_model.js";

export const WTK_FILE_MODEL = "WtkFile";
export const WTK_FILE_NAME_UI: string = "filename";
export const WTK_FILE_SYNCED: string = "synced";
export const DEFAULT_WTK_FILE_NAME: string = "untitled";

export const WtkFileProps = WTK_GEN_MODEL_PROPS();
export const WtkFileLogic = WTK_GEN_MODEL_LOGIC(WtkFileProps);

const PROPS = [
    { name: WTK_FILE_NAME_UI, default: DEFAULT_WTK_FILE_NAME },
    { name: WTK_FILE_SYNCED, default: false }
]

export const AppDbFileProps = WTK_GEN_MODEL_ITEM_PROPS(WtkFileProps, {
    props: PROPS
});

export const AppDbFileLogic = WTK_GEN_MODEL_ITEM_LOGIC(WtkFileLogic, {});