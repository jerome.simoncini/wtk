import { Constructor } from "./Constructor.js";
import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { _HTML_ } from "./Compiler.js";
import { ASSERT, MESSAGE } from "./utils.js";
import { AppNodeFlag } from "./AppComponent.js";

const WTK_CONTAINER_CONTENT = "wtk-container";

export type WTK_CONTAINER_ID = number|string;  //

export class WTK_CONTAINER_LOGIC extends WTK_COMPONENT_LOGIC {
    private _contentId: WTK_CONTAINER_ID| undefined = undefined;
    private _contentTagname: string = WTK_CONTAINER_CONTENT;
    protected _store: Map<WTK_CONTAINER_ID, WTK_COMPONENT_LOGIC> = new Map();    ////

    registerContent<T extends WTK_COMPONENT_LOGIC>(importType: Constructor<T>, id: WTK_CONTAINER_ID|undefined = undefined): number {
        //const content = this._localStore.get(importType);              ////
        //const factory = this.getObjectStore(Scope.GLOBAL).get(ComponentFactory);
        //factory.setObjectName('Component Factory');
        //factory.setObjectType("Object Factory");
        const content = this.___(importType);              ////
       // content.setAppModule(this.module);
        this._store.set(id ?? content._objectId, content);     ////
        return content._objectId;
    }


    setContentTagname(tagname: string) {
       this._contentTagname = tagname;
    }

    async setContent(contentId: WTK_CONTAINER_ID) {
        this._contentId = contentId;
        await this.mount(this.contentTagname, ASSERT(this.current, MESSAGE(this, `current content undefined`)));
    }

    async releaseContent(contentId: WTK_CONTAINER_ID) {   //
        const content = this._store.get(contentId);
        if(content?.nodeflags.has(AppNodeFlag.MOUNTED)) {
            await this.umount(this.contentTagname);
        }
        this._store.delete(contentId);
        this._contentId = undefined;
    }
    
    async clear() {
        const content = this._store.get(this.contentId);
        if(content?.nodeflags.has(AppNodeFlag.MOUNTED)) {
            await this.umount(this.contentTagname);
            await this.render();
        }
    }


    getContent(contentId: WTK_CONTAINER_ID): WTK_COMPONENT_LOGIC | undefined {
        return this._store.get(contentId);
    }

    get current(): WTK_COMPONENT_LOGIC | undefined {  //
        return this._contentId ? this._store.get(this.contentId) : undefined;    ////
    }

    get contentId(): WTK_CONTAINER_ID {
        return ASSERT(this._contentId, MESSAGE(this,`content id undefined`));
    }

    get contentTagname(): string {
        return this._contentTagname != '' ? this._contentTagname : WTK_CONTAINER_CONTENT;
    }
}