import { AppObject } from "./AppObject.js";
import { LOGGER } from "./Logger.js";
import { Constructor } from "./Constructor.js";
import { ASSERT, DoubleBuffer, MESSAGE, Mask } from "./utils.js";
import { AppStateEvent, System } from "./events.js";
import { IModelGroupItem, ModelGroupItem, ModelType } from "./ModelGroup.js";   //
import { ObjectStore } from "./ObjectStore.js";
import { ObjectFactory } from "./ObjectFactory.js";

const DEBUG = LOGGER("App State");
DEBUG.on();



export interface IModelGroupItemState {
   onTypeUpdate?: number;
   onValueUpdate: number;
}

/********************************************************************************************************/

export class AppStateService extends AppObject {
   private _appState: AppState | undefined = undefined;
   private _state: StateObject | undefined = undefined;

   init<A extends AppState>(appState: Constructor<A>, stateName: string) {
      this._appState = this.___(appState);
      this._state = this.appState.getState(stateName);
   }

   reset(settings: IModelGroupItem, force: boolean = false): void {
      DEBUG.log(`(${this.constructor.name})(reset) type=${settings.type} props=${settings.props._serialize()}`);       ////
      this.state.update(settings, this.appState, force);
      this.appState.triggerUpdates();
   }

   update() {
      const state = this.getState();
      this.reset(state);
   }

   getState(): IModelGroupItem {
      return this.state.getData();
   }

   getStateId(): number {
      return this.state._objectId;
   }

   private get state() {
      return ASSERT(this._state, MESSAGE(this,`state object undefined`));
   }


   get appState() {
      return ASSERT(this._appState, MESSAGE(this,`app state undefined`));
   }
}

/********************************************************************************************************/

export abstract class StateObject extends AppObject {
   public abstract _deserialize(json: string): void;
   public abstract _serialize(): string;
   public abstract _initState(...args: any[]): void;

   update(...args: any[]): void { }  //
   getData(): any { } //
}

/********************************************************************************************************/

export abstract class StateVariable<T extends StateObject> extends StateObject {
   private _value: T;

   public __init(valueType: Constructor<T>, ...args: any[]) {
      this.createLocalStore();
      this._value = this._localStore.get(valueType);
   }

   public _serialize(): string {
      return this.value._serialize();
   }

   public _deserialize(json: string): void {
      this.value._deserialize(json);
   }

   public _initState(...args: any[]): void {
      this.value._initState(...args);
   }

   public getData() {
      return this.value.getData();
   }

   public get value() {
      return ASSERT(this._value, MESSAGE(this,`value undefined`));
   }
}


/********************************************************************************************************/

export class ModelGroupItemState<M extends ModelGroupItem> extends StateVariable<M> {
   private _onTypeUpdate: any = undefined;
   private _onValueUpdate: any = undefined;

   update(settings: IModelGroupItem, appState: AppState, force: boolean): boolean {
      console.warn(`(${this.constructor.name})(update) type=${settings.type} props=${settings.props._serialize()}`);    /////
      this.value.setProps(settings.props);
      appState.setUpdate(this._onValueUpdate);
      appState.setUpdateData(this._onValueUpdate, { objectId: this._objectId });
      console.warn(`(update state) event=${this._onTypeUpdate} id=${this._objectId}`);          /////
      if ((this.value.type != settings.type || force) && this._onTypeUpdate != undefined) {
         this.value.setType(settings.type);
         appState.setUpdate(this._onTypeUpdate);
         console.warn(`(set object id) event=${this._onTypeUpdate} id=${this._objectId}`);      /////
         appState.setUpdateData(this._onTypeUpdate, { objectId: this._objectId });
      }
      return true;
   }

   setUpdateEvents(events: IModelGroupItemState) {
      this._onTypeUpdate = events.onTypeUpdate;
      this._onValueUpdate = events.onValueUpdate;
   }
}


/********************************************************************************************************/

export interface IStateUpdate {
   mask: Mask;
   data: any; //
}




/********************************************************************************************************/

const SEND = [
   System.STATE_UPDATE,
   System.STATE_ERROR
];

class StateUpdateQueue extends AppObject {
   private _updates: IStateUpdate[] = [];
   private _lock: boolean = false;

   onCreate() {
      this.startNotifier(SEND);
   }

   onDestroy() {
      this.stopNotifier(SEND);
   }

   push(update: IStateUpdate) {
      this._updates.push(update);
      if (!this._lock) {
         this.trigger();
      }
   }

   private * getUpdateIterator() {
      while (this._updates.length) {
         yield this._updates.shift()
      }
   }

   private trigger() {
      this._lock = true;

      const it = this.getUpdateIterator();

      let result: IteratorResult<IStateUpdate | undefined> = { done: false, value: { mask: new Mask(), data: null } }; //

      while (!result.done) {
         this.action(System.STATE_UPDATE, { action: AppStateEvent.UPDATE, params: result.value });
         result = it.next();
      }

      this._lock = false;
   }
}

/********************************************************************************************************/

export interface IModelState {
   name: string;
   ctor: Constructor<StateObject>;
   default: ModelType
}

export class AppState extends StateObject {
   protected _name: string | undefined = undefined;
   protected _updateMask: DoubleBuffer<Mask> = new DoubleBuffer();
   protected _updateData: DoubleBuffer<Map<number, any>> = new DoubleBuffer();   //
   private _updateQueue: StateUpdateQueue | undefined = undefined;
   private _states: IModelState[] = [];
   private __states: Map<string, StateObject> = new Map();

   onCreate(...args: any[]): void {
      super.onCreate();
      this._updateMask.set(new Mask());
      this._updateData.reset(new Map());
      this.createLocalStore();  //
   }

   onDestroy(...args: any[]): void {  //
      this.getGlobalStore()!.release(StateUpdateQueue);   //
      sessionStorage.removeItem(this.name);
   }

   async onInit(...args: any[]) {
      this._updateQueue = this.___(StateUpdateQueue);  //
      if (!this.loadTmp()) {
         this._initState();
      }
   }

   setName(name: string) {
      this._name = name;
   }

   setUpdate(...bits: number[]) {
      this._updateMask.update((mask: Mask) => mask.setBits(...bits)); //
   }

   setUpdateData(event: number, data: any) {
      const buffer = this._updateData._get();
      ASSERT(buffer, MESSAGE(this,`data buffer undefined`));
      buffer!.set(event, data);
   }

   createStates(states: IModelState[]) {
      this._states = states;
      const store = new ObjectStore();
      for (const state of this._states) {
         DEBUG.log(`create state '${state.name}' -> '${state.ctor}'`);                                ////
         const stateObject = store.get(ObjectFactory).create(state.ctor) as StateObject;
         this.__states.set(state.name, stateObject);
      }
   }

   getState(stateName: string) {
      return ASSERT(this.__states.get(stateName), MESSAGE(this,`state undefined : '${stateName}'`));
   }

   triggerUpdates(): void {
      this.saveTmp();
      this._notifyUpdates();
   }

   protected _notifyUpdates() {
      this._updateMask.swap();
      this._updateData.swap();
      this._updateMask.reset(new Mask());
      this._updateData.reset(new Map());
      this.updateQueue.push({ mask: this.updateMask, data: this.updateData });
   }

   saveTmp = () => {
      sessionStorage.setItem(this.name + '-tmp', this._serialize());
   }

   loadTmp(): boolean {
      const state = sessionStorage.getItem(this.name + '-tmp');
      if (state) {
         this._deserialize(state);
      }
      return state != null;
   }

   public _deserialize(json: string): void {
      const data = JSON.parse(json);
      for (const state of this._states) {
         this.__states.get(state.name)!._deserialize(data[state.name]);    //
      }
   }

   public _serialize(): string {
      const data = {};
      for (const state of this._states) {
         data[state.name] = this.__states.get(state.name)!._serialize();   //
      }
      return JSON.stringify(data);
   }

   public _initState(...args: any[]): void {
      for (const state of this._states) {
         this.__states.get(state.name)!._initState({ type: state.default });    //
      }
   }

   public _resetState() {
      this._onReset(this);
   }

   public _onReset(state: AppState) { }

   private get updateMask(): Mask {
      const mask = this._updateMask.get();
      return mask ? mask : new Mask();
   }

   private get updateData(): Map<number, any> {
      const data = this._updateData.get();
      return data ? data : new Map();
   }

   private get updateQueue() {
      return ASSERT(this._updateQueue, MESSAGE(this,`update queue undefined`));
   }

   private get name() {
      return ASSERT(this._name, MESSAGE(this,`state group name undefined`));
   }
}