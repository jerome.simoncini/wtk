import { AppModule } from "./AppModule.js";
import { WTK_OVERLAY_LOGIC } from "./AppOverlay.js";
import { Constructor } from "./Constructor.js";
import { IAppContainer, WTK_GEN_CONTAINER } from "./_gen_container.js";
import { __skull__ } from "./_skull.js";


export interface IAppOverlay extends IAppContainer {
}

export const WTK_GEN_OVERLAY = <T extends WTK_OVERLAY_LOGIC>(overlay: Constructor<T>, settings: IAppOverlay): Constructor<WTK_OVERLAY_LOGIC> => {
    const skull = __skull__(overlay, {
        onCreate: _create_overlay_,
        onInit: _init_overlay_,
        settings: settings
    }) as Constructor<WTK_OVERLAY_LOGIC>;
    return WTK_GEN_CONTAINER(skull, {
        ...settings,
        onResize: async (_: T) => {
            //////////////////////////////
            const module = _.getModule(AppModule);
            if(module) {
                //console.warn(`width=${module.root.clientWidth} height=${module.root.clientHeight}`);
                const root = _.current?.root as HTMLElement;
                console.warn(root);
                if(root) {
                   root.style.width = module.root.clientWidth + 'px';
                   root.style.height = module.root.clientHeight + 'px';
                }
                else {
                    // notify error
                }
            }
            else {
                // notify error
            }
        }
    }) as Constructor<WTK_OVERLAY_LOGIC>
}

const _create_overlay_ = (overlay: WTK_OVERLAY_LOGIC, settings: IAppOverlay): void => {
    if (settings.content) {
        for (const layer of settings.content) {
            overlay.createLayer(layer.id, layer.ctor)
        }
    }
}

const _init_overlay_ = async (overlay: WTK_OVERLAY_LOGIC, settings: IAppOverlay) => {

}