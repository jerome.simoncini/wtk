export enum System {
    STATE_ACTION,
    STATE_UPDATE,
    STATE_ERROR,
    PAGE_ACTION,
    PAGE_UPDATE,
    PAGE_ERROR,
    DB_ACTION,
    DB_UPDATE,
    DB_ERROR,
    CONTROL_ACTION,
    CONTROL_UPDATE,
    CONTROL_ERROR,
    LOGIC_ACTION,
    LOGIC_UPDATE,
    LOGIC_ERROR,
    SYNC_ACTION,
    SYNC_UPDATE,
    SYNC_ERROR,
    AUTH_ACTION,
    AUTH_UPDATE,
    AUTH_ERROR,
    WINDOW_ACTION,
    WINDOW_UPDATE,
    WINDOW_ERROR,
    ROUTER_ACTION,
    ROUTER_UPDATE,
    ROUTER_ERROR,
    MEMORY_ACTION,
    MEMORY_UPDATE,
    MEMORY_ERROR,
    APP_ACTION,
    APP_UPDATE,
    APP_ERROR
}

export enum PageAction {  //
    SHOW_TAB,
}

export enum WindowAction {  //
    HIDE
}

export enum AppDbEvent {  //
    UPDATE
}

export enum AppStateEvent { //
    UPDATE,
}
