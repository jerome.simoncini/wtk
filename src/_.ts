import { WTK_DB_LOGIC } from "./AppDB.js";
import { WTK_DB_STORE_LOGIC, _dbStoreCREATE } from "./AppDBStore.js";
import { Constructor } from "./Constructor.js";
import { _modelSettingsHTML, _modelSettingsINIT, _modelSettingsPRERENDER } from "./settings/ModelGroupUI.js";
import { __skull__ } from "./_skull.js";
import { WTK_GEN_DB, WTK_GEN_DB_STORE } from "./_gen_db.js";

export interface IDatabaseSetup {
    name: string;
    version: number;
    stores: string[];
};

export interface IAppSetup {
    database?: IDatabaseSetup[];
};

////////////////
export const WTK_SETUP = (setup: IAppSetup): any => {
    let app = {};
    if (setup.database) {
        for (const db of setup.database) {
            let stores: Constructor<WTK_DB_STORE_LOGIC>[] = []
            for (const storeName of db.stores) {  //
                app[`${storeName}Store`] = WTK_GEN_DB_STORE({
                    storeName,
                    onCreate: _dbStoreCREATE,
                    name: storeName
                });
                stores.push(app[`${storeName}Store`]);
            }
            app[`${db.name}DB`] = WTK_GEN_DB({
                stores: stores,
                dbName: db.name,
                dbVersion: db.version,
                name: db.name
            });
        }
    }
    return app;
};


export const WTK_GET_DB_STORE = (app: {}, storeName: string): Constructor<WTK_DB_STORE_LOGIC> => {
    return app[`${storeName}Store`];
};

export const WTK_GET_DB = (app: {}, dbName: string): Constructor<WTK_DB_LOGIC> => {
    return app[`${dbName}DB`];
};