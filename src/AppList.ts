import { Constructor } from "./Constructor.js"
import { ComponentFactory, WTK_COMPONENT_LOGIC } from "./AppComponent.js"
import { ASSERT } from "./utils.js"
import { _HTML_ } from "./Compiler.js";

const LIST_ITEM = 'item';

export class WTK_LIST_LOGIC extends WTK_COMPONENT_LOGIC {
    items: any[];                                                                    ///////////////
    private _itemCtor: Constructor<WTK_COMPONENT_LOGIC> | undefined = undefined

    setItemCtor(ctor: Constructor<WTK_COMPONENT_LOGIC>) {
        this._itemCtor = ctor;
    }

    build() {
        ASSERT(this.items, `[WTK_LIST_LOGIC] items undefined`);

        for (const slot of this._slots.keys()) {
            this.destroySlot(slot);
        }
        const slotsHTML: string[] = []
        for (const [index, item] of this.items.entries()) {
            const slotname = this.getSlotName(index);
            this.createSlot(slotname);
            const component = this.___(ComponentFactory).create(this.itemCtor);
            component['parentList'] = this;                                        //////////////////
            //component.setParent(this);                                             //////////////////
            this.mount(slotname, component);
            component._onrenderstart();
            slotsHTML.push(`<< ${slotname} | ${JSON.stringify(item)} >>`);
        }
        this.setState('slots', slotsHTML.join(''));
    }

    getItem(predicate: ((item: any) => boolean)): WTK_COMPONENT_LOGIC | null | undefined {
        const index = this.items.findIndex(predicate);
        return this.getSlot(this.getSlotName(index));
    }

    getItems(): WTK_COMPONENT_LOGIC[] {
        return Array.from(this._slots.values()) as WTK_COMPONENT_LOGIC[];
    }

    private get itemCtor() {
        return ASSERT(this._itemCtor, `[WTK_LIST_LOGIC] item ctor undefined`)
    }

    getSlotName(index: number) {
        return `${LIST_ITEM}-${index}`
    }
}


