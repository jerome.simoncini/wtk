import { Constructor } from "./Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { ICommandSpec } from "./services/script/Command.js";
import { WTK_COMMAND_HANDLER_LOGIC } from "./services/script/CommandHandler.js";


export interface ICommandHandler extends IAppObject{
    cmds: ICommandSpec[];
}


export const WTK_GEN_COMMAND_HANDLER = (interpreter: Constructor<WTK_COMMAND_HANDLER_LOGIC>, settings: ICommandHandler): Constructor<WTK_COMMAND_HANDLER_LOGIC> => {
    const skull = __skull__(interpreter, {
        onCreate: _create_cmd_handler_,
        onInit: _init_cmd_handler_,
        settings: settings
    }
    ) as Constructor<WTK_COMMAND_HANDLER_LOGIC>;
    return WTK_GEN_OBJECT(skull, settings) as Constructor<WTK_COMMAND_HANDLER_LOGIC>
}

const _create_cmd_handler_ = (hdl: WTK_COMMAND_HANDLER_LOGIC, settings: ICommandHandler) => {
    hdl.registerCommands(settings.cmds);

}

const _init_cmd_handler_ = async (hdl: WTK_COMMAND_HANDLER_LOGIC, settings: ICommandHandler) => {

}

