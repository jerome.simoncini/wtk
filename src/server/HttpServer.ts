import { AppObject } from "../AppObject.js";
import { PWA, ServerPage } from "./ServerPage.js";
import { Constructor } from "../Constructor.js";
import { ASSERT, IURLContent, URLParser } from "../utils.js";

interface ICertificates {
    certFile: string;
    keyFile: string;
}

interface IServerResourcesPaths {
    extensions: string[];
    paths: string[];
    contentType: string;
}

export interface IServerRoute {
    path: string;
    target?: Constructor<ServerPage>;
    redirectTo?: string;
    ssr?: boolean;
}
export class IServerRoute { }

type ServerRoutes = Map<string, ServerPage | string>; //

export interface IServerConfig {
    ports: {
        http?: number;
        https?: number;
    }
    certificates?: ICertificates,
    pwa?: PWA,
    resources?: IServerResourcesPaths[],
    router: {
        http?: IServerRoute[]
        https?: IServerRoute[]
    }
}

interface IResponseInfo {
    paths: string[];
    options: any;  //
}

export class WTK_HTTP_SERVER_LOGIC extends AppObject {
    private _config: IServerConfig | undefined = undefined;
    private _http: ServerRoutes = new Map();
    private _https: ServerRoutes = new Map();
    private _cache: Map<string,string> = new Map();

    init(config: IServerConfig) {
        this._config = config;

        if (config.router.http) {
            this._initRoutes(config.router.http, this._http);
        }
        if (config.router.https) {
            this._initRoutes(config.router.https, this._https);
        }
    }

    run() {
        if (this.http.size > 0) {  //
            if (this.config.ports.http == undefined) {   //
                throw `(${this.constructor.name}) http port undefined`;
            }
            this.startHttpService();
        }

        if (this.https.size > 0) {  //
            if (this.config.ports.https == undefined) {   //
                throw `(${this.constructor.name}) https port undefined`;
            }
            this.startHttpsService();
        }
    }

    serverError(status: number, message: string | null = null): Response {
        return new Response(message, { status });
    }

    async onHttpRequest(request: Request) {
        return this.dispatch(request, this.http);
    }

    async onHttpsRequest(request: Request) {
        return this.dispatch(request, this.https);
    }

    getResponseInfo(requestUrl: string): IResponseInfo | undefined {
        const config = ASSERT(this.config.resources, 'resources config undefined');
        let responseInfo: IResponseInfo | undefined = undefined;
        for (const resourceCfg of config) {
            for (const ext of resourceCfg.extensions) {
                if (requestUrl.endsWith(ext)) {
                    return {
                        paths: resourceCfg.paths,
                        options: {
                            headers: {
                                "content-type": resourceCfg.contentType,
                            },
                        }
                    }
                }
            }
        }
        return responseInfo;
    }

    private async dispatch(request: Request, routes: ServerRoutes): Promise<Response> {   //
        console.error(`[GET] ${request.url}`)
        
        if(this._cache.has(request.url)) {

            console.error(`[HIT] ${request.url}`)
            return new Response(
                ASSERT(this._cache.get(request.url), `html response undefined [url=${request.url}]`),
                {
                    headers: {
                        "content-type": "text/html; charset=utf-8",
                    },
                },
            );
        }
        else {
            let url = request.url;
    
            if (url.endsWith('/')) {
                url = url.slice(0, -1);
            }
    
            const setup: IURLContent = URLParser.parse(url);
    
            let responseInfo: IResponseInfo | undefined = this.getResponseInfo(setup.origin);
    
            if (responseInfo) {
                const options = responseInfo.options;
                let response: Response | null = null;
                let i = 0;
                while (!response && i < responseInfo.paths.length) {
                    const path = responseInfo.paths[i++];
                    try {
                        response = await this.readFile(path + setup.origin).then(data => {
                            return data ? new Response(data, options) : null;
                        })
                    }
                    catch (error) { console.log(`[FILE NOT FOUND] ${path + setup.origin}`) }
                }
                return response ?? this.serverError(417);
            }
            else if (setup.origin.includes('.')) {
                console.error(`file not found: '${setup.path}${setup.origin}'`)
                return this.serverError(417);
            }
            else {
                //console.log(`[DISPATCH] rootPath=${setup.rootPath} origin=${setup.origin}`)
                const routeMatch = Array.from(routes.keys()).find(route => route == setup.path) || '*';   //
               // console.log(`${setup.host} <-> ${setup.path} <-> ${routeMatch}`)
                if (routeMatch != '*') {
                    return this.buildResponse(setup, routes);  //
                }
                else if (routes.has('*')) {
                    return await this.buildResponse({
                        protocol: setup.protocol,
                        host: setup.host,
                        port: setup.port,
                        path: '*',
                        origin: '/'
                    }, routes);
                }
                else {
                    return this.serverError(417);
                }
            }
        }
    }

    private async buildResponse(url: IURLContent, routes: ServerRoutes): Promise<Response> {
        const target = ASSERT(routes.get(url.path), `unkwown route '${url.path}'`);
        const root = `${url.protocol}://${url.host}:${url.port}`;

        if (target instanceof ServerPage) {
            target.setOrigin(url.origin);
            if (this.config.pwa) {
                target.setPWA(this.config.pwa);
            }
            await target.build(root);
            console.error(`[BUILD] ${url.url}`)
            if(url.url) {
                this._cache.set(url.url, target.html())
                console.error(`[CACHE] ${url.url}`)
            }
            return new Response(
                target.html(),
                {
                    headers: {
                        "content-type": "text/html; charset=utf-8",
                    },
                },
            );
        }
        else if (typeof target == 'string') {
            return Response.redirect(`${root}${target}`);
        }
        return this.serverError(417);
    }

    private _initRoutes(routes: IServerRoute[], target: ServerRoutes) {
        for (const route of routes) {
            if (route.target) {
                const page: ServerPage = this._localStore.get(route.target);
                page.onCreate();
                target.set(route.path, page);
            }
            else if (route.path == '*' && route.redirectTo) {
                target.set(route.path, route.redirectTo);
            }
            else {
                //notify error
            }
        }
    }

    get config() {
        return ASSERT(this._config, `${this.constructor.name} config undefined`);
    }

    get http() {
        return this._http;
    }

    get https() {
        return this._https;
    }

    startHttpService(): void {
        this._startHttpService();
    }

    startHttpsService(): void {
        this._startHttpsService();
    }

    readFile(resourcePath: string): Promise<BodyInit | null> {
        return this._readFile(resourcePath);
    }

    _startHttpService = () => { };
    _startHttpsService = () => { };
    _readFile = (url: string): Promise<BodyInit | null> => { return new Promise(resolve => null) };
}