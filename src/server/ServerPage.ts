import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { Constructor } from "../Constructor.js";
import { ASSERT, Mask } from "../utils.js";
import { AppObject } from "../AppObject.js";
import { LOGGER } from "../Logger.js";
import { WTK_APP_ROOT, WTK_SERVICE_WORKER_SCOPE, WTK_SERVICE_WORKER_SCRIPT, WTK_SSR } from "./settings.js";
import { IPageModule } from "../_gen_index.js";
import { ObjectStore } from "../ObjectStore.js";

const THIS = "Server Page";
const DEBUG = LOGGER(THIS);
DEBUG.on();


enum ServerPageResource {
    META,
    CSS,
    SCRIPTS
}

interface ServerPageMeta {
    name: string;
    content: string;
};

enum ServerPageFlag {
    INITIALIZED,
    SSR,
    ERROR
}

interface PWA {
    script: string;
    scope: string;
    manifest: string;
}

class ServerPage extends AppObject {
    private _root: string;
    private _rootPath: string;
    private _origin: string;
    private _pModule: IPageModule;
    private _flags: Mask = new Mask();
    private _html: string;
    private _resources: Map<ServerPageResource, string> = new Map();
    private _metas: ServerPageMeta[] = [];
    private _pwa: PWA | undefined = undefined;
    private _ssrCtor: Constructor<WTK_COMPONENT_LOGIC> | undefined = undefined;
    private _cache: boolean = false;

    setRoot(tagname: string) {
        this._root = tagname;
    }

    setRootPath(path: string) {
        this._rootPath = path;
    }

    setOrigin(origin: string) {
        this._origin = origin;
    }

    setPWA(pwa: PWA) {
        this._pwa = pwa;
    }

    setModule(module: IPageModule) {
        this._pModule = module;
    }

    setSSR(ctor: Constructor<WTK_COMPONENT_LOGIC> | undefined) {
        if (ctor) {
            this._flags.setBits(ServerPageFlag.SSR);
            this._ssrCtor = ctor;
        }
        else {
            this._flags.unset(ServerPageFlag.SSR);
        }
    }

    setMetas(metas: ServerPageMeta[]) {
        this._metas = metas;
    }

    importCSS(paths: string[]) {     //
        let imports: string[] = []; 
        for (const path of paths) {
            imports.push(`<link rel="stylesheet" href="${path}">`);
        }
        this._resources.set(ServerPageResource.CSS, imports.join(""));
    }

    importScripts(paths: string[]) {    //
        let imports: string[] = [];
        for (const path of paths) {
            imports.push(`<script src="${path}"></script>`);
        }
        this._resources.set(ServerPageResource.SCRIPTS, imports.join(""));
    }

    cache() {
        this._cache = true;
    }

    async render(): Promise<string> {
        const store = new ObjectStore();
        const module = store.get(ASSERT(this._ssrCtor, `ssr ctor undefined`)) as WTK_COMPONENT_LOGIC;
        //console.log(`MODULE_ID=${module._objectId}`);
        await module.onInit();   //
        await module.initSSR(this.metas);
        const content = await module.renderStatic(this.metas);
        return `<${this._root} class=${module.styles}>${content}</${this._root}>`;
    }

    async build(staticJS: string) {    ///
        if (!this._root) {
            throw new Error(`[${this.constructor.name}] (build) root tag undefined`);
        }

        this._metas = [];
        this._metas.push({ name: WTK_APP_ROOT, content: this._root });
        this._metas.push({ name: 'path', content: this._rootPath });
        this._metas.push({ name: 'origin', content: this._origin });

        let metasHTML: string[] = [];
        for (const meta of this._metas) {
            metasHTML.push(`<meta name=${meta.name} content=${meta.content}>`);
        }
        this._resources.set(ServerPageResource.META, metasHTML.join(""));


        let content: string = '';

        if (this._flags.has(ServerPageFlag.SSR)) {
            content = await this.render();
        }
        else {

            if (!this._pModule.path) {
                throw new Error(`[] (build) module path undefined`);
            }

            content = `<${this._root}></${this._root}>`;
        }

        const metas = this._resources.get(ServerPageResource.META) || '';
        const css = this._resources.get(ServerPageResource.CSS) || '';
        const scripts = this._resources.get(ServerPageResource.SCRIPTS) || '';
        const ssr = `<meta name="${WTK_SSR}" content=${this._flags.has(ServerPageFlag.SSR)}>`;

        const pwa = this._pwa ? `<meta name="${WTK_SERVICE_WORKER_SCRIPT}" content="${this._pwa?.script}"> 
                                 <meta name="${WTK_SERVICE_WORKER_SCOPE}" content="${this._pwa?.scope}"> 
                                 <link rel="manifest" href="${this._pwa?.manifest}">` : '';

        this._html = `<!DOCTYPE html>
                        <html>
                         <head>
                           ${metas}
                           ${ssr}
                           ${pwa}
                           ${css}
                         </head>
                         <body>
                           ${content}
                           ${scripts}
                           <script>
                           document.onkeydown = function (t) {
                            if(t.which == 9){
                             return false;
                            }
                           }
                           </script>
                           <script type="module">
                              ${this._gen_launcher(this._pModule, staticJS)}
                           </script>
                         </body>
                      </html>`;
    }

    private _gen_launcher(module: IPageModule, staticJS: string) {
        return `
        import { ObjectStore } from '${staticJS}/wtk/src/ObjectStore.js';
        import { ${module.name} } from '${staticJS}/${module.path}';
        
        (async () => {
            try {
                const store = new ObjectStore();
                const module = store.get(${module.name});
                module.load().then(module=>module.start())
            }
            catch (e) {
                console.error(e);
            }
        })()`
    }

    html = () => {
        return this._html;
    }

    get metas() {
        return this._metas;
    }

    get origin() {
        return this._origin || '';
    }
}

export { ServerPage, ServerPageResource, ServerPageMeta, PWA };