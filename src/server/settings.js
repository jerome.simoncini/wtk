export const SERVER_NAME = "localhost";
export const HTTP_PORT = 8000;
export const HTTPS_PORT = 8443;

export const WTK_SERVICE_WORKER_SCRIPT = "sw_script";
export const WTK_SERVICE_WORKER_SCOPE = "sw_scope";
export const WTK_APP_ROOT = "root";
export const WTK_ASSET_ROOT = "asset-root";
export const WTK_SSR = "ssr";