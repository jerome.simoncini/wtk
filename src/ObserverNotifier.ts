import { AppEntity } from "./AppEntity.js";
import { AppEvent } from "./AppEvent.js";
import { __skull__ } from "./_skull.js";

//const DEBUG = LOGGER("Notifier");
//DEBUG.off();

export interface IObserverNotifierNames {
    event: number;
    names: [];
}


export abstract class ObserverNotifier extends AppEntity {

    protected _observers: Map<number, Set<ObserverNotifier>> = new Map();

    addObserver(observer: ObserverNotifier, ...eventCodes: number[]) {
        if (!this._observers) {
            this._observers = new Map();
        }
        const codes: number[] = [...eventCodes];
        codes.forEach((code: number) => {
            if (!this._observers.has(code)) {
                this._observers.set(code, new Set<ObserverNotifier>());
            }
            this._observers.get(code)!.add(observer);
        });
    }

    removeObserver(observer: ObserverNotifier, ...eventCodes: number[]) {
        const codes: number[] = [...eventCodes];
        codes.forEach((code: number) => {
            let observers = this._observers?.get(code);   //
            if (observers) {
                observers.delete(observer);
                // this._observers.set(code,observers.filter(obs=>obs!=observer));
            }
        });
    }

    _notify(event: AppEvent) {
        //DEBUG.log("notify");
        //DEBUG.log(`${event.code}`);
        this._observers.get(event.code)?.forEach(observer => {
            //DEBUG.log(observer.constructor.name);
            observer.onNotify(event);
        });
    }

    abstract onNotify(update: AppEvent): void;

    get observers(): IObserverNotifierNames[] {
        return [...this._observers].map(o => {
            return {
                event: o[0],
                names: [...o[1]].map((s: ObserverNotifier) => s.__name__ != '' ? s.__name__ : s.inode)     //
            } as IObserverNotifierNames;
        });
    }
}