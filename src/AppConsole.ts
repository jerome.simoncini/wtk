import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { WTK_LIST_LOGIC } from "./AppList.js";
import { WTK_APP_TREE_LOGIC } from "./AppTree.js";
import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { ICommand, WtkSystemCommand } from "./services/script/Command.js";
import { WTK_COMMAND_HANDLER_LOGIC } from "./services/script/CommandHandler.js";
import { WTK_INTERPRETER_LOGIC } from "./services/script/Interpreter.js";
import { ASSERT, MESSAGE, Mask } from "./utils.js";

export const WTK_CONSOLE_LINES = 'console-lines';

export enum ConsoleLineFlag {
    BOLD,
    ITALIC
}

export enum ConsoleLineColor {
    WHITE = 'antiquewhite',
    GREEN = 'green',
    BLUE = 'blue',
    YELLOW = 'yellow',
    RED = 'red'
}

export enum WtkConsoleFormat {
    MESSAGE,
    ERROR,
    WARNING,
    INFO,
    DIRECTORY,
    FILE,
    SYMLINK
}

export enum WtkConsolePrompt {
    USER = '$',
    ROOT = '#',
    INVITE = '~',
    PUBLIC = '\\'
}

export interface IConsoleLineCell {
    content: string;
    flags?: ConsoleLineFlag[];
    color?: ConsoleLineColor;
}

export interface IConsoleLine {
    prompt: string;
    cmd: IConsoleLineCell[];
    active: boolean;
}

export class WTK_CONSOLE_LINE_ITEM extends WTK_COMPONENT_LOGIC {
    prompt: string;
    cmd: IConsoleLineCell[];
    active: boolean;
    format: WtkConsoleFormat | undefined;      //
}

const DEFAULT_APP_NAME = 'console';

export class WTK_CONSOLE_LOGIC extends WTK_COMPONENT_LOGIC {
    private _appName: string = DEFAULT_APP_NAME;
    private _lines: IConsoleLine[] = [];
    private _cursor: string = '';
    private _waiting: boolean = false;
    private _focus: boolean = false;
    private _timer: number | undefined = undefined;
    private _cursorSpeed: number = 1000;
    private _interpreter: WTK_INTERPRETER_LOGIC | undefined = undefined;
    private _commandHandler: WTK_COMMAND_HANDLER_LOGIC | undefined = undefined;
    private _appTree: WTK_APP_TREE_LOGIC | undefined = undefined;
    private _history: string[] = [];
    private _hCursor: number = 0;
    private _username: string = '';
    private _env: string | undefined = undefined;
    private _prompt: string | undefined = undefined;
    private _autocompleteTimestamp: number = 0;

    setAppName(appName: string) {
        this._appName = appName;
    }

    setAppTree(appTree: WTK_APP_TREE_LOGIC) {
        this._appTree = appTree;
    }

    setUsername(username: string) {
        this._username = username;
    }

    setEnv(env: string) {
        this._env = env;
    }

    clearEnv() {
        this._env = undefined;
    }

    setPrompt(prompt: WtkConsolePrompt) {
        this._prompt = prompt;
    }

    setInterpreter(interpreter: Constructor<WTK_INTERPRETER_LOGIC>) {
        this._interpreter = this.getObjectStore(Scope.LOCAL).get(interpreter);
    }

    setCommandHandler(handler: Constructor<WTK_COMMAND_HANDLER_LOGIC>) {
        this._commandHandler = this.getObjectStore(Scope.GLOBAL).get(handler);               //
    }

    setSlowness(speed: number) {
        this._cursorSpeed = speed;
    }

    reset() {
        this.onReset();

        this.newLine().then(() => {
            this.stop();                     //
            this.getSlot(WTK_CONSOLE_LINES)?.render({ items: this._lines });
        });
    }

    start() {
        this._waiting = true;
        this._timer = setInterval(() => this.refresh(), this._cursorSpeed);
    }

    stop() {
        clearInterval(this._timer);
        this._timer = undefined;
        this._waiting = false;

        /////////////
        const l = this._lines.find(line => line.active);
        const cl = this._cursor.length;
        if (l && cl > 0) {
            l.cmd[0].content = l.cmd[0].content.slice(0, l.cmd[0].content.length - cl);
            this._cursor = '';
        }
    }

    setFocus(enable: boolean) {
        if (enable && !this._focus) {
            this.start();
        }
        else if (this._focus) {
            this.stop();
        }
        this._focus = enable;
    }

    private refresh() {
        if (this._waiting) {
            const cl = this._cursor.length;
            this._cursor = cl > 0 ? '' : '[]';

            const l = this._lines.find(line => line.active);
            if (l) {
                l.cmd[0].content = l.cmd[0].content.slice(0, l.cmd[0].content.length - cl) + this._cursor;

                const list = this.getSlot(WTK_CONSOLE_LINES) as WTK_LIST_LOGIC;
                const line = list?.getItem((item: WTK_CONSOLE_LINE_ITEM) => item.active == true)?.getElement('cmd');
                if (line) {
                    line.innerText = l.cmd[0].content;
                }
            }
        }
        else {
            console.warn('waiting');
        }
    }

    async handle(key: string) {
        //console.warn(MESSAGE(this, key));
        const l = this._lines.find(line => line.active);

        if (l && this._waiting) {

            const cl = this._cursor.length;

            if (cl > 0) {
                l.cmd[0].content = l.cmd[0].content.slice(0, l.cmd[0].content.length - cl);
                this._cursor = '';
            }

            switch (key) {

                case "Enter":
                    await this.process(l.cmd[0].content).then(() => {
                        if (l.cmd[0].content != "") {                          //
                            this._history.push(l.cmd[0].content);
                            this._hCursor = this._history.length;
                        }
                        this.updateLines(l);
                    });
                    break;

                case "Backspace":
                    l.cmd[0].content = l.cmd[0].content.slice(0, l.cmd[0].content.length - 1);
                    break;

                case "Alt":
                case "AltGraph":
                case "ArrowLeft":
                case "ArrowRight":
                case "Control":
                case "Shift":
                    break;

                case "ArrowUp":
                    if (this._history.length && this._hCursor > 0) {
                        this._hCursor--;
                        l.cmd[0].content = this._history[this._hCursor];
                        this.updateCurrentLine(l.cmd[0].content);
                    }
                    break;

                case "ArrowDown":
                    if (this._history.length && this._hCursor < this._history.length - 1) {
                        this._hCursor++;
                        l.cmd[0].content = this._history[this._hCursor];
                        this.updateCurrentLine(l.cmd[0].content);
                    }
                    else {
                        l.cmd[0].content = '';
                        this.updateCurrentLine(l.cmd[0].content);
                    }
                    break;


                case "Tab":
                    await this.autocomplete(l);
                    break;

                default:
                    l.cmd[0].content += key;

                    this.updateCurrentLine(l.cmd[0].content);

                    break;
            }
        }
    }

    private updateLines(nl: IConsoleLine | undefined = undefined) {
        const cl = this.getSlot(WTK_CONSOLE_LINES);
        if (cl) {
            this.stop();
            cl.render({ items: this._lines });
            if (this._focus) {
                this.start();
            }
        }
    }

    protected async autocomplete(line: IConsoleLine): Promise<void> {                            //

        await this.commandHandler.onAutocomplete(line, this.interpreter);

        const c = line.cmd[0].content.split(' ');

        if (this.interpreter.cmds.includes(c[0])) {
            const names = this.appTree.get(this.appTree.current, Scope.LIST)
                .map(i => this.appTree.get(i, Scope.NAME))
                .filter((n: string) => n.startsWith(c[1]));
            console.warn(names);
            if (names.length == 1) {
                c[1] = names[0];
                line.cmd[0].content = c.join(' ');
                this.updateCurrentLine(line.cmd[0].content);
            }
            else {
                const t = Date.now();
                const d = t - this._autocompleteTimestamp;
                if (d < 1000) {
                    console.warn(`format: ${names.join(' ')}`)
                    await this.format(WtkConsoleFormat.MESSAGE, [names.join(' ')])
                        .then(() => this.newLine(undefined, false, c.join(' ')))           //
                        .then(() => this.updateLines());
                    this._autocompleteTimestamp = 0;
                }
                else {
                    this._autocompleteTimestamp = t;
                }
            }

        }

    }


    private updateCurrentLine(cmd: string) {
        const list = this.getSlot(WTK_CONSOLE_LINES) as WTK_LIST_LOGIC;
        const line = list?.getItem((item: WTK_CONSOLE_LINE_ITEM) => item.active == true)?.getElement('cmd');
        if (line) {
            line.innerText = cmd;
        }
    }

    private async process(cmd: string): Promise<void> {
        await this.exec(cmd).then(() => this.newLine());
    }

    async exec(cmd: string): Promise<void> {                    //////////

        if (cmd != '') {

            await this.interpreter.exec(cmd)

                .then(async (response: ICommand[]) => {

                    for (const cmd of response) {

                        switch (cmd.id) {

                            case WtkSystemCommand.PRINT:
                                {
                                    const message = cmd.args.length ? cmd.args[0] : ''
                                    await this.format(WtkConsoleFormat.MESSAGE, [message]);
                                }
                                break;

                            case WtkSystemCommand.CLEAR:
                                {
                                    await this.clearLines();
                                }
                                break;

                            default:
                                console.warn(cmd.id);
                                await this.commandHandler.handle(cmd).then(response => {
                                    if (response.table) {
                                        console.warn(response.table);
                                        for (const line of response.table) {

                                            this.format(line.format, line.content)    //
                                        }
                                    }
                                });
                        }
                    }
                });
        }
    }

    private async format(level: WtkConsoleFormat, message: string[]) {
        switch (level) {
            case WtkConsoleFormat.DIRECTORY:
                {
                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => i < message.length - 1 ?
                            { content: m, flags: [], color: ConsoleLineColor.WHITE } :
                            { content: m, flags: [ConsoleLineFlag.BOLD], color: ConsoleLineColor.BLUE });

                    await this.newLine(line);
                }
                break;
            case WtkConsoleFormat.FILE:
                {
                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => {
                            return { content: m, flags: [], color: ConsoleLineColor.WHITE }
                        });
                    await this.newLine(line);
                }
                break;
            case WtkConsoleFormat.SYMLINK:
                {
                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => {
                            return { content: m, flags: [], color: ConsoleLineColor.YELLOW }
                        });
                    await this.newLine(line);
                }
                break;
            case WtkConsoleFormat.MESSAGE:
                {
                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => {
                            return { content: m, flags: [], color: ConsoleLineColor.WHITE }
                        });
                    await this.newLine(line);
                }
                break;
            case WtkConsoleFormat.ERROR:
                {

                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => {
                            return { content: m, flags: [ConsoleLineFlag.BOLD], color: ConsoleLineColor.RED }
                        });
                    await this.newLine(line);
                }
                break;
            case WtkConsoleFormat.WARNING:
                {
                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => {
                            return { content: m, flags: [ConsoleLineFlag.BOLD], color: ConsoleLineColor.YELLOW }
                        });
                    await this.newLine(line);
                }
                break;
            case WtkConsoleFormat.INFO:
                {
                    const line: IConsoleLineCell[] = message.map(
                        (m, i) => {
                            return { content: m, flags: [ConsoleLineFlag.BOLD], color: ConsoleLineColor.GREEN }
                        });
                    await this.newLine(line);
                }
                break;
            default:
                const line: IConsoleLineCell[] = message.map(
                    (m, i) => {
                        return { content: `invalid format '${level}'`, flags: [], color: ConsoleLineColor.WHITE }
                    });
                await this.newLine(line);
                break;
        }
    }

    private async clearLines(): Promise<void> {
        this._lines = [];
    }


    /******************************************************************************/


    async newLine(
        line: IConsoleLineCell[] | undefined = undefined,
        force = false,
        nl: string = ""
    ): Promise<void> {
        this._waiting = false;
        const l = this._lines.find(line => line.active);
        if (l || this._lines.length == 0) {

            console.warn(line);

            const n = line ? {
                prompt: '',
                active: force,
                cmd: line.map(nl => {
                    return {
                        content: nl.content,
                        flags: nl.flags,            //
                        color: nl.color
                    }
                })
            } :
                {
                    prompt: this.buildPrompt(),     //////////////////////
                    cmd: [{
                        content: nl
                    }],
                    active: true,
                };

            console.warn(n);

            this._lines.push(n);

            console.warn(this._lines);

            if (!line && l) {
                l.active = false;
            }
        }
        else {
            // notify error
        }
        this._waiting = true;
    }

  
    private buildPrompt(): string {    /////////

        let prompt = this.env ? `<span style="color: goldenrod; font-style: italic">(${this.env})</span><span style="white-space: pre"> </span>` : '';
      
        prompt += `<span>${this.username}</span>`
        prompt += `<span>@\\${this._appName}</span>`;
        prompt += `<span style="color: antiquewhite">:</span>`;
        prompt += `<span style="color: blue">${this.path.replace('@','@\\')}</span>`;
        prompt += `<span style="color: antiquewhite; font-weight: bold;">${this._prompt || WtkConsolePrompt.USER}</span>`;      ////
        prompt += `<span style="white-space: pre"> </span>`;

        return prompt;
    }

    get interpreter(): WTK_INTERPRETER_LOGIC {
        return ASSERT(this._interpreter, MESSAGE(this, `interpreter undefined`));
    }

    get commandHandler(): WTK_COMMAND_HANDLER_LOGIC {
        return ASSERT(this._commandHandler, MESSAGE(this, `commandHandler undefined`));
    }
    get username(): string {
        return this._username;
    }

    get path(): string {                                                       ///////////////////////
        return this.appTree.getPath(this.appTree.current);
    }

    get appName(): string {
        return ASSERT(this._appName, MESSAGE(this, `app name undefined`));
    }

    get env(): string {
        return this._env || '';
    }

    get appTree(): WTK_APP_TREE_LOGIC {
        return ASSERT(this._appTree, MESSAGE(this, `app tree undefined`));
    }

    get lines(): IConsoleLine[] {
        return this._lines || [];
    }

    get active(): boolean {
        return this._timer != undefined;
    }

    onReset() {
        this._onReset(this)
    }

    _onReset(console: WTK_CONSOLE_LOGIC): void { }
}
