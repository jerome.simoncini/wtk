import { AppObject } from "./AppObject.js";
import { IStateUpdate } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { AppStateEvent, System } from "./events.js";
import { __skull__ } from "./_skull.js";
import { MemoryPool } from "./MemoryPool.js";
import { Scope } from "./Scope.js";

const TYPE = "AppObject";

export interface IListener {
    event: number;
    action: number;
    cmd: (_: AppObject, params: any) => void;
};

export interface IMemory {
    name: string;
    ctor: Constructor<MemoryPool>;
    scope: Scope,
    notifier?: Scope,
};

export interface IExtension {
    slot: string;
    cmd: (object: AppObject) => void;
}

export interface IAppEntity {
    name?: string;
    jsonIgnore?: string[];
    echo?: boolean;
}

export interface IAppObject extends IAppEntity {
    listen?: IListener[];
    memory?: IMemory[];
    extensions?: IExtension[];
    onCreate?: (object: AppObject) => void;
    onDestroy?: (object: AppObject, ...args: any[]) => void;
    onInit?: (object: AppObject) => Promise<void>;
    onUpdate?: (object: AppObject, ...args: any[]) => void;
    onStateUpdate?: (object: AppObject, update: IStateUpdate) => void;
    createLocalStore?: boolean;                                           ///////////////////////////
};

export const WTK_GEN_OBJECT = <T extends AppObject>(object: Constructor<T>, settings?: IAppObject): Constructor<T> => {
    return __skull__(object, {
        onCreate: _create_object_,
        onInit: _init_object_,
        settings: settings
    }) as Constructor<T>;
};

const _create_object_ = (object: AppObject, settings?: IAppObject): void => {
    //console.warn(`CREATE ${settings?.name}`);
    object.setObjectType(TYPE);

    object.extensions.createSlots([
        'beforeOnInit', 'afterOnInit',
        'beforeOnCreate', 'afterOnCreate',
        'beforeOnDestroy', 'afterOnDestroy',
        'beforeOnUpdate', 'afterOnUpdate',
        'beforeOnStateUpdate', 'afterOnStateUpdate',
    ]);

    //if (settings?.createLocalStore) {              //////////////////////////////
        object.createLocalStore();
    //}
    if (settings?.listen) {  //
        for (const listener of settings.listen) {
            object.register(listener.event, listener.action, (params) => listener.cmd(object, params));
        }
    }
    if (settings?.memory) {  //
        for (const memory of settings.memory) {
            const notifierScope = memory.notifier ?? memory.scope;    ////////////
            const notifier = object.getObjectStore(notifierScope);
            object.setMemoryPool(memory.name, object.getObjectStore(memory.scope).get(memory.ctor, notifier));
            console.warn(object.inode);
        }
    }

    if (settings?.onCreate) {
        object._onCreate = settings.onCreate;
    }
    if (settings?.onDestroy) {
        object._onDestroy = settings.onDestroy;
    }
    if (settings?.onInit) {
        object._onInit = settings.onInit;
    }
    if (settings?.onUpdate) {
        object._onUpdate = settings.onUpdate;
    }
    if (settings?.onStateUpdate) {                            ////////////////////
        //object.addObserverActions([System.STATE_UPDATE]);
        object.register(System.STATE_UPDATE, AppStateEvent.UPDATE, (update) => object.onStateUpdate(update));
        object._onStateUpdate = settings.onStateUpdate;
    }
    if (settings?.name) {
        object.setObjectName(settings.name);
    }


    if (settings?.extensions) {
       // console.error(settings.extensions);
        for (const extension of settings.extensions) {
          //  console.warn(`bind extension '${extension.slot}`);
            if (!object.extensions.add(extension.slot, extension.cmd)) {
                //  console.error(MESSAGE(object, `extension slot not found '${extension.slot}'`));  //
            }
            else {
                //  console.warn(MESSAGE(object,`add extension slot '${extension.slot}'`));
            }
        }
    }
};

const _init_object_ = async (object: AppObject, settings?: IAppObject) => { };