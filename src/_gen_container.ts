import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { Constructor } from "./Constructor.js";
import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { __skull__ } from "./_skull.js";
import { _HTML_ } from "./Compiler.js";
import { WTK_CONTAINER_ID, WTK_CONTAINER_LOGIC } from "./AppContainer.js";

export interface IAppContainerContent {
    id: WTK_CONTAINER_ID,   
    ctor: Constructor<WTK_COMPONENT_LOGIC>
}

export interface IAppContainer extends IAppComponent {
    content?: IAppContainerContent[];
    tagname?: string;                     /////////
}

export const WTK_GEN_CONTAINER = <T extends WTK_CONTAINER_LOGIC>(container: Constructor<T>, settings: IAppContainer): Constructor<WTK_CONTAINER_LOGIC> => {
    const skull = __skull__(container, {
        onCreate: create_container,
        onInit: init_container,
        settings: settings
    }) as Constructor<WTK_CONTAINER_LOGIC>;

    return WTK_GEN_COMPONENT(skull, {
        html: (_: WTK_CONTAINER_LOGIC) => {
            return _HTML_(`<< ${_.contentTagname} >>`, _);
        },
        ...settings,
        createLocalStore: true                      ////////
    }) as Constructor<WTK_CONTAINER_LOGIC>
}

const create_container = (container: WTK_CONTAINER_LOGIC, settings: IAppContainer): void => {
    if(settings.tagname) {
        container.setContentTagname(settings.tagname);
    }

    container.createSlot(container.contentTagname);

    if(settings.content) {
        for(const content of settings.content) {
            container.registerContent(content.ctor, content.id);
        }
    }

}

const init_container = async (container: WTK_CONTAINER_LOGIC, settings: IAppContainer) => {
    //container._localStore.setParent(container);
    //container._localStore.setAppModule(container.module);
}
