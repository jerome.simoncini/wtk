import { ComponentFactory, WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { NavbarPosition } from "./AppNavbar.js";
import { _HTML_ } from "./Compiler.js";
import { Constructor } from "./Constructor.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { __skull__ } from "./_skull.js";
import { INavbarItem, WTK_NAVBAR_LOGIC } from "./AppNavbar.js";
import { NavbarItem } from "./components/NavbarItem.js";
import { ASSERT, HtmlArray } from "./utils.js";

export interface INavbar extends IAppComponent {
    items: INavbarItem[],
    default?: string
};

export const WTK_GEN_NAVBAR = (navbar: Constructor<WTK_NAVBAR_LOGIC>, settings: INavbar): Constructor<WTK_NAVBAR_LOGIC> => {
    const skull = __skull__(navbar, {
        onCreate: _create_navbar_,
        onInit: _init_navbar_,
        settings: settings
    }) as Constructor<WTK_COMPONENT_LOGIC>
    return WTK_GEN_COMPONENT(skull, {
        html: (_: WTK_NAVBAR_LOGIC) => {
            return _HTML_(`<div style="display: flex; flex-direction: row; justify-content: space-between;">
                              <div style="text-align: left;">{{ safe | ${NavbarPosition.LEFT} }}</div>
                              <div style="flex: 1; text-align: center;">{{ safe | ${NavbarPosition.CENTER} }}</div>
                              <div style="text-align: right;">{{ safe | ${NavbarPosition.RIGHT} }}</div>
                           </div>`, _)
        },
        onprerender: async (_: WTK_NAVBAR_LOGIC) => {
            for (const slot of _.listSlots()) {
                _.destroySlot(slot)
            }

            const htmlMap: Map<NavbarPosition, HtmlArray> = new Map(Object.values(NavbarPosition).map(position => [position as NavbarPosition, new HtmlArray()]));

            const p = _.items.filter(item => !item.hidden)
                .map(async item => {
                    _.createSlot(item.id);
                    const c = _._localStore.get(ComponentFactory).create(item.component || NavbarItem)
                    return _.mount(item.id, c)
                        .then(() => {
                            c._onrenderstart()
                        })
                        .then(() => {

                            const props = JSON.stringify({
                                label: item.label,
                                selected: item.id == _.selected?.id
                            });

                            const html = _HTML_(`<< ${item.id} | ${props} >>`, _);

                            htmlMap.get(item.position)?.push(html)
                        })
                });

            await Promise.all(p);

            Object.values(NavbarPosition).forEach(position => {
                _.setState(position, htmlMap.get(position as NavbarPosition)?.get() || '');
            });

            _.setup(_.itemSetup.filter(setup => !(ASSERT(_.items.find(item => item.id == setup.name), ``)).hidden));
        }

        ,
        onInit: async (_: WTK_NAVBAR_LOGIC): Promise<void> => {
            _.createLocalStore();
        },
        ...settings
    }) as Constructor<WTK_NAVBAR_LOGIC>
};

const _create_navbar_ = (navbar: WTK_NAVBAR_LOGIC, settings: INavbar): void => {

    for (const item of settings.items) {
        navbar.addItem(item);
    }

    if (settings.default) {
        navbar.setDefault(settings.default);
    }

    navbar.setStylesheetProperty('navbar-item', 'wtk-navbar-item');
    navbar.setStylesheetProperty('navbar-item-selected', 'wtk-navbar-item-selected');
};

const _init_navbar_ = async (navbar: WTK_NAVBAR_LOGIC, settings: INavbar) => {
};