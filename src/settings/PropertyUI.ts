import { ModelPropertySpec } from "../ModelProps.js";
import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { AppNodeFlag } from "../AppComponent.js";
import { LOGGER } from "../Logger.js";
import { IPropsUI } from "./PropsUI.js";
import { MemRef, MemoryEvent, Ref } from "../MemoryPool.js";
import { System } from "../events.js";
import { WTK_MODEL_GROUP_UI_LOGIC } from "./ModelGroupUI.js";
import { ASSERT } from "../../src/utils.js";

const THIS = "Property UI";
const DEBUG = LOGGER(THIS);
DEBUG.on();

export const _uiPropertyINIT = async <P extends IPropsUI, V = string>(_: WTK_UI_PROPERTY_LOGIC<P, V>) => {
    _.addObserverActions([System.MEMORY_UPDATE])
    _.register(System.MEMORY_UPDATE, MemoryEvent.WRITE, (params) => _.onMemoryUpdate(params));
}

export const _uiPropertyPRERENDER = async <P extends IPropsUI, V = string>(_: WTK_UI_PROPERTY_LOGIC<P, V>) => {
    _.validate();
}

export const _uiPropertyPOSTRENDER = async <P extends IPropsUI, V = string>(_: WTK_UI_PROPERTY_LOGIC<P, V>) => {
    if (_.key) {
        _.__.push({
            name: _.key,
            sel: `${_.tagname} ${_.selector}`,
            actions: [{
                event: _.updateOn,
                cb: () => _.onPropertyUpdate()
            }],  //
            auto: true
        });
        _._loadSetup(); //
    }
    else {
        // notify error
    }
}

const DEFAULT_SELECTOR = "input";
const DEFAULT_UPDATE_EVENT = "input";

export class WTK_UI_PROPERTY_LOGIC<P extends IPropsUI, V = string> extends WTK_COMPONENT_LOGIC {
    private _key: string | undefined;
    private _ref: MemRef<string, any>;   //
    protected _updateFunc: (...args: any[]) => void = () => { };
    private _group: WTK_MODEL_GROUP_UI_LOGIC | undefined = undefined;
    private _property: ModelPropertySpec<P> | undefined = undefined;

    setProperty(property: ModelPropertySpec<P>) {
        this._property = property;
    }

    validate() {
        if (this._property == undefined) {
            this.nodeflags.setBits(AppNodeFlag.ERROR);
            DEBUG.error(`[${this.constructor.name}] property undefined`);  //
        }

        if (this.value == undefined) {
            this.nodeflags.setBits(AppNodeFlag.ERROR);
            DEBUG.error(`[${this.constructor.name}] value undefined`);  //
        }
    }

    onMemoryUpdate(params: any) {   //
        const poolId: number = params.poolId as number;
        const key: string = params.key as string;

        if (poolId && key) {
            if (this.ref.poolId == poolId && this.ref.key == key && this.ref.value != this.value) {
                this.setValue(this.ref.value);
                this.sync();
            }
            else if (this.hasRef(key, poolId)) {
                this.onRefUpdate(key);
            }
        }
    }

    onPropertyUpdate() {
        this._updateFunc(this.key, this.value);
    }

    hasRef(key: string, poolId: number): boolean {
        return false;
    }

    onRefUpdate(key: string): void { }

    initState(key: string, ref: MemRef<string, any>) {
        this._key = key;
        this._ref = ref;
        this.setState(key, this._ref.value);
    }

    onUpdate(fun: (...args: any[]) => void) {
        this._updateFunc = fun;
    }

    setGroup(group: WTK_MODEL_GROUP_UI_LOGIC) {
        this._group = group;
    }

    setValue(value: V) {
        this.setState(this.key, value);
    }

    async sync() {
        this._[this.key].value = this.value;
    }

    get selector(): string {
        return DEFAULT_SELECTOR;
    }

    get updateOn(): string {
        return DEFAULT_UPDATE_EVENT;
    }

    get group() {
        return this._group;
    }

    get value(): V | undefined {
        return this.$(this.key);
    }

    get refValue(): V | undefined {
        return this._ref.value;
    }

    get ref(): MemRef<string, any> {
        return ASSERT(this._ref, `(${this.constructor.name}) ref undefined`);
    }

    get key(): string {
        return ASSERT(this._key, `[${this.constructor.name}] key undefined`)
    }

    get property(): ModelPropertySpec<P> {
        return ASSERT(this._property, `[${this.constructor.name}] property undefined`);
    }
}