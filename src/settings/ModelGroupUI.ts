import { WTK_COMPONENT_LOGIC } from "../AppComponent.js";
import { _HTML_ } from "../Compiler.js";
import { WTK_UI_FACTORY_LOGIC } from "./UI.js";
import { IModelGroupItem, ModelType, PropsType } from "../ModelGroup.js";
import { ModelPropertySpec, ModelProps } from "../ModelProps.js";
import { ObjectStore } from "../ObjectStore.js";
import { IPropsUI } from "./PropsUI.js";
import { AppStateService, ModelGroupItemState } from "../../src/AppState.js";
import { ASSERT, MESSAGE } from "../../src/utils.js";
import { Constructor } from "../../src/Constructor.js";


export class WTK_MODEL_GROUP_UI_LOGIC extends WTK_COMPONENT_LOGIC {
    updateProperty(key: any, value: any): void {
        throw "not implemented"
    }
    getProperty(key: string): ModelPropertySpec<IPropsUI> {
        throw "not implemented"
    }
}

export const _modelSettingsINIT = async <P = PropsType, M = ModelType>(_: WTK_MODEL_GROUP_UI_SPEC_LOGIC<P, M>) => {
    _._initModel()
}

export const _modelSettingsPRERENDER = async <P = PropsType, M = ModelType>(_: WTK_MODEL_GROUP_UI_SPEC_LOGIC<P, M>) => {
    _._updateModel();
}

export const _modelSettingsHTML = (_: WTK_MODEL_GROUP_UI_LOGIC) => {
    return _HTML_(`<@ for property in settings: property @>`, _);
}

export class WTK_MODEL_GROUP_UI_SPEC_LOGIC<P = PropsType, M = ModelType> extends WTK_MODEL_GROUP_UI_LOGIC {
    protected type: M | undefined = undefined;
    protected props: ModelProps<P> | undefined = undefined;
    protected _uiFactoryCtor: Constructor<WTK_UI_FACTORY_LOGIC> | undefined = undefined;
    protected _uiFactory: WTK_UI_FACTORY_LOGIC | undefined = undefined;
    protected _stateService: AppStateService | undefined = undefined;

    setStateService<S extends AppStateService>(stateService: Constructor<S>) {
        this._stateService = this.___(stateService);
    }

    setUIFactoryCtor(uiFactoryCtor: Constructor<WTK_UI_FACTORY_LOGIC>) {
        this._uiFactoryCtor = uiFactoryCtor;
    }

    _initModel() {
        const modelItem = this.getModelItem();
        ASSERT(modelItem.props, `(${this.__name__}) model props undefined`);
        const uiStore = ASSERT(modelItem.props.getGlobalStore(), `(${this.__name__}) ui store undefined`);
        this.type = modelItem.type;
        this.props = modelItem.props;
        this._resetUIFactory(uiStore!);
        this.setState('settings', this.buildModelSettings());
    }

    _updateModel() {
        const modelType = this.getModelItem().type;
        if (this.type != modelType) {
            this._initModel();
        }
    }

    private _resetUIFactory(uiStore: ObjectStore) {
        if (this._uiFactory) {
            this._localStore.release(this.uiFactoryCtor);
        }
        this._uiFactory = this._localStore.get(this.uiFactoryCtor, uiStore) as WTK_UI_FACTORY_LOGIC;
    }

    buildModelSettings(): string[] {    //
        ASSERT(this.props, `[${this.__name__}] props undefined`);

        for (const slotname of this._slots.keys()) {  //
            this.umount(slotname);
            this.destroySlot(slotname);
        }

        const settingsHTML: string[] = [];

        for (const [propertyKey, property] of this.props!.data) {
            if (property && !property.data.hidden) {
                const input = ASSERT(this.uiFactory.createModelPropertyUI(property.type), `property ui undefined`);
                const slotname = propertyKey.replace(/\s/g, "");
                this.createSlot(slotname);
                this.mount(slotname, input);
                input.setProperty(property as ModelPropertySpec<IPropsUI>);
                input.initState(propertyKey, this.props!.getRef(propertyKey));     //
                input.setGroup(this);
                input.onUpdate((key: string, value: P) => {
                    this.updateProperty(key, value);
                });
                settingsHTML.push(`<< ${slotname} >>`);
            }
        }

        return settingsHTML;
    }

    updateProperty(key: string, value: P) {
        if (this.props && this.type) {
            const rollback = { type: this.type, props: this.props }
            this.props.set(key, value);
            this.props.onUpdate(key, this);
            this.onUpdate(rollback);
        }
        else {
            // notify error
        }
    }

    getModelItem(): IModelGroupItem<M> {
        return this.stateService.getState();
    }

    getProperty(key: string): ModelPropertySpec<IPropsUI> {
        return ASSERT(this.props?.data.get(key),`unknown property key '${key}'`);
    }

    private get uiFactory() {
        return ASSERT(this._uiFactory, `(${this.__name__}) ui factory undefined`)
    }

    private get uiFactoryCtor() {
        return ASSERT(this._uiFactoryCtor, `(${this.__name__}) ui factory ctor undefined`)
    }

    get stateService() {
        return ASSERT(this._stateService, `(${this.__name__}) state service undefined`)
    }
}