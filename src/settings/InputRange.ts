import { IPropsUI, RangeProperty } from "./PropsUI.js";
import { WTK_UI_PROPERTY_LOGIC, _uiPropertyINIT, _uiPropertyPOSTRENDER, _uiPropertyPRERENDER } from "./PropertyUI.js";
import { Ref, StaticRef } from "../MemoryPool.js";
import { ModelPropertySpec } from "../ModelProps.js";
import { WTK_GEN_COMPONENT } from "../_gen_component.js";
import { _HTML_ } from "../Compiler.js";

export const _inputRangeHTML = (_: WTK_UI_INPUT_RANGE_LOGIC) => {
    const min: number = (<Ref>_.property.data.min).value as number;
    const max: number = (<Ref>_.property.data.max).value as number;
    const step: number = (<Ref>_.property.data.step).value as number;
    const label: string = (_.property.data.label || _.key) as string;

    return _HTML_(`<div>
                      <div>${label}</div>
                      <input type="range" 
                             min=${min} max=${max} step=${step} 
                             value={{ safe | ${_.key} }}>
                   </div>`, _);
}

export class WTK_UI_INPUT_RANGE_LOGIC extends WTK_UI_PROPERTY_LOGIC<RangeProperty> {

    hasRef(key: string, poolId: number): boolean {
        const min = this.property.data.min as Ref;
        const max = this.property.data.max as Ref;
        const step = this.property.data.step as Ref;

        return (min.key == key && min.poolId == poolId) ||
            (max.key == key && max.poolId == poolId) ||
            (step.key == key && step.poolId == poolId);
    }

    onRefUpdate(key: string) {
        const minRef = <Ref>this.property.data.min as Ref;
        const maxRef = <Ref>this.property.data.max as Ref;
        const stepRef = <Ref>this.property.data.step as Ref;

        if (this.refValue) {
            const value = +this.refValue;
            switch (key) {
                case minRef.key: 
                    if (value < minRef.value) {
                        this.ref.setValue(minRef.value);
                    }
                    break;

                case maxRef.key: 
                    if (value > maxRef.value) {
                        this.ref.setValue(maxRef.value);
                    }
                    break;
            }
        }

        this.setValue(this.ref.value);
        this._[this.key].min = minRef.value;
        this._[this.key].max = maxRef.value;
        this._[this.key].step = stepRef.value;
        this._[this.key].value = this.value;
    }
}

export const WtkInputRange = WTK_GEN_COMPONENT(WTK_UI_INPUT_RANGE_LOGIC, {
    onCreate: _uiPropertyINIT,
    onprerender: _uiPropertyPRERENDER,
    onpostrender: _uiPropertyPOSTRENDER,
    html: _inputRangeHTML
});


export const _create_input_range_ = (prop: ModelPropertySpec<RangeProperty>, ui: IPropsUI): ModelPropertySpec<RangeProperty> => {
    const data = ui as RangeProperty;
    prop.create({
        name: data.name,
        type: data.type,
        label: data.label,
        default: data.default,
        hidden: data.hidden || false,
        min: new StaticRef(data.min),
        max: new StaticRef(data.max),
        step: new StaticRef(data.step),
    });
    return prop;
}