
enum InputType {
    NUMBER,
    COLOR,
    TEXT,
    CHECKBOX,
    RANGE,
    NONE
}

interface IPropsUI {  
    name: string;
    type?: number;
    label?: string;
    default: any;  //
    hidden?: boolean;
}

interface NumberProperty extends IPropsUI {
    min?: number;
    max?: number;
    step?: number;
}
class NumberProperty { };

interface ColorProperty extends IPropsUI {
}
class ColorProperty { };

interface TextProperty extends IPropsUI {
}
class TextProperty { };

interface NoneProperty extends IPropsUI {
    min?: number;
    max?: number;
    step?: number;
}
class NoneProperty { };

interface CheckboxProperty extends IPropsUI {
}
class CheckboxProperty { };

interface RangeProperty extends IPropsUI {
    min: number;
    max: number;
    step: number;
}
class RangeProperty { };


export { InputType };
export { IPropsUI, NumberProperty, ColorProperty, TextProperty, CheckboxProperty, RangeProperty, NoneProperty }
