import { IPropsUI, TextProperty } from "./PropsUI.js";
import { WTK_UI_PROPERTY_LOGIC, _uiPropertyINIT, _uiPropertyPOSTRENDER, _uiPropertyPRERENDER } from "./PropertyUI.js";
import { ModelPropertySpec } from "../ModelProps.js";
import { WTK_GEN_COMPONENT } from "../_gen_component.js";
import { _HTML_ } from "../Compiler.js";

export const _inputTextHTML = (_: WTK_UI_INPUT_TEXT_LOGIC) => {
    const label: string = (_.property.data.label || _.key) as string;
    return  _HTML_(`<div>
                       <div>${label}</div>
                       <input type="text" value={{ safe | ${_.key } }}>
                    </div>`,_)
}

export class WTK_UI_INPUT_TEXT_LOGIC extends WTK_UI_PROPERTY_LOGIC<TextProperty> {}

export const WtkInputText = WTK_GEN_COMPONENT(WTK_UI_INPUT_TEXT_LOGIC, {
    onCreate: _uiPropertyINIT,
    onprerender: _uiPropertyPRERENDER,
    onpostrender: _uiPropertyPOSTRENDER,
    html: _inputTextHTML
});

export const _create_input_text_ = (prop: ModelPropertySpec<TextProperty>, ui: IPropsUI): ModelPropertySpec<TextProperty> => {
    prop.create({
        name: ui.name,
        type: ui.type,
        label: ui.label,
        default: ui.default,
        hidden: ui.hidden || false 
    });
    return prop;
}