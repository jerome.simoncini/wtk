import { CheckboxProperty, IPropsUI } from "./PropsUI.js";
import { WTK_UI_PROPERTY_LOGIC, _uiPropertyINIT, _uiPropertyPOSTRENDER, _uiPropertyPRERENDER } from "./PropertyUI.js";
import { ModelPropertySpec } from "../ModelProps.js";
import { WTK_GEN_COMPONENT } from "../_gen_component.js";


export const _inputCheckboxHTML = async (_: WTK_UI_INPUT_CHECKBOX_LOGIC) => {
    const checked = _.value ? "checked" : "";                   //
    const label: string = (_.property.data.label || _.key) as string;
    return `<div>
              <div>${label}</div>
              <input type="checkbox" ${checked}>
            </div>`;
}

export class WTK_UI_INPUT_CHECKBOX_LOGIC extends WTK_UI_PROPERTY_LOGIC<CheckboxProperty> {
    
    onPropertyUpdate() {
        const state = this._[this.key].checked;
        this._updateFunc(this.key, state);
    }
}

export const WtkInputCheckbox = WTK_GEN_COMPONENT(WTK_UI_INPUT_CHECKBOX_LOGIC, {
    onCreate: _uiPropertyINIT,
    onprerender: _uiPropertyPRERENDER,
    onpostrender: _uiPropertyPOSTRENDER,
    html: _inputCheckboxHTML
});

export const _create_input_checkbox_ = (prop: ModelPropertySpec<CheckboxProperty>, ui: IPropsUI): ModelPropertySpec<CheckboxProperty> => {
    const data = ui as CheckboxProperty;
    prop.create({
        name: data.name,
        type: data.type,
        label: data.label,
        default: data.default,
        hidden: data.hidden || false 
    });
    return prop;
}