import { _HTML_ } from "../Compiler.js";
import { ColorProperty, IPropsUI } from "./PropsUI.js";
import { WTK_UI_PROPERTY_LOGIC, _uiPropertyINIT, _uiPropertyPOSTRENDER, _uiPropertyPRERENDER } from "./PropertyUI.js";
import { ModelPropertySpec } from "../ModelProps.js";
import { WTK_GEN_COMPONENT } from "../_gen_component.js";


export const _inputColorHTML = (_: WTK_UI_INPUT_COLOR_LOGIC) => {
    const label: string = (_.property.data.label || _.key) as string;
    return _HTML_(`<div>
                     <div>${label}</div>
                     <input type="color" value={{ safe | ${_.key} }}>
                   </div>`, _)
}


export class WTK_UI_INPUT_COLOR_LOGIC extends WTK_UI_PROPERTY_LOGIC<ColorProperty> {}

export const WtkInputColor = WTK_GEN_COMPONENT(WTK_UI_INPUT_COLOR_LOGIC, {
    onCreate: _uiPropertyINIT,
    onprerender: _uiPropertyPRERENDER,
    onpostrender: _uiPropertyPOSTRENDER,
    html: _inputColorHTML
})


export const _create_input_color_ = (prop: ModelPropertySpec<ColorProperty>, ui: IPropsUI): ModelPropertySpec<ColorProperty> => {
    const data = ui as ColorProperty;
    prop.create({
        name: data.name,
        type: data.type,
        label: data.label,
        default: data.default,
        hidden: data.hidden || false 
    });
    return prop;
}