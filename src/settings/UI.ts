import { WTK_COMPONENT_LOGIC, ComponentFactory } from "../AppComponent.js";
import { AppObject } from "../AppObject.js";
import { WtkInputCheckbox, _create_input_checkbox_ } from "./InputCheckbox.js";
import { WtkInputColor, _create_input_color_ } from "./InputColor.js";
import { WtkInputNumber, _create_input_number_ } from "./InputNumber.js";
import { WtkInputRange, _create_input_range_ } from "./InputRange.js";
import { WtkInputText, _create_input_text_ } from "./InputText.js";
import { ModelProperty, ModelPropertySpec } from "../ModelProps.js";
import { Constructor } from "../Constructor.js";
import { WTK_GEN_OBJECT } from "../_gen_object.js";
import { InputType, IPropsUI, NoneProperty } from "./PropsUI.js";
import { WTK_UI_PROPERTY_LOGIC } from "./PropertyUI.js";
import { ASSERT } from "../utils.js";

interface IModelProperty {
    type: number;
    ui: Constructor<WTK_COMPONENT_LOGIC>;
    onCreate?: (prop: ModelPropertySpec<IPropsUI>, ui: IPropsUI) => ModelPropertySpec<IPropsUI>;
}

export class WTK_UI_FACTORY_LOGIC extends AppObject {
    private _propertyFactory: ComponentFactory | undefined = undefined;
    private _modelProperties: IModelProperty[] = [];

    setModelProperties(properties: IModelProperty[]) {
        this._modelProperties = properties;
    }

    initPropertyFactory() {
        const propertyFactory = class extends ComponentFactory { }
        this._propertyFactory = this._localStore.get(propertyFactory, this.getGlobalStore());
    }

    createModelPropertyUI(inputType: InputType): WTK_UI_PROPERTY_LOGIC<IPropsUI> | undefined {
        return this.propertyFactory.create(this.getModelProperty(inputType).ui) as WTK_UI_PROPERTY_LOGIC<IPropsUI>;
    }

    createModelProperty(data: IPropsUI): ModelProperty {
        const prop = new ModelPropertySpec<IPropsUI>();
        return data.type == undefined ?
            prop.create({
                name: data.name,
                type: InputType.NONE,
                label: data.label,
                default: data.default,
                hidden: true
            })
            : (() => {
                const _ = this.getModelProperty(data.type).onCreate || _wtk_create_property_ui_;
                return _(prop, data);
            })()
    }

    private getModelProperty(type: InputType) {
        if (type == InputType.NONE) return NoneProperty;  //
        const modelProperty = this._modelProperties.find(p => p.type == type);
        return ASSERT(modelProperty, `(${this.constructor.name}) unknown model property '${type}'`);
    }

    private get propertyFactory() {
        return ASSERT(this._propertyFactory, `(${this.constructor.name}) property factory undefined`);
    }
}


//----------------------------------------------------------


const MODEL_PROPERTIES = [
    { type: InputType.NUMBER, ui: WtkInputNumber, onCreate: _create_input_number_ },
    { type: InputType.COLOR, ui: WtkInputColor, onCreate: _create_input_color_ },
    { type: InputType.TEXT, ui: WtkInputText, onCreate: _create_input_text_ },
    { type: InputType.CHECKBOX, ui: WtkInputCheckbox, onCreate: _create_input_checkbox_ },
    { type: InputType.RANGE, ui: WtkInputRange, onCreate: _create_input_range_ },
]

const initUI = (_: WTK_UI_FACTORY_LOGIC): void => {
    _.setModelProperties(MODEL_PROPERTIES);
    _.initPropertyFactory();
}

export const WtkUI = WTK_GEN_OBJECT(WTK_UI_FACTORY_LOGIC, {
    onCreate: initUI,
    createLocalStore: true
}) as Constructor<WTK_UI_FACTORY_LOGIC>;

//-----------------------------------------------------------------------------------------

export const _wtk_create_property_ui_ = (prop: ModelPropertySpec<IPropsUI>, ui: IPropsUI): ModelPropertySpec<IPropsUI> => {  //
    prop.create({
        name: ui.name,
        type: ui.type,
        label: ui.label,
        default: ui.default,
        hidden: ui.hidden || false,
    });
    return prop;
}