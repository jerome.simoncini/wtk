import { AppEvent, EventType } from "./AppEvent.js";
import { IStateUpdate } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { MemoryPool } from "./MemoryPool.js";
import { ObjectLoader } from "./ObjectLoader.js";
import { IObserverNotifierNames } from "./ObserverNotifier.js";
import { Scope } from "./Scope.js";
import { ASSERT, MESSAGE } from "./utils.js";


export class AppObject extends ObjectLoader {
    private _dispatcher: EventDispatcher = new EventDispatcher();
    private _memoryPool: Map<string, MemoryPool> = new Map();
    protected _notifierActions: Map<Scope, number[]> = new Map();
    protected _observerActions: Map<Scope, number[]> = new Map();

    startNotifier(eventCodes: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            try {
                const store = this.getObjectStore(s);
                this.addObserver(store, ...eventCodes);

                eventCodes.map(n => {
                    if (!this.sends(s)?.includes(n)) {
                        this.addNotifierActions([n], scope);
                    }
                });
            }
            catch (error) {
                //
            }
        });
    }

    stopNotifier(eventCodes: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            try {
                const store = this.getObjectStore(s);
                this.removeObserver(store, ...eventCodes);

                eventCodes.map(n => {
                    if (this.sends(s)?.includes(n)) {
                        this.removeNotifierActions([n], scope);
                    }
                });
            }
            catch (error) {
                //
            }
        });
    }

    startObserver(eventCodes: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            try {
                const store = this.getObjectStore(s);
                store.addObserver(this, ...eventCodes);

                eventCodes.map(n => {
                    if (!this.listens(s)?.includes(n)) {
                        this.addObserverActions([n], scope);
                    }
                });
            }
            catch (error) {
                //
            }
        });
    }

    stopObserver(eventCodes: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            try {
                const store = this.getObjectStore(s);
                store.removeObserver(this, ...eventCodes);

                eventCodes.map(n => {
                    if (this.listens(s)?.includes(n)) {
                        this.removeObserverActions([n],scope);
                    }
                });
            }
            catch (error) {
                //
            }
        });
    }

    addNotifierActions(actions: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            let notifier = this._notifierActions.get(s);
            if (!notifier) {
                this._notifierActions.set(s, actions);
            }
            else {
                notifier.concat(actions);
            }
        });
    }

    addObserverActions(actions: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            let observer = this._observerActions.get(s);
            if (!observer) {
                this._observerActions.set(s, actions);
            }
            else {
                observer.concat(actions);
            }
        });
    }

    removeNotifierActions(actions: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            let notifier = this._notifierActions.get(s);
            if (notifier) {
                notifier = notifier.filter(action => {
                    return actions.some(value => value == action) == false;
                });
            }
        });
    }

    removeObserverActions(actions: number[], scope: Scope | undefined = undefined) {
        (scope ? [scope] : [Scope.GLOBAL, Scope.LOCAL]).map((s) => {
            let observer = this._observerActions.get(s);
            if (observer) {
                observer = observer.filter(action => {
                    return actions.some(value => value == action) == false;
                });
            }
        });
    }

    send(event: number, action: number, params: any = {}) {
        this.action(event, { action, params });
    }

    action(code: any, data: any) {  //
        const type = EventType.ACTION;    //
        this._notify({ type, code, data });
    }

    register(eventCode: number, action: number, fun: (...args: any[]) => void) {
        this._dispatcher.register(eventCode, action, fun);
    }

    onNotify(event: AppEvent) {
        this._dispatcher.dispatch(event.code, event.data.action, event.data.params);
    }

    async onInit(...args: any[]) {
        this._extensions.execute('beforeOnInit', this);
        await this._onInit(this);
        this._extensions.execute('afterOnInit', this);
    }

    onCreate(...args: any[]) {
        this._extensions.execute('beforeOnCreate', this);
        this._onCreate(this);
        this._extensions.execute('afterOnCreate', this);
    }

    onDestroy(...args: any[]) {
        this._extensions.execute('beforeOnDestroy', this);
        console.warn(MESSAGE(this, '(on destroy)'));
        console.warn(args);
        //this.getObjectStore(Scope.LOCAL)?.clear(...args);     /////////////////
        this._onDestroy(this, ...args);
        this._extensions.execute('afterOnDestroy', this);
    }

    onUpdate(...args: any[]) {   //
        this._extensions.execute('beforeOnUpdate', this);
        this._onUpdate(this, ...args);
        this._extensions.execute('afterOnUpdate', this);
    }

    onStateUpdate(update: IStateUpdate) {
        this._extensions.execute('beforeOnStateUpdate', this);
        this._onStateUpdate(this, update);
        this._extensions.execute('afterOnStateUpdate', this);
    }

    setMemoryPool(name: string, memoryPool: MemoryPool) {
        this._memoryPool.set(name, memoryPool);
    }

    mem(name: string = '_') {
        return ASSERT(this._memoryPool.get(name), `memory pool undefined '${name}'`);
    }

    async _onInit(object: AppObject) { }
    
    _onCreate(object: AppObject) { }
    _onDestroy(object: AppObject, ...args: any[]) { }

    _onUpdate: (object: AppObject, ...args: any[]) => void = () => { };    //////////
    _onStateUpdate(object: AppObject, update: IStateUpdate) { }          /////////

    get memory(): string[] {
        return Array.from(this._memoryPool).map((k) => `${k[1].inode}`);
    }

    sends(scope: Scope): number[] {        //
        return this._notifierActions.get(scope) || [];
    }

    listens(scope: Scope): number[] {      //
        return this._observerActions.get(scope) || [];
    }

    getObservers(scope: Scope): IObserverNotifierNames[] {
        return this.getObjectStore(scope)?.observers || [];
    }

    getEmitters(scope: Scope): IObserverNotifierNames[] {

        const events = new Map<number, string[]>();
        this.getObjectStore(scope).loop(AppObject, (instance: AppObject) => {
            instance.sends(scope).map(n => {
                let e = events.get(n);
                if (e) {
                    e.push(instance.__name__ != '' ? instance.__name__ : instance.inode);  //
                }
                else {
                    events.set(n, [instance.__name__ != '' ? instance.__name__ : instance.inode]);    //
                }
            })
        });

        return Array.from(events.entries()).map(e => {
            return {
                event: e[0],
                names: e[1]
            } as IObserverNotifierNames;
        });
    }
}

/****************************************************************************************/

class EventDispatcher {
    private _handlers: Map<number, Map<number, (...args: any[]) => void>> = new Map();

    register(eventCode: number, action: number, fun: (...args: any[]) => void) {
        if (!this._handlers) {
            this._handlers = new Map();
        }
        if (!this._handlers.has(eventCode)) {
            this._handlers.set(eventCode, new Map());
        }
        const handler = this._handlers.get(eventCode);
        if (handler) {
            handler.set(action, fun);
        }
        else {
            // notify error
        }
    }

    dispatch(eventCode: number, action: number, params: any) {
        const handler = this._handlers.get(eventCode);
        if (handler) {
            const fun = handler.get(action);
            if (fun) {
                fun(params);
            }
            else {
                // notify error
            }
        }
        else {
            // notify error
        }
    }
}


