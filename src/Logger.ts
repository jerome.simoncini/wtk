enum LogLevel {
    INFO  = 1,
    ALERT = 1<<1,
    ERROR = 1<<2
}

class Logger {
    private _name: string;
    private _active: boolean;
    private _levelMask: number = LogLevel.INFO | LogLevel.ALERT | LogLevel.ERROR;

    constructor(name: string, active: boolean){
        this._name = name;
        this._active = active;
    }

    log(message: string, level=LogLevel.INFO){
        const logMessage: boolean = (this._levelMask & level) != 0;
        if(this._active && logMessage) {
            const output: string = `[${this._name}] ${message}`;
            if(level==LogLevel.ERROR) {
                console.error(output);
            }
            else {
                console.log(output);
            }
        }
    }

    alert(message: any){
        this.log(message, LogLevel.ALERT);
    }

    error(message: any){
        this.log(message, LogLevel.ERROR);
    }

    on() {
        this._active = true;
    }

    off() {
        this._active = false;
    }

    setLevelMask(mask: number){
        this._levelMask = mask;
    }
}

const LOGGER = (name: string, active=false) => {
    return new Logger(name, active);
}

export { LOGGER, LogLevel };