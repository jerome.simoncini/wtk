import { _HTML_ } from "./Compiler.js";
import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { ASSERT, MESSAGE } from "./utils.js";

export const WTK_LAYOUT_ELEMENT_1 = 'wtk-layout-e1';
export const WTK_LAYOUT_ELEMENT_2 = 'wtk-layout-e2';

export class WTK_LAYOUT_ELEMENT extends WTK_COMPONENT_LOGIC {
    private _layout: WTK_LAYOUT_LOGIC|undefined = undefined;

    setLayout(layout: WTK_LAYOUT_LOGIC): void {
        this._layout = layout;
    }

    get layout(): WTK_LAYOUT_LOGIC {
        return ASSERT(this._layout, MESSAGE(this,`parent layout undefined`));
    }
}

export abstract class WTK_LAYOUT_LOGIC extends WTK_COMPONENT_LOGIC {
    private _resize: boolean = false;
    private _element1: HTMLElement|undefined = undefined;
    private _element2: HTMLElement|undefined = undefined;
    private _scaleRatio: number|undefined = undefined;

    startResize() {
        this._resize = true;
    }

    stopResize() {
        this._resize = false;
    }

    async setContent(e1: WTK_COMPONENT_LOGIC, e2: WTK_COMPONENT_LOGIC, scaleRatio: number) {
        [e1,e2].map(e=>{
            if(e instanceof WTK_LAYOUT_ELEMENT) {
                e.setLayout(this);
            }
        });
        await this.mount(WTK_LAYOUT_ELEMENT_1, e1);
        await this.mount(WTK_LAYOUT_ELEMENT_2, e2);
        this.scale(scaleRatio);
    }

    initResizer(...args: any[]) {
        this._element1 = this.getElement('e1');
        this._element2 = this.getElement('e2'); 
        this.element1.style.flex = this.scaleRatio.toString();
        this.element2.style.flex = (1-this.scaleRatio).toString();
    }

    scale(scaleRatio: number) {
        this._scaleRatio = scaleRatio;
    }

    get resize(): boolean {
        return this._resize;
    }

    get scaleRatio(): number {
        return ASSERT(this._scaleRatio, `[WTK_LAYOUT_LOGIC] scale ratio undefined`)
    }

    get element1(): HTMLElement {
        return ASSERT(this._element1, `[WTK_LAYOUT_LOGIC] element 1 undefined`);
    }
    
    get element2(): HTMLElement {
        return ASSERT(this._element2, `[WTK_LAYOUT_LOGIC] element 2 undefined`);
    }

    onLayoutResize(mousePosX: number, mousePosY: number) {
        const rcbr = this.root.getBoundingClientRect();
        this.scale(this.rescale(rcbr, mousePosX, mousePosY));

        //console.warn(MESSAGE(this, `scale=${this.scaleRatio}`));
        const e1 = this.scaleRatio;  
        const e2 = 1 - this.scaleRatio;
        
        this.element1.style.flex = e1.toString();
        this.element2.style.flex = e2.toString();

        this.getSlot(WTK_LAYOUT_ELEMENT_1)?.onResize();        ///////////
        this.getSlot(WTK_LAYOUT_ELEMENT_2)?.onResize();        ///////////
    }

    abstract rescale(rect: DOMRect, mousePosX: number, mousePosY: number): number;
}

//--------------------------------------------------------------------------------------------------//


export class WTK_HORIZONTAL_LAYOUT_LOGIC extends WTK_LAYOUT_LOGIC {

    rescale(rect: DOMRect, mousePosX: number, mousePosY: number): number {
        const rw = rect.right - rect.left;
        const mw = mousePosX - rect.left;
        return mw/rw
    }
}


//--------------------------------------------------------------------------------------------------//


export class WTK_VERTICAL_LAYOUT_LOGIC extends WTK_LAYOUT_LOGIC {

    rescale(rect: DOMRect, mousePosX: number, mousePosY: number): number {
        const rw = rect.bottom - rect.top;
        const mw = mousePosY - rect.top;
        return mw/rw
    }
}
