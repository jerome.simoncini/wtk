import { WTK_CANVAS } from "./AppCanvas.js";
import { _HTML_ } from "./Compiler.js";
import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { __skull__ } from "./_skull.js";
import { WtkInput } from "./services/input/InputSystem.js";

export const WTK_CANVAS_ID = "wtk-canvas";

export interface ICanvasSettings extends IAppComponent {

}

export const WTK_GEN_CANVAS = <T extends WTK_CANVAS>(canvas: Constructor<T>, settings?: ICanvasSettings): Constructor<T> => {
    const skull = __skull__(canvas, {
        onCreate: _create_canvas_,
        onInit: _init_canvas_,
        settings
    }) as Constructor<T>;

    return WTK_GEN_COMPONENT(skull, {
        ...settings,
        html: (_: WTK_CANVAS) => {                      
            return _HTML_(`<canvas @${WTK_CANVAS_ID} tabindex='1'></canvas>`, _);
        },
        styles: [
            'wtk-canvas'   
        ],
        setup: [
            {
                name: WTK_CANVAS_ID,
                selector: { id: true, value: WTK_CANVAS_ID },
                actions: [
                    {
                        event: 'keydown',
                        cmd: (_: WTK_CANVAS, e: KeyboardEvent): void => {
                            _.input.onKeyDown(e);
                        }
                    },
                    {
                        event: 'keyup',
                        cmd: (_: WTK_CANVAS, e: KeyboardEvent): void => {
                            _.input.onKeyUp(e);
                        }
                    },
                    {
                        event: 'mousedown',
                        cmd: (_: WTK_CANVAS, e: MouseEvent): void => {
                            _.input.onMouseDown(e);
                        }
                    },
                    {
                        event: 'mouseup',
                        cmd: (_: WTK_CANVAS, e: MouseEvent): void => {
                            _.input.onMouseUp(e);
                        }
                    },
                    {
                        event: 'mousemove',
                        cmd: (_: WTK_CANVAS, e: MouseEvent): void => {
                            _.input.onMouseMove(e);
                        }
                    },
                    {
                        event: 'mouseout',
                        cmd: (_: WTK_CANVAS): void => {
                            _.input.onMouseOut();
                        }
                    },
                    {
                        event: 'mouseover',
                        cmd: (_: WTK_CANVAS, e: MouseEvent): void => {
                            _.getElement(WTK_CANVAS_ID).focus();
                        }
                    }
                ]
            }
        ],
        onCreate: (_: WTK_CANVAS): void => {
            _.setInputManager(_.getObjectStore(Scope.LOCAL).get(WtkInput));
        },
        onDestroy: (_: WTK_CANVAS): void => {
            _.getObjectStore(Scope.LOCAL).release(WtkInput);
        },
        createLocalStore: true,
    }) as Constructor<T>;
}


const _create_canvas_ = (canvas: WTK_CANVAS, settings?: ICanvasSettings) => {

}

const _init_canvas_ = async (canvas: WTK_CANVAS, settings?: ICanvasSettings) => {
}