import { WTK_DB_LOGIC } from "./AppDB.js";
import { WTK_DB_STORE_LOGIC } from "./AppDBStore.js";
import { AppStateService, IStateUpdate } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { System } from "./events.js";
import { WtkFileFactory } from "./models/file/setup.js";
import { WTK_FILE_SERVICE_LOGIC } from "./services/FileService.js";


export interface IFileService extends IAppObject {
    db: Constructor<WTK_DB_LOGIC>,
    store: Constructor<WTK_DB_STORE_LOGIC>
    fileStateService: Constructor<AppStateService>,
    fileUpdateEvents: number[],
    modelUpdateEvents: number[],
}

export const WTK_GEN_FILE_SERVICE = <T extends WTK_FILE_SERVICE_LOGIC>(service: Constructor<T>, settings: IFileService): Constructor<WTK_FILE_SERVICE_LOGIC> => {
    const skull = __skull__(service, {
        onCreate: _create_file_service_,
        onInit: _init_file_service_,
        settings: settings
    }) as Constructor<WTK_FILE_SERVICE_LOGIC>
    return WTK_GEN_OBJECT(skull, {
        ...{
            onCreate: (_: WTK_FILE_SERVICE_LOGIC) => {   //
                _.startObserver([System.STATE_UPDATE]);
            },
            onDestroy: (_: WTK_FILE_SERVICE_LOGIC) => {  //
                _.stopObserver([System.STATE_UPDATE]);
            }
        },
        ...settings
    }) as Constructor<WTK_FILE_SERVICE_LOGIC>
}

const _create_file_service_ = (service: WTK_FILE_SERVICE_LOGIC, settings: IFileService): void => {
    service.setDB(settings.db);
    service.setDBStore(settings.store);
    service.setStateService(settings.fileStateService);
    service.startObserver([System.STATE_UPDATE]);
    service.setFileFactory(WtkFileFactory);

    service._onStateUpdate = (_: WTK_FILE_SERVICE_LOGIC, update: IStateUpdate) => {
        if (update.mask.hasOne(...settings.fileUpdateEvents)) {
            _.syncFile();
        }
        if (update.mask.hasOne(...settings.modelUpdateEvents)) {
            _.syncFile({
                modified: true
            })
        }
    }
}

const _init_file_service_ = async (service: WTK_FILE_SERVICE_LOGIC, settings: IFileService) => {
    service.syncFile();    //////
}