import { ConsoleLineColor, ConsoleLineFlag, WTK_CONSOLE_LINES, WTK_CONSOLE_LINE_ITEM, WTK_CONSOLE_LOGIC } from "./AppConsole.js";
import { WTK_LIST_LOGIC } from "./AppList.js";
import { AppObject } from "./AppObject.js";
import { _HTML_ } from "./Compiler.js";
import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { WTK_GEN_LIST } from "./_gen_list.js";
import { __skull__ } from "./_skull.js";
import { WTK_COMMAND_HANDLER_LOGIC } from "./services/script/CommandHandler.js";
import { WTK_INTERPRETER_LOGIC } from "./services/script/Interpreter.js";
import { escapeHTML } from "./utils.js";

export interface IConsole extends IAppComponent {
    interpreter: Constructor<WTK_INTERPRETER_LOGIC>,
    commandHandler: Constructor<WTK_COMMAND_HANDLER_LOGIC>,
    root?: Constructor<AppObject>,                          //
    onReset?: (_: WTK_CONSOLE_LOGIC) => void;
    slowness?: number;
}

export const WTK_GEN_CONSOLE = <T extends WTK_CONSOLE_LOGIC>(c: Constructor<T>, settings: IConsole): Constructor<T> => {
    const skull = __skull__(c, {
        onCreate: _create_console_,
        onInit: _init_console_,
        settings
    }) as Constructor<T>;

    return WTK_GEN_COMPONENT(skull, {
        html: (_: WTK_CONSOLE_LOGIC) => {
            return _HTML_(`<div @console class="wtk-console" tabindex="0" >
                             << ${WTK_CONSOLE_LINES} | ${JSON.stringify({ items: _.lines })} >>
                           </div>`, _);
        },
        styles: [
            'wtk-console'
        ],
        onCreate: (_: WTK_CONSOLE_LOGIC): void => {
            if (settings.root) {                             //////
                _.setAppModule(_.___(settings.root));         //////
            }
            //_.module.setFakeRoot(true);
        },
        onDestroy: (_: WTK_CONSOLE_LOGIC): void => {
            _.getObjectStore(Scope.LOCAL).clear();      ////////
        },
        onReady: async (_: WTK_CONSOLE_LOGIC) => {
            if (!_.active) {
                _.reset();
            }
        },
        onHide: async (_: WTK_CONSOLE_LOGIC) => {
            _.stop();
        },
        setup: [
            {
                name: 'console',
                selector: { id: true, value: 'console' },
                actions: [
                    {
                        event: 'keydown',
                        cmd: (_: WTK_CONSOLE_LOGIC, e: KeyboardEvent): void => {
                            _.handle(e.key);
                        }
                    },
                    {
                        event: 'focusin',
                        cmd: (_: WTK_CONSOLE_LOGIC): void => {
                            _.setFocus(true);
                        }
                    },
                    {
                        event: 'focusout',
                        cmd: (_: WTK_CONSOLE_LOGIC): void => {
                            _.setFocus(false);
                        }
                    },
                ]
            },
            {
                name: 'lines',
                selector: { tagname: true, value: WTK_CONSOLE_LINES },
                actions: [
                ]
            }
        ],
        slots: [
            {
                name: WTK_CONSOLE_LINES,
                content: WTK_GEN_LIST(WTK_LIST_LOGIC, {
                    item: WTK_GEN_COMPONENT(WTK_CONSOLE_LINE_ITEM, {
                        html: (_: WTK_CONSOLE_LINE_ITEM) => {
                            _.setState('prompt', _.prompt);

                            const content = _.cmd.map(c => {
                                const fontWeight = c.flags?.includes(ConsoleLineFlag.BOLD) ? 'bold' : 'normal';
                                const color = c.color || ConsoleLineColor.WHITE;
                                const padding = c.content ? "20px" : 0;
                                const f = c.content;
                                return `<div style="padding-right: ${padding}; color: ${color}; font-weight: ${fontWeight}">${escapeHTML(f)}</div>`
                            })

                            _.setState('content', content.join(''));

                            return _HTML_(`
                        <div style="display: flex; flex-direction: row">
                            <div style="padding: 2px; font-weight: bold; color: lightgreen;">{{ safe | prompt }}</div>
                            <div @cmd style="padding: 2px; display:flex; flex-direction: row">{{ safe | content }}</div>
                        </div>`, _);     ///////////////////////////////////////////
                        },
                        setup: [
                            {
                                name: 'cmd',
                                selector: { id: true, value: 'cmd' },
                                actions: []
                            }
                        ]
                    }),
                    styles: [
                        'wtk-console-lines'
                    ],
                }),
                scope: Scope.LOCAL
            }
        ],
        createLocalStore: true,
        name: 'WtkConsole'

    }) as Constructor<T>
}


const _create_console_ = (c: WTK_CONSOLE_LOGIC, settings: IConsole) => {
    c.setInterpreter(settings.interpreter);
    c.setCommandHandler(settings.commandHandler);

    c.commandHandler.cmdspecs.map((cmd, index) => c.interpreter.bind(cmd.name, index + 1));


    if (settings.onReset) {
        c._onReset = settings.onReset;
    }
}

const _init_console_ = async (c: WTK_CONSOLE_LOGIC, settings: IConsole) => {

    if (settings.slowness) {
        c.setSlowness(settings.slowness);
    }

    c.commandHandler.bindCommands(c.interpreter);
}
