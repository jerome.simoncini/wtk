import { AppObject } from "./AppObject.js";
import { StateObject } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { LOGGER } from "./Logger.js";
import { MemRef, Ref } from "./MemoryPool.js";
import { ObjectFactory } from "./ObjectFactory.js";
import { InputType, IPropsUI } from "./settings/PropsUI.js";
import { WTK_UI_FACTORY_LOGIC } from "./settings/UI.js";
import { ASSERT, MESSAGE } from "./utils.js";

const THIS = "Model Props"
const DEBUG = LOGGER(THIS);
DEBUG.on();

abstract class ModelProperty {
    abstract get type(): number;
    abstract get data(): any;  //
}

type ValueType = any;

type PropertyMapping<I> = {
    [Property in keyof I]: Ref | string | boolean | number | {};  //
};

class ModelPropertySpec<I extends IPropsUI> extends ModelProperty {
    private _data: PropertyMapping<I> | undefined = undefined;

    create(data: PropertyMapping<I>) {
        this._data = data;
    }

    get type(): InputType {
        return ASSERT(this.data.type, `(${this.constructor.name}) type undefined`) as InputType;
    }

    get data(): PropertyMapping<I> {
        return ASSERT(this._data, `(${this.constructor.name}) data undefined`);
    }
}


class ModelProps<V = ValueType> extends AppObject {
    protected _props: IPropsUI[] = [];  //
    protected _data: Map<string, ModelProperty> = new Map();
    protected _uiFactoryCtor: Constructor<WTK_UI_FACTORY_LOGIC> | undefined = undefined;

    setProps(props: IPropsUI[]) {
        this._props = props;
    }

    setUIFactoryCtor(uiFactoryCtor: Constructor<WTK_UI_FACTORY_LOGIC>) {
        this._uiFactoryCtor = uiFactoryCtor;
    }

    private get uiFactoryCtor() {
        return ASSERT(this._uiFactoryCtor, MESSAGE(this,`ui factory ctor undefined`))
    }

    keys() {
        return Array.from(this._data.keys());
    }

    set(property: string, value: V) {
        this.mem().write(property, value);
    }

    get(property: string): V {
        const value = this.mem().read(property);
        return value != undefined ? value : (() => { throw MESSAGE(this,`unknwn property "${property}" value=${value}`)})();
    }

    getRef(property: string): MemRef<string, any> {
        const memref = class extends MemRef<string, any> { }
        return this.mem().createRef(property, memref);
    }

    getModelProperty(ui: string): ModelProperty | undefined {
        return this._data.get(ui);
    }

    merge(props: ModelProps<V>) {
        const data: Map<string, V> | null = props.getData();
        if (data) {
            const propsArray = Array.from(data.entries());
            for (const [key, value] of propsArray) {
                this.mem().write(key, value);
            }
        }
    }

    private _initProps() {
        for (const property of this._props) {
            const uiFactory = this.___(this.uiFactoryCtor) as WTK_UI_FACTORY_LOGIC;       
            this._data.set(property.name, uiFactory.createModelProperty(property));
            this.set(property.name, property.default);
        }
    }

    get data() {
        return this._data;
    }

    init(): void {
        this._initProps();
        this.onInit();
    }

    getData(): Map<string, any> | null {
        const data: Map<string, V> = new Map();
        for (const key of this._data.keys()) {
            const value = this.mem().read(key);
            data.set(key, value);
        }
        return data.size ? data : null;
    }

    public _deserialize(json: string): void {
        this.init();   //
        const state = JSON.parse(json);

        if (state.data instanceof Array) {  
            state.data.forEach(entry => {
                this.mem().write(entry[0], entry[1]);
            });
        }
        else {
            // notify error
        }
    }

    public _serialize(): string {
        return JSON.stringify({
            data: Array.from(this.getData() || [])
        });
    }

    public _initState(props: ModelProps): void    //
    {
        const data = props?.getData();
        //DEBUG.log(`(${this.constructor.name})(init state) data=${JSON.stringify(data)}`);
        data?.forEach(entry => {
            this.mem().write(entry[0], entry[1]);
        });
    }
}

class ModelPropsFactory extends ObjectFactory<StateObject> { }   //

export { ModelProps, ModelPropsFactory, ModelPropertySpec, ModelProperty, PropertyMapping }