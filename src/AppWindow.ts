import { AppObject } from "./AppObject.js";
import { WTK_GEN_OBJECT } from "./_gen_object.js";
import { ASSERT } from "./utils.js";

interface IAppEventListener {
    event: keyof WindowEventMap;
    fun: (event: Event) => void;
}

export class APP_WINDOW_LOGIC extends AppObject {
    private _window: globalThis.Window | undefined = undefined;
    private _listeners: Map<string, IAppEventListener> = new Map();

    setWindow(window: globalThis.Window): void {
        this._window = window;
    }

    get window() {
        return ASSERT(this._window, `(${this.constructor.name}) window undefined`);
    }

    addEventListener(name: string, event: keyof WindowEventMap, fun: (event: Event) => void) {
        this.window.addEventListener(event, fun);
        this._listeners.set(name, { event, fun });
    }

    removeEventListener(name: string) {
        const listener = this._listeners.get(name);
        if (listener) {
            this.window.removeEventListener(listener.event, listener.fun);
        }
    }
}

export const AppWindow = WTK_GEN_OBJECT(APP_WINDOW_LOGIC, {
    onCreate: (_: APP_WINDOW_LOGIC): void => {
        _.setWindow(window);
    },
    name: "App Window"
})
