import { Constructor } from "./Constructor.js";
import { __skull__ } from "./_skull.js";
import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { WTK_HORIZONTAL_LAYOUT_LOGIC, WTK_LAYOUT_ELEMENT_1, WTK_LAYOUT_ELEMENT_2, WTK_LAYOUT_LOGIC, WTK_VERTICAL_LAYOUT_LOGIC } from "./AppLayout.js";
import { Scope } from "./Scope.js";
import { IAppComponent, WTK_GEN_COMPONENT } from "./_gen_component.js";
import { _HTML_ } from "./Compiler.js";
import { APP_WINDOW_LOGIC, AppWindow } from "./AppWindow.js";
import { MESSAGE } from "./utils.js";


export interface ILayout extends IAppComponent {
    element1: Constructor<WTK_COMPONENT_LOGIC>,
    element2: Constructor<WTK_COMPONENT_LOGIC>,
    scaleRatio: number,
    scope?: Scope;   //
};

const LAYOUT_MOUSE_UP = "wtk-layout-mu";

export const WTK_GEN_LAYOUT = (layout: Constructor<WTK_LAYOUT_LOGIC>, settings: ILayout): Constructor<WTK_LAYOUT_LOGIC> => {
    const skull = __skull__(layout, {
        onCreate: _create_layout_,
        onInit: _init_layout_,
        settings: settings
    }) as Constructor<WTK_COMPONENT_LOGIC>;
    return WTK_GEN_COMPONENT(skull, {
        html: (_: WTK_HORIZONTAL_LAYOUT_LOGIC) => {
            return _HTML_(`<div --element1 @e1 >
                          << ${WTK_LAYOUT_ELEMENT_1} >>
                       </div>
                       <div --resizer @resizer >
                       </div>
                       <div --element2 @e2 >
                          << ${WTK_LAYOUT_ELEMENT_2} >>
                       </div>`, _)
        },
        slots: [
            { name: WTK_LAYOUT_ELEMENT_1 },
            { name: WTK_LAYOUT_ELEMENT_2 }
        ],
        setup: [
            {
                name: 'resizer',
                selector: { id: true, value: 'resizer' },
                actions: [
                    {
                        event: 'mousedown',
                        cmd: (_: WTK_LAYOUT_LOGIC): void => {
                            _.startResize()
                        }
                    }
                ]
            },
            {
                name: 'e1',
                selector: { id: true, value: 'e1' },
                actions: [
                    {
                        event: 'mousemove',
                        cmd: (_: WTK_LAYOUT_LOGIC, e: MouseEvent): void => {
                            if (_.resize) {
                                const mx = (<MouseEvent>e).clientX;
                                const my = (<MouseEvent>e).clientY;
                                _.onLayoutResize(mx, my);  //
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }
                    }
                ]
            },
            {
                name: 'e2',
                selector: { id: true, value: 'e2' },
                actions: [
                    {
                        event: 'mousemove',
                        cmd: (_: WTK_LAYOUT_LOGIC, e: MouseEvent): void => {
                            if (_.resize) {
                                const mx = (<MouseEvent>e).clientX;
                                const my = (<MouseEvent>e).clientY;
                                _.onLayoutResize(mx, my);  //
                                e.stopPropagation();
                                e.preventDefault();
                            }
                        }
                    }
                ]
            }
        ],
        onShow: async (_: WTK_LAYOUT_LOGIC) => {               ////////////
            (<APP_WINDOW_LOGIC>_.___(AppWindow)).addEventListener(_.getId(LAYOUT_MOUSE_UP), 'mouseup', (e) => {
                if (_.resize) {
                    _.stopResize();
                    e.stopPropagation();
                    e.preventDefault();
                }
            });
        },
        onHide:async (_: WTK_LAYOUT_LOGIC) => {            ////////////
            (<APP_WINDOW_LOGIC>_.___(AppWindow)).removeEventListener(_.getId(LAYOUT_MOUSE_UP));
        },
        onReady: async (_: WTK_LAYOUT_LOGIC) => {           ////////////
            console.warn(MESSAGE(_, `on ready`));
            _.initResizer();
        },
        ...settings
    }) as Constructor<WTK_LAYOUT_LOGIC>
};

const _create_layout_ = async (layout: WTK_LAYOUT_LOGIC, settings: ILayout): Promise<void> => {
};

const _init_layout_ = async (layout: WTK_LAYOUT_LOGIC, settings: ILayout): Promise<void> => {
    const store = layout.getObjectStore(settings.scope ?? Scope.GLOBAL);
    const e1 = store.get(settings.element1);              
    const e2 = store.get(settings.element2);              
    await layout.setContent(e1, e2, settings.scaleRatio);
};

/***********************************************************************************************************/


export const WTK_GEN_HORIZONTAL_LAYOUT = <T extends WTK_HORIZONTAL_LAYOUT_LOGIC>(layout: Constructor<T>, settings: ILayout): Constructor<WTK_HORIZONTAL_LAYOUT_LOGIC> => {
    return WTK_GEN_LAYOUT(layout, {
        styles: [
            'wtk-horizontal-layout'
        ],
        stylesheet: [
            { name: 'element1', className: 'wtk-horizontal-layout-left' },
            { name: 'element2', className: 'wtk-horizontal-layout-right' },
            { name: 'resizer', className: 'wtk-horizontal-layout-resizer' },
        ],
        ...settings
    }) as Constructor<WTK_HORIZONTAL_LAYOUT_LOGIC>
}


/***********************************************************************************************************/


export const WTK_GEN_VERTICAL_LAYOUT = <T extends WTK_VERTICAL_LAYOUT_LOGIC>(layout: Constructor<T>, settings: ILayout): Constructor<WTK_VERTICAL_LAYOUT_LOGIC> => {
    return WTK_GEN_LAYOUT(layout, {
        styles: [
            'wtk-vertical-layout'
        ],
        stylesheet: [
            { name: 'element1', className: 'wtk-vertical-layout-left' },
            { name: 'element2', className: 'wtk-vertical-layout-right' },
            { name: 'resizer', className: 'wtk-vertical-layout-resizer' },
        ],
        ...settings
    }) as Constructor<WTK_VERTICAL_LAYOUT_LOGIC>
}