import { AppObject } from "../../AppObject.js";
import { ICommand, WtkSystemCommand, __gen_command } from "./Command.js";


export class WTK_INTERPRETER_LOGIC extends AppObject {
    private _commands: Map<string, number> = new Map();

    bind(name: string, cmd: number) {
        this._commands.set(name, cmd);
    }

    getCommandId(name: string): number | undefined {
        return this._commands.get(name);
    }

    async exec(cmd: string): Promise<ICommand[]> {
        const output: ICommand[] = [];

        const a = cmd.split(' ');
        const op = a.shift();
        const args: string[] = [];
        while (a.length && !a[0].startsWith('-')) {        /////////
            args.push(a.shift()!);
        }
        const d = a.join(' ').split('-');
        const options: Map<string, string[]> = new Map();
        d.map(f => {
            const g = f.split(' ');
            const h = g.shift() || '';
            if (h != '') {
                [...h].map(c=>options.set(c, g.filter(i => i != '')));
            }
        });

        console.warn(args);
        console.warn(options);

        if (op && this._commands.has(op)) {
            const cmd = this._commands.get(op);
            output.push(__gen_command(cmd!, args, options));
        }
        else {
            output.push(__gen_command(WtkSystemCommand.PRINT, [`${op}: unknown command`], options));
        }

        /*
                switch (op) {
                    ///////////////////////////////
                    case "type":
                        {
                            const type = appTree.get(appTree.current, Scope.TYPE);
                            output.push(__command(WtkCommand.PRINT, [type], options));
                        }
                        break;
        
                    ///////////////////////////////
                    case "name":
                        {
                            const name = appTree.get(appTree.current, Scope.NAME);
                            if (name && name != '') {
                                output.push(__command(WtkCommand.PRINT, args, options));
                            }
                        }
                        break;
        
                    ///////////////////////////////
                    case "flags":
                        {
                            const flags = appTree.get(appTree.current, Scope.FLAGS) || [];
                            output.push(__command(WtkCommand.PRINT, [flags.join(',')], options));
                        }
                        break;
        
                    ///////////////////////////////
                    case "inode":
                        {
                            const inode = appTree.get(appTree.current, Scope.INODE);
                            output.push(__command(WtkCommand.PRINT, [inode], options));
                        }
                        break;
        
                    ///////////////////////////////
                    case "slots":
                        {
                            const slots = appTree.get(appTree.current, Scope.SLOTS);
                            for (const slot of slots) {
                                output.push(__command(WtkCommand.PRINT, [slot], options));
                            }
                        }
                        break;
        
                    ////////////////////////////////
                    case "memory":
                        {
                            const memories = appTree.get(appTree.current, Scope.MEMORY);
                            for (const memory of memories) {
                                output.push(__command(WtkCommand.PRINT, [memory], options));
                            }
                        }
                        break;
        
                    ///////////////////////////////
                    case "clear":
                        {
                            output.push(__command(WtkCommand.CLEAR, args, options));
                        }
                        break;
        
                    default:
                        output.push(__command(WtkCommand.PRINT,[`Command '${op}' not found`], options));
                }
        */

        console.warn(output);

        return output;
    }

    get cmds(): string[] {
        return Array.from(this._commands.keys())
    }

    //********************************************************************/

}

