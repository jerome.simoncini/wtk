import { WtkConsoleFormat } from "../../../../AppConsole.js";
import { Scope } from "../../../../Scope.js";
import { ICommandSpec, WtkSystemCommand } from "../../Command.js";
import { ICommandHandlerResponse } from "../../CommandHandler.js";
import { DEBUG_HANDLER } from "../../DebugHandler.js";



export const LS: ICommandSpec = {
    name: 'ls',
    handler: async (hdl: DEBUG_HANDLER, args: string[], options: Map<string, string[]>) => {
        const output: ICommandHandlerResponse = { table: [] };

        const path = args.length ? args[0] : '.' as string;                    ///

        try {
            const list = hdl.appTree.get(hdl.appTree.current, Scope.LIST) as string[];
            for (const inode of list) {
                const n = hdl.appTree.get(inode, Scope.NAME);
                output.table.push({ content: [n], format: WtkConsoleFormat.MESSAGE });
            }
        }
        catch (e) {
            output.table.push({ content: [`ls: error`], format: WtkConsoleFormat.MESSAGE });     //
        }

        return output;
    }
}
