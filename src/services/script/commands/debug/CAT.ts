import { Scope } from "../../../../Scope.js";
import { ICommandSpec, WtkSystemCommand } from "../../Command.js";
import { ICommandHandlerResponse } from "../../CommandHandler.js";
import { DEBUG_HANDLER } from "../../DebugHandler.js";
import { WTK_FILE_LOGIC } from "../../../../AppFile.js";
import { WtkConsoleFormat } from "../../../../AppConsole.js";

export const CAT: ICommandSpec = {
    name: 'cat',
    handler: async (hdl: DEBUG_HANDLER, args: string[], options: Map<string, string[]>) => {
        const output: ICommandHandlerResponse = { table: [] };

        if (args.length) {
            const path = args[0] as string;

            const list = hdl.appTree.get(hdl.appTree.current, Scope.LIST) || [];
            const i = list.find(inode => inode == path || hdl.appTree.get(inode, Scope.NAME) == path);
            if (i) {
                const f = hdl.appTree.get(i, Scope.ENTITY);
                if (f) {
                    if (f instanceof WTK_FILE_LOGIC) {
                        for (const l of (<WTK_FILE_LOGIC>f).content || []) {
                            output.table.push({ content: [l], format: WtkConsoleFormat.MESSAGE });
                        }
                    }
                    else {
                        output.table.push({ content: [`cat: ${path}: Is a directory`], format: WtkConsoleFormat.MESSAGE });
                    }
                }
                else {
                    output.table.push({ content: [`cat: ${path}: No such file or directory`], format: WtkConsoleFormat.MESSAGE });
                }
            }
            else {
                output.table.push({ content: [`cat: ${path}: No such file or directory`], format: WtkConsoleFormat.MESSAGE });
            }
        }
        else {
            // error
        }

        return output;
    }
}
