import { ICommandSpec, WtkSystemCommand } from "../../Command.js";
import { ICommandHandlerResponse } from "../../CommandHandler.js";
import { DEBUG_HANDLER } from "../../DebugHandler.js";

export const TOUCH: ICommandSpec = {
    name: 'touch',
    handler: async (hdl: DEBUG_HANDLER, args: string[], options: Map<string, string[]>) => {
        const output: ICommandHandlerResponse = { table: [] };

        if (args.length) {
            const filename = args[0] as string;
            await hdl.appTree.createFile(filename);
        }
        else {
            // error
        }
        return output;
    }
}
