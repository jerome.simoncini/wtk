import { WtkConsoleFormat } from "../../../../AppConsole.js";
import { Scope } from "../../../../Scope.js";
import { ICommandSpec, WtkSystemCommand } from "../../Command.js";
import { ICommandHandlerResponse } from "../../CommandHandler.js";
import { DEBUG_HANDLER } from "../../DebugHandler.js";


export const CD: ICommandSpec = {
    name: 'cd',
    handler: async (hdl: DEBUG_HANDLER, args: string[], options: Map<string, string[]>) => {
        const output: ICommandHandlerResponse = { table: [] };

        if (args.length) {

            const path = args[0] as string;                    ///

            switch (path) {
                case '..':
                    {
                        const p = hdl.appTree.current.split(':').slice(0, -1).join(':');

                        return hdl.appTree.changeDir(p != '' ? p : '0').then(r => {
                            return { table: [] } as ICommandHandlerResponse;
                        });
                    }
                    break;
                default:
                    {
                        const inode = hdl.appTree.get(hdl.appTree.current, Scope.LIST)
                            .map(i => {
                                return {
                                    inode: i,
                                    name: hdl.appTree.get(i, Scope.NAME)
                                }
                            })
                            .find(e => e.name == path)?.inode;

                        if (inode) {
                            return hdl.appTree.changeDir(inode).then(r => {
                                return { table: [] } as ICommandHandlerResponse;
                            });
                        }
                        else {
                            output.table.push({ content: [`'${path}': no such file or directory'`], format: WtkConsoleFormat.MESSAGE });
                        }
                    }
                    break;
            }
        }
        else {
            output.table.push({ content: [`'cd': invalid parameters'`], format: WtkConsoleFormat.MESSAGE });
        }

        return output;
    }
}
