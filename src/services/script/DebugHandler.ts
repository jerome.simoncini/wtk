import { WTK_APP_TREE_LOGIC } from "../../AppTree.js";
import { ASSERT, MESSAGE } from "../../utils.js";
import { WTK_GEN_COMMAND_HANDLER } from "../../_gen_command_handler.js";
import { WTK_COMMAND_HANDLER_LOGIC } from "./CommandHandler.js";
import { LS } from "./commands/debug/LS.js";
import { TOUCH } from "./commands/debug/TOUCH.js";
import { CAT } from "./commands/debug/CAT.js";
import { CD } from "./commands/debug/CD.js";



export class DEBUG_HANDLER extends WTK_COMMAND_HANDLER_LOGIC {
    private _appTree: WTK_APP_TREE_LOGIC | undefined = undefined;
  
    setAppTree(t: WTK_APP_TREE_LOGIC) {
        this._appTree = t;
    }

    get appTree(): WTK_APP_TREE_LOGIC {
        return ASSERT(this._appTree, MESSAGE(this, `app tree undefined`));
    }
}


export const DebugHandler = WTK_GEN_COMMAND_HANDLER(DEBUG_HANDLER, {
    name: 'Debug Handler',
    cmds:  [
        LS,
        CD,
        TOUCH,
        CAT
    ]
})