import { ICommandHandlerResponse, WTK_COMMAND_HANDLER_LOGIC } from "./CommandHandler.js";

export interface ICommandSpec {
    id?: number;                             //
    name: string;
    handler: (hdl: WTK_COMMAND_HANDLER_LOGIC, args: string[], options: Map<string, string[]>) => Promise<ICommandHandlerResponse>;
}



export interface ICommand {
    id: number;
    args: string[];
    options: Map<string, string[]>;
}

//////////////////////////////////////////
export enum WtkSystemCommand {
    PRINT = -9999,
    CLEAR,
}


export interface ICommandData { }


export interface ITableData extends ICommandData {
    headers: string[];
    table: string[][];
}


export const __gen_command = (id: number, args: string[], options: Map<string, string[]>): ICommand => {
    return {
        id,
        args,
        options
    }
}


