import { MESSAGE } from "../../utils.js";
import { IConsoleLine, WtkConsoleFormat } from "../../AppConsole.js";
import { AppObject } from "../../AppObject.js";
import { ICommand, ICommandSpec } from "./Command.js";
import { WTK_INTERPRETER_LOGIC } from "./Interpreter.js";

export interface ICommandeHandlerResponseCell {
    content: string[],
    format: WtkConsoleFormat
}

export interface ICommandHandlerResponse {
    table: ICommandeHandlerResponseCell[];
}


export abstract class WTK_COMMAND_HANDLER_LOGIC extends AppObject {
    private _cmdspecs: ICommandSpec[] = [];
    private _commands: Map<number, (hdl: WTK_COMMAND_HANDLER_LOGIC, args: string[], options:Map<string,string[]>)=>Promise<ICommandHandlerResponse>> = new Map()

    bindCommand(cmd: ICommandSpec) {
        if(cmd.id) {
            this._commands.set(cmd.id, cmd.handler);
        }
    }

    async handle(cmd: ICommand): Promise<ICommandHandlerResponse> {
        console.warn(MESSAGE(this, `handle '${cmd.id}'`));

        const h = this._commands.get(cmd.id);

        return h ? h(this, cmd.args, cmd.options) : { table: [{ content: ["0_0"], format: WtkConsoleFormat.MESSAGE }] }; 
    }

    bindCommands(interpreter: WTK_INTERPRETER_LOGIC) {
        this.cmdspecs.map(c=>{
            const id = interpreter.getCommandId(c.name);
            if(id) {
                c.id = id;
                this.bindCommand(c);
            }
        });
    }

    registerCommands(cmds: ICommandSpec[]) {
        this._cmdspecs = cmds;
    }

    get cmdspecs(): ICommandSpec[] {
        return this._cmdspecs;
    }

    async onAutocomplete(line: IConsoleLine, interpreter: WTK_INTERPRETER_LOGIC): Promise<void> {
    }
}
