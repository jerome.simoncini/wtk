import { AppObject } from "../../AppObject.js";
import { WTK_GEN_OBJECT } from "../../_gen_object.js";

export type InputSequence = Set<string | number>;

export interface IInput {
    sequence: (string | number)[];
    cmd: (_: AppObject, ...args: any[]) => boolean;
}

export interface InputFunctor {
    cmd: (...args: any[]) => any,
    caller: AppObject
}

export class WTK_INPUT_LOGIC extends AppObject {
    private _keys: Set<string> = new Set();
    private _buttons: Set<number> = new Set();
    private _mousePos: number[] | undefined = undefined;
    private _mouseUpdate: number[] | undefined = undefined;
    //private _wheelPos: number[] | undefined = undefined;     //
    //private _wheelMove: number[] = [0, 0, 0];                //
    private _sequences: Map<InputSequence, InputFunctor> = new Map();

    onKeyDown(event: KeyboardEvent) {
        this._keys.add(event.key)
    }

    onKeyUp(event: KeyboardEvent) {
        this._keys.delete(event.key)
    }

    onMouseMove(event: MouseEvent) {
        this._mouseUpdate = [event.offsetX, event.offsetY];
    }

    onMouseOut() {
        this._buttons = new Set();
        this._mousePos = undefined;
    }

    onMouseDown(event: MouseEvent) {
        this._buttons.add(event.button)
    }

    onMouseUp(event: MouseEvent) {
        this._buttons.delete(event.button)
    }
    /*
        onMouseWheel(event: WheelEvent) {  ///
            this._wheelPos = [event.deltaX, event.deltaY, event.deltaZ];
        }
    */
    bind(sequence: (string | number)[], cmd: (...args: any[]) => any, caller: AppObject) {
        this._sequences.set(new Set(sequence), { cmd, caller });
    }

    unbind(sequence: (string | number)[]) {
        this._sequences.delete(new Set(sequence));
    }

    process() {
        const mouseMove = (this._mousePos && this._mouseUpdate) ? [this._mousePos[0] - this._mouseUpdate[0], this._mousePos[1] - this._mouseUpdate[1]] : [0, 0];
        this._mousePos = this._mouseUpdate;
        const sequences = this.getSequences();
        for (const sequence of sequences) {
            if (this.inputMatch(sequence)) {
                const f = this._sequences.get(sequence)!;
                if (f.cmd(f.caller, mouseMove)) {    //////
                    return;
                }
            }
        }
    }

    private inputMatch(sequence: InputSequence): boolean {
        /* if(this._keys.size) {
             console.warn(Array.from(this._keys.keys()));
         }
         if(this._buttons.size) {
             console.warn(Array.from(this._buttons.keys()));
         }*/
        let match = true;
        for (const input of sequence) {
            if (typeof input === "string") {
                if (!this._keys.has(input)) {
                    match = false;
                    break;
                }
            }
            else if (typeof input === "number") {
                if (!this._buttons.has(input)) {
                    match = false;
                    break;
                }
            }
        }
        return match;
    }

    private getSequences(): InputSequence[] {
        return Array.from(this._sequences.keys()).sort((a, b) => b.size - a.size);
    }
}

export const WtkInput = WTK_GEN_OBJECT(WTK_INPUT_LOGIC, {
    onCreate: (_: WTK_INPUT_LOGIC): void => {

    }
});