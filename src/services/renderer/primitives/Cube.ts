import { IPrimitiveShapeProps } from "../Primitives.js";
import { PrimitiveShape } from "./PrimitiveShape.js";


export interface ICubeShapeProps extends IPrimitiveShapeProps {
    width: number;
    height: number;
    depth: number;
}

//                                                               Z   Y   X
//               7--------5                                   0: 0,  1,  2
//              /|       /|                                   1: 3,  4,  5
//             3--------1 |                y                  2: 6,  7,  8
//             | 6------|-4             x__|                  3: 9,  10, 11
//             |/       |/                 /                  4: 12, 13, 14
//             2--------0                 z                   5: 15, 16, 17
//                                                            6: 18, 19, 20
//                                                            7: 21, 22, 23


export class CubeShape extends PrimitiveShape {

    genVertices(props: ICubeShapeProps): number[] {
        return [
            -props.width/2, -props.height/2, props.depth/2,
            -props.width/2, -props.height/2, props.depth/2,
            -props.width/2, -props.height/2, props.depth/2,
            -props.width/2, props.height/2, props.depth/2,
            -props.width/2, props.height/2, props.depth/2,
            -props.width/2, props.height/2, props.depth/2,
            props.width/2, -props.height/2, props.depth/2,
            props.width/2, -props.height/2, props.depth/2,
            props.width/2, -props.height/2, props.depth/2,
            props.width/2, props.height/2, props.depth/2,
            props.width/2, props.height/2, props.depth/2,
            props.width/2, props.height/2, props.depth/2,
            -props.width/2, -props.height/2, -props.depth/2,
            -props.width/2, -props.height/2, -props.depth/2,
            -props.width/2, -props.height/2, -props.depth/2,
            -props.width/2, props.height/2, -props.depth/2,
            -props.width/2, props.height/2, -props.depth/2,
            -props.width/2, props.height/2, -props.depth/2,
            props.width/2, -props.height/2, -props.depth/2,
            props.width/2, -props.height/2, -props.depth/2,
            props.width/2, -props.height/2, -props.depth/2,
            props.width/2, props.height/2, -props.depth/2,
            props.width/2, props.height/2, -props.depth/2,
            props.width/2, props.height/2, -props.depth/2
        ];
    }
    genUvCoords(props: ICubeShapeProps): number[] {
        return [
        ];
    }
    genNormals(props: ICubeShapeProps): number[] {
        return [
            // 0
            0, 0, -1,
            0, -1, 0,
            1, 0, 0,
            // 1
            0, 0, -1,
            0, 1, 0,
            1, 0, 0,
            // 2
            0, 0, -1,
            0, -1, 0,
            -1, 0, 0,
            // 3
            0, 0, -1,
            0, 1, 0,
            -1, 0, 0,
            // 4
            0, 0, 1,
            0, -1, 0,
            1, 0, 0,
            // 5
            0, 0, 1,
            0, 1, 0,
            1, 0, 0,
            // 6
            0, 0, 1,
            0, -1, 0,
            -1, 0, 0,
            // 7
            0, 0, 1,
            0, 1, 0,
            -1, 0, 0
        ];
    }
    genIndices(props: ICubeShapeProps): number[] {

        return [
            // front
            0, 6, 3,
            3, 6, 9,
            // back
            18, 12, 21,
            21, 12, 15,
            // left
            8, 20, 11,
            11, 20, 23,
            // right
            14, 2, 17,
            17, 2, 5,
            // top
            22, 16, 10,
            10, 16, 4,
            // bottom
            7, 1, 19,
            19, 1, 13
        ];
    }

}
