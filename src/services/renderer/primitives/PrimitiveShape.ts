import { Mask } from "../../../utils.js";
import { IPrimitiveShape, IPrimitiveShapeProps, PrimitiveShapeDataFlag } from "../Primitives.js";

export abstract class PrimitiveShape {
    create(flags: Mask, props: IPrimitiveShapeProps): IPrimitiveShape {

        const data: Map<PrimitiveShapeDataFlag, number[]> = new Map();

        if (flags.has(PrimitiveShapeDataFlag.VERTICES)) {
            data.set(PrimitiveShapeDataFlag.VERTICES, this.genVertices(props));
        }

        if (flags.has(PrimitiveShapeDataFlag.UV_COORDS)) {
            data.set(PrimitiveShapeDataFlag.UV_COORDS, this.genUvCoords(props));
        }

        if (flags.has(PrimitiveShapeDataFlag.NORMALS)) {
            data.set(PrimitiveShapeDataFlag.NORMALS, this.genNormals(props));
        }

        if (flags.has(PrimitiveShapeDataFlag.INDICES)) {
            data.set(PrimitiveShapeDataFlag.INDICES, this.genIndices(props));
        }

        return {
            vertices: data.get(PrimitiveShapeDataFlag.VERTICES) || [],
            uvcoords: data.get(PrimitiveShapeDataFlag.UV_COORDS) || [],
            normals: data.get(PrimitiveShapeDataFlag.NORMALS) || [],
            indices: data.get(PrimitiveShapeDataFlag.INDICES) || [],
        }
    }

    abstract genVertices(props: IPrimitiveShapeProps): number[];
    abstract genUvCoords(props: IPrimitiveShapeProps): number[];
    abstract genNormals(props: IPrimitiveShapeProps): number[];
    abstract genIndices(props: IPrimitiveShapeProps): number[];
}
