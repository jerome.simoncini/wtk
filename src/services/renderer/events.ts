
export enum SceneEvent {
    ROTATE_CAMERA = 1000,
    TRANSLATE_CAMERA = 1001
}




export enum RendererEvent {
    WEBGL2_NOT_SUPPORTED = 10000,
    EXIT = 10001,                       //
}