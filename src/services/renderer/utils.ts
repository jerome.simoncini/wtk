import { CameraType, ICameraProjection, ICameraView, IOrthographicCameraProps, IPerspectiveCameraProps } from "./Cameras.js";
import { ISceneNodeTransform, SceneNode } from "./scenes/SceneGraph.js";


export interface IVec3 {
    x: number;
    y: number;
    z: number;
}

export interface IVec4 {
    x: number;
    y: number;
    z: number;
    w: number;
}

export interface IColor4 {
    r: number;
    g: number;
    b: number;
    a: number;
}

export interface IColor3 {
    r: number;
    g: number;
    b: number;
}

export class Color {
    private _data: number[] = [];

    constructor(values: number[]) {
        if ([3, 4].includes(values.length)) {
            this._data = values;
        }
    }

    get rgb(): IColor3 {
        return this._data.length >= 3 ? {
            r: this._data[0],
            g: this._data[1],
            b: this._data[2],
        } : {
            r: 0, g: 0, b: 0
        }
    }

    get rgba(): IColor4 {
        return this._data.length == 4 ? {
            r: this._data[0],
            g: this._data[1],
            b: this._data[2],
            a: this._data[3],
        } : {
            r: 0, g: 0, b: 0, a: 1
        }
    }
}

declare const glMatrix: any;
export declare type Mat4 = Float32Array;
export declare type Vec4 = Float32Array;
export declare type Quaternion = number[];

export class Utils {

    //////////////////////////////////
    static buildCameraView(node: SceneNode): Mat4 {
        const model = Array.from(node.worldTransform);
        console.warn(model);
        const direction: number[] = model.slice(8,11);
        const up: number[] = model.slice(4,7);
        const position: number[] = model.slice(12,15);
        const lookAt: number[] = [
            position[0]+direction[0],
            position[1]+direction[1],
            position[2]+direction[2]
        ];

        console.warn(direction);
        console.warn(up);
        console.warn(position);

        const m = Math.max(...up);
        up[0] /= m;
        up[1] /= m;
        up[2] /= m;

        const view = glMatrix.mat4.create();
        glMatrix.mat4.lookAt(view, position, lookAt, up);
        return view;
    }

    //////////////////////////////////
    static buildCameraPosition(node: SceneNode): Vec4 {
        const model = Array.from(node.worldTransform);
        const position = glMatrix.vec4.create();
        glMatrix.vec4.set(position, ...model.slice(12,16));
        return position;
    }

    //////////////////////////////////
    static buildCameraProjection(type: CameraType, setup: ICameraProjection): Mat4 {
        const projection = glMatrix.mat4.create();
        switch(type) {
            case CameraType.ORTHOGRAPHIC:
                {
                    const pSetup = setup as IOrthographicCameraProps;
                    const [left, right, bottom, top] = [
                        pSetup.left,
                        pSetup.right,
                        pSetup.bottom,
                        pSetup.top,
                    ];
                    glMatrix.mat4.ortho(projection, left, right, bottom, top, pSetup.near, pSetup.far);
                }
                break;

            case CameraType.PERSPECTIVE:
                {
                    const pSetup = setup as IPerspectiveCameraProps;
                    const [fov, aspectRatio] = [
                        pSetup.fov * Math.PI / 180,
                        pSetup.aspectRatio
                    ];
                    glMatrix.mat4.perspective(projection, fov, aspectRatio, pSetup.near, pSetup.far);
                }
                break;
        }
        return projection;
    }

    static buildMatrices(model: Float32Array): Float32Array {
      //  console.warn(model);
        const matrices = new Float32Array(model.length);
        matrices.set(model);
        return matrices;
    }

    static m4mul(a: Mat4, b: Mat4, output: Mat4 | undefined = undefined): Mat4 {
        if (output == undefined) {
            output = glMatrix.mat4.create();
        }
        glMatrix.mat4.mul(output!, a, b);
        return output!;
    }
}




//*****************************************************************************************************/




