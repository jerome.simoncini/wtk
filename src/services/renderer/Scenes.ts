import { Constructor } from "../../Constructor.js";
import { AppObject } from "../../AppObject.js";
import { WTK_GEN_OBJECT } from "../../_gen_object.js";
import { Scope } from "../../Scope.js";
import { SCENE_LOGIC } from "./scenes/Scene.js";
import { ASSERT, MESSAGE } from "../../utils.js";
import { CONTEXT, IRendererSetup, RENDERER } from "./Renderer.js";
import { WTK_INPUT_LOGIC } from "../input/InputSystem.js";
import { WtkCanvasContextID } from "../../AppCanvas.js";
import { RendererEvent } from "./events.js";
import { System } from "../../events.js";

export interface SceneSetup {
    name: string;
    state: number;
    pipeline: string;
}

export interface ISceneLoader<S extends SCENE_LOGIC<RENDERER, IRendererSetup>> {
    scene: S;
    context: WtkCanvasContextID;
    renderer: Constructor<RENDERER>;
}

export interface ISceneLoaderSetup<R extends RENDERER, S extends SCENE_LOGIC<R, IRendererSetup>> {
    name: string;
    scene: Constructor<S>;
    renderer: Constructor<R>;
    context: WtkCanvasContextID;
}


export class SCENES_LOGIC extends AppObject {
    private _loaders: Map<string, ISceneLoader<SCENE_LOGIC<RENDERER, IRendererSetup>>> = new Map();     //
    private _current: SCENE_LOGIC<RENDERER, IRendererSetup> | undefined = undefined;

    add<
        T extends RENDERER,
        S extends SCENE_LOGIC<T, IRendererSetup>
    >(
        setup: ISceneLoaderSetup<T, S>) {
        const scene = this.getObjectStore(Scope.LOCAL).get(setup.scene) as S;
        scene.setManager(this);
        this._loaders.set(setup.name, { scene, context: setup.context, renderer: setup.renderer });
    }

    remove(name: string) {
        this._loaders.delete(name);
    }

    getContextId(name: string): WtkCanvasContextID | undefined {
        return this._loaders.get(name)?.context;
    }

    load(name: string, inputSystem: WTK_INPUT_LOGIC, context: CONTEXT): boolean {
        console.warn(MESSAGE(this, `(load) ${name}`));
        if (this._current) {
            this._current.onUnload();
            this._current = undefined;
        }
        const loader = this._loaders.get(name);
        if (loader) {
            //
            loader.scene.setRenderer(loader.renderer);
            loader.scene.setInputSystem(inputSystem);  ////
            loader.scene.onLoad();    ////
            loader.scene.renderer.onInit();
            loader.scene.startRenderer(context);

            this._current = loader.scene;
        }
        else {
            // notify error
        }

        return this.current != undefined;
    }

    unload(name: string) {
        const loader = this._loaders.get(name);
        if (loader?.scene && this._current == loader.scene) {
            this._current.onUnload();
            this._current = undefined;
        }
    }

    unloadCurrent() {
        if (this._current) {
            this._current.onUnload();
            this._current._localStore.clear();
            this._current = undefined;
        }

        /////////
        this.send(System.LOGIC_ACTION, RendererEvent.EXIT);
    }

    clear() {
        if (this._current) {
            this.current.onUnload();
        }
        this.getObjectStore(Scope.LOCAL).clear();
    }

    list(): string[] {
        return Array.from(this._loaders.keys());
    }

    get current(): SCENE_LOGIC<RENDERER, IRendererSetup> {
        return ASSERT(this._current, MESSAGE(this, `current scene undefined`));
    }
}

export const Scenes = WTK_GEN_OBJECT(SCENES_LOGIC, {
    /*services: [

    ],*/
    onCreate: (_: SCENES_LOGIC): void => {
        _.startNotifier([System.LOGIC_ACTION], Scope.GLOBAL);          /////
    },
    onDestroy: (_: SCENES_LOGIC) => {
        _.stopNotifier([System.LOGIC_ACTION], Scope.GLOBAL);          /////
        _.clear();
    },
    createLocalStore: true,
    name: "Scenes"
})