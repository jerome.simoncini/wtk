import { ASSERT, MESSAGE } from "../../../utils.js";
import { AppObject } from "../../../AppObject.js";
import { IVec3, IVec4, Mat4, Quaternion, Utils } from "../utils.js";
import { ICameraView } from "../Cameras.js";


export enum SceneNodeType {
    ENTITY,
    LIGHT,
    CAMERA
}

export interface ISceneNodeTransformSetup {
    position?: number[];
    scale?: number[];
    orientation?: number[];
}

export interface ISceneNodeTransform {
    position: IVec3;
    scale: IVec3;
    orientation: Quaternion;
}

export interface ISceneNodeAttachment {
    type: SceneNodeType;
    name: string;
}

export interface ISceneNodeSetup {
    name: string;
    transform?: ISceneNodeTransformSetup;
    attach?: ISceneNodeAttachment[];
    branch?: ISceneNodeSetup[];
}

export interface ISceneNode {
    transform: ISceneNodeTransform;
    trunk?: SceneNode;
    attach?: ISceneNodeAttachment[];
    branch?: ISceneNode[];
    level?: number;
}

export interface ISceneGraphActors {               ////////
    nodes: SceneNode[];
    entities: ISceneGraphActor[];
    lights: ISceneGraphActor[];
    cameras: ISceneGraphActor[];
}


export interface ISceneGraphActor {
    name: string;
    nodeId: number;
}

export const SCENE_GRAPH_ROOT = 'sg-root';

declare const glMatrix: any;

export class SceneNode {
    transform: ISceneNodeTransform;
    attach: ISceneNodeAttachment[] | undefined;
    trunk: SceneNode | undefined;
    branch: Set<SceneNode>;
    level: number;
    model: Mat4;

    constructor(setup: ISceneNode) {
        Object.assign(this, setup);
        this.model = glMatrix.mat4.create();
    }

    init() {

        this.translate(
            this.worldPosition[0],
            this.worldPosition[1],
            this.worldPosition[2]
        );

        this.rotateFromQuat(this.worldOrientation, this.worldPosition, false);

        this.scale(
            this.worldScale[0],
            this.worldScale[1],
            this.worldScale[2]
        );



        Array.from(this.branch).map(n => {
            n?.init();
        });
    }

    get entities(): string[] {
        let names = this.attach?.filter(a => a.type == SceneNodeType.ENTITY).map(a => a.name) || [];
        Array.from(this.branch).map(n => names = names.concat(n.entities));
        return names;
    }

    get lights(): string[] {
        let names = this.attach?.filter(a => a.type == SceneNodeType.LIGHT).map(a => a.name) || [];
        Array.from(this.branch).map(n => names = names.concat(n.lights));
        return names;
    }

    ///////////
    get cameras(): SceneNode[] {
        let nodes: SceneNode[] = [this];
        Array.from(this.branch).map(n => nodes = nodes.concat(n.cameras));
        return nodes;
    }

    get nodes(): SceneNode[] {
        let list: SceneNode[] = [this];
        Array.from(this.branch).map(n => list = list.concat(n.nodes));
        return list;
    }


    get worldPosition() {
        return [
            this.transform.position.x + (this.trunk?.worldPosition[0] || 0),
            this.transform.position.y + (this.trunk?.worldPosition[1] || 0),
            this.transform.position.z + (this.trunk?.worldPosition[2] || 0)
        ];
    }

    get worldScale() {
        return [
            this.transform.scale.x * (this.trunk?.worldScale[0] || 1),
            this.transform.scale.y * (this.trunk?.worldScale[1] || 1),
            this.transform.scale.z * (this.trunk?.worldScale[2] || 1)
        ];
    }


    get worldOrientation() {
        if (this.trunk) {
            const out = glMatrix.quat.create();
            glMatrix.quat.multiply(out, this.trunk.worldOrientation, this.transform.orientation);
            return out;
        }
        else {
            return this.transform.orientation;
        }
    }

    get worldTransform(): Mat4 {
        return this.model;
    }

    rotate(x: number, y: number, z: number, p: boolean = false, pivot: number[] | undefined = undefined) {

        if (pivot == undefined) {
            const worldTransform = Array.from(this.worldTransform);
            pivot = worldTransform.slice(12, 15);
        }

        const rot = glMatrix.quat.create();
        glMatrix.quat.fromEuler(rot, x, y, z);
        this.rotateFromQuat(rot, pivot, p);
    }

    rotateFromQuat(quat: Quaternion, pivot: number[], p: boolean) {
        const rot = glMatrix.mat4.create();

        glMatrix.mat4.fromQuat(rot, quat);

        const worldTransform = Array.from(this.worldTransform);

        const position = worldTransform.slice(12, 15);

        const origin = glMatrix.vec3.create();

        glMatrix.vec3.set(origin, ...[
            -position[0],
            -position[1],
            -position[2]
        ]);

        const d: Float32Array = glMatrix.vec3.create();
        glMatrix.vec3.set(d, ...[
            position[0] - pivot[0],
            position[1] - pivot[1],
            position[2] - pivot[2]
        ]);

        const m = glMatrix.vec3.length(d) || 0;

        glMatrix.vec3.normalize(d, d);

        glMatrix.vec3.transformQuat(d, d, quat);

        glMatrix.mat4.translate(this.model, this.model, origin);

        glMatrix.mat4.multiply(this.model, rot, this.model);

        const a = Array.from(d);

        this.model[12] = pivot[0] + m * a[0];
        this.model[13] = pivot[1] + m * a[1];
        this.model[14] = pivot[2] + m * a[2];

        glMatrix.quat.add(this.transform.orientation, quat, this.transform.orientation);

        glMatrix.quat.normalize(this.transform.orientation, this.transform.orientation);

        if (p) {
            Array.from(this.branch).map(n => {
                n?.rotateFromQuat(quat, pivot, p);
            });
        }
    }

    translate(x: number, y: number, z: number, p: boolean = false) {
        const worldTransform = Array.from(this.worldTransform);
        const right = worldTransform.slice(0, 3);
        const up = worldTransform.slice(4, 7);
        const forward = worldTransform.slice(8, 11);

        const move: number[] = [
            x * right[0] + x * up[0] + x * forward[0],
            y * right[1] + y * up[1] + y * forward[1],
            z * right[2] + z * up[2] + z * forward[2],
        ];

        const position = glMatrix.vec3.create();
        glMatrix.vec3.set(position, ...move);
        glMatrix.mat4.translate(this.model, this.model, position);

        this.transform.position = {
            x: this.transform.position.x + move[0],
            y: this.transform.position.y + move[1],
            z: this.transform.position.z + move[2],
        };

        if (p) {
            Array.from(this.branch).map(n => {
                n?.translate(x, y, z);
            });
        }
    }

    scale(x: number, y: number, z: number, p: boolean = false) {
        const scale = glMatrix.vec3.create();

        glMatrix.vec3.set(scale, x, y, z);

        glMatrix.mat4.scale(this.model, this.model, scale);

        if (p) {
            Array.from(this.branch).map(n => {
                n?.scale(x, y, z);
            });
        }
    }
}

export abstract class SCENE_GRAPH extends AppObject {
    private _getByName: Map<string, SceneNode> = new Map();
    private _root: SceneNode | undefined = undefined;

    load(nodes: ISceneNodeSetup[]) {
        this._root = this.createNode({ name: SCENE_GRAPH_ROOT, branch: nodes });
        this.root.init();
    }

    abstract build(camera: ICameraView | undefined): Promise<ISceneGraphActors>;

    createNode(setup: ISceneNodeSetup, parent?: SceneNode): SceneNode {
        const node: SceneNode = new SceneNode({
            trunk: parent,

            //////////////////////
            transform: {
                position: setup.transform?.position?.length || 0 >= 3 ? {
                    x: setup.transform?.position![0]!,
                    y: setup.transform?.position![1]!,
                    z: setup.transform?.position![2]!,
                } : {
                    x: 0,
                    y: 0,
                    z: 0
                },
                scale: setup.transform?.scale?.length || 0 >= 3 ? {
                    x: setup.transform?.scale![0]!,
                    y: setup.transform?.scale![1]!,
                    z: setup.transform?.scale![2]!,
                } : {
                    x: 1,
                    y: 1,
                    z: 1
                },
                orientation: (() => {
                    const e = setup.transform?.orientation?.length || 0 >= 3 ?
                        [
                            setup.transform?.orientation![0]!,
                            setup.transform?.orientation![1]!,
                            setup.transform?.orientation![2]!
                        ] : [0, 0, 0];
                    const o = glMatrix.quat.create();
                    glMatrix.quat.fromEuler(o, ...e);
                    return o;
                })()
            },


            attach: setup.attach
        });

        node.branch = new Set(setup.branch?.map((n, index) => this.createNode(n, node)) || []);

        this._getByName.set(setup.name, node);

        return node;
    }

    destroyNode(name: string) {
        const node = this._getByName.get(name);
        if (node) {
            this._getByName.delete(name);
            if (node.trunk) {
                node.trunk.branch.delete(node)
            }
            else {
                this._root = undefined;
            }
        }
    }

    get(name: string): SceneNode | undefined {
        return this._getByName.get(name);
    }

    getActors(): ISceneGraphActors {
        const entities: ISceneGraphActor[] = [];
        const lights: ISceneGraphActor[] = [];
        const cameras: ISceneGraphActor[] = [];
        const nodes: SceneNode[] = [];

        this.nodes.map((n) => {

            if (n.attach?.find(a =>
                (a.type == SceneNodeType.CAMERA) ||
                (a.type == SceneNodeType.LIGHT) ||
                (a.type == SceneNodeType.ENTITY)
            )) {
                const id = nodes.push(n) - 1;

                n.attach?.filter(a => a.type == SceneNodeType.ENTITY).map(e => {
                    entities.push({
                        name: e.name,
                        nodeId: id
                    })
                });

                n.attach?.filter(a => a.type == SceneNodeType.LIGHT).map(e => {
                    lights.push({
                        name: e.name,
                        nodeId: id
                    })
                });

                n.attach?.filter(a => a.type == SceneNodeType.CAMERA).map(e => {
                    cameras.push({
                        name: e.name,
                        nodeId: id
                    })
                });
            }
        });

        return {
            nodes,
            entities,
            lights,
            cameras,
        };
    }

    protected get nodes(): SceneNode[] {
        return this.root.nodes as SceneNode[];
    }

    get root(): SceneNode {
        return ASSERT(this._root, MESSAGE(this, `root node undefined`));
    }

    get entities(): Set<string> {
        return new Set(this.root.entities);
    }

    get cameras(): SceneNode[] {
        return this.root.cameras;
    }
}
