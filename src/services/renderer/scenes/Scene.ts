import { Constructor } from "../../../Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "../../../_gen_object.js";
import { AppObject } from "../../../AppObject.js";
import { __skull__ } from "../../../_skull.js";
import { ASSERT, DoubleBuffer, MESSAGE } from "../../../utils.js";
import { ObjectFactory } from "../../../ObjectFactory.js";
import { IResource } from "../Resources.js";
import { IGLScene } from "../webgl/Renderer.js";
import { ISceneGraphActors, ISceneGraphActor, ISceneNodeSetup, SCENE_GRAPH, SceneNode } from "./SceneGraph.js";
import { BspTree } from "./graphs/BSPTree.js";
import { Utils } from "../utils.js";
import { CONTEXT, IRendererSetup, IViewport, RENDERER, RENDERER_LOGIC, RenderFlag } from "../Renderer.js";
import { IInput, WTK_INPUT_LOGIC } from "../../input/InputSystem.js";
import { Scope } from "../../../Scope.js";
import { IPrimitive } from "../Primitives.js";
import { MaterialType } from "../Materials.js";
import { ICameraResource } from "../Cameras.js";
import { SCENES_LOGIC } from "../Scenes.js";
import { System } from "../../../events.js";

export interface IScene extends IAppObject {
    resources?: IResource[];
    primitives?: IPrimitive[];
    states: ISceneState[];
    inputs?: IInput[];
}

export interface ISceneState {
    name: number;
    lighting: MaterialType;
    gl?: IGLScene;
    // webgpu?: any;    //    
    nodes?: ISceneNodeSetup[];
    sceneGraph?: Constructor<AppObject>;
    inputs?: IInput[];
}



export interface ISceneDefaults {
    state: number;
    pipeline: string;
}

export enum SceneTick {
    ANIMATE
}

export interface ISceneActors {
    data: number[];
    cameras: ISceneGraphActor[];
    lights: ISceneGraphActor[];
    entities: ISceneGraphActor[];
}

const ANIMATION_PERIOD = 1;

export abstract class SCENE_LOGIC<R extends RENDERER_LOGIC<CONTEXT, G>, G extends IRendererSetup> extends AppObject {
    private _resources: IResource[] | undefined = undefined;
    private _primitives: IPrimitive[] | undefined = undefined;
    private _graphics: Map<number, G> = new Map();
    private _states: Map<number, ISceneState> = new Map();
    private _sceneGraphs: Map<number, SCENE_GRAPH> = new Map();
    private _currentState: DoubleBuffer<ISceneState | undefined> = new DoubleBuffer();
    private _nodes: DoubleBuffer<ISceneGraphActors | undefined> = new DoubleBuffer();
    private _currentPipeline: string | undefined = undefined;     //
    private _inputs: IInput[] = [];
    private _renderer: RENDERER_LOGIC<CONTEXT, G> | undefined = undefined;
    private _inputSystem: WTK_INPUT_LOGIC | undefined = undefined;
    private _defaults: ISceneDefaults | undefined = undefined;
    private _ticks: Map<SceneTick, number> = new Map();
    private _animationPeriod: number = ANIMATION_PERIOD;    //
    private _manager: SCENES_LOGIC | undefined = undefined;

    setRenderer(renderer: Constructor<R>) {
        this._renderer = this.getObjectStore(Scope.LOCAL).get(renderer);
    }

    setSceneGraph(state: number, sceneGraph: SCENE_GRAPH) {
        this._sceneGraphs.set(state, sceneGraph);
    }

    setInputSystem(inputSystem: WTK_INPUT_LOGIC) {
        this._inputSystem = inputSystem;
    }

    getSceneGraph(state: number): SCENE_GRAPH | undefined {
        return this._sceneGraphs.get(state);
    }

    protected setDefaults(defaults: ISceneDefaults) {
        this._defaults = defaults;
    }

    registerState(state: ISceneState) {
        console.warn(`(register state) ${state.name}`);
        this._states.set(state.name, state);
    }

    registerInputs(inputs: IInput[]) {
        this._inputs = inputs;
    }

    bindInputs(inputs: IInput[] = []) {
        for (const input of inputs) {
            this.inputSystem.bind(input.sequence, input.cmd, this);
        }
    }

    unbindInputs(inputs: IInput[] = []) {
        for (const input of inputs) {
            this.inputSystem.unbind(input.sequence);
        }
    }

    startRenderer(context: CONTEXT) {
        console.warn(MESSAGE(this, `start renderer`));
        this.renderer.start(context);
        this.renderer.loadScene(this);
        this.setCurrentState(this.defaults.state, this.defaults.pipeline);
        this.syncState();

        ///////////
        this.graph.build(undefined).then((nodes) => {
            this.updateNodes(nodes);
            this.renderer.flags.setBits(RenderFlag.SYNC_SCENE_GRAPH);
        });
    }

    stopRenderer() {

    }

    onLoad() {    /////
        this._nodes.set({
            nodes: [],
            entities: [],
            lights: [],
            cameras: []
        });
        this._nodes.swap();

        this.bindInputs(this._inputs);
    }

    onUnload() {   /////
        let inputs = this._inputs;

        const state = this._currentState.get();
        if (state && state.inputs) {
            inputs = inputs.concat(state.inputs);
        }

        this.unbindInputs(this._inputs);
    }

    setCurrentState(name: number, pipeline: string) {      //////////////
        console.warn(MESSAGE(this, `(set state) ${name}`));
        console.warn(`(set current state) ${name}`);
        const state = this._states.get(name);
        if (state) {
            this._currentState.set(state);
            this._currentPipeline = pipeline;         //
            this.renderer.flags.setBits(RenderFlag.SYNC_SCENE_STATE);
        }
        else {
            // error
        }
    }

    setCurrentPipeline(name: string, syncRenderState: boolean = false) {      ///////////
        console.warn(MESSAGE(this, `(setCurrentPipeline) ${name}`));
        this._currentPipeline = name;
        if (syncRenderState) {     ///////////////
            this.renderer.flags.setBits(RenderFlag.SYNC_RENDER_STATE);
        }
    }

    syncState(): boolean {               //////
        console.warn(MESSAGE(this, `(sync state)`));

        let state = this._currentState.get();
        if (state) {
            this.unbindInputs(state.inputs);
        }

        this._currentState.swap();

        state = this._currentState.get();
        if (state) {
            this.bindInputs(state.inputs);
            const p = this._currentPipeline ?? (state.gl ? state.gl.pipelines[0].name : undefined);
            if (p) {
                this.setCurrentPipeline(p);
            }
            else {
                // notify error
            }
        }

        return this._currentState.get() != undefined;
    }

    setManager(manager: SCENES_LOGIC) {
        this._manager = manager;
    }

    setResources(resources: IResource[]) {
        this._resources = resources;
    }

    setPrimitives(primitives: IPrimitive[]) {
        this._primitives = primitives;
    }

    loadGraphics(state: number, graphics: G) {
        this._graphics?.set(state, graphics);
    }

    updateNodes(nodes: ISceneGraphActors): void {
        this._nodes.set(nodes);
    }

    syncNodes(): void {
        this._nodes.swap();
    }

    ////////////////
    quit() {
        console.warn('quit');
        this.manager.unloadCurrent();
    }

    async updateScene(): Promise<ISceneActors> {

        const data = this.nodes.nodes.map((node: SceneNode) => Utils.buildMatrices(                                  //////
            node.worldTransform
        ));

        return {
            data: data.map(d => Array.from(d)).flat(),
            cameras: this.nodes.cameras,
            lights: this.nodes.lights,
            entities: this.nodes.entities,
        }
    }

    resize(viewport: IViewport) {
        this.renderer.resize(viewport.width, viewport.height);   ///
    }

    //abstract onLoad(): void;
    //abstract onUnload(): void;

    animate() {
        if (this._animationPeriod-- == 0) {
            const elapsed = this.updateTick(SceneTick.ANIMATE);
            this.onAnimate(elapsed);
            this._animationPeriod = ANIMATION_PERIOD;
        }
    }

    update() {
        this.renderer.flags.setBits(RenderFlag.UPDATE_SCENE);
    }

    updateTick(tick: SceneTick) {
        let elapsed: number = 0;
        const now = Date.now();
        const past = this._ticks.get(tick);
        if (past) {
            elapsed = past - now;
        }
        this._ticks.set(tick, now);
        return elapsed;
    }

    onAnimate(elapsed: number): void { }

    onSceneUpdate(...args: any[]) {    //////////////////
        throw MESSAGE(this, `(onSceneUpdate) not implemented`);
    }

    get resources(): IResource[] {
        return ASSERT(this._resources, MESSAGE(this, `resources undefined`));
    }

    get primitives(): IPrimitive[] {
        return ASSERT(this._primitives, MESSAGE(this, `primitives undefined`));
    }

    get graphics(): G[] {
        return Array.from(this._graphics.values());
    }

    get state(): ISceneState {
        return ASSERT(this._currentState.get(), MESSAGE(this, `current state undefined`));
    }

    get graph(): SCENE_GRAPH {
        const sceneGraph = this._sceneGraphs.get(this.state.name);
        return ASSERT(sceneGraph, MESSAGE(this, `current scene graph undefined`));
    }

    get nodes(): ISceneGraphActors {
        return ASSERT(this._nodes.get(), MESSAGE(this, `scene nodes undefined`));
    }

    ///////////////////////////
    get camera(): ICameraResource {
        const cameras = this.state.gl?.cameras || [];
        return ASSERT(cameras.find(c => c.selected == true), MESSAGE(this, 'current camera undefined'));
    }

    get states() {
        return Array.from(this._states.values());
    }

    get pipeline() {
        //return ASSERT(this._currentPipeline, MESSAGE(this, `current pipeline undefined`));
        return this._currentPipeline;
    }

    get renderer(): R {
        return ASSERT(this._renderer, MESSAGE(this, `renderer undefined`));
    }
    /*
        private get syncFlags(): SceneSyncFlag[] {
            return [...this._syncFlags];
        }
    */
    get defaults(): ISceneDefaults {
        return ASSERT(this._defaults, MESSAGE(this, `defaults undefined`));
    }

    get inputSystem(): WTK_INPUT_LOGIC {
        return ASSERT(this._inputSystem, MESSAGE(this, `input system undeifned`));
    }

    private get manager(): SCENES_LOGIC {
        return ASSERT(this._manager, MESSAGE(this, `manager undeifned`));
    }
}



export const gen_scene = <T extends SCENE_LOGIC<RENDERER, IRendererSetup>>(scene: Constructor<T>, settings: IScene): Constructor<T> => {
    const skull = __skull__(scene, {
        onCreate: _create_scene_,
        onInit: _init_scene_,
        settings: settings
    }) as Constructor<T>;
    return WTK_GEN_OBJECT(skull, {
        ...settings,
        listen: [
        ],
        onDestroy: (_: SCENE_LOGIC<RENDERER, IRendererSetup>): void => {
            _.getObjectStore(Scope.LOCAL).clear();
        },
        createLocalStore: true
    }) as Constructor<T>
}

export const _create_scene_ = (scene: SCENE_LOGIC<RENDERER, IRendererSetup>, settings: IScene) => {
    scene.setResources(settings.resources || []);
    scene.setPrimitives(settings.primitives || []);

    for (const state of settings.states) {
        let graphics: IRendererSetup | undefined = undefined;
        if (state.gl) {
            graphics = state.gl;
        }
        if (graphics) {
            scene.loadGraphics(state.name, graphics);

            scene.registerState(state);

            const sceneGraph = scene.___(ObjectFactory).create(state.sceneGraph ?? BspTree) as SCENE_GRAPH;   ///////
            if (sceneGraph) {
                scene.setSceneGraph(state.name, sceneGraph);

                if (state.nodes) {
                    sceneGraph.load(state.nodes);
                }
            }
            else {
                // notify error
            }
        }
        if (settings.inputs) {
            scene.registerInputs(settings.inputs);
        }
    }
}

export const _init_scene_ = async (scene: SCENE_LOGIC<RENDERER, IRendererSetup>, settings: IScene) => {
}

