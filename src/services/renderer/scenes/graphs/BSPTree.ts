import { MESSAGE } from "../../../../utils.js";
import { WTK_GEN_OBJECT } from "../../../../_gen_object.js";
import { ISceneGraphActors, SCENE_GRAPH } from "../SceneGraph.js";
import { ICameraView } from "../../Cameras.js";



export class BSP_TREE_LOGIC extends SCENE_GRAPH {
   async build(camera: ICameraView|undefined): Promise<ISceneGraphActors> {
      //console.warn(MESSAGE(this,JSON.stringify(this.nodes)))

      const actors = this.getActors();

      console.warn(actors);

      return actors;
   }
}

export const BspTree = WTK_GEN_OBJECT(BSP_TREE_LOGIC, {
   name: 'BSP Tree'
})