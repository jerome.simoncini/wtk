import { Constructor } from "../../../Constructor.js";
import { ASSERT, MESSAGE } from "../../../utils.js";
import { WGLEnum } from "./enum.js";
import { IWGLTextureBinding, WGL_TEXTURES, WglTextures } from "./Textures.js";
import { WGL_FRAMEBUFFERS, WglFramebuffers } from "./Framebuffers.js";
import { IWGLDrawSubPassUniform, WEBGL2_RENDERER, WglStateFlag } from "./Renderer.js";
import { IRendererResource, IRendererResourceSetup, RENDERER_RESOURCES, gen_renderer_resources } from "../RendererResources.js";


export interface IWGLProgramResource {
    name: string;
    shaders: string[];
    storage?: IWGLProgramStorage[];
    uniforms?: IWGLProgramUniform[];
    framebuffer?: IWGLBindFramebuffer;
}

export interface IWGLProgram extends IRendererResource {
    shaders: string[];
    storage: IWGLProgramStorage[];
    uniforms: IWGLProgramUniform[];
    framebuffer?: IWGLBindFramebuffer;
    glProgram: WebGLProgram;
    locations?: Map<string, IWGLProgramUniformLocation>;
}

export interface IWGLProgramSetup extends IRendererResourceSetup {
    shaders: WebGLShader[];
    storage: IWGLProgramStorage[];
    uniforms: IWGLProgramUniform[];
    framebuffer?: IWGLBindFramebuffer;
}


export interface IWGLProgramUniformLocation {
    location: WebGLUniformLocation;
    type: WGLEnum;
}

export interface IWGLProgramUniform {
    name: string,
    type: WGLEnum
}

export interface IWGLBindFramebuffer {
    name: string;
    bindings?: IWGLFramebufferProgramBinding[];
}

export interface IWGLFramebufferProgramBinding {
    texture: string;
    location: string;
    index: number;
    usage: WGLEnum;
    glLocation?: WebGLUniformLocation;

}

export interface IWGLProgramStorage {
    name: string;
    location: string;
    usage: WGLEnum;
    data?: BufferSource;
    binding: number;
    glLocation?: WebGLUniformLocation | number;
}



export class WGL_PROGRAMS extends RENDERER_RESOURCES<
    WebGL2RenderingContext,
    IWGLProgram,
    IWGLProgramSetup
> {

    protected createResource(context: WebGL2RenderingContext, timestamp: number): IWGLProgram | undefined {
        const glProgram = context.createProgram();
        if (glProgram) {
            return {
                timestamp,
                glProgram,
                shaders: [],
                storage: [],
                uniforms: []
            }
        }
    }

    protected unloadResource(context: WebGL2RenderingContext, program: IWGLProgram): void {
        context.deleteProgram(program.glProgram);
    }

    protected async editResource(context: WebGL2RenderingContext, setup: IWGLProgramSetup): Promise<void> {
        const program = this.get(setup.name);
        if (program) {
            for (const shader of setup.shaders) {
                context.attachShader(program.glProgram, shader);
            }
            context.linkProgram(program.glProgram);
            if (context.getProgramParameter(program.glProgram, context.LINK_STATUS)) {

                for (const storage of setup.storage) {
                    switch (storage.usage) {
                        case WGLEnum.DYNAMIC_UBO:
                            {
                                storage.glLocation = context.getUniformBlockIndex(program.glProgram, storage.location);
                                context.uniformBlockBinding(program.glProgram, storage.glLocation as number, storage.binding);
                            }
                            break;
                        case WGLEnum.TEXTURE_1D:
                        case WGLEnum.TEXTURE_2D:
                        case WGLEnum.TEXTURE_2D_ARRAY:
                        case WGLEnum.TEXTURE_3D:
                            {
                                const location = context.getUniformLocation(program.glProgram, storage.location);
                                if (location) {
                                    storage.glLocation = location;
                                }
                            }
                            break;
                    }
                }

                if (setup.uniforms) {
                    program.locations = new Map();

                    for (const uniform of setup.uniforms) {
                        const location = context.getUniformLocation(program.glProgram, uniform.name);
                        if (location) {
                            program.locations.set(uniform.name, { location, type: uniform.type });
                        }
                        else {
                            // notify error
                        }
                    }
                }
                
                program.storage = setup.storage;
                program.uniforms = setup.uniforms;
                program.framebuffer = setup.framebuffer;

                console.warn(program);
            }
            else {
                console.warn(context.getProgramInfoLog(program.glProgram));
                this.unload(context, setup.name);
            }

        }
        else {
            // notify error
        }
    }

    /******************************************************************************/

    private _current: IWGLProgram | undefined = undefined;

    use(name: string, renderer: WEBGL2_RENDERER): void {
        console.warn(name);

        const program = this.get(name);
        const context = renderer.context;

        console.warn(program);

        if (context && program && program.glProgram) {

            context.useProgram(program.glProgram);

            for (const storage of program.storage) {
                switch (storage.usage) {
                    case WGLEnum.TEXTURE_1D:
                    case WGLEnum.TEXTURE_2D:
                    case WGLEnum.TEXTURE_2D_ARRAY:
                    case WGLEnum.TEXTURE_3D:
                        {
                            //console.warn(storage);
                            if (!this.textures.bind(context, storage as IWGLTextureBinding)) {
                                // notify error
                            }
                        }
                        break;
                }
            }

            if (program.framebuffer) {

                const fb = this.framebuffers.get(program.framebuffer.name);
                if (fb && fb.glFb) {
                    context.bindFramebuffer(context.FRAMEBUFFER, fb.glFb);
                    renderer['useState'](WglStateFlag.DEPTH_MASK, true);        //
                    renderer['useState'](WglStateFlag.BLEND, false);            //
                }
                else {
                    // notify error
                }
            }
            else {
                context.bindFramebuffer(context.FRAMEBUFFER, null);   //
                // renderer['useState'](WglStateFlag.DEPTH_MASK, false);     //
                // renderer['useState'](WglStateFlag.BLEND, true);           //
            }
        }
        else {
            // notify error
        }

        this._current = program;
    }


    setUniform(context: WebGL2RenderingContext, program: IWGLProgram, uniform: IWGLDrawSubPassUniform) {
        const programUniform = program.locations?.get(uniform.name);
        if (programUniform) {
            switch (programUniform.type) {
                case WGLEnum.U1:
                    if (Array.isArray(uniform.value) && uniform.value.length >= 1) {
                        const value = uniform.value[0];
                        context.uniform1ui(programUniform.location, value);
                    }
                    break;

                case WGLEnum.VEC3:
                    if (Array.isArray(uniform.value) && uniform.value.length >= 3) {
                        const value: number[] = uniform.value.slice(0, 3);
                        context.uniform3fv(programUniform.location, value);
                    }
                    break;

                case WGLEnum.VEC4:
                    if (Array.isArray(uniform.value) && uniform.value.length >= 4) {
                        const value: number[] = uniform.value.slice(0, 4);
                        context.uniform4fv(programUniform.location, value);
                    }
                    break;
            }
        }
        else {
            // notify error
        }
    }


    get current(): IWGLProgram | undefined {
        return this._current;
    }

    /************************************************************************/

    private _textures: WGL_TEXTURES | undefined = undefined;
    private _framebuffers: WGL_FRAMEBUFFERS | undefined = undefined;

    setTextures(textures: WGL_TEXTURES) {
        this._textures = textures;
    }

    setFramebuffers(framebuffers: WGL_FRAMEBUFFERS) {
        this._framebuffers = framebuffers;
    }

    private get textures(): WGL_TEXTURES {
        return ASSERT(this._textures, MESSAGE(this, `textures manager undefined`));
    }

    private get framebuffers(): WGL_FRAMEBUFFERS {
        return ASSERT(this._framebuffers, MESSAGE(this, `framebuffers manager undefined`));
    }

}

export const WglPrograms = gen_renderer_resources(WGL_PROGRAMS, {
    onCreate: (_: WGL_PROGRAMS): void => {
        _.setTextures(_.___(WglTextures) as WGL_TEXTURES);
        _.setFramebuffers(_.___(WglFramebuffers) as WGL_FRAMEBUFFERS);
    },
    name: "WglPrograms"
});