import { ASSERT, DoubleBuffer, MESSAGE } from "../../../../utils.js";
import { RenderFlag, gen_renderer } from "../../Renderer.js";
import { ISubEntity, IWGLDrawPass, IWGLDrawSubPass, IWGLSubMeshSetup, WEBGL2_RENDERER, WGLDrawCommandType, WGLPrimitiveType, WglStateFlag } from "../Renderer.js";
import { IWGLProgram } from "../Programs.js";
import { ISceneActors } from "../../scenes/Scene.js";
import { LightType } from "../../Lights.js";
import { SceneNodeType } from "../../scenes/SceneGraph.js";
import { WGLEnum } from "../enum.js";


export interface IRenderGroup {
    name: string;
    instances: Float32Array;
    type: SceneNodeType;
}


export interface IR1Groups {
    transforms: number[];
    bindings: number[];
    entities: IR1GroupSetup<string>[];
    lights: IR1GroupSetup<LightType>[];
    cameras: IR1GroupSetup<string>[];
}

export interface IR1GroupSetup<T> {
    name: T;
    instanceCount: number;
    offset?: number;
}

export interface IR1Binding {
    nodeId: number;
    lightId: number;
    materialId: number;
    cameraId: number;
}


export class WGL2_R1 extends WEBGL2_RENDERER {
    private _renderGroups: DoubleBuffer<IR1Groups | undefined> = new DoubleBuffer();

    updateRenderGroups(groups: IR1Groups) {
        this._renderGroups.set(groups);
    }

    swapRenderGroups(): void {
        this._renderGroups.swap();
    }

    syncScene(): void {
        this.scene.onSceneUpdate(this.renderGroups);
    }


    ///////////////////////////////////////////////////////////////////////////////
    protected renderNodes(draw: IWGLDrawPass, pass: IWGLDrawSubPass, program: IWGLProgram) {

        const gl = this.context;

        gl.frontFace(gl.CCW);

        this['useState'](WglStateFlag.CULL_FACE, gl.BACK);   //

        if (program.locations?.has("WGL_MaterialType")) {
            gl.uniform1ui(program.locations.get("WGL_MaterialType")!.location, this.scene.state.lighting as number);
        }

        //console.warn(scene.nodes);

        //console.warn(this.renderGroups.entities);
        //console.warn(draw.cmds);

        for (const group of this.renderGroups.entities) {   ////

            if (group) {

                //console.warn(group);

                if (program.locations?.has("WGL_RenderGroupId")) {
                    gl.uniform1ui(program.locations.get("WGL_RenderGroupId")!.location, group.offset || 0);
                }

                const cmds = draw.cmds?.get(group.name) || [];
                //console.warn(MESSAGE(this, `renderGroup=${renderGroup} count=${instancesCount}`));

                for (const cmd of cmds) {
                    //console.warn(cmd);
                    switch (cmd.type) {

                        case WGLDrawCommandType.ARRAYS:
                        case WGLDrawCommandType.ARRAYS_INSTANCED: {
                            const setup = cmd.buffers.get(this.context.ARRAY_BUFFER);
                            if (setup) {
                                const buffer = this.buffers.getSubMeshSetup(setup.name);
                                if (buffer && this.arrays.bind(gl, setup.name)) {
                                    const setup = buffer.setup as IWGLSubMeshSetup;       ///
                                    //console.warn(setup);
                                    const primitiveType = this.getPrimitiveType(gl, setup.primitiveType) || gl.POINTS;
                                    switch (cmd.type) {
                                        case WGLDrawCommandType.ARRAYS:
                                            gl.drawArrays(primitiveType, setup.first || 0, setup.count || 0);
                                            break;
                                        case WGLDrawCommandType.ARRAYS_INSTANCED:
                                            gl.drawArraysInstanced(primitiveType, setup.first || 0, setup.count || 0, group.instanceCount);
                                            break;
                                    }
                                }
                            }
                            else {
                                // notify error
                            }
                        }
                            break;

                        case WGLDrawCommandType.ELEMENTS:
                        case WGLDrawCommandType.ELEMENTS_INSTANCED: {
                            ///////////////////////////////////////
                            const elements = pass.wireframe ? WGLEnum.WIREFRAME : this.context.ELEMENT_ARRAY_BUFFER;
                            const elementSetup = cmd.buffers.get(elements);
                            const arraySetup = cmd.buffers.get(this.context.ARRAY_BUFFER);
                            if (elementSetup && arraySetup) {
                                const subMeshSetup = this.buffers.getSubMeshSetup(arraySetup.name)?.setup as IWGLSubMeshSetup;
                                if (subMeshSetup) {
                                    if (this.arrays.bind(gl, arraySetup.name) && this.buffers.bind(gl, gl.ELEMENT_ARRAY_BUFFER, elementSetup.name)) {
                                        //////////////////////////////////////////////////////////
                                        const [
                                            count,
                                            primitiveType
                                        ] = pass.wireframe ?
                                                [
                                                    subMeshSetup.wireframeCount,
                                                    this.getPrimitiveType(gl, WGLPrimitiveType.LINES)!
                                                ] :
                                                [
                                                    subMeshSetup.count,
                                                    this.getPrimitiveType(gl, subMeshSetup.primitiveType) || gl.POINTS
                                                ];
                                        //console.warn(setup);
                                        switch (cmd.type) {
                                            case WGLDrawCommandType.ELEMENTS:
                                                gl.drawElements(primitiveType, count || 0, gl.UNSIGNED_INT, subMeshSetup.first || 0);
                                                break;
                                            case WGLDrawCommandType.ELEMENTS_INSTANCED:
                                                gl.drawElementsInstanced(primitiveType, count || 0, gl.UNSIGNED_INT, subMeshSetup.first || 0, group.instanceCount);
                                                break;
                                        }
                                    }
                                    else {
                                        // notify error
                                    }
                                }
                                else {
                                    // notify error
                                }
                            }
                            else {
                                // notify error
                            }
                        }
                            break;

                    }
                }
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////////
    protected computeLighting(draw: IWGLDrawPass, pass: IWGLDrawSubPass, program: IWGLProgram) {
        const gl = this.context;

        gl.enable(gl.BLEND);
        gl.blendFunc(gl.ONE, gl.ONE);

        if (program.locations?.has("WGL_MaterialType")) {
            gl.uniform1ui(program.locations.get("WGL_MaterialType")!.location, this.scene.state.lighting as number);
        }

        if (program.locations?.has("WGL_CameraId")) {
            gl.uniform1ui(program.locations.get("WGL_CameraId")!.location, 0);    /////
        }

        

        for (const group of this.renderGroups.lights) {   ////

            if (group) {

                //console.warn(group);

                if (program.locations?.has("WGL_RenderGroupId")) {
                    gl.uniform1ui(program.locations.get("WGL_RenderGroupId")!.location, group.offset || 0);
                }

                if (program.locations?.has("WGL_LightType")) {
                    gl.uniform1ui(program.locations.get("WGL_LightType")!.location, group.name);   //
                }

                gl.drawArraysInstanced(gl.TRIANGLES, 0, 3, group.instanceCount);
            }
            else {
                // notify error
            }
        }

        gl.disable(gl.BLEND);
    }


    get renderGroups(): IR1Groups {
        return ASSERT(this._renderGroups.get(), MESSAGE(this, `render groups undefined`));
    }

    onSceneUpdate(actors: ISceneActors) {

        //console.warn(actors);

        const cameraNodeId = actors.cameras[0]?.nodeId;   /////////////

        if(cameraNodeId) {

            const bindings: IR1Binding[] = [];
    
            const camerasGroup: Map<string, IR1GroupSetup<string>> = new Map();
            const lightsGroup: Map<LightType, IR1GroupSetup<LightType>> = new Map();
            const entitiesGroup: Map<string, IR1GroupSetup<string>> = new Map();

            actors.cameras.map(a => {                     ///////////////////////
                bindings.push({
                    nodeId: a.nodeId,
                    lightId: 0,
                    materialId: 0,
                    cameraId: 0
                })
                const c = camerasGroup.get(a.name);
                if (c) {
                    c.instanceCount++
                }
                else {
                    camerasGroup.set(a.name, {
                        name: a.name,
                        instanceCount: 1
                    })
                }
            });
    
            actors.lights.map(a => {              ///////////////////////
                bindings.push({
                    nodeId: a.nodeId,
                    lightId: this.lights.index(a.name),
                    materialId: 0,
                    cameraId: cameraNodeId
                })
                const group = lightsGroup.get(LightType.POINT_LIGHT);
                if (group) {
                    group.instanceCount++
                }
                else {
                    lightsGroup.set(LightType.POINT_LIGHT, {
                        name: LightType.POINT_LIGHT,
                        instanceCount: 1
                    })
                }
            });
    
            actors.entities.map(a => {              ///////////////////////
                const subs: ISubEntity[] = this.getSubEntities(a.name);
                if (subs.length) {
                    //console.warn(a.name);
    
                    this.getSubEntities(a.name).map(e => {
                        //console.warn(e);
                        //console.warn(a.nodeId);
                        bindings.push({
                            nodeId: a.nodeId,
                            lightId: 0,
                            materialId: e.materialId,
                            cameraId: cameraNodeId
                        })
                        const group = entitiesGroup.get(e.name);
                        if (group) {
                            group.instanceCount++
                        }
                        else {
                            entitiesGroup.set(e.name, {
                                name: e.name,
                                instanceCount: 1
                            })
                        }
                    });
                }
                else {
                    // notify error
                }
            });
    
            //console.warn(Array.from(entitiesGroup));
    
            let offset = 0;
            [
                camerasGroup,
                lightsGroup,
                entitiesGroup
            ].map(_ => {
                [..._.values()].map(g => {
                    g.offset = offset;
                    offset += g.instanceCount;
                })
            });
    
            this.updateRenderGroups({
                transforms: actors.data,
                bindings: bindings.map(b => [b.nodeId, b.lightId, b.materialId, b.cameraId]).flat(),
                cameras: Array.from(camerasGroup.values()) as IR1GroupSetup<string>[],
                lights: Array.from(lightsGroup.values()) as IR1GroupSetup<LightType>[],
                entities: Array.from(entitiesGroup.values()) as IR1GroupSetup<string>[],
            });
        }
        else {

            this.updateRenderGroups({
                transforms: [],
                bindings: [],
                cameras: [],
                lights: [],
                entities: [],
            });
        }

    }
}



export const WGL2_r1 = gen_renderer(WGL2_R1, {
    defaults: {
        clearColor: {
            r: 0,
            g: 0,
            b: 0,
            a: 0
        }
    },
    onStart: (_: WGL2_R1): void => {
        console.warn(MESSAGE(_, "****************"));
        console.warn(MESSAGE(_, "    ON START   "));
        console.warn(MESSAGE(_, "****************"));

        _.__onStart();     /////////////

        _.ext.add(
            [
                {
                    flag: RenderFlag.SWAP_SCENE,
                    cmd: (renderer: WGL2_R1): void => {
                        renderer.swapRenderGroups();
                    }
                },
                {
                    flag: RenderFlag.SYNC_SCENE,
                    cmd: (renderer: WGL2_R1): void => {
                        renderer.syncScene();
                    }
                }
            ]
        )
    },
    onShutdown: (_: WGL2_R1): void => {
        console.warn(MESSAGE(_, "******************"));
        console.warn(MESSAGE(_, "    ON SHUTDOWN   "));
        console.warn(MESSAGE(_, "******************"));

        _.__onShutdown();     //////////////
    },
    name: "WGL2_r1"
});