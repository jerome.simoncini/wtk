import { ASSERT, MESSAGE } from "../../../utils.js";
import { WGLTextureType, WGL_TEXTURES, WglTextures } from "./Textures.js";
import { IRendererResource, IRendererResourceSetup, RENDERER_RESOURCES, gen_renderer_resources } from "../RendererResources.js";


export enum WGLFramebufferAttachment {
    COLOR_0,
    COLOR_1,
    COLOR_2,
    COLOR_3,
    COLOR_4,
    COLOR_5,
    COLOR_6,
    COLOR_7,
    COLOR_8,
    COLOR_9,
    COLOR_10,
    COLOR_11,
    COLOR_12,
    COLOR_13,
    COLOR_14,
    COLOR_15,
    DEPTH,
    STENCIL,
    DEPTH_STENCIL,
    RENDERBUFFER
}

export interface IWGLFramebufferBinding {
    name: string;
    attachments: WGLFramebufferAttachment[],
    type: WGLTextureType
}

export interface IWGLFramebufferResource {
    name: string;
    bindings: IWGLFramebufferBinding[];
}

export interface IWGLFramebuffer extends IRendererResource {
    glFb: WebGLFramebuffer;
    bindings: IWGLFramebufferBinding[];
}

export interface IWGLFramebufferSetup extends IRendererResourceSetup {
    bindings: IWGLFramebufferBinding[];
}


export class WGL_FRAMEBUFFERS extends RENDERER_RESOURCES<
    WebGL2RenderingContext,
    IWGLFramebuffer,
    IWGLFramebufferSetup
> {

    protected createResource(context: WebGL2RenderingContext, timestamp: number): IWGLFramebuffer | undefined {
        const glFb = context.createFramebuffer() || undefined;
        if (glFb) {
            return {
                timestamp,
                glFb,
                bindings: []
            }
        }
    }

    protected unloadResource(context: WebGL2RenderingContext, fb: IWGLFramebuffer): void {

        if (fb.glFb) {
            context.deleteFramebuffer(fb.glFb);
        }
        for (const name of fb.bindings.map(t => t.name)) {
            this.textures.unload(context, name);
        }

    }

    protected async editResource(context: WebGL2RenderingContext, setup: IWGLFramebufferSetup): Promise<void> {
        const fb = this.get(setup.name);
        if (fb) {

            context.bindFramebuffer(context.FRAMEBUFFER, fb.glFb);

            context.activeTexture(context.TEXTURE1);   /////////////////

            let bounded: boolean = false;

            for (const binding of setup.bindings) {

                this.textures.write(context, {
                    name: binding.name,
                    width: context.canvas.width,
                    height: context.canvas.height,
                    depth: binding.attachments.length,
                    type: binding.type
                }, true);

                const texture = this.textures.get(binding.name);

                if (texture && texture.glTexture) {
                    if (texture.layout) {
                        for (const [index, attachment] of binding.attachments.entries()) {
                            //console.warn(index);
                            switch (texture.layout.target) {
                                case context.TEXTURE_2D:
                                    context.framebufferTexture2D(
                                        context.FRAMEBUFFER,
                                        fbAttachment(context, attachment)!,    //////
                                        context.TEXTURE_2D,
                                        texture.glTexture,
                                        0                         //////
                                    );
                                    break;

                                case context.TEXTURE_2D_ARRAY:
                                case context.TEXTURE_3D:
                                    context.framebufferTextureLayer(
                                        context.FRAMEBUFFER,
                                        fbAttachment(context, attachment)!,    //////
                                        texture.glTexture,
                                        0,                       //////
                                        index
                                    );
                                    break;
                            }
                        }
                        bounded = true;      ////////////////////////////
                    }
                    else {
                        // notify error
                        bounded = false;
                    }
                }
                else {
                    // notify error
                    bounded = false;
                }
            }

            if(bounded) {

                context.drawBuffers(
                    setup.bindings.map(
                        b => b.attachments
                            .filter(a => a < WGLFramebufferAttachment.DEPTH)
                            .map(a => fbAttachment(context, a) as number)
                    )
                        .reduce((a, b) => a.concat(b), [])
                );

                fb.bindings = setup.bindings;
            }

            context.bindFramebuffer(context.FRAMEBUFFER, null);
        }
        else {
            // notify error
        }
    }


    /********************************************************************************/

    private _textures: WGL_TEXTURES | undefined = undefined;

    setTextures(textures: WGL_TEXTURES) {
        this._textures = textures;
    }


    get textures(): WGL_TEXTURES {
        return ASSERT(this._textures, MESSAGE(this, `textures manager undefined`));
    }
}

export const WglFramebuffers = gen_renderer_resources(WGL_FRAMEBUFFERS, {
    onCreate: (_: WGL_FRAMEBUFFERS): void => {
        _.setTextures(_.___(WglTextures) as WGL_TEXTURES);
    },
    name: "WglFrameBuffers"
});



export const fbAttachment = (context: WebGL2RenderingContext, attachment: WGLFramebufferAttachment) => {         //////////
    switch (attachment) {
        case WGLFramebufferAttachment.COLOR_0:
            return context.COLOR_ATTACHMENT0;
        case WGLFramebufferAttachment.COLOR_1:
            return context.COLOR_ATTACHMENT1;
        case WGLFramebufferAttachment.COLOR_2:
            return context.COLOR_ATTACHMENT2;
        case WGLFramebufferAttachment.COLOR_3:
            return context.COLOR_ATTACHMENT3;
        case WGLFramebufferAttachment.COLOR_4:
            return context.COLOR_ATTACHMENT4;
        case WGLFramebufferAttachment.COLOR_5:
            return context.COLOR_ATTACHMENT5;
        case WGLFramebufferAttachment.COLOR_6:
            return context.COLOR_ATTACHMENT6;
        case WGLFramebufferAttachment.COLOR_7:
            return context.COLOR_ATTACHMENT7;
        case WGLFramebufferAttachment.COLOR_8:
            return context.COLOR_ATTACHMENT8;
        case WGLFramebufferAttachment.COLOR_9:
            return context.COLOR_ATTACHMENT9;
        case WGLFramebufferAttachment.COLOR_10:
            return context.COLOR_ATTACHMENT10;
        case WGLFramebufferAttachment.COLOR_11:
            return context.COLOR_ATTACHMENT11;
        case WGLFramebufferAttachment.COLOR_12:
            return context.COLOR_ATTACHMENT12;
        case WGLFramebufferAttachment.COLOR_13:
            return context.COLOR_ATTACHMENT13;
        case WGLFramebufferAttachment.COLOR_14:
            return context.COLOR_ATTACHMENT14;
        case WGLFramebufferAttachment.COLOR_15:
            return context.COLOR_ATTACHMENT15;
        case WGLFramebufferAttachment.DEPTH:
            return context.DEPTH_ATTACHMENT;
        case WGLFramebufferAttachment.STENCIL:
            return context.STENCIL_ATTACHMENT;
        case WGLFramebufferAttachment.DEPTH_STENCIL:
            return context.DEPTH_STENCIL_ATTACHMENT;
        case WGLFramebufferAttachment.RENDERBUFFER:
            return context.RENDERBUFFER;                 ////
        default:
        /////////
    }
}