import { IRendererResource, IRendererResourceSetup, RENDERER_RESOURCES, gen_renderer_resources } from "../RendererResources.js";
import { WGLEnum } from "./enum.js";

export enum WGLTextureAlignment {
    BYTE = 1 << 0,
    SHORT = 1 << 1,
    WORD = 1 << 2,
    DOUBLE = 1 << 3
}

export interface IWGLTextureSetup extends IRendererResourceSetup {
    width: number;
    height?: number;
    depth?: number;
    type: WGLTextureType;
    data?: any[];
}




export enum WGLTextureType {
    UNSIGNED_BYTE_R_2D,
    UNSIGNED_BYTE_RGBA_2D_ARRAY,
    FLOAT_RGBA_2D_ARRAY,
    UNSIGNED_INT_RGBA_2D_ARRAY,
    UNSIGNED_BYTE_RGBA_2D,
    FLOAT_RGBA_2D,
    DEPTH16,
    DEPTH32F,
    UNSIGNED_BYTE_RG_2D
}

export interface IWGLTextureLayout {
    target: number;
    alignment: number;
    internalFormat: number;
    format: number;
    type: number;
    length: number;
}

export interface IWGLTexture extends IRendererResource {
    layout?: IWGLTextureLayout;
    glTexture?: WebGLTexture;
}

export interface IWGLTextureBinding {
    name: string;
    usage: WGLEnum;
    binding: number;
    glLocation: WebGLUniformLocation;
}

export class WGL_TEXTURES extends RENDERER_RESOURCES<
    WebGL2RenderingContext,
    IWGLTexture,
    IWGLTextureSetup
> {

    protected createResource(context: WebGL2RenderingContext, timestamp: number): IWGLTexture | undefined {
        const texture = context.createTexture();
        if (texture) {
            return {
                glTexture: texture,
                timestamp: timestamp
            };
        }
    }

    protected unloadResource(context: WebGL2RenderingContext, t: IWGLTexture) {
        if (t.glTexture) {
            context.deleteTexture(t.glTexture);
        }
    }


    protected async editResource(context: WebGL2RenderingContext, setup: IWGLTextureSetup): Promise<void> {

        const texture: IWGLTexture | undefined = this.get(setup.name);

        if (texture && texture.glTexture) {
            texture.layout = getTextureLayout(context, setup.type);

            if (!texture.layout) {
                // notify error
            }
            else if (texture.layout.target == context.TEXTURE_2D && setup.height == undefined) {
                // notify error
            }
            else if (([context.TEXTURE_3D, context.TEXTURE_2D_ARRAY] as number[]).includes(texture.layout.target) &&
                [setup.height, setup.depth].includes(undefined)) {
                // notify error
            }
            else {
                context.bindTexture(texture.layout.target, texture.glTexture);

                ///////////////
                context.texParameteri(texture.layout.target, context.TEXTURE_MIN_FILTER, context.NEAREST);         //
                context.texParameteri(texture.layout.target, context.TEXTURE_MAG_FILTER, context.NEAREST);          //
                context.texParameteri(texture.layout.target, context.TEXTURE_WRAP_S, context.CLAMP_TO_EDGE);        //
                context.texParameteri(texture.layout.target, context.TEXTURE_WRAP_T, context.CLAMP_TO_EDGE);        //
                //context.texParameteri(texture.layout.target, context.TEXTURE_MAX_LEVEL, 0);                         //

                if (setup.data) {

                    context.pixelStorei(context.PACK_ALIGNMENT, texture.layout.alignment);
                    context.pixelStorei(context.UNPACK_ALIGNMENT, texture.layout.alignment);

                    switch (texture.layout.target) {
                        case context.TEXTURE_2D:
                            context.texImage2D(
                                texture.layout.target,
                                0,
                                texture.layout.internalFormat,
                                setup.width,
                                setup.height!,
                                0,
                                texture.layout.format,
                                texture.layout.type,
                                bufferView(
                                    context,
                                    texture.layout.type,
                                    setup.data,
                                    setup.width * setup.height! * texture.layout.length)!,      ///
                                0);
                            break;
                        case context.TEXTURE_2D_ARRAY:
                        case context.TEXTURE_3D:
                            context.texImage3D(
                                texture.layout.target,
                                0,
                                texture.layout.internalFormat,
                                setup.width,
                                setup.height!,
                                setup.depth!,
                                0,
                                texture.layout.format,
                                texture.layout.type,
                                bufferView(
                                    context,
                                    texture.layout.type,
                                    setup.data,
                                    setup.width * setup.height! * setup.depth! * texture.layout.length)!,      ///
                                0
                            );
                            break;
                    }
                }
                else { ////////////////////

                    switch (texture.layout.target) {
                        case context.TEXTURE_2D:
                            context.texStorage2D(
                                texture.layout.target,
                                1,
                                texture.layout.internalFormat,
                                setup.width,
                                setup.height!);
                            break;
                        case context.TEXTURE_2D_ARRAY:
                        case context.TEXTURE_3D:
                            context.texStorage3D(
                                texture.layout.target,
                                1,
                                texture.layout.internalFormat,
                                setup.width,
                                setup.height!,
                                setup.depth!
                            );
                            break;
                    }
                }


               // context.bindTexture(texture.layout.target, null);
            }
        }
        else {
            // notify error
        }
    }

    /*****************************************************************************/

    /////////////////
    activate(context: WebGL2RenderingContext, name: string, target: number, index: number): void {
        const texture = this.get(name);
        if (texture && texture.glTexture) {
            context.activeTexture(context.TEXTURE0 + index);
            context.bindTexture(target, texture.glTexture);
        }
        else {
            console.warn('error');
        }
    }

    bind(context: WebGL2RenderingContext, storage: IWGLTextureBinding): boolean {
        const texture = this.get(storage.name);
        if (texture && texture.glTexture) {
            const target = getTextureTarget(context, storage.usage);
            if (target) {
                context.uniform1i(storage.glLocation as WebGLUniformLocation, storage.binding);
                context.activeTexture(context.TEXTURE0 + storage.binding);
                context.bindTexture(target, texture.glTexture);
                return true;
            }
            else {
                // notify error
            }
        }
        else {
            // notify error
        }
        return false;
    }

}

export const WglTextures = gen_renderer_resources(WGL_TEXTURES, {
    name: 'WglTextures'
});

export const getTextureTarget = (context: WebGL2RenderingContext, usage: WGLEnum): number | undefined => {
    let type: number | undefined = undefined;
    switch (usage) {
        case WGLEnum.TEXTURE_2D:
            type = context.TEXTURE_2D;
            break;
        case WGLEnum.TEXTURE_2D_ARRAY:
            type = context.TEXTURE_2D_ARRAY;
            break;
        case WGLEnum.TEXTURE_3D:
            type = context.TEXTURE_3D;
            break;
    }
    return type;
}

export const getTextureLayout = (context: WebGL2RenderingContext, type: WGLTextureType): IWGLTextureLayout | undefined => {
    switch (type) {
        case WGLTextureType.UNSIGNED_BYTE_R_2D:
            return {
                target: context.TEXTURE_2D,
                alignment: WGLTextureAlignment.BYTE,
                internalFormat: context.R8UI,
                format: context.RED_INTEGER,
                type: context.UNSIGNED_BYTE,
                length: 1
            }

        case WGLTextureType.UNSIGNED_BYTE_RG_2D:
            return {
                target: context.TEXTURE_2D,
                alignment: WGLTextureAlignment.SHORT,
                internalFormat: context.RG16UI,
                format: context.RG_INTEGER,
                type: context.UNSIGNED_SHORT,
                length: 2
            }

        case WGLTextureType.UNSIGNED_BYTE_RGBA_2D_ARRAY:
            return {
                target: context.TEXTURE_2D_ARRAY,
                alignment: WGLTextureAlignment.WORD,
                internalFormat: context.RGBA,
                format: context.RGBA,
                type: context.UNSIGNED_BYTE,
                length: 4
            }

        case WGLTextureType.FLOAT_RGBA_2D_ARRAY:
            return {
                target: context.TEXTURE_2D_ARRAY,
                alignment: WGLTextureAlignment.DOUBLE,
                internalFormat: context.RGBA16F,
                format: context.RGBA,
                type: context.HALF_FLOAT,
                length: 2
            }

        case WGLTextureType.FLOAT_RGBA_2D:
            return {
                target: context.TEXTURE_2D,
                alignment: WGLTextureAlignment.DOUBLE,
                internalFormat: context.RGBA16F,
                format: context.RGBA,
                type: context.HALF_FLOAT,
                length: 2
            }

        case WGLTextureType.UNSIGNED_INT_RGBA_2D_ARRAY:
            return {
                target: context.TEXTURE_2D_ARRAY,
                alignment: WGLTextureAlignment.DOUBLE,
                internalFormat: context.RGBA16UI,
                format: context.RGBA,
                type: context.UNSIGNED_INT,
                length: 2
            }

        case WGLTextureType.DEPTH16:
            return {
                target: context.TEXTURE_2D,
                alignment: WGLTextureAlignment.SHORT,
                internalFormat: context.DEPTH_COMPONENT16,
                format: context.R16UI,
                type: context.UNSIGNED_INT,
                length: 1
            }

        case WGLTextureType.DEPTH32F:
            return {
                target: context.TEXTURE_2D,
                alignment: WGLTextureAlignment.WORD,
                internalFormat: context.DEPTH_COMPONENT32F,
                format: context.R32F,
                type: context.FLOAT,
                length: 1
            }

        default:
            return undefined
    }
}


const bufferView = (context: WebGL2RenderingContext, type: number, data?: any[], size?: number) => {       ///////
    const source = data ?? new Array(size);
    //console.warn(`${source.length}  ${size}`);

    switch (type) {
        case context.FLOAT:
            return new Float32Array(source)

        case context.UNSIGNED_BYTE:
            return new Uint8Array(source)

        case context.UNSIGNED_SHORT:
            return new Uint16Array(source)

        case context.UNSIGNED_INT:
            return new Uint32Array(source)
    }
}

