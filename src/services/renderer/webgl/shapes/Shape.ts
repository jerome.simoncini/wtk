import { Constructor } from "../../../../Constructor.js";
import { AppObject } from "../../../../AppObject.js";
import { IAppObject, WTK_GEN_OBJECT } from "../../../../_gen_object.js";


export class PRIMITIVE_SHAPE_LOGIC extends AppObject {

}

export interface IPrimitiveShape extends IAppObject {

}

export const gen_primitive_shape = <T extends PRIMITIVE_SHAPE_LOGIC>(shape: Constructor<T>, settings: IPrimitiveShape): Constructor<T> => {
    return WTK_GEN_OBJECT(PRIMITIVE_SHAPE_LOGIC, {

}) as Constructor<T>
}


