import { ASSERT, MESSAGE } from "../../../utils.js";
import { WGL_BUFFERS, WglBuffers } from "./Buffers.js";
import { IRendererResource, IRendererResourceSetup, RENDERER_RESOURCES, gen_renderer_resources } from "../RendererResources.js";
import { IWGLSubMeshSetup } from "./Renderer.js";



export interface IWGLBindVAO {
    name: string;
    buffer: string;
}

export interface IWGLVAO extends IRendererResource {
    glVAO: WebGLVertexArrayObject;

}

export interface IWGLVAOSetup extends IRendererResourceSetup {
    glProgram: WebGLProgram
}

export class WGL_ARRAYS extends RENDERER_RESOURCES<
    WebGL2RenderingContext,
    IWGLVAO,
    IWGLVAOSetup
> {

    protected createResource(context: WebGL2RenderingContext, timestamp: number): IWGLVAO | undefined {
        const glVAO = context.createVertexArray();
        if(glVAO) {
            return {
                timestamp,
                glVAO
            }
        }
    }

    protected unloadResource(context: WebGL2RenderingContext, vao: IWGLVAO): void {
        context.deleteVertexArray(vao.glVAO);
    }

    protected async editResource(context: WebGL2RenderingContext, setup: IWGLVAOSetup): Promise<void> {
        
        const vao = this.get(setup.name);
        if (vao) {
            context.bindVertexArray(vao.glVAO);
            const bData = this.buffers.getSubMeshSetup(setup.name);     ////////////////////////
            //console.warn(bData);
            if (bData && bData.setup) {     //
                //console.warn(bData.setup);
                const glBuffer = this.buffers.get(setup.name)?.glBuffer
                if (glBuffer) {
                    //console.warn(meshSetup.count);
                    context.bindBuffer(context.ARRAY_BUFFER, glBuffer);
                    for (const s of bData.arrays || []) {    //////
                        //console.warn(s.setup);
                        const location = context.getAttribLocation(setup.glProgram, s.setup.location);
                        //console.warn(location);
                        if (location != -1) {
                            context.enableVertexAttribArray(location);
                            const size = s.setup.size || 0;                            ////////
                            const type = s.setup.type || context.FLOAT;                ////////
                            const normalize = s.setup.normalize || false;              ////////
                            const stride = s.setup.stride || 0;                        ////////
                            const offset = s.setup.offset || 0;                        ////////
                            context.vertexAttribPointer(location, size, type, normalize, stride, offset);
                        }
                    }
                }
                else {
                    // notify error
                }
            }
            else {
                // notify error
            }
        }
        else {
            // notify error
        }
    }


    /*************************************************************************/


    bind(context: WebGL2RenderingContext, name: string): boolean {
        const vao = this.get(name);
        if (vao) {
            context.bindVertexArray(vao.glVAO);
        }
        return vao != undefined;
    }

    /*************************************************************************/

    private _buffers: WGL_BUFFERS | undefined = undefined;

    setBuffers(buffers: WGL_BUFFERS) {
        this._buffers = buffers;
    }

    private get buffers(): WGL_BUFFERS {
        return ASSERT(this._buffers, MESSAGE(this, `buffers manager undefined`));
    }
}


export const WglArrays = gen_renderer_resources(WGL_ARRAYS, {
    onCreate: (_: WGL_ARRAYS): void => {
        _.setBuffers(_.___(WglBuffers) as WGL_BUFFERS)
    },
    name: "WglArrays"
});

