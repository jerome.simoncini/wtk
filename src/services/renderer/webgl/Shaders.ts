import { IRendererResource, IRendererResourceSetup, RENDERER_RESOURCES, gen_renderer_resources } from "../RendererResources.js";

export enum ShaderStage {
    VERTEX,
    FRAGMENT
}

export interface IWGLShaderResource {
    resource: string;
    stage: ShaderStage;
}


export interface IWGLShader extends IRendererResource {
    glShader?: WebGLShader;
}


export interface IWGLShaderSetup extends IRendererResourceSetup {
    source: string;
    stage: ShaderStage;
}


export class WGL_SHADERS extends RENDERER_RESOURCES<
    WebGL2RenderingContext,
    IWGLShader,
    IWGLShaderSetup
> {
    protected createResource(context: WebGLRenderingContext, timestamp: number): IWGLShader | undefined {
        return { timestamp }
    }

    protected unloadResource(context: WebGL2RenderingContext, shader: IWGLShader): void {
        if(shader.glShader) {
            context.deleteShader(shader.glShader);
        }
    }

    protected async editResource(context: WebGL2RenderingContext, setup: IWGLShaderSetup): Promise<void> {
        const shader = this.get(setup.name);
        if (shader) {
            const glShader = this.createShader(context, setup.stage, setup.source);
            console.warn(glShader);
            if (glShader) {
                this._resources.get(setup.name)?.set({
                    ...shader,
                    glShader
                });
            }
            else {
                // notify error
            }
        }
    }

    /********************************************************************************************************/

    find(names: string[]): WebGLShader[] {
        console.warn(names);
        console.warn(Array.from(this._resources));
        return [...this._resources.entries()].filter(s => names.includes(s[0]))
            .map(s => s[1]._get()?.glShader)                 ///////////////////////////////
            .filter(s => s != undefined) as WebGLShader[];
    }

    bindSource(context: WebGLRenderingContext, stage: ShaderStage): number {
        switch (stage) {
            case ShaderStage.VERTEX:
                return context.VERTEX_SHADER
            case ShaderStage.FRAGMENT:
                return context.FRAGMENT_SHADER
            default:
                return -1
        }
    }

    createShader(context: WebGLRenderingContext, stage: ShaderStage, source: string) {
        const shader = context.createShader(this.bindSource(context, stage)) as WebGLShader | null;
        if (shader) {
            context.shaderSource(shader, source);
            context.compileShader(shader);
            const success = context.getShaderParameter(shader, context.COMPILE_STATUS);
            if (success) {
                return shader;
            }
            console.warn(context.getShaderInfoLog(shader));  //
            context.deleteShader(shader);
        }
        else {
            // notify error
        }
    }
}

export const WglShaders = gen_renderer_resources(WGL_SHADERS, {
    onCreate: (_: WGL_SHADERS): void => {
    },
    name: "WglShaders"
})