import { ASSERT, MESSAGE } from "../../../utils.js";
import { RENDERER_LOGIC, IRendererSetup } from "../Renderer.js";
import { SCENE_GRAPH, SceneNodeType } from "../scenes/SceneGraph.js";
import { CLEAR_COLOR_DEFAULT } from "../settings.js";
import { Color, IColor4 } from "../utils.js";
import { WGL_ARRAYS, WglArrays } from "./Arrays.js";
import { IWGLBindBuffer, IWGLBufferData, IWGLBufferDataSetup, IWGLBufferSubArrayData, IWGLSubBufferSetup, WGLBufferAction, WGLBufferDataType, WGLBufferUsage, WGL_BUFFERS, WglBuffers } from "./Buffers.js";
import { IWGLFramebufferResource, WGL_FRAMEBUFFERS, WglFramebuffers } from "./Framebuffers.js";
import { IWGLProgram, IWGLProgramResource, WGL_PROGRAMS, WglPrograms } from "./Programs.js";
import { IWGLShaderResource, WGL_SHADERS, WglShaders } from "./Shaders.js";
import { WGL_TEXTURES, WglTextures } from "./Textures.js";
import { WGLEnum } from "./enum.js";
import { Scope } from "../../../Scope.js";
import { IPrimitiveShapeProps, IPrimitiveShapeSetup } from "../Primitives.js";
import { IRendererResourceSetup } from "../RendererResources.js";
import { ILightResource, Lights } from "../Lights.js";
import { IMaterials, Materials } from "../Materials.js";
import { CameraType, ICameraNode, ICameraResource } from "../Cameras.js";


export interface IWGLDrawPass {
    name: string;
    program: string;
    clear?: IWGLClear;
    cmds?: Map<string, IWGLDrawCommand[]>;
    subpass: IWGLDrawSubPass[];
}

export interface IWGLDrawSubPass {
    renderNodes?: boolean;
    lighting?: boolean;
    viewport?: number[];   ////
    uniforms?: IWGLDrawSubPassUniform[];
    wireframe?: boolean;
    camera?: string;
}

export interface IWGLDrawSubPassUniform {
    name: string;
    value: any;       /////
}

export interface IGLScene extends IRendererSetup {
    shaders: IWGLShaderResource[];
    programs: IWGLProgramResource[];
    entities?: IWGLEntityResource[];
    materials?: IMaterials;
    lights?: ILightResource[];
    cameras?: ICameraResource[];
    framebuffers?: IWGLFramebufferResource[];
    stages: IWGLDrawPass[];
    pipelines: IWGLPipeline[];
}

export interface IWGLPipeline {
    name: string;
    stages: string[];
}


export enum WglStateFlag {
    VIEWPORT,
    PROGRAM,
    DEPTH_TEST,
    DEPTH_FUNC,
    DEPTH_MASK,
    BLEND,
    CLEAR_COLOR,
    CLEAR_BITS,
    UNIFORMS,
    RENDER_GROUP,
    CULL_FACE
}



export abstract class WEBGL2_RENDERER extends RENDERER_LOGIC<WebGL2RenderingContext, IGLScene> {


    resize(width: number, height: number) {                                 /////////////
        console.warn(MESSAGE(this, `width=${width} height=${height}`));
        this.context.canvas.width = width;
        this.context.canvas.height = height;
        const glScene = this.renderState;
        if (glScene) {
            this.destroyFramebuffers(glScene)
                .then(() => this.initFramebuffers(glScene))
                .then(() => this['useState'](WglStateFlag.VIEWPORT, [0, 0, 0, 0]));

        }
    }

    protected async _loadRenderStates(): Promise<void> {
        console.warn(MESSAGE(this, `(load render states) ${JSON.stringify(this.scene.states)}`));

        const loaded: Promise<void>[] = [];

        for (const renderState of this.scene.states) {

            const glScene = renderState.gl;

            if (glScene) {

                const sceneGraph = this.scene.getSceneGraph(renderState.name);

                if (sceneGraph) {

                    loaded.push(this.loadEntities(glScene, sceneGraph)                      
                        .then(() => this.loadMaterials(glScene, sceneGraph))
                        .then(() => this.loadLights(glScene, sceneGraph))
                        .then(() => this.loadCameras(glScene, sceneGraph))
                        .then(() => this.initFramebuffers(glScene))
                        .then(() => this.loadPrograms(glScene)));
                }
                else {
                    // notify error
                }
            }
            else {
                // notify error
            }
        }

        //await new Promise(r=>setTimeout(r, 1000));        //

        await Promise.all(loaded);
    }

    protected _syncRenderState(): boolean {
        const sync = this.scene.state.gl != undefined;
        if (sync) {
            console.warn(MESSAGE(this, `(synced)`));
            this._renderState.set(this.scene.state.gl);
        }
        return sync;
    }

    protected _render(): void {
        const gl = this.context;

        if (this.renderState) {

            const pipeline = this.scene.pipeline;

            if (pipeline) {

                const stages: (IWGLDrawPass | undefined)[] = this.renderState.pipelines.find(p => p.name == pipeline)?.stages
                    .map(s => this.renderState?.stages.find(stage => stage.name == s)) || [];

                //console.warn(stages);

                for (const draw of stages) {

                    if (draw && draw.program) {

                        //console.warn(this['useState']);

                        this['useState'](WglStateFlag.PROGRAM, draw.program);      //// 

                        const program: IWGLProgram | undefined = this.programs.current;

                        if (program) {
                            if (draw.clear) {
                                const clearBits: number[] = [];
                                if (draw.clear.color) {
                                    let c: IColor4 | undefined = undefined;
                                    switch (draw.clear.color.length) {
                                        case 3:
                                            c = (new Color([...draw.clear.color, 1])).rgba;
                                            break;
                                        case 4:
                                            c = (new Color(draw.clear.color)).rgba;
                                            break;
                                        default:
                                            c = this._defaults?.clearColor || CLEAR_COLOR_DEFAULT;
                                    }
                                    if (c) {
                                        this['useState'](WglStateFlag.CLEAR_COLOR, c);     //// 

                                        clearBits.push(gl.COLOR_BUFFER_BIT);
                                    }
                                }

                                ////////

                                if (draw.clear.depthFunc) {
                                    this['useState'](WglStateFlag.DEPTH_TEST, true);
                                    this['useState'](WglStateFlag.DEPTH_FUNC, gl.LESS);    /////// 
                                }
                                else {
                                    this['useState'](WglStateFlag.DEPTH_TEST, false);
                                }

                                clearBits.push(gl.DEPTH_BUFFER_BIT);                  /////// 
                                gl.clear(clearBits.reduce((a, b) => a | b, 0));
                            }


                            // console.warn("---------------");
                            //console.warn(draw.subpass);
                            for (const subpass of draw.subpass) {
                                //console.warn(subpass);

                                if (subpass.viewport) {
                                    this['useState'](WglStateFlag.VIEWPORT, subpass.viewport);
                                }

                                if (subpass.uniforms) {       ///////////
                                    // console.warn(subpass.uniforms);
                                    this['useState'](WglStateFlag.UNIFORMS, subpass.uniforms);
                                }

                                if (program.locations?.has("WGL_CameraId")) {
                                    const cameraId: number | undefined = subpass.camera ? this.cameras.index(subpass.camera) : undefined;
                                    gl.uniform1ui(program.locations.get("WGL_CameraId")!.location, cameraId || 0);
                                }

                                if (subpass.renderNodes) {
                                    // console.warn('geometry');

                                    this.renderNodes(draw, subpass, program);

                                }
                                else {                          ///////////////////////////////
                                    // full screen quad
                                    //console.warn("full screen quad");
                                    const emptyVAO = gl.createVertexArray();
                                    gl.bindVertexArray(emptyVAO);

                                    if (subpass.lighting) {
                                        // console.warn('lighting');
                                        this.computeLighting(draw, subpass, program);
                                    }
                                    else {
                                        // console.warn('postprocessing');
                                        gl.drawArrays(gl.TRIANGLES, 0, 3);
                                    }

                                    gl.deleteVertexArray(emptyVAO);
                                }
                            }

                        }
                        else {
                            // loading...
                        }
                    }
                }
            }
            else {
                // 
            }
        }
        else {
            //
        }

        //gl.flush();

    }


    protected abstract renderNodes(draw: IWGLDrawPass, pass: IWGLDrawSubPass, program: IWGLProgram): void;
    protected abstract computeLighting(draw: IWGLDrawPass, pass: IWGLDrawSubPass, program: IWGLProgram): void;


    //-------------------------------------------------------------------------------------------------//

    setClearColor(color: IColor4): boolean {
        //console.warn(`clearColor=${JSON.stringify(color)}`);
        this.context.clearColor(color.r, color.g, color.b, color.a);
        return true;
    }

    enableDepthTest(enabled: boolean): boolean {
        //console.warn(`depthTest=${enabled}`);
        enabled ? this.context.enable(this.context.DEPTH_TEST) : this.context.disable(this.context.DEPTH_TEST);
        return true;
    }

    enableDepthMask(enabled: boolean): boolean {
        //console.warn(`depthMask=${enabled}`);
        this.context.depthMask(enabled);
        return true;
    }

    enableBlend(enabled: boolean): boolean {
        //console.warn(`blend=${enabled}`);
        enabled ? this.context.enable(this.context.BLEND) : this.context.disable(this.context.BLEND);
        return true;
    }

    cullFace(face: number | undefined): boolean {
        //console.warn(`blend=${enabled}`);
        if (face) {
            this.context.enable(this.context.CULL_FACE);
            this.context.cullFace(face);
        }
        else {
            this.context.disable(this.context.CULL_FACE);
        }
        return true;
    }

    setDepthFunc(depthFunc: number): boolean {
        //console.warn(`depthFunc=${depthFunc}`);
        this.context.depthFunc(depthFunc);
        return true;
    }

    updateViewport(viewport: number[]): boolean {
        //console.warn(`viewport=${JSON.stringify(viewport)}`);
        const [x, y, width, height] = [
            this.context.canvas.width * viewport[0],
            this.context.canvas.height * viewport[1],
            this.context.canvas.width * viewport[2],
            this.context.canvas.height * viewport[3],
        ]
        this.context.viewport(x, y, width, height);
        return true;
    }

    setUniforms(uniforms: IWGLDrawSubPassUniform[]): boolean {
        //console.warn(`uniforms=${JSON.stringify(uniforms)}`);
        //console.warn('update uniforms');
        const program = this.programs.current;
        if (program) {
            for (const uniform of uniforms) {
                const location = program?.locations?.get(uniform.name);
                if (location) {
                    uniform.value = uniform.value.map((v: number | string) => this.get(v));

                    this.programs.setUniform(this.context, program, uniform);       ////
                }
                else {
                    // notify error
                }
            }
        }
        else {
            // notify error
        }
        return true;
    }


    get(property: number | string): number | string {
        switch (property) {
            default:
                return property;
        }
    }

    useProgram(pName: string): boolean {   ////////////
        //console.warn(`------------------------------`);
        console.warn(`program=${JSON.stringify(pName)}`);
        this.programs.use(pName, this);
        this['updateStateSlot'](WglStateFlag.UNIFORMS, '');        /////// 
        return this.programs.current != undefined;
    }



    //-------------------------------------------------------------------------------------------------//

    private async loadPrograms(glScene: IGLScene): Promise<void> {
        const loaded: Promise<void>[] = [];

        for (const pass of glScene.stages || []) {

            const resource = glScene.programs.find(p => p.name == pass.program);

            if (resource) {

                loaded.push(this.loadProgram(glScene, resource)
                    .then(async (program) => {
                        if (program) {
                            if (program.glProgram) {
                                await this.initStorage(program)
                                    .then(() => this.loadArrays(glScene, program.glProgram));
                            }
                            else {
                                // notify error
                            }
                        }
                        else {
                            // notify error
                        }
                    }));
            }
        }

        await Promise.all(loaded);
    }


    private async loadProgram(glScene: IGLScene, resource: IWGLProgramResource): Promise<IWGLProgram | undefined> {
        const loadShaders = resource.shaders
            .map(name => glScene.shaders.find(s => s.resource == name))
            .filter(s => s && this.shaders.get(s.resource) === undefined) as IWGLShaderResource[];

        console.warn(loadShaders);

        const created: Promise<void>[] = [];

        if (loadShaders.length > 0) {     ///
            for (const resource of loadShaders) {
                const data = this.resources.get(resource.resource);
                console.warn(data);
                if (data) {
                    if (this.shaders.create(this.context, resource.resource)) {
                        created.push(this.shaders.write(this.context, {
                            name: resource.resource,
                            source: data,
                            stage: resource.stage
                        }));
                    }
                }
            }

            await Promise.all(created);
        }

        const glShaders = this.shaders.find(resource.shaders);

        console.warn(glShaders);

        if (resource.shaders.length != glShaders.length) {
            return undefined;
        }

        console.warn(glShaders);


        console.warn(resource.name);

        await this.programs.write(this.context, {
            name: resource.name,
            shaders: glShaders,
            storage: resource.storage || [],
            uniforms: resource.uniforms || [],
            framebuffer: resource.framebuffer
        }, true);

        return this.programs.get(resource.name)
    }

    private async loadArrays(glScene: IGLScene, glProgram: WebGLProgram): Promise<void> {    //
        const loaded: Promise<void>[] = [];

        for (const draw of glScene.stages.filter(p => p.subpass.find(s => s.renderNodes == true)) || []) {
            for (const cmds of draw.cmds?.values() || []) {
                for (const setup of cmds.map(c => c.buffers.get(this.context.ARRAY_BUFFER)).filter(c => c != undefined)) {
                    if (setup) {
                        loaded.push(this.arrays.write(this.context, {
                            name: setup.name,
                            glProgram
                        }, true));
                    }
                }
            }
        }
        await Promise.all(loaded);
    }

    private async initFramebuffers(renderState: IGLScene): Promise<void> {       //
        const loaded: Promise<void>[] = [];
        for (const fb of renderState.framebuffers || []) {
            loaded.push(this.framebuffers.write(this.context, {
                name: fb.name,
                bindings: fb.bindings
            }, true));
        }

        await Promise.all(loaded);
    }

    private async destroyFramebuffers(renderState: IGLScene): Promise<void> {       //
        for (const fb of renderState.framebuffers || []) {
            this.framebuffers.unload(this.context, fb.name);
        }
    }


    private async loadEntities(renderState: IGLScene, graph: SCENE_GRAPH): Promise<void> {
        const loaded: Promise<void>[] = [];

        for (const entity of graph.entities) {
            const setup: IWGLEntityResource | undefined = renderState.entities!.find(g => g.name == entity);
            if (setup) {
                let mesh: IWGLMesh | undefined = setup.resource ?
                    JSON.parse(this.resources.get(setup.resource)) as IWGLMesh
                    : this.createMesh(ASSERT(this.scene.primitives.find(p => p.name == setup.primitive), MESSAGE(this, `'${setup.primitive}' unknown primitive`)).setup);

                if (mesh) {
                    loaded.push(this.loadEntity({
                        name: entity,
                        mesh
                    }, renderState));
                }
                else {
                    // notify error
                }
            }
        }

        await Promise.all(loaded);
    }

    protected createMesh(primitive: IPrimitiveShapeSetup<IPrimitiveShapeProps>): IWGLMesh | undefined {
        let mesh: IWGLMesh | undefined = undefined;
        const shape = this.primitives.create(primitive);
        if (shape) {

            console.warn(JSON.stringify(shape));

            const arrays: IWGLSubMeshArray[] = [];

            if (shape.vertices.length) {
                arrays.push({
                    data: shape.vertices,
                    setup: {
                        dataType: "f32",
                        size: 3,
                        //offset: 0,
                        normalize: false,
                        stride: 0,
                        location: "position"
                    }
                });
            }

            if (shape.uvcoords.length) {
                arrays.push({
                    data: shape.uvcoords,
                    setup: {
                        dataType: "f32",
                        size: 2,
                        //offset: 0,
                        normalize: false,
                        stride: 0,
                        location: "uvcoord"
                    }

                })
            }

            if (shape.normals.length) {
                arrays.push({
                    data: shape.normals,
                    setup: {
                        dataType: "f32",
                        size: 3,
                        //offset: 0,
                        normalize: true,
                        stride: 0,
                        location: "normal"
                    }

                })
            }


            mesh = {
                subs: [
                    {
                        arrays,
                        elements: shape.indices.length ? shape.indices : undefined,
                        setup: {
                            first: 0,
                            primitiveType: WGLPrimitiveType.TRIANGLES
                        }
                    } as IWGLSubMesh
                ]
            };

        }
        return mesh;
    }

    ///////////////////////////////////////////////////////////////////////////////////

    protected _subEntities: Map<string, ISubEntity[]> = new Map();

    protected getSubEntities(name: string): ISubEntity[] {
        return this._subEntities.get(name) || [];
    }

    ///////////////////////////////////////////////////
    protected async loadEntity(setup: IWGLEntitySetup, renderState: IGLScene): Promise<void> {
        console.warn(`LOAD ENTITY '${setup.name}`);
        const subs = await this.loadMesh(this.context, setup);        ////////////////
        if (subs.length) {                    //////////
            for (const pass of renderState.stages) {
                if (pass.subpass.find(s => s.renderNodes == true)) {
                    if (!pass.cmds) {   ///
                        pass.cmds = new Map();
                    }
                    for (const [index, subSetup] of subs.entries()) {
                        console.warn(Array.from(subSetup.values()));
                        const subName = `${setup.name}-${index}`;
                        pass.cmds.set(subName, [{         ////////////////
                            type: subSetup.has(this.context.ELEMENT_ARRAY_BUFFER) ? WGLDrawCommandType.ELEMENTS_INSTANCED : WGLDrawCommandType.ARRAYS_INSTANCED,
                            buffers: subSetup
                        }]);

                        const subNames = this._subEntities.get(setup.name);
                        if (subNames) {
                            if (!subNames.find(s => s.name == subName)) {
                                subNames.push({
                                    name: subName,
                                    materialId: 0          /////
                                });
                            }
                        }
                        else {
                            this._subEntities.set(setup.name, [{
                                name: subName,
                                materialId: 0             /////
                            }]);
                        }
                    }
                }
            }
            //loaded.push(entity);
        }
        else {
            // notify error

            throw 'ERROR ERROR ERROR ERROR ERROR ERROR '
        }
    }

    private async initStorage(program: IWGLProgram): Promise<void> {
        const loaded: Promise<void>[] = [];

        for (const storage of program.storage) {
            switch (storage.usage) {
                case WGLEnum.DYNAMIC_UBO:
                    {
                        loaded.push(this.buffers.write(this.context, {
                            name: storage.name,
                            target: this.context.UNIFORM_BUFFER,
                            action: WGLBufferAction.INIT,
                            glProgram: program.glProgram!,
                            storage
                        }, true));
                    }
                    break;
                case WGLEnum.TEXTURE_1D:
                case WGLEnum.TEXTURE_2D:
                case WGLEnum.TEXTURE_3D:
                    {
                        if (!this.textures.create(this.context, storage.name)) {
                            // notify error
                        }
                    }
                    break;
            }
        }

        await Promise.all(loaded);
    }

    protected async _syncSceneGraph(): Promise<void> {

    }


    ///////////////////////////////////////////////////
    private async loadMaterials(renderState: IGLScene, graph: SCENE_GRAPH): Promise<void> {

        const loaded: Promise<void>[] = [];

        loaded.push(this.materials.load(renderState.materials?.setup || []));

        for (const binding of renderState.materials?.bindings || []) {
            const sub = this._subEntities.get(binding.entity)?.find(s => s.name == binding.sub);
            if (sub) {
                const materialId = this.materials.index(binding.name);
                sub.materialId = materialId != -1 ? materialId : 0;       /////
            }
            else {
                // notify error
            }
        }

        await Promise.all(loaded);
    }


    ///////////////////////////////////////////////////
    private async loadLights(renderState: IGLScene, graph: SCENE_GRAPH): Promise<void> {

        const loaded: Promise<void>[] = [];

        loaded.push(this.lights.load(renderState.lights || []));

        await Promise.all(loaded);
    }


    ///////////////////////////////////////////////////
    private async loadCameras(renderState: IGLScene, graph: SCENE_GRAPH): Promise<void> {

        const loaded: Promise<void>[] = [];

        const cameras: ICameraNode[] = graph.cameras.map(n => n.attach?.filter(c => c.type == SceneNodeType.CAMERA)?.map(i => {
            return {
                name: i.name,
                node: n,
                projection: renderState.cameras?.find(camera => camera.name == i.name)?.projection,
                type: renderState.cameras?.find(camera => camera.name == i.name)?.type
            } as ICameraNode;
        }) || []).flat();

        loaded.push(this.cameras.load(cameras || []));

        await Promise.all(loaded);
    }

    /*************************************************************************************************/

    protected getPrimitiveType(context: WebGLRenderingContext, pType: WGLPrimitiveType): number | undefined {
        switch (pType) {
            case WGLPrimitiveType.POINTS:
                return context.POINTS;

            case WGLPrimitiveType.LINES:
                return context.LINES;

            case WGLPrimitiveType.TRIANGLES:
                return context.TRIANGLES;

            default:
                return undefined;
        }
    }


    /*************************************************************************************************/

    ///////////////////////////////////////////////////////
    private async loadMesh(context: WebGL2RenderingContext, setup: IWGLEntitySetup): Promise<WGLEntitySubMeshSetup[]> {
        const meshSetup: WGLEntitySubMeshSetup[] = [];
        if (setup.mesh && setup.mesh.subs.length) {
            const subs = this.bufferSource(context, setup.mesh);
            console.warn(subs);
            for (const [index, subMesh] of subs.entries()) {
                const subMeshSetup: Map<number, IWGLBindBuffer> = new Map();
                const bufferName = `${setup.name}-${index}`;
                console.warn(bufferName);
                console.warn(setup.mesh.subs[index]);
                if (subMesh.arrays) {

                    const loaded: Promise<void>[] = [];

                    loaded.push(this.buffers.write(context, {
                        name: bufferName,
                        target: context.ARRAY_BUFFER,
                        arrays: subMesh.arrays,
                        usage: context.STATIC_DRAW
                    }, true).then(() => {

                        subMeshSetup.set(context.ARRAY_BUFFER, {
                            name: bufferName,
                            usage: WGLBufferUsage.STATIC_DRAW
                        });
                    }));

                    if (subMesh.elements) {
                        const eName = `${bufferName}e`;
                        loaded.push(this.buffers.write(context, {
                            name: eName,
                            target: context.ELEMENT_ARRAY_BUFFER,
                            elements: subMesh.elements,
                            usage: context.STATIC_DRAW
                        }, true).then(() => {

                            subMeshSetup.set(context.ELEMENT_ARRAY_BUFFER, {
                                name: eName,
                                usage: WGLBufferUsage.STATIC_DRAW
                            });
                        }));

                        ////////////////////////////////////////////////////////////////
                        const wireframe: number[] = [];
                        const indices = Array.from(subMesh.elements);
                        for (let i = 0; i < indices.length; i++) {
                            wireframe.push(indices[i]);
                            if (i % 3 == 2) {
                                wireframe.push(indices[i - 2]);
                            }
                        }

                        (<IWGLSubMeshSetup>subMesh.setup).wireframeCount = wireframe.length;

                        const wName = `${bufferName}w`;
                        loaded.push(this.buffers.write(context, {
                            name: wName,
                            target: context.ELEMENT_ARRAY_BUFFER,
                            elements: new Uint32Array(wireframe),
                            usage: context.STATIC_DRAW
                        }, true).then(() => {

                            subMeshSetup.set(WGLEnum.WIREFRAME, {
                                name: wName,
                                usage: WGLBufferUsage.STATIC_DRAW
                            });
                        }));

                    }

                    //////////////////////////////////////////////////////
                    this.buffers.createSubMeshSetup(bufferName, {
                        arrays: subMesh.arrays,
                        elements: subMesh.elements,
                        setup: subMesh.setup,
                    });

                    await Promise.all(loaded);
                    meshSetup.push(subMeshSetup);
                }
                else {
                    // notify error
                }
            }
        }
        return meshSetup;
    }

    ////////////////////////////////////////////////////////////////////
    private bufferSource(context: WebGLRenderingContext, mesh: IWGLMesh): IWGLBufferData<IWGLBufferDataSetup>[] {
        const data: IWGLBufferData<IWGLBufferDataSetup>[] = [];
        if (!Array.isArray(mesh.subs)) {                                   //////
            throw MESSAGE(this, `(bufferSource) error`);
        }
        for (const submesh of mesh.subs) {

            const r = /*submesh.shape ? this.primitives.create(submesh.shape) :*/ submesh;

            if (r && r.arrays) {

                let elementsData: Uint32Array | undefined = undefined;
                const arrayData: IWGLBufferSubArrayData[] = [];
                let offset = 0;
                for (const array of r.arrays) {
                    let bData: BufferSource | undefined = undefined;
                    switch (array.setup.dataType as WGLBufferDataType) {                       ///
                        case WGLBufferDataType.F32:
                            bData = new Float32Array(array.data) as BufferSource;
                    }
                    console.warn(bData);
                    if (bData) {
                        arrayData.push({
                            data: bData,
                            setup: {
                                type: WGL_BUFFERS.glType(context, array.setup.dataType as WGLBufferDataType),
                                size: array.setup.size,
                                normalize: array.setup.normalize,
                                stride: array.setup.stride,
                                location: array.setup.location,
                                offset
                            }
                        });
                        // offset += bData.byteLength;
                    }
                }
                if (r.elements) {
                    elementsData = new Uint32Array(r.elements);
                }
                data.push({
                    arrays: arrayData,
                    elements: elementsData,
                    setup: {
                        count: r.elements ? r.elements.length : r.arrays[0]?.data.length / r.arrays[0]?.setup.size || 0,
                        first: submesh.setup.first,
                        primitiveType: submesh.setup.primitiveType
                    } as IWGLSubMeshSetup
                });

            }
            else {
                // notify error
            }
        }
        return data;
    }


    /*******************************************************************************************/
    private _buffers: WGL_BUFFERS | undefined = undefined;
    private _arrays: WGL_ARRAYS | undefined = undefined;
    private _textures: WGL_TEXTURES | undefined = undefined;
    private _programs: WGL_PROGRAMS | undefined = undefined;
    private _shaders: WGL_SHADERS | undefined = undefined;
    private _framebuffers: WGL_FRAMEBUFFERS | undefined = undefined;

    setWglBuffers(buffers: WGL_BUFFERS) {
        this._buffers = buffers;
    }

    setWglArrays(arrays: WGL_ARRAYS) {
        this._arrays = arrays;
    }

    setWglTextures(textures: WGL_TEXTURES) {
        this._textures = textures;
    }

    setWglPrograms(programs: WGL_PROGRAMS) {
        this._programs = programs;
    }

    setWglShaders(shaders: WGL_SHADERS) {
        this._shaders = shaders;
    }

    setWglFramebuffers(framebuffers: WGL_FRAMEBUFFERS) {
        this._framebuffers = framebuffers;
    }

    get buffers(): WGL_BUFFERS {
        return ASSERT(this._buffers, MESSAGE(this, `wgl buffers undefined`));
    }

    get arrays(): WGL_ARRAYS {
        return ASSERT(this._arrays, MESSAGE(this, `wgl arrays undefined`));
    }

    get textures(): WGL_TEXTURES {
        return ASSERT(this._textures, MESSAGE(this, `wgl textures undefined`));
    }

    get programs(): WGL_PROGRAMS {
        return ASSERT(this._programs, MESSAGE(this, `wgl programs undefined`));
    }

    get shaders(): WGL_SHADERS {
        return ASSERT(this._shaders, MESSAGE(this, `wgl shaders undefined`));
    }

    get framebuffers(): WGL_FRAMEBUFFERS {
        return ASSERT(this._framebuffers, MESSAGE(this, `wgl framebuffers undefined`));
    }


    /*****************************************************************************************/

    /////////////////////////////
    __onStart() {
        this.setWglBuffers(this.getObjectStore(Scope.LOCAL).get(WglBuffers));
        this.setWglArrays(this.getObjectStore(Scope.LOCAL).get(WglArrays));
        this.setWglTextures(this.getObjectStore(Scope.LOCAL).get(WglTextures));
        this.setWglPrograms(this.getObjectStore(Scope.LOCAL).get(WglPrograms));
        this.setWglShaders(this.getObjectStore(Scope.LOCAL).get(WglShaders));
        this.setWglFramebuffers(this.getObjectStore(Scope.LOCAL).get(WglFramebuffers));

        this['createStateSlot'](WglStateFlag.PROGRAM, this.useProgram);
        this['createStateSlot'](WglStateFlag.CLEAR_COLOR, this.setClearColor);
        this['createStateSlot'](WglStateFlag.DEPTH_TEST, this.enableDepthTest);
        this['createStateSlot'](WglStateFlag.DEPTH_FUNC, this.setDepthFunc);
        this['createStateSlot'](WglStateFlag.VIEWPORT, this.updateViewport);
        this['createStateSlot'](WglStateFlag.UNIFORMS, this.setUniforms);
        this['createStateSlot'](WglStateFlag.DEPTH_MASK, this.enableDepthMask);
        this['createStateSlot'](WglStateFlag.BLEND, this.enableBlend);
        this['createStateSlot'](WglStateFlag.CULL_FACE, this.cullFace);
    }

    /////////////////////////////////
    __onShutdown() {
        this.getObjectStore(Scope.LOCAL).release(WglBuffers, this.context);
        this.getObjectStore(Scope.LOCAL).release(WglArrays, this.context);
        this.getObjectStore(Scope.LOCAL).release(WglTextures, this.context);
        this.getObjectStore(Scope.LOCAL).release(WglPrograms, this.context);
        this.getObjectStore(Scope.LOCAL).release(WglShaders, this.context);
        this.getObjectStore(Scope.LOCAL).release(WglFramebuffers, this.context);
    }
}

/********************************************************************************************/



export type WGLEntitySubMeshSetup = Map<number, IWGLBindBuffer>;

export interface IWGLEntityResource {
    name: string;
    resource?: string;
    primitive?: string;
}

//export interface IWGLEntity extends IRendererResource {}

export interface IWGLEntitySetup extends IRendererResourceSetup {
    mesh: IWGLMesh;
}

/////////////////
export interface ISubEntity {
    name: string;
    materialId: number
}


/***************************************************************************/

export interface IWGLSubMesh extends IWGLSubBufferSetup {
    shape?: IPrimitiveShapeSetup<IPrimitiveShapeProps>;
    arrays?: IWGLSubMeshArray[];
    elements?: number[];
    setup: IWGLSubMeshSetup;   /////////
}

export interface IWGLSubMeshArray {
    data: any[];
    setup: IWGLSubMeshArraySetup;
}

export interface IWGLSubMeshArraySetup {
    dataType: string;
    size: number;
    normalize: boolean;
    stride: number;
    location: string;
}

export interface IWGLSubMeshSetup extends IWGLBufferDataSetup {
    count: number;          ////    
    first: number;             //// 
    primitiveType: WGLPrimitiveType;    ////    
    wireframeCount?: number;
    triangleCount?: number;
}


export interface IWGLMesh {
    subs: IWGLSubMesh[];
}
/****************************************************************************/
export enum WGLPrimitiveType {
    POINTS = "points",
    TRIANGLES = "triangles",
    LINES = "lines"
}


export enum WGLDrawCommandType {
    ARRAYS,
    ELEMENTS,
    ARRAYS_INSTANCED,
    ELEMENTS_INSTANCED
}

export interface IWGLArrayBufferSetup {   ///////
    count: number;
    first: number;
    primitiveType: WGLPrimitiveType;
}

export interface IWGLDrawArrays {
    name: string;
    buffer: string;
    setup?: IWGLArrayBufferSetup;
}

export interface IWGLClear {
    color?: number[];
    depthFunc?: WGLEnum;
}

export interface IWGLDrawCommand {
    type: WGLDrawCommandType;
    buffers: Map<number, IWGLBindBuffer>;
}
