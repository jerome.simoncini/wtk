import { IWGLProgramStorage } from "./Programs.js";
import { WGLEnum } from "./enum.js";
import { IRendererResource, IRendererResourceSetup, RENDERER_RESOURCES, gen_renderer_resources } from "../RendererResources.js";



export enum WGLBufferAction {
    INIT,
    UPDATE
}

export enum WGLBufferUsage {
    STATIC_DRAW
}

export enum WGLBufferDataType {
    F32 = "f32"
}

export interface IWGLBufferDataSetup {
}

export interface IWGLSubBufferSetup {
    type: number;
    size: number;
    normalize: boolean;
    stride: number;
    location: string;
    offset: number;
}

export interface IWGLBufferData<T extends IWGLBufferDataSetup> {
    arrays?: IWGLBufferSubArrayData[];
    elements?: Uint32Array;                ///////////////
    setup: T;
}

export interface IWGLBufferSubArrayData {
    data: BufferSource;
    setup: IWGLSubBufferSetup;
}

export interface IWGLBindBuffer {
    name: string;
    usage: WGLBufferUsage;
    setup?: IWGLSubBufferSetup[];
}

export interface IWGLBuffer extends IRendererResource {
    glBuffer: WebGLBuffer;
}

export interface IWGLBufferSetup extends IRendererResourceSetup {
    target: number;
    action?: WGLBufferAction;
    storage?: IWGLProgramStorage;
    glProgram?: WebGLProgram;
    data?: BufferSource;
    arrays?: IWGLBufferSubArrayData[];
    elements?: Uint32Array;                 ///////////
    usage?: number;
}

export class WGL_BUFFERS extends RENDERER_RESOURCES<
    WebGL2RenderingContext,
    IWGLBuffer,
    IWGLBufferSetup
> {

    protected createResource(context: WebGL2RenderingContext, timestamp: number): IWGLBuffer | undefined {
        const glBuffer = context.createBuffer();
        if (glBuffer) {
            return {
                timestamp,
                glBuffer
            }
        }
    }

    protected unloadResource(context: WebGL2RenderingContext, buffer: IWGLBuffer): void {
        context.deleteBuffer(buffer.glBuffer);
    }

    protected async editResource(context: WebGL2RenderingContext, setup: IWGLBufferSetup): Promise<void> {
        const buffer = this.get(setup.name);
        if (buffer) {

            switch (setup.target) {
                case context.ARRAY_BUFFER:
                    if(Array.isArray(setup.arrays) && setup.usage) {
                        context.bindBuffer(context.ARRAY_BUFFER, buffer.glBuffer);
                        const bufSize = setup.arrays
                            .map(s => s.data.byteLength)
                            .reduce((a, b) => a += b, 0);
                        context.bufferData(context.ARRAY_BUFFER, bufSize, setup.usage);
                        let offset = 0;
                        for (const s of setup.arrays) {
                            context.bufferSubData(context.ARRAY_BUFFER, offset, s.data);
                            s.setup.offset = offset;
                            offset += s.data.byteLength;
                        }
                    }
                    else {
                        // notify error
                    }
                    break;
                case context.ELEMENT_ARRAY_BUFFER:
                    if(setup.usage && setup.elements) {
                        context.bindBuffer(context.ELEMENT_ARRAY_BUFFER, buffer.glBuffer);
                        context.bufferData(context.ELEMENT_ARRAY_BUFFER, setup.elements, setup.usage);
                    }
                    else {
                        // notify error
                    }
                    break;
                case context.UNIFORM_BUFFER:
                    switch (setup.action || WGLBufferAction.INIT) {
                        case WGLBufferAction.INIT:
                            if (setup.storage && setup.glProgram) {
                                context.bindBuffer(context.UNIFORM_BUFFER, buffer.glBuffer);
                                if (setup.storage.usage == WGLEnum.DYNAMIC_UBO) {    ////////

                                    const location = context.getUniformBlockIndex(setup.glProgram, setup.storage.location);

                                    const blockSize = context.getActiveUniformBlockParameter(
                                        setup.glProgram,
                                        location,
                                        context.UNIFORM_BLOCK_DATA_SIZE
                                    );

                                    context.bufferData(context.UNIFORM_BUFFER, blockSize, context.DYNAMIC_DRAW);
                                }
                                else {   //
                                    const data = setup.storage.data || new Float32Array(0);
                                    context.bufferData(context.UNIFORM_BUFFER, data , context.STATIC_DRAW);

                                }

                                context.bindBuffer(context.UNIFORM_BUFFER, null);    //////
                                context.bindBufferBase(context.UNIFORM_BUFFER, setup.storage.binding, buffer.glBuffer);
                            }
                            else {
                                // notify error
                            }
                            break;
                        case WGLBufferAction.UPDATE:
                            context.bindBuffer(context.UNIFORM_BUFFER, buffer.glBuffer);
            
                            const bufferSource: BufferSource = setup.data || new Float32Array(0);   
                            //console.warn(bufferSource.byteLength);

                            //////////////////////////////////
                            context.bufferSubData(
                                context.UNIFORM_BUFFER,
                                0,
                                bufferSource
                            );

                            context.bindBuffer(context.UNIFORM_BUFFER, null);
                            break;
                    }
                    break;
            }
        }
        else {
            // notify error
        }
    }

    /***********************************************************************/

    private _setup: Map<string, IWGLBufferData<IWGLBufferDataSetup>> = new Map();        ////////

    getSubMeshSetup(name: string): IWGLBufferData<IWGLBufferDataSetup> | undefined {
        return this._setup.get(name);
    }

    createSubMeshSetup(name: string, setup: IWGLBufferData<IWGLBufferDataSetup>): void {
        this._setup.set(name, setup);
    }

    bind(context: WebGLRenderingContext, target: number, name: string): boolean {
        const buffer: IWGLBuffer | undefined = this.get(name);
        if (buffer) {
            context.bindBuffer(target, buffer.glBuffer);
        }
        return buffer != undefined;
    }

    ///////////////////
    static glType(context: WebGLRenderingContext, type: WGLBufferDataType) {
        switch (type) {
            case WGLBufferDataType.F32:
                return context.FLOAT;
            default:
                return -1;
        }
    }

}

export const WglBuffers = gen_renderer_resources(WGL_BUFFERS, {
    onCreate: (_: WGL_BUFFERS): void => {
    },
    name: "WglBuffers"
});

