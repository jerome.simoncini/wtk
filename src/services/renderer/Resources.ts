import { WTK_GEN_OBJECT } from "../../_gen_object.js";
import { AppObject } from "../../AppObject.js";

////////////////
const APP_SERVER_URL = "http://localhost:8000";   //////


export interface IResource {
    name: string;
    path: string;
}

export class RESOURCES_LOGIC extends AppObject {
    private _resources: Map<string, any> = new Map();     //

    reset() {
        this._resources = new Map();
    }

    async load(request: IResource[]): Promise<void> {   //
        const resources = request.filter(r => this.get(r.name) == undefined);
        //console.warn(resources);
        if(resources.length) {
            const responses: Promise<Response>[] = [];
            
            for (const resource of resources) {
                responses.push(fetch(APP_SERVER_URL+resource!.path));
            }

            const r = await Promise.all(responses);
            
            await Promise.all(r.map(async response=>{
                //console.warn(response.url);
                const path = response.url.split(APP_SERVER_URL).pop();             /////
                //console.warn(path);
                const name = path ? resources.find(r=>r.path==path)?.name : undefined;   /////
                if(name) {
                    //console.warn(name);
                    const data = await response.text();
                    //console.warn(data);
                    this._resources.set(name, data);
                }
                else {
                    // notify error
                }
            }));
        }
    }

    get(name: string) {   //
        return this._resources.get(name);
    }
}

export const Resources = WTK_GEN_OBJECT(RESOURCES_LOGIC, {
    onInit: async (_: RESOURCES_LOGIC) => {
       _.reset()
    },
    name: "Resources"
});