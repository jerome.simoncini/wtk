import { Constructor } from "../../Constructor.js";
import { AppObject } from "../../AppObject.js";
import { IAppObject, WTK_GEN_OBJECT } from "../../_gen_object.js";
import { __skull__ } from "../../_skull.js";
import { IRendererResourceSetup } from "./RendererResources.js";



export abstract class RESOURCES_DTO<R extends IRendererResourceSetup> extends AppObject {
    private _offsets: Map<string, number> = new Map();
    private _data: Float32Array | undefined = undefined;
    private _blockLength: number | undefined = undefined;

    abstract loadResource(resource: R): Float32Array;

    setBlockLength(blockLength: number) {
        this._blockLength = blockLength;
    }

    async load(resources: R[]): Promise<void> {

        this._data = new Float32Array(resources.length * this.blockLength);

        for (const [index, resource] of resources.entries()) {

            const i = index * this.blockLength;

            let data: Float32Array = this.loadResource(resource);

            this._offsets.set(resource.name, index);
            this._data.set(data, i);
        }
    }

    
    async update(resource: R): Promise<void> {
        const i = this.index(resource.name);
        if (i != -1) {
            const offset = i * this.blockLength;
            const data = this.loadResource(resource);
            this.write(offset, data);
        }
        else {
            // notify error
        }
    }

    raw(): BufferSource {
        return this._data || new Float32Array(0);
    }

    index(name: string) {
        return Array.from(this._offsets.keys()).findIndex(k => k == name);
    }

    read(start: number, length: number): number[] | undefined {               //
        const data = Array.from(this._data || new Float32Array(0));
        const end = start+length;
        if(data.length >= end) {
            return data.slice(start, end); 
        }
    }

    write(start: number, data: Float32Array) {       //
        this._data?.set(data, start);
    }

    getOffset(name: string): number|undefined {
        //console.warn(Array.from(this._offsets.keys()));
        return this._offsets.get(name);
    }

    get blockLength(): number {
        return this._blockLength || 0;
    }
}

export interface IRendererResourceDTO extends IAppObject {
    blockLength: number;
}


export const WTK_GEN_RENDERER_RESOURCE_DTO = <D extends RESOURCES_DTO<IRendererResourceSetup>>(dto: Constructor<D>, settings: IRendererResourceDTO): Constructor<D> => {

    const skull = __skull__(dto, {
        onCreate: createResourceDTO,
        onInit: initResourceDTO,
        settings
    }) as Constructor<D>;

    return WTK_GEN_OBJECT(skull, settings);
}

const createResourceDTO = <D extends RESOURCES_DTO<IRendererResourceSetup>>(dto: D, settings: IRendererResourceDTO) => {
    dto.setBlockLength(settings.blockLength);
}

const initResourceDTO = async <D extends RESOURCES_DTO<IRendererResourceSetup>>(dto: D, settings: IRendererResourceDTO) => {
}