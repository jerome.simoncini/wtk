import { IRendererResourceSetup } from "./RendererResources.js";
import { RESOURCES_DTO, WTK_GEN_RENDERER_RESOURCE_DTO } from "./ResourcesDTO.js";
import { ISceneNodeTransform, SceneNode } from "./scenes/SceneGraph.js";
import { Utils } from "./utils.js";


export enum CameraType {
    PERSPECTIVE,
    ORTHOGRAPHIC
}


export interface ICameraResource {
    name: string;
    type: CameraType;
    projection: ICameraProjection;
    selected?: boolean;
}


export interface ICameraView {
    origin: number[];
    lookAt: number[];
    up: number[];
}


export interface ICameraProjection {
    near: number;
    far: number;
}

export interface IPerspectiveCameraProps extends ICameraProjection {
    fov: number;
    aspectRatio: number;
}

export interface IOrthographicCameraProps extends ICameraProjection {
    left: number;
    right: number;
    bottom: number;
    top: number;
}

export interface ICameraNode extends IRendererResourceSetup {
    node: SceneNode;
    projection: ICameraProjection;
    type: CameraType;
}

const CAMERA_BLOCK_SIZE = 32;

export class CAMERAS_LOGIC extends RESOURCES_DTO<ICameraNode> {
    
    loadResource(camera: ICameraNode): Float32Array {
        const data: Float32Array = new Float32Array(CAMERA_BLOCK_SIZE);
        data.set(Utils.buildCameraView(camera.node));
        data.set(Utils.buildCameraProjection(camera.type,camera.projection), 16);
        return data;
    }
    
    updateNode(name: string, node: SceneNode) {
        console.warn(`update node '${name}'`);
        const offset = this.getOffset(name);
        if(offset!=undefined) {
            console.warn(offset);
            this.write(offset,Utils.buildCameraView(node))   
        }
    }
}

export const Cameras = WTK_GEN_RENDERER_RESOURCE_DTO(CAMERAS_LOGIC, {
    blockLength: CAMERA_BLOCK_SIZE,
    name: "Cameras"
});