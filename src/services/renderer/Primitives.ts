import { Mask } from "../../../src/utils.js";
import { AppObject } from "../../AppObject.js";
import { WTK_GEN_OBJECT } from "../../_gen_object.js";
import { CubeShape } from "./primitives/Cube.js";

export enum PrimitiveShapeType {
    PLANE,
    CUBE,
    UV_SPHERE,
    ICOSPHERE,
    PYRAMID,
    TORUS,
}

export enum PrimitiveShapeDataFlag {
    VERTICES,
    UV_COORDS,
    NORMALS,
    INDICES
}

export interface IPrimitiveShapeProps {

}

export interface IPrimitiveShapeSetup<T extends IPrimitiveShapeProps> {    
    type: PrimitiveShapeType,
    flags: PrimitiveShapeDataFlag[],
    props: T;
}

export interface IPrimitiveShape {
    vertices: number[],
    uvcoords: number[],
    normals: number[],
    indices: number[]
}

export interface IPrimitive {
    name: string;
    setup: IPrimitiveShapeSetup<IPrimitiveShapeProps>
}

/*********************************************************************************/

export class PRIMITIVES_LOGIC extends AppObject {

    create(setup: IPrimitiveShapeSetup<IPrimitiveShapeProps>): IPrimitiveShape|undefined {
        let shape: IPrimitiveShape|undefined = undefined; 

        switch(setup.type) {
            case PrimitiveShapeType.CUBE:
                const p = new CubeShape();
                const flags = new Mask();
                flags.setBits(...setup.flags);
                shape = p.create(flags, setup.props)
        }

        return shape;
    }
    
}

export const Primitives = WTK_GEN_OBJECT(PRIMITIVES_LOGIC, {
    name: "Primitives"
});
