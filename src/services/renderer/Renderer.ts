import { ASSERT, DoubleBuffer, MESSAGE } from "../../utils.js";
import { AppObject } from "../../AppObject.js";
import { __skull__ } from "../../_skull.js";
import { IAppObject, WTK_GEN_OBJECT } from "../../_gen_object.js";
import { Constructor } from "../../Constructor.js";
import { System } from "../../events.js";
import { Mask } from "../../utils.js";
import { StateSlots } from "../../mixins/StateSlots.js";
import { IResource, RESOURCES_LOGIC, Resources } from "./Resources.js";
import { ISceneActors, SCENE_LOGIC } from "./scenes/Scene.js";
import { IColor4 } from "./utils.js";
import { ISceneGraphActors } from "./scenes/SceneGraph.js";
import { PRIMITIVES_LOGIC, Primitives } from "./Primitives.js";
import { MATERIALS_LOGIC, Materials } from "./Materials.js";
import { LIGHTS_LOGIC, Lights } from "./Lights.js";
import { CAMERAS_LOGIC, Cameras } from "./Cameras.js";

export type CONTEXT = WebGL2RenderingContext | any;    //////////////////////////
export type RENDERER = RENDERER_LOGIC<CONTEXT, IRendererSetup>;

export interface IRenderer extends IAppObject {
    defaults?: IRendererDefaults;
    onStart?: (renderer: RENDERER_LOGIC<any, any>) => void;
    onShutdown?: (renderer: RENDERER_LOGIC<any, any>) => void;
}

export interface IRendererDefaults {
    clearColor?: IColor4;
}




export enum RenderFlag {
    SYNC_RESOURCES_MANAGER,
    SYNC_CURRENT_SCENE,
    SYNC_RENDER_STATE,
    SYNC_SCENE_STATE,
    RENDER_FRAME,
    LOAD_SCENE,
    UPDATE_SCENE_GRAPH,
    SYNC_SCENE_GRAPH,
    SYNC_SCENE_NODES,
    SWAP_SCENE,
    LOAD_RENDER_STATES,
    SHUTDOWN,
    SYNC_SCENE,
    UPDATE_SCENE
}

export interface IRendererSetup {
    resources?: IResource[];
}

export interface IViewport {
    width: number;
    height: number;
}

export interface ICommandPipelineStage {
    flag: number;
    cmd: (renderer: RENDERER) => void;
}

export class CommandPipeline {
    private _cmds: Map<number, (renderer: RENDERER) => void> = new Map();

    add(stages: ICommandPipelineStage[]) {
        for (const stage of stages) {
            this._cmds.set(stage.flag, stage.cmd);
        }
    }

    execute(renderer: RENDERER) {
        for (const flag of this.flags) {
            if (renderer.flags.has(flag)) {
                this._cmds.get(flag)!(renderer);
                renderer.flags.unset(flag);
            }
        }
    }

    private get flags(): number[] {
        return Array.from(this._cmds.keys())
    }
}


export abstract class RENDERER_LOGIC<C extends CONTEXT, S extends IRendererSetup> extends StateSlots(AppObject) {
    private _ctx: C | undefined = undefined;
    private _flags: Mask = new Mask();
    private _resources: DoubleBuffer<RESOURCES_LOGIC | undefined> = new DoubleBuffer();
    private _scene: DoubleBuffer<SCENE_LOGIC<RENDERER_LOGIC<CONTEXT, S>, S> | undefined> = new DoubleBuffer();
    protected _renderState: DoubleBuffer<S | undefined> = new DoubleBuffer();
    protected _defaults: IRendererDefaults | undefined = undefined;
    protected _setupCmds: CommandPipeline = new CommandPipeline();
    protected _extCmds: CommandPipeline = new CommandPipeline();
    protected _primitives: PRIMITIVES_LOGIC | undefined = undefined;

    setPrimitives(primitives: PRIMITIVES_LOGIC) {
        this._primitives = primitives;
    }

    setDefaults(defaults: IRendererDefaults) {
        this._defaults = defaults;
    }

    initState() {
        this._resources.swap();
        this._scene.swap();
        this._renderState.swap();

        ///////////////////////////////////////////////
        this._setupCmds.add(
            [
                {
                    flag: RenderFlag.SYNC_RESOURCES_MANAGER,
                    cmd: (renderer: RENDERER) => {
                        renderer.swapResources();
                    }
                },
                {
                    flag: RenderFlag.SYNC_CURRENT_SCENE,
                    cmd: (renderer: RENDERER) => {
                        renderer.swapScene();
                        renderer.flags.setBits(RenderFlag.LOAD_SCENE);
                    }
                },
                {
                    flag: RenderFlag.SYNC_RENDER_STATE,
                    cmd: (renderer: RENDERER) => {
                        renderer.swapRenderState();
                        renderer.flags.setBits(RenderFlag.RENDER_FRAME, RenderFlag.UPDATE_SCENE);        ///////
                    }
                },
                {
                    flag: RenderFlag.SYNC_SCENE_STATE,
                    cmd: (renderer: RENDERER) => {
                        if (renderer.scene.syncState()) {
                            renderer.syncRenderState();
                        }
                    }
                },
                {
                    flag: RenderFlag.SYNC_SCENE_GRAPH,
                    cmd: (renderer: RENDERER) => {
                        renderer._syncSceneGraph().then(() => {
                            renderer.flags.setBits(RenderFlag.SYNC_SCENE_NODES);
                        })
                    }
                },
                {
                    flag: RenderFlag.SYNC_SCENE_NODES,
                    cmd: (renderer: RENDERER) => {
                        renderer.scene.syncNodes();                              //
                    }
                },
                {
                    flag: RenderFlag.LOAD_RENDER_STATES,
                    cmd: (renderer: RENDERER) => {
                        renderer._loadRenderStates().then(() => {
                            renderer.syncRenderState();
                        });
                    }
                },
                {
                    flag: RenderFlag.UPDATE_SCENE_GRAPH,         //////////////
                    cmd: (renderer: RENDERER) => {
                        renderer.scene.graph.build(undefined).then((nodes: ISceneGraphActors) => {
                            renderer.scene.updateNodes(nodes);
                            renderer.flags.setBits(RenderFlag.SYNC_SCENE_GRAPH);
                        });
                    }
                },
                {
                    flag: RenderFlag.LOAD_SCENE,
                    cmd: (renderer: RENDERER) => {
                        renderer.flags.unset(RenderFlag.RENDER_FRAME);               //////////////
                        renderer.resources.load(this.scene.resources)
                            .then(() => {
                                const p: Promise<void>[] = [];
                                //console.warn(this.scene.graphics);
                                for (const graphics of renderer.scene.graphics) {
                                    p.push(renderer.resources.load(graphics.resources || []));
                                }
                                Promise.all(p).then(() => {
                                    renderer.flags.setBits(RenderFlag.LOAD_RENDER_STATES);
                                });
                            })
                    }
                },
                {
                    flag: RenderFlag.UPDATE_SCENE,
                    cmd: (renderer: RENDERER) => {
                        renderer.scene.updateScene()   //       
                            .then((nodes) => {
                                renderer.onSceneUpdate(nodes);
                                renderer.flags.setBits(RenderFlag.SYNC_SCENE, RenderFlag.SWAP_SCENE);
                            });
                    }
                },
            ]
        );
    }

    syncScene() {

    }

    setResources(resources: RESOURCES_LOGIC): void {
        this._resources.set(resources);
        this._flags.setBits(RenderFlag.SYNC_RESOURCES_MANAGER);
    }

    loadScene(scene: SCENE_LOGIC<RENDERER_LOGIC<CONTEXT, S>, S>): void {
        this._scene.set(scene);
        this._flags.setBits(RenderFlag.SYNC_CURRENT_SCENE);
    }

    swapResources() {
        this._resources.swap();
    }

    swapScene() {
        this._scene.swap();
    }

    swapRenderState() {
        this._renderState.swap();
    }

    syncRenderState(): void {
        console.warn(MESSAGE(this, `(sync render state)`));
        if (this._syncRenderState()) {
            this._flags.setBits(RenderFlag.SYNC_RENDER_STATE);
        }
        else {
            // notify error
        }
    }

    start(context: C) {
        this._ctx = context;
        this.startNotifier([System.LOGIC_ACTION]);

        this.initState();

        this.onStart();

        this._renderLoop();
    }

    shutdown() {
        this.stopNotifier([System.LOGIC_ACTION]);

        this._flags.setBits(RenderFlag.SHUTDOWN);
    }

    _renderLoop() {
        if (!this.renderState) {   //
            this.flags.unset(RenderFlag.RENDER_FRAME);
        }

        this._setupCmds.execute(this);
        this._extCmds.execute(this);

        if (this.flags.has(RenderFlag.RENDER_FRAME)) {

            this.scene.inputSystem.process();

            this.scene.animate();

            this.scene.update();
            
            this._render();

        }
        else {
            // loading...
        }

        if (this.flags.has(RenderFlag.SHUTDOWN)) {
            this.onShutdown();
        }
        else {
            const start = Date.now(); 

            requestAnimationFrame(() => {

                this._renderLoop();
                
                const end = Date.now(); 
                const elapsed = (end - start) * 0.001;
                const fps = (1.0 / elapsed).toFixed();
                //console.warn(`start=${start} end=${end} fps=${fps}`);
            });
        }
    }

    private onStart() {
        this._onStart(this);
    }

    private onShutdown() {
        this._onShutdown(this);
    }

    get context(): C {
        return ASSERT(this._ctx, MESSAGE(this, `rendering context undefined`));
    }

    protected async _loadRenderStates(): Promise<void> {    /////
        throw MESSAGE(this, `(_loadRenderStates) not implemented`);
    }

    protected _syncRenderState(): boolean {    //////
        return false;
    }

    protected get resources(): RESOURCES_LOGIC {
        return ASSERT(this._resources.get(), MESSAGE(this, `resources undefined`));
    }

    protected get scene(): SCENE_LOGIC<RENDERER_LOGIC<any, S>, S> {
        return ASSERT(this._scene.get(), MESSAGE(this, `scene undefined`));
    }

    protected get renderState() {
        return this._renderState.get();
    }

    protected get primitives(): PRIMITIVES_LOGIC {
        return ASSERT(this._primitives, MESSAGE(this, `primitives manager undefined`));
    }

    get flags(): Mask {
        return ASSERT(this._flags, MESSAGE(this, `render flags undefined`));
    }

    get ext(): CommandPipeline {        ///////////////////////
        return this._extCmds;
    }

    protected abstract _syncSceneGraph(): Promise<void>;
    protected abstract _render(): void;

    _onStart(renderer: RENDERER_LOGIC<any, any>): void { }
    _onShutdown(renderer: RENDERER_LOGIC<any, any>): void { }


    abstract onSceneUpdate(actors: ISceneActors);   ////

    abstract resize(width: number, height: number): void;   ////

    /*******************************************************************************************/

    private _materials: MATERIALS_LOGIC | undefined = undefined;
    private _lights: LIGHTS_LOGIC | undefined = undefined;
    private _cameras: CAMERAS_LOGIC | undefined = undefined;

    setMaterialsManager(materials: MATERIALS_LOGIC) {
        this._materials = materials;
    }

    setLightsManager(lights: LIGHTS_LOGIC) {
        this._lights = lights;
    }

    setCamerasManager(cameras: CAMERAS_LOGIC) {
        this._cameras = cameras;
    }

    get materials(): MATERIALS_LOGIC {
        return ASSERT(this._materials, MESSAGE(this, `materials manager undefined`));
    }

    get lights(): LIGHTS_LOGIC {
        return ASSERT(this._lights, MESSAGE(this, `lights manager undefined`));
    }

    get cameras(): CAMERAS_LOGIC {
        return ASSERT(this._cameras, MESSAGE(this, `cameras manager undefined`));
    }
}

export const gen_renderer = <T extends RENDERER_LOGIC<any, any>>(renderer: Constructor<T>, settings: IRenderer): Constructor<T> => {
    const skull = __skull__(renderer, {
        onCreate: _create_renderer_,
        onInit: _init_renderer_,
        settings: settings
    }) as Constructor<T>;
    return WTK_GEN_OBJECT(skull, {
        ...settings,
        onCreate: (_: T) => {
            console.warn(MESSAGE(_, "******************"));
            console.warn(MESSAGE(_, "     ON CREATE    "));
            console.warn(MESSAGE(_, "******************"));

            _.setResources(_._localStore.get(Resources));
            _.setPrimitives(_._localStore.get(Primitives));
            _.setMaterialsManager(_._localStore.get(Materials));
            _.setLightsManager(_._localStore.get(Lights));
            _.setCamerasManager(_._localStore.get(Cameras));


        },
        onDestroy: (_: T): void => {
            console.warn(MESSAGE(_, "******************"));
            console.warn(MESSAGE(_, "     ON DESTROY   "));
            console.warn(MESSAGE(_, "******************"));

            _.shutdown();

            _._localStore.release(Resources);
            _._localStore.release(Primitives);
            _._localStore.release(Materials);
            _._localStore.release(Lights);
            _._localStore.release(Cameras);

        },
        createLocalStore: true,
    }) as Constructor<T>
}

const _create_renderer_ = (renderer: RENDERER_LOGIC<any, any>, settings?: IRenderer) => {
    if (settings?.defaults) {
        renderer.setDefaults(settings.defaults);
    }

    if (settings?.onStart) {
        renderer._onStart = settings.onStart;
    }

    if (settings?.onShutdown) {
        renderer._onShutdown = settings.onShutdown;
    }
}

const _init_renderer_ = async (renderer: RENDERER_LOGIC<any, any>, settings?: IRenderer) => {

}
