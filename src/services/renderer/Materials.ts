import { IRendererResourceSetup } from "./RendererResources.js";
import { RESOURCES_DTO, WTK_GEN_RENDERER_RESOURCE_DTO } from "./ResourcesDTO.js";

export enum MaterialType {
    PHONG
}

export interface IMaterials {
    setup: IMaterialResource[];
    bindings: IMaterialBindings[];
}


export interface IMaterialBindings {
    entity: string;
    sub: string;
    name: string;
}

export interface IMaterialResource extends IRendererResourceSetup {
    type: MaterialType;
    props: IMaterialProps;
}

export interface IMaterialProps {
    blur: boolean;
    lightSource: boolean;
}

export interface IPhongMaterialProps extends IMaterialProps {
    diffuseColor: number[];
    emissiveColor: number[];
    specularColor: number[];
    shininess: number;
    emissiveIntensity: number;
}


const MATERIAL_BLOCK_SIZE = 20;


export class MATERIALS_LOGIC extends RESOURCES_DTO<IMaterialResource> {

    loadResource(material: IMaterialResource): Float32Array {
        let data: number[] = [];

        switch (material.type) {
            case MaterialType.PHONG:
                const props = material.props as IPhongMaterialProps;
                data = [
                    ...props.diffuseColor, 0,
                    ...props.emissiveColor, 0,
                    ...props.specularColor, 0,
                    ...[props.shininess, props.emissiveIntensity, 0, 0],
                    ...[Number(props.blur), Number(props.lightSource), 0, 0]
                ];
                break;
        }

        return new Float32Array(data);
    }

}

export const Materials = WTK_GEN_RENDERER_RESOURCE_DTO(MATERIALS_LOGIC, {
    blockLength: MATERIAL_BLOCK_SIZE,
    name: "Materials"
});