import { IColor4 } from "./utils.js";

export declare type WGLResource = any;   //////

export const CLEAR_COLOR_DEFAULT: IColor4 = {  
    r: 0, 
    g: 0, 
    b: 0, 
    a: 1
}