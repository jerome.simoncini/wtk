import { AppObject } from "../../AppObject.js";
import { DoubleBuffer, MESSAGE } from "../../utils.js";
import { Constructor } from "../../Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "../../_gen_object.js";
import { CONTEXT } from "./Renderer.js";


export interface IRendererResource {
    timestamp: number;
}

export interface IRendererResourceSetup {
    name: string;
}

export abstract class RENDERER_RESOURCES<
    C extends CONTEXT,
    T extends IRendererResource,
    S extends IRendererResourceSetup,
> extends AppObject {
    protected _resources: Map<string, DoubleBuffer<T>> = new Map();

    get(name: string): T | undefined {
         console.warn(this._resources.get(name));

        return this._resources.get(name)?.get() || undefined;
    }

    create(context: C, name: string): boolean {
        if(!this._resources.get(name)) {
            const r = this.createResource(context, Date.now());
            if (r) {
                if (this.submit(name, r)) {
                    this.swap(name);
                }
            }
            else {
                // notify error
            }
        }
        return this._resources.get(name)?.get() != undefined;
    }

    async write(context: C, setup: S, create: boolean = false): Promise<void> {
        if (!this._resources.has(setup.name) && create == true) {
            if (!this.create(context, setup.name)) {
                // notify error
                return;
            }
        }

        const r = this._resources.get(setup.name)?.get();
        if (r) {
            r.timestamp = Date.now();
            await this.editResource(context, setup)
                .then(() => {
                    if (this.submit(setup.name, r)) {
                        this.swap(setup.name);              //////
                    }
                });
        }
        else {
            // notify error
        }

    }

    unload(context: C, name: string) {
        const r = this.get(name);
        if (r) {
            console.warn(MESSAGE(this,`unload ${name}`));
            this.unloadResource(context, r);
            this._resources.delete(name);
        }
    }

    submit(name: string, resource: T): boolean {
        if (!this._resources.has(name)) {
            this._resources.set(name, new DoubleBuffer());
        }
        const r = this._resources.get(name);
        if (r) {
            const current = r.get();
            if (!current || current.timestamp < resource.timestamp) {
                r.set(resource);
                return true;
            }
        }
        else {
            // notify error
        }
        return false
    }

    swap(name: string): void {
        if (this._resources.has(name)) {
            this._resources.get(name)?.swap();
        }
        else {
            //
        }
    }

    clearAll(context: C): void {
        Array.from(this._resources.keys())
            .forEach(r => this.unload(context, r));
    }

    protected abstract createResource(context: C, timestamp: number): T | undefined;     //
    protected abstract unloadResource(context: C, r: T): void;                           //
    protected async editResource(context: C, setup: S): Promise<void> { throw MESSAGE(this, `(editResource) not implemented`) }
}

export interface IRendererResources extends IAppObject {

}

export const gen_renderer_resources = <T extends RENDERER_RESOURCES<any, any, any>>(_: Constructor<T>, settings: IRendererResources): Constructor<T> => {
    return WTK_GEN_OBJECT(_, {
        onCreate: settings.onCreate,
        onDestroy: (_: RENDERER_RESOURCES<any, any, any>, context: any): void => {
            _.clearAll(context);
        },
        name: settings.name || ''
    })
}
