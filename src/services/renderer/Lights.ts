import { IRendererResourceSetup } from "./RendererResources.js";
import { RESOURCES_DTO, WTK_GEN_RENDERER_RESOURCE_DTO } from "./ResourcesDTO.js";


export enum LightType {
    POINT_LIGHT
}


export interface ILightResource extends IRendererResourceSetup {
    type: LightType;
    props: ILightProps;
}

export interface ILightProps {
}

export interface IPointLightProps extends ILightProps {
    color: number[];
    intensity: number;
}

const LIGHT_BLOCK_SIZE = 4;


export class LIGHTS_LOGIC extends RESOURCES_DTO<ILightResource> {
    
    loadResource(light: ILightResource): Float32Array {
        let data: number[] = [];

        switch (light.type) {
            case LightType.POINT_LIGHT:
                const props = light.props as IPointLightProps;
                data = [
                    ...props.color,
                    props.intensity
                ];
                break;
        }

        return new Float32Array(data);
    }
}

export const Lights = WTK_GEN_RENDERER_RESOURCE_DTO(LIGHTS_LOGIC, {
    blockLength: LIGHT_BLOCK_SIZE,
    name: "Lights"
});