

export enum WEvent {
    CREATE_ACCOUNT,
    LOCAL_LOGIN,
    OPEN_SESSION,
    CLOSE_SESSION,
    SESSION_OPENED,
    SESSION_CLOSED
}


export enum WAuthorization {
    READ_LINES = 0,    ///
    WRITE_LINES = 1,   ///
    GRANT = 2,
    REVOKE = 3,
    LS = 5,
    FIND = 6,
    TOUCH = 7,
    MKDIR = 8,
    RM = 9,
    LN = 10,
    CP = 11,
    MV = 12,
    NODE = 13,
    SU = 14,
    CHANGE_PASSWORD = 15
}

export enum WActionStatus {
    SUCCESS,
    ERROR,
    UNAUTHORIZED,
    LOCKED
}

export interface IWAuthorization {
    required: WAuthorization[];
    optional?: WAuthorization[];
    path: string;
}

export interface IWActionResult<T> {
    status: WActionStatus;
    message: string;
    result: T;
}

export interface IReadLines {
    lines: string[][];
}

export interface IWriteLines {
}

