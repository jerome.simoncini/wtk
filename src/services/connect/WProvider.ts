import { WTK_GEN_OBJECT } from "../../_gen_object.js";
import { Constructor } from "../../Constructor.js";
import { ASSERT, MESSAGE } from "../../utils.js";
import { AppObject } from "../../AppObject.js";
import { System } from "../../events.js";
import { Scope } from "../../Scope.js";
import { WService } from "./WService.js";
import { IUserSession, IWProviderInit, W_SYS_LOGIC } from "./WSys.js";
import { IWActionResult, WAuthorization, WEvent } from "./W.js";



const w = WService(AppObject) as Constructor<AppObject>;


export interface IPod {
    name: string;
    url: string;
}

export interface IWLoginInit {
    id?: string;
    c_url?: string;
    s_url: string;
}


type SOCKET = any;    //
declare const io: SOCKET;

export abstract class W_PROVIDER_LOGIC extends w {

    protected _socket: SOCKET | undefined = undefined;
    protected _session_id: string | undefined = undefined;
    protected _s_url: string | undefined = undefined;
    protected _c_url: string | undefined = undefined;


    init(config: IWProviderInit) {

        const c_url = config.url.href;
        const s_url = `${config.url.protocol}//${config.url.hostname}:${config.w_port}`;
        this.setCUrl(c_url);
        this.setSUrl(s_url);
    }

    async initLogin(pod_url: string, username: string): Promise<IWLoginInit> {

        console.warn(this.s_url);

        let output: IWLoginInit = await fetch(this.s_url + '/init_login', {
            headers: {

            },
            body: JSON.stringify({
                pod_url, username
            }),
            method: 'POST'
        }

        )
            .then(response => response.json())
            .then(data => {
                console.warn(data);
                return {
                    id: data?.code,
                    c_url: this.c_url,
                    s_url: this.s_url
                }
            });

        return output
    }


    async connect(pod: IPod, username: string, password: string) {


        await fetch(this.s_url + '/connect', {
            headers: {

            },
            body: JSON.stringify({
                username, password
            }),
            method: 'POST'
        })
            .then(response => response.json())
            .then(data => {
                console.warn(data);

                if (data?.session) {
                    this.send(System.LOGIC_ACTION, WEvent.OPEN_SESSION, {       //
                        id: data.session, username, podname: pod.name, url: pod.url
                    } as IUserSession);
                }
            });

    }


    async openSession(session_id: string) {

        await fetch(`${this.s_url}/open`, {
            headers: {},
            body: JSON.stringify({ session_id }),
            method: 'POST'
        })
            .then(response => response.json())
            .then(data => {
                if (data.session) {

                    this._socket = io(this.s_url);
                    console.warn(this._socket);

                    try {

                        this.socket.on('connect', () => {
                            console.warn(MESSAGE(this, 'system socket connected'));

                            this.socket.emit(
                                'connected',
                                { session_id },
                                (response) => {
                                    console.warn(response);

                                    this._session_id = session_id;     ///

                                    this.send(System.LOGIC_ACTION, WEvent.SESSION_OPENED, {
                                        session_id,
                                        username: data.session.username,    //
                                        podname: data.session.podname,       //
                                        env: "@admin",     ////////
                                        level: 3             /////////
                                    });       //
                                });

                        });

                    }
                    catch (message) {
                        console.error(message);
                    }

                }
                else {
                    // error
                }
            })

    }



    async closeSession() {

        return new Promise(resolve => this.socket.emit(
            'logout',
            { session_id: this._session_id },
            (response) => {
                console.warn(response);
                const r = response ? JSON.parse(response) : [];     //
                return resolve(r.result);
            })
        ).then(() => {
            this._socket = undefined;
        });
    }


    ////////////////////////////////////////////////////////////////////////////////////////////


    async useCode<T>(action: WAuthorization, paths: string[], options: string[], data: {} = {}): Promise<IWActionResult<T>> {
        const wsys = this["get_wsys"]() as W_SYS_LOGIC;

        if (wsys.session) {
            data["env"] = wsys.session.env;
            data["level"] = wsys.session.level;
        }

        return new Promise(resolve => this.socket.emit(
            'use_code',
            { action, paths, options, data },
            (response) => {
                return resolve(JSON.parse(response));      //////////
            })
        )
    }


    ////////////////////////////////////////////////////////////////////////////////////////

    setCUrl(url: string) {
        this._c_url = url;
    }

    setSUrl(url: string) {
        this._s_url = url;
    }

    private get socket(): SOCKET {
        return ASSERT(this._socket, MESSAGE(this, `socket undefined`));
    }

    private get s_url(): string {
        return ASSERT(this._s_url, MESSAGE(this, `s_url undefined`));
    }

    private get c_url(): string {
        return ASSERT(this._c_url, MESSAGE(this, `c_url undefined`));
    }
}



export const WTK_GEN_W_PROVIDER = <T extends W_PROVIDER_LOGIC>(provider: Constructor<T>): Constructor<T> => {
    return WTK_GEN_OBJECT(provider, {
        onCreate: (_: W_PROVIDER_LOGIC): void => {
            _.startNotifier([System.LOGIC_ACTION], Scope.GLOBAL);
        },
        onDestroy: (_: W_PROVIDER_LOGIC): void => {
            _.stopNotifier([System.LOGIC_ACTION], Scope.GLOBAL);
        },
        name: 'Pod Provider'
    }) as Constructor<T>
}
