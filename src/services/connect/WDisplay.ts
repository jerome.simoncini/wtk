import { WTK_APP_ROUTER_LOGIC } from "../../AppRouter.js";
import { IAppPage, WTK_GEN_APP_PAGE } from "../../_gen_page.js";
import { WTK_APP_PAGE_LOGIC } from "../../AppPage.js";
import { AppNodeFlag } from "../../AppComponent.js";
import { Constructor } from "../../Constructor.js";
import { Scope } from "../../Scope.js";
import { _HTML_ } from "../../Compiler.js";
import { WService } from "./WService.js";
import { IWConnectInit, IWProviderInit, W_SYS_LOGIC } from "./WSys.js";

const W_ROUTER = 'w-router';

const p = WService(WTK_APP_PAGE_LOGIC) as Constructor<WTK_APP_PAGE_LOGIC>;

export class W_DISPLAY_LOGIC extends p {

    async startDisplay(page_root: string) {
        const router = this.wsys.router as WTK_APP_ROUTER_LOGIC | undefined;

        return router ? this.mount(W_ROUTER, router)
            .then(async () => {
                const origin = this.getMetas('origin')[0].replace(page_root, '');    ////

                console.warn(origin);
                console.warn(router);

                if (router) {
                    console.warn(this.path);

                    router.initState(this.path, '/login');   //

                    await router.render().then(() => origin.startsWith('/connect') ? router.navigate(origin) : router.display(true));
                }
                else {
                    // error
                    this.nodeflags.setBits(AppNodeFlag.ERROR)
                }

            }) : this.nodeflags.setBits(AppNodeFlag.ERROR);
    }

    async stopDisplay() {                                //////
        console.warn(`[WDisplay] stop`);
        return this.umount(W_ROUTER)
            .then(() => this.stopWSys())
            .then(() => {
                this.getObjectStore(Scope.LOCAL).clear();
                this.setMeta('origin', '');                     ///////////
            });
    }

    async startWSys<T extends IWConnectInit>(wsys_config: T, provider_config: IWProviderInit) {

        const s = await this.wsys.start(wsys_config, provider_config);

        if (!s) {                                           ////////////////
            this.nodeflags.setBits(AppNodeFlag.ERROR);
        }
    }

    async stopWSys() {
        this.wsys.stop();     //
    }

    get wsys(): W_SYS_LOGIC {
        return this["get_wsys"]();
    }
}


export interface IWDisplay<T extends IWConnectInit> extends IAppPage {
    connect: T,
    wsys: Constructor<W_SYS_LOGIC>,
    session: string
    port: number,
    s_url?: URL                    //

}

export const WTK_GEN_W_DISPLAY = <T extends W_DISPLAY_LOGIC, I extends IWConnectInit>(c: Constructor<T>, settings: IWDisplay<I>): Constructor<T> => {
    return WTK_GEN_APP_PAGE(c, {
        html: (_: T) => {
            return _HTML_(`<< ${W_ROUTER} >>`, _)
        },
        slots: [
            { name: W_ROUTER }
        ],
        onCreate: (_: T) => {                              
            _["setWSys"](_.getObjectStore(Scope.LOCAL).get(settings.wsys));
        },
        onShow: async (_: T) => {                
    
            return _.startWSys(
                settings.connect,
                {
                    url: new URL(window.location.href),
                    w_port: settings.port
                }
            );
    
        },
        onHide: async (_: T) => {
    
            return _.stopDisplay()                          ////
        },
        onReady: async (_: T) => {                     ///////
    
            return  _.startDisplay(settings.session);
        },
        createLocalStore: true,
        name: "W Dsiplay",
        ...settings,
    }) as Constructor<T>;
}