import { WTK_GEN_APP_ROUTER } from "../../_gen_router.js"
import { WTK_APP_PAGE_LOGIC } from "../../AppPage.js"
import { WTK_APP_ROUTER_LOGIC } from "../../AppRouter.js"
import { Constructor } from "../../Constructor.js"


export interface IWRouter {
    login: Constructor<WTK_APP_PAGE_LOGIC>,
    connect: Constructor<WTK_APP_PAGE_LOGIC>,
    session: Constructor<WTK_APP_PAGE_LOGIC>,
    error: Constructor<WTK_APP_PAGE_LOGIC>
} 


export const WTK_GEN_W_ROUTER = <T extends WTK_APP_ROUTER_LOGIC>(router: Constructor<T>, settings: IWRouter): Constructor<T> => {
    return WTK_GEN_APP_ROUTER(router, {
       styles: [
           'router'
       ],
       routes: [
           { path: '/login', target: settings.login, name: 'login' },
           { path: '/connect/:session', target: settings.connect, name: 'connect' },
           { path: '/session', target: settings.session, name: 'session' },
           { path: '/error', target: settings.error, name: 'error' },
           { path: "*", redirectTo: '' },
       ],
       tagname: 'current-display',
       name: "W Router"
   }) as Constructor<T>
}

