import { IAppPage, WTK_GEN_APP_PAGE } from "../../../_gen_page.js";
import { IWLoginInit, W_PROVIDER_LOGIC } from "../../../../../wtk/src/services/connect/WProvider.js";
import { _HTML_ } from "../../../Compiler.js";
import { Constructor } from "../../../Constructor.js";
import { W_PAGE_LOGIC } from "./WPage.js";



export class W_LOGIN_LOGIC extends W_PAGE_LOGIC {

    enable() {
        const webId = this.getElement('web_id')?.value;
        const continueBtn = this.getElement('continue');
        if (webId && continueBtn) {
            continueBtn.disabled = webId == "";      ///
        }
        else {
            // error
        }
    }
};



export const WTK_GEN_W_LOGIN = <T extends W_LOGIN_LOGIC>(page: Constructor<T>, settings?: IAppPage): Constructor<T> => {
    return WTK_GEN_APP_PAGE(page, {
        html: async (_: T) => {

            console.warn(`[WLOGIN] html`);

            return _HTML_(`<div --title >
                                Please login to your pod provider
                           </div>
                           <div --form >
                               <div --field >
                                  <input @web_id type="text" style="width:300px" placeholder="http://localhost:9981/\@username">
                               </div>
                           </div>
                           <div --actions >
                               <button @continue disabled>continue</button>
                           </div>`, _);
        },
        onReady: async (_: T): Promise<void> => {
            console.warn(`[WLOGIN] on ready`);
            if (_.wsys.session?.id && _.wsys.session?.username) {         //
                await _.router.navigate('session');
            }
            else {
                _.wsys.resetSession();
            }
        },
        setup: [
            {
                name: "web_id",
                selector: { id: true, value: "web_id" },
                actions: [
                    {
                        event: 'input',
                        cmd: (_: T): void => {
                            _.enable()
                        }
                    }
                ]
            },
            {
                name: "continue",
                selector: { id: true, value: "continue" },
                actions: [
                    {
                        event: 'click',
                        cmd: async (_: T): Promise<void> => {
                            const webId = _.getElement('web_id')?.value;
                            const s = webId.split('@');
    
                            const provider = _.wsys.provider as W_PROVIDER_LOGIC;
    
                            console.warn(s);
                            console.warn(provider);
    
                            if (provider) {
    
                                const response: IWLoginInit = await provider.initLogin(s[0], s[1]);
    
                                if (response?.id) {
    
                                    const url = `${s[0]}/auth?username=${s[1]}&id=${response.id}&c_url=${response.c_url}&s_url=${response.s_url}`;
    
                                    window.location.href = url;
                                }
                                else {
                                    console.warn(response);
                                }
    
                            }
                            else {
                                console.error('error');
                            }
    
    
                        }
                    }
                ]
            }
        ],
        name: "WLogin",
        ...settings
    }) as Constructor<T>;
} 




