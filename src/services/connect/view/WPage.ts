import { WTK_APP_PAGE_LOGIC } from "../../../AppPage.js";
import { W_SYS_LOGIC } from "../WSys.js";


export abstract class W_PAGE_LOGIC extends WTK_APP_PAGE_LOGIC {

    get wsys(): W_SYS_LOGIC {  
        return this.router["get_wsys"]();
    }
}