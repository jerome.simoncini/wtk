import { Constructor } from "../../../Constructor.js";
import { IAppPage, WTK_GEN_APP_PAGE } from "../../../_gen_page.js";
import { _HTML_ } from "../../../Compiler.js";
import { W_PAGE_LOGIC } from "./WPage.js";


export class W_CONNECT_LOGIC extends W_PAGE_LOGIC {
    async initSocket() {
        const session = this.router.getRouteParameter('session');

        await this.wsys.openSession(session);
    }
}



export const WTK_GEN_W_CONNECT = <T extends W_CONNECT_LOGIC>(page: Constructor<T>, settings?: IAppPage): Constructor<T> => {
    return  WTK_GEN_APP_PAGE(W_CONNECT_LOGIC, {
        onShow: async (_: W_CONNECT_LOGIC) => {
            await _.initSocket();
        },
        ...settings
    }) as Constructor<T>
}
