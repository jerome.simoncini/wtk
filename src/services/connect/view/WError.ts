import { IAppPage, WTK_GEN_APP_PAGE } from "../../../_gen_page.js";
import { _HTML_ } from "../../../Compiler.js";
import { Constructor } from "../../../Constructor.js";
import { W_PAGE_LOGIC } from "./WPage.js";


export interface IWError {
    code: number;
    message: string;
}

export class W_ERROR_LOGIC extends W_PAGE_LOGIC {

    print(error: IWError) {
        const e = this.getElement('message');
        if(e) {
            e.innerHTML = `<div>${error.code}</div><div>${error.message}</div>`
        }
    }
}



export const WTK_GEN_W_ERROR = <T extends W_ERROR_LOGIC>(page: Constructor<T>, settings?: IAppPage): Constructor<T> => {
    return WTK_GEN_APP_PAGE(page, {
        html: (_: T) => {
            return _HTML_(`<div style="background-color: blue; color: snow;">
                              <div --title >
                                  Error
                              </div>
                              <div @message --message >
                              </div>
                              <div --actions >
                                 <button @cancel >back to login</button>
                              </div>
                            </div>`, _);
        },
        setup: [
            {
                name: "cancel",
                selector: { id: true, value: "cancel" },
                actions: [
                    {
                        event: 'click',
                        cmd: (_: T): void => {
                            _.router.navigate("/login");        //////
                        }
                    }
                ]
            },
            {
                name: "message",
                selector: { id: true, value: "message" },
                actions: [
                ]
            },
        ],
        name: "WError",
        ...settings
    }) as Constructor<T>;
} 




