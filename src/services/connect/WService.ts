import { ASSERT, MESSAGE } from "../../utils.js";
import { AppEntity } from "../../AppEntity.js";
import { Constructor } from "../../Constructor.js";
import { W_SYS_LOGIC } from "./WSys.js";



export const WService = (base: Constructor<AppEntity>): Constructor<AppEntity> => {
    return class extends base {

        private _wsys: W_SYS_LOGIC | undefined = undefined;

        setWSys(wsys: W_SYS_LOGIC) {
            this._wsys = wsys;
        }
    
        get_wsys(): W_SYS_LOGIC {
            return ASSERT(this._wsys, MESSAGE(this, `wsys undefined`));
        }

        wsys_undefined(): boolean {
            return this._wsys == undefined;
        }

    } as Constructor<AppEntity>;
}