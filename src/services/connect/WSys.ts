import { System } from "../../events.js";
import { AppObject } from "../../AppObject.js";
import { WTK_APP_ROUTER_LOGIC } from "../../AppRouter.js";
import { Constructor } from "../../Constructor.js";
import { Scope } from "../../Scope.js";
import { IAppObject, WTK_GEN_OBJECT } from "../../_gen_object.js";
import { ASSERT, MESSAGE } from "../../utils.js";
import { W_PROVIDER_LOGIC } from "./WProvider.js";
import { WEvent } from "./W.js";
import { W_ERROR_LOGIC } from "./view/WError.js";


export interface IUserSession {
    id: string;
    username: string;
    podname: string;
    url: string;
    env: string;  ///////
    level: number;   ///////
}


export interface IWConnectInit {
    router: Constructor<WTK_APP_ROUTER_LOGIC>;
    provider: Constructor<W_PROVIDER_LOGIC>;
}

export interface IWProviderInit {
    url: URL;
    w_port: number;     
}

export abstract class W_SYS_LOGIC extends AppObject {
    protected _router: WTK_APP_ROUTER_LOGIC | undefined = undefined;
    protected _provider: W_PROVIDER_LOGIC | undefined = undefined;
    protected _session: IUserSession | undefined = undefined;

    protected setRouter(router: WTK_APP_ROUTER_LOGIC) {
        this._router = router;
        this.router["setWSys"](this);
    }

    protected setProvider(provider: W_PROVIDER_LOGIC) {
        this._provider = provider;
        this.provider["setWSys"](this);
    }



    setSession(session: IUserSession) {
        this._session = session;
    }

    resetSession() {
        this._session = undefined;
    }


    async start(wsys_config: IWConnectInit, provider_config: IWProviderInit): Promise<boolean> {
        return this.onstart(wsys_config, provider_config);

    }

    async stop() {

        return this.onstop().then(() => {
            this.provider.closeSession()
                .then(async () => {
                    this._session = undefined;

                    await this.router.navigate('/login');
                });
        });
    }

    async openSession(session_id: string) {
        await this.provider.openSession(session_id);    /////
    }

    abstract onstart(config: IWConnectInit, provider_config: IWProviderInit): Promise<boolean>;
    abstract onstop(): Promise<boolean>;
    abstract onready(): Promise<boolean>;


    get router(): WTK_APP_ROUTER_LOGIC {
        return ASSERT(this._router, MESSAGE(this, `router undefined`));
    }

    get provider(): W_PROVIDER_LOGIC {
        return ASSERT(this._provider, MESSAGE(this, `provider undefined`));
    }

    get session(): IUserSession | undefined {           ////////////
        return this._session;
    }
}

/********************************************************************************/


export interface IWConnectWSys extends IAppObject {

}

export const WTK_GEN_WSYS = <T extends W_SYS_LOGIC>(wsys: Constructor<T>, settings?: IWConnectWSys): Constructor<T> => {
    return WTK_GEN_OBJECT(wsys, {
        name: "WSys",
        onCreate: (_: W_SYS_LOGIC): void => {
            _.startObserver([System.LOGIC_ACTION], Scope.LOCAL);
        },
        onDestroy: (_: W_SYS_LOGIC): void => {
            _.stopObserver([System.LOGIC_ACTION], Scope.LOCAL);
            _.stop();
        },
        listen: [
            {
                event: System.LOGIC_ACTION,
                action: WEvent.OPEN_SESSION,
                cmd: (_: W_SYS_LOGIC, session: IUserSession): void => {
                    //_.openSession(session);
                }
            },
            {
                event: System.LOGIC_ACTION,
                action: WEvent.CLOSE_SESSION,
                cmd: (_: W_SYS_LOGIC): void => {
                    _.stop();

                }
            },
            {
                event: System.LOGIC_ACTION,
                action: WEvent.SESSION_OPENED,
                cmd: (_: W_SYS_LOGIC, params: any): void => {
                    _.setSession({
                        id: params.session_id,
                        username: params.username,
                        podname: params.podname,
                        url: params.podname,
                        env: params.env,
                        level: params.level
                    });

                    _.onready().then(success => {
                        if (success) {
                            _.router.navigate('/session');
                        }
                        else {
                            _.router.navigate('/error');

                            const e = _.router.current as W_ERROR_LOGIC;
                            e.print({
                                code: 0,
                                message: "Error..."
                            });

                        }
                    })
                }
            },
            {
                event: System.LOGIC_ACTION,
                action: WEvent.SESSION_CLOSED,
                cmd: (_: W_SYS_LOGIC): void => {
                    _.router.navigate('/login');
                }
            }
        ],
    }) as Constructor<T>;
}
