import { WTK_DB_LOGIC } from "../AppDB.js";
import { IQueryResultHandler, WTK_DB_STORE_LOGIC } from "../AppDBStore.js";
import { AppObject } from "../AppObject.js";
import { AppStateService, StateVariable } from "../AppState.js";
import { Constructor } from "../Constructor.js";
import { FactoryMode, IModelGroupItem, ModelGroupItem, ModelLogic, WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC } from "../ModelGroup.js";
import { ASSERT } from "../utils.js";
import { ModelProps } from "../ModelProps.js";
import { WTK_FILE_SYNCED } from "../models/file/model.js";

export class WTK_FILE_SERVICE_LOGIC extends AppObject {
    private _file: ModelLogic<ModelProps> | undefined = undefined;
    private _fileFactory: Constructor<WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<ModelLogic<ModelProps>, ModelProps>> | undefined = undefined;
    private _fileStateService: Constructor<AppStateService> | undefined = undefined;
    private _db: Constructor<WTK_DB_LOGIC> | undefined = undefined;
    private _dbStore: Constructor<WTK_DB_STORE_LOGIC> | undefined = undefined;

    syncFile(params: any = undefined) {  //
        if(params?.modified) {
            const file = this.getFile();
            file.props.set(WTK_FILE_SYNCED, false);
            (<AppStateService>this.___(this.fileStateService)).reset(file);
        }
        else {
            this.resetFile(this.getFile());
        }
    }

    getFile() {
        return ASSERT((<AppStateService>this.___(this.fileStateService)).getState(), 'file state undefined')
    }

    resetFile(modelItem: IModelGroupItem) {
        this._file = (<WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<ModelLogic<ModelProps>, ModelProps>>this.___(this.fileFactory)).get(modelItem, FactoryMode.SINGLETON) as ModelLogic<ModelProps>;
    }

    save(filename: string, data: StateVariable<ModelGroupItem>, hdl: IQueryResultHandler): void {
        (<WTK_DB_LOGIC>this.___(this.db)).getStore(this.dbStore)!.create(filename, data.value, hdl);      //
    }

    delete(filename: string, hdl: IQueryResultHandler): void {
        (<WTK_DB_LOGIC>this.___(this.db)).getStore(this.dbStore)!.delete(filename, hdl);      //
    }

    getList(hdl: IQueryResultHandler) {
        (<WTK_DB_LOGIC>this.___(this.db)).getStore(this.dbStore)!.getAll(hdl);      //
    }

    private get file() {   //
        return ASSERT(this._file, `(${this.constructor.name}) file undefined`);
    }

    private get fileFactory() {
        return ASSERT(this._fileFactory, 'file factory undefined');
    }

    private get fileStateService() {
        return ASSERT(this._fileStateService, 'file state service undefined');
    }

    private get db() {
        return ASSERT(this._db, `(${this.constructor.name}) db undefined`);
    }

    private get dbStore() {
        return ASSERT(this._dbStore, `(${this.constructor.name}) db store undefined`);
    }

    setFileFactory(fileFactory: Constructor<WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<ModelLogic<ModelProps>,ModelProps>>) {
        this._fileFactory = fileFactory;
    }

    setStateService(stateService: Constructor<AppStateService>) {
        this._fileStateService = stateService;
    }

    setDB(db: Constructor<WTK_DB_LOGIC>) {
        this._db = db;
    }

    setDBStore(dbStore: Constructor<WTK_DB_STORE_LOGIC>) {
        this._dbStore = dbStore;
    }
}