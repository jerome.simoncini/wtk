import { Constructor } from "./Constructor.js";
import { MemoryPool } from "./MemoryPool.js";
import { Scope } from "./Scope.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";
import { System } from "./events.js";

const TYPE = "MemoryPool";

export const WTK_GEN_MEMORY = (page: Constructor<MemoryPool>, settings: IAppObject): Constructor<MemoryPool> => {
    const skull = __skull__(page, {
        onCreate: _create_memory_,
        onInit: _init_memory_,
        settings: settings
    }) as Constructor<MemoryPool>;
    return WTK_GEN_OBJECT(skull, {
        onCreate: (_: MemoryPool): void => {
            _.createRefFactory();

            _.startNotifier([
                System.MEMORY_UPDATE,
                System.MEMORY_ERROR
            ], Scope.GLOBAL);
        },
        onDestroy: (_: MemoryPool): void => {
            _.clearRefFactory();

            _.stopNotifier([
                System.MEMORY_UPDATE,
                System.MEMORY_ERROR
            ], Scope.GLOBAL);
        },
        ...settings
    }) as Constructor<MemoryPool>
};

const _create_memory_ = (memory: MemoryPool, settings: IAppObject): void => {
    memory.setObjectType(TYPE);
};

const _init_memory_ = async (memory: MemoryPool, settings: IAppObject) => {

};