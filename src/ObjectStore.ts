import { Constructor } from "./Constructor.js";
import { AppEntity } from "./AppEntity.js";
import { ObjectLoader } from "./ObjectLoader.js";
import { IObserverNotifierNames, ObserverNotifier } from "./ObserverNotifier.js";
import { AppEvent } from "./AppEvent.js";
import { AppObject } from "./AppObject.js";


export class ObjectStore extends ObserverNotifier {
    private _instances: AppEntity[] = [];

    private nextId: number = 1;

    _genId(): number {
        return this.nextId++;
    }

    get<T extends AppEntity>(importType: Constructor<T>, store: ObjectStore = this): T {
        let instance = this._find(importType);
        if (!instance) {
            instance = new importType(this._genId());
            if (instance) {
                instance.setParent(this.parent||this);    ////
                if(this.parent?.getModule(AppEntity)) {
                    instance.setAppModule(this.parent.module);
                }
                if(instance instanceof ObjectLoader) {
                    instance.setGlobalStore(store);
                }
                instance.onCreate();
                this._instances.push(instance);
            }
            else {
                // notify error
            }
        }

        return instance;
    }

    release<T extends AppEntity>(importType: Constructor<T>, ...args: any[]) {
        console.warn('realse object');                                           ///////////////
        const object = this._find(importType);
        console.warn(object);                                                    ///////////////
        if (object) {
            console.warn("destroy object");                                      ///////////////
            object.onDestroy(...args);
            this._instances = this._instances.filter(o => o != object);
        }
    }

    clear(...args: any[]) {
        for (const _ of this._instances) {
            _.onDestroy(...args);
        }
        this._instances = [];
    }

    protected _find<T extends AppEntity>(importType: Constructor<T>): T | undefined {
        if (!this._instances) {
            this._instances = [];
        }
        return this._instances.find(o => o instanceof importType) as T;
    }

    list(): string[] {
        return this._instances.map(i => i.inode);
    }

    getInode(inode: string): AppEntity|undefined {
        return this._instances.find(i=>i.inode==inode);
    }

    find(objectId: number): AppEntity|undefined {
        return this._instances.find(o=>o._objectId==objectId);
    }

    onNotify(event: AppEvent): void {
        this._notify(event);
    }

    loop<T extends AppEntity>(t: Constructor<T>, fun: (instance: T)=>void) {
        this._instances.forEach(i=>{
            if(i instanceof t) {
                //console.warn(`fun ${i.inode}`)
                fun(i);
            }
        })
    }

};