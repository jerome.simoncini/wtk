import { WTK_CONTAINER_LOGIC } from "./AppContainer.js";
import { Constructor } from "./Constructor";
import { LOGGER } from "./Logger.js";
import { WTK_APP_PAGE_LOGIC } from "./AppPage.js";
import { System } from "./events.js";
import { ASSERT } from "./utils.js";

const THIS = "App Router";
const DEBUG = LOGGER(THIS);
DEBUG.on();

interface IAppRoute {
    path: string;
    name?: string;
    target?: Constructor<WTK_APP_PAGE_LOGIC>;
    redirectTo?: string;
}

enum AppRouterUpdate {
    NAVIGATE
}

export class WTK_APP_ROUTER_LOGIC extends WTK_CONTAINER_LOGIC {
    private _names: Map<string, number> = new Map();
    private _routes: Map<string, string> = new Map();
    private _rootPath: string | undefined = undefined;
    private _origin: string | undefined = undefined;
    private _currentPageId: number | undefined = undefined;
    private _params: Map<string, string> = new Map();
    private _dynamicRouteMatch: string | undefined = undefined;
    private _level: number | undefined = undefined;
    private _history: string[] = [];

    initRoutes(routes: IAppRoute[]) {
        DEBUG.log(JSON.stringify(routes));
        for (const route of routes) {
            if (route.target && route.name) {
                const id = this.registerContent(route.target);
                const content = ASSERT(this.getContent(id) as WTK_APP_PAGE_LOGIC, `page create error`);
                if (content) {
                    content.setRouter(this);  //
                    content.setPath(this.rootPath + route.path);  //
                    content.setOrigin(route.path.replace(this.origin, ''));  //
                    DEBUG.log(`register content '${id}'->'${route.path}'`);
                    this._routes.set(route.path, route.name);
                    this._names.set(route.name, id);
                }
            }
            else if (route.redirectTo) {
                ASSERT(this._names.get(route.redirectTo), `route name undefined '${route.redirectTo}'`);
                this._routes.set(route.path, route.redirectTo);
            }
        }
        DEBUG.log(JSON.stringify(this._routes));
    }

    setRootPath(rootPath: string) {
        this._rootPath = rootPath;
    }

    setOrigin(origin: string) {
        this._origin = origin;
    }

    setLevel(level: number) {
        this._level = level;
    }

    initState(path: string, origin: string, level: number = 0) {
        this.setLevel(level);
        this.setRootPath(path);
        this.setOrigin(`/${origin.split('/')[level + 1]}`);
        this.onInit();   //
    }

    setCurrentPath(path: string) {
        const routeName = this._routes.get(path) || this.dynamicMatch(path) || this._routes.get('*');
        const name = ASSERT(routeName, `path not found '${path} : ${Array.from(this._routes.entries())}'`);
        const id = this._names.get(name) || (() => {
            const r = [...this._names.keys()].filter(route => name.startsWith(route));
            return r.length ?
                r.reduce((a, b) => a.length > b.length ? a : b) :
                this._names.get(this.origin) ?? ASSERT(this._names.get('*'), `path name not found, ${this.origin}`);
        })();

        this._currentPageId = id;
        this.pushState(id, this._dynamicRouteMatch ?? `/${name}`);
    }

    dynamicMatch(path: string) {
        const dynamicRoutes: [string, string][] = [...this._routes].filter(route => route[0].includes(':'));
        if (dynamicRoutes.length) {
            const sp = path.split('/');
            const candidates = dynamicRoutes.filter(route => route[0].split('/').length == sp.length);
            let match: string | boolean = false;
            let i = 0;
            while (!match && i < candidates.length) {
                let c = candidates[i++];
                const p = c[0].split('/');
                let m = true;
                let j = 0;
                while (m && j < p.length) {
                    const v = p[j];
                    if (!v.includes(":")) {
                        m = v == sp[j];
                    }
                    j++;
                }
                if (m) {
                    this._dynamicRouteMatch = '';
                    p.forEach((value: string, pos: number) => {
                        if (value.startsWith(':')) {
                            this._params.set(value.replace(':', ''), sp[pos]);
                            this._dynamicRouteMatch += pos ? '/' + sp[pos] : sp[pos];
                        }
                        else {
                            this._dynamicRouteMatch += pos ? '/' + p[pos] : p[pos];
                        }
                    });
                    match = c[1];
                }
            }
            return match;
        }
    }

    hasRouteParameter(name: string): boolean {
        return this._params.has(name);
    }

    getRouteParameter(name: string): string {
        return ASSERT(this._params.get(name), `unknown route parameter '${name}'`);
    }

    async display(force: boolean = false) {
        if ((this.current?._objectId != this._currentPageId) || force) {
            await this.setContent(ASSERT(this._currentPageId, "current page id undefined"));
            await this.render();
        }
        else {
            console.error(`[AppRouter] display error`)
        }
    }

    async navigate(path: string) {    //
        this._params.clear();
        this._dynamicRouteMatch = undefined;
        this.setCurrentPath(path);
        return this.display()
            .then(() => {
                this._history.push(path);  //
                this.send(System.ROUTER_UPDATE,
                    AppRouterUpdate.NAVIGATE,
                    {
                        router: this._objectId,
                        path: this._routes.get(path) || this._routes.get('*')
                    }
                );

            })
    }

    back() {
        this._history.pop();
        const path = this._history.pop();
        if (path) {
            this.navigate(path);
        }
    }

    get historyLength() {
        return this._history.length
    }

    async toOrigin(): Promise<void> {
        await this.navigate(this.origin);
    }

    async setStaticPath(path: string) {
        const name = ASSERT(this._routes.get(path), `unknwon path '${path}'`);
        return this.setStaticPathByName(name);
    }

    async setStaticPathByName(name: string) {
        let id = this._names.get(name.replace('/', ''));
        if (!id) {
            if (this._routes.has('*')) {
                id = this._names.get(this._routes.get('*')!)
            }
            else {
                throw 'route name not found'
            }
        }
        else {
            await this.setContent(id);
        }
    }

    pushState(id: number, path: string) {  //
        DEBUG.log(`push state id=${id} path=${path}`);
        try {
            history.pushState({ page: id }, '', this.rootPath + path);
        }
        catch (e) { }
    }

    getPath(routeName: string) {
        return ASSERT(this._routes.get(routeName) || this._routes.get('*'), `unknwon route '${routeName}'`);
    }

    getPagePath(page: WTK_APP_PAGE_LOGIC): string {    /////
        const r = [...this._names.entries()].filter(it => it[1] == page?._objectId);
        return r.length > 0 ? r[0][0] : '';
    }

    getPageId(path: string) {
        console.error([...this._routes.entries()])
        const route = ASSERT(this._routes.get(path), `(getPageId) unknwon path '${path}'`);
        return ASSERT(this._names.get(route), `(getPageId) unknwon name '${route}'`);
    }

    get rootPath() {
        return ASSERT(this._rootPath, `(${this._objectId}) rooter path undefined`);
    }

    get origin() {
        return ASSERT(this._origin, `(${this._objectId}) rooter origin undefined`);
    }

    get level() {
        return ASSERT(this._level, `(${this._objectId}) rooter level undefined`);
    }
}

export { IAppRoute, AppRouterUpdate };
