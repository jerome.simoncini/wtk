import { IAppDAO, WTK_APP_DAO } from "./AppDAO.js";
import { Constructor } from "./Constructor.js";
import { IAppObject, WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";

const TYPE = "AppDAO";

export interface IAppDao extends IAppObject {
    setup: IAppDAO;
};

export const WTK_GEN_DAO = <P extends WTK_APP_DAO>(dao: Constructor<P>, settings?: IAppDao): Constructor<P> => {
    const skull = __skull__(dao, {
        onCreate: _create_app_dao_, 
        onInit: _init_app_dao_, 
        settings: settings
    }) as Constructor<P>;
    return WTK_GEN_OBJECT(skull, settings as IAppObject) as Constructor<P>;
};

const _create_app_dao_ = (dao: WTK_APP_DAO, settings: IAppDao): void => {
    
    dao.setSetup(settings.setup);

    dao.setObjectType(TYPE);
};

const _init_app_dao_ = async (dao: WTK_APP_DAO, settings?: IAppDao) => {
    dao.load();
};

//------------------------------------------------------------------------------------------------------------//


