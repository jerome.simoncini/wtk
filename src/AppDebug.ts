import { ComponentFactory, WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { WTK_CONSOLE_LOGIC } from "./AppConsole.js";
import { WTK_CONTAINER_LOGIC } from "./AppContainer.js";
import { AppEntity } from "./AppEntity.js";
import { WTK_FILE_LOGIC } from "./AppFile.js";
import { WTK_HORIZONTAL_LAYOUT_LOGIC, WTK_LAYOUT_ELEMENT, WTK_LAYOUT_ELEMENT_2, WTK_LAYOUT_LOGIC, WTK_VERTICAL_LAYOUT_LOGIC } from "./AppLayout.js";
import { WTK_LIST_LOGIC } from "./AppList.js";
import { AppModule } from "./AppModule.js";
import { AppObject } from "./AppObject.js";
import { WTK_APP_PAGE_LOGIC } from "./AppPage.js";
import { WTK_APP_ROUTER_LOGIC } from "./AppRouter.js";
import { _HTML_ } from "./Compiler.js";
import { Constructor } from "./Constructor.js";
import { MemoryPool } from "./MemoryPool.js";
import { ObjectFactory } from "./ObjectFactory.js";
import { ObjectLoader } from "./ObjectLoader.js";
import { Scope } from "./Scope.js";
import { WTK_GEN_COMPONENT } from "./_gen_component.js";
import { WTK_GEN_CONSOLE } from "./_gen_console.js";
import { WTK_GEN_CONTAINER } from "./_gen_container.js";
import { WTK_GEN_INTERPRETER } from "./_gen_interpreter.js";
import { WTK_GEN_HORIZONTAL_LAYOUT, WTK_GEN_VERTICAL_LAYOUT } from "./_gen_layout.js";
import { WTK_GEN_LIST } from "./_gen_list.js";
import { IAppModule, _create_app_module_ } from "./_gen_module.js";
import { __skull__ } from "./_skull.js";
import { WTK_INPUT_LOGIC } from "./services/input/InputSystem.js";
import { DEBUG_HANDLER, DebugHandler } from "./services/script/DebugHandler.js";
import { WTK_INTERPRETER_LOGIC } from "./services/script/Interpreter.js";
import { ASSERT, MESSAGE } from "./utils.js";


export class WTK_DEBUG_LOGIC extends AppModule {
    private _input: WTK_INPUT_LOGIC | undefined = undefined;

}

export interface IAppDebug extends IAppModule {
    root: Constructor<WTK_COMPONENT_LOGIC>;
    module: IAppModule;
    scale: number
}

const ROOT = 'debug-root';
const ROUTER = 'debug-router';
const DETAILS = 'debug-details';
const TREE = "debug-tree";


class DEBUG_LAYOUT extends WTK_HORIZONTAL_LAYOUT_LOGIC {
    private _current: string | undefined = undefined;

    async select(stateId: string) {
        if (this._current != stateId) {
            const router = this.getSlot(WTK_LAYOUT_ELEMENT_2)?.getSlot(ROUTER) as WTK_CONTAINER_LOGIC;            ///////////
            const btn = this.getSlot(WTK_LAYOUT_ELEMENT_2)?.getElement(stateId) as HTMLElement;                   ///////////
            if (router && btn) {
                await router.setContent(stateId)
                    .then(() => router.render())
                    .then(() => {

                        ///////////////////
                        if (this._current) {
                            const c = this.getSlot(WTK_LAYOUT_ELEMENT_2)?.getElement(this._current) as HTMLElement;       ///////////
                            if (c) {
                                c.style.fontWeight = 'normal';
                                c.style.color = 'lightgrey';
                            }
                            else {
                                // error
                            }
                        }
                        btn.style.fontWeight = 'bold';
                        btn.style.color = 'snow';
                        this._current = stateId;

                    })
                //

            }
            else {
                // error
            }
        }
    }

}

class DEBUG_PANEL extends WTK_VERTICAL_LAYOUT_LOGIC { }


class DEBUG_TREE extends WTK_LAYOUT_ELEMENT {
    private _current: string | undefined = undefined;
    private _selected: string | undefined = undefined;

    setCurrent(inode: string) {
        this._current = inode;
        this._selected = inode;
    }

    async select(inode: string) {
        this._selected = inode;
        await this.render();
    }

    get current(): string {
        return ASSERT(this._current, MESSAGE(this, `current inode undefined`));
    }

    get selected(): string | undefined {
        return this._selected;
    }
}

interface IDebugTreeItem {
    target: string;
    name: string;
    isDirectory: boolean;
    selected: boolean;
}

class DEBUG_TREE_ITEM extends WTK_COMPONENT_LOGIC implements IDebugTreeItem {
    target: string;
    name: string;
    isDirectory: boolean;
    selected: boolean;
}

export const WTK_GEN_DEBUG = <T extends WTK_DEBUG_LOGIC>(debug: Constructor<T>, settings: IAppDebug): Constructor<WTK_DEBUG_LOGIC> => {
    const skull = __skull__(debug, {
        onCreate: _create_debug_,
        onInit: _init_debug_,
        settings
    }) as Constructor<WTK_DEBUG_LOGIC>
    return WTK_GEN_COMPONENT(skull, {
        html: (_: WTK_DEBUG_LOGIC) => {
            return _HTML_(`<< ${ROOT} >>`, _);
        },
        styles: [
            'wtk-debug'
        ],
        slots: [
            {
                name: ROOT,
                content: WTK_GEN_HORIZONTAL_LAYOUT(DEBUG_LAYOUT, {
                    element1: settings.root,
                    element2: WTK_GEN_COMPONENT(WTK_LAYOUT_ELEMENT, {
                        html: (_: WTK_LAYOUT_ELEMENT) => {
                            return _HTML_(`<div style="display:flex; flex-direction:row; background-color: forestgreen;">
                                                <div @debug style="padding: 2px; color: lightgrey">debug</div>
                                                <div @tests style="padding: 2px; color: lightgrey;">tests</div>  
                                                <div @console style="padding: 2px; color: lightgrey;">console</div>                       
                                           </div>
                                           << ${ROUTER} >>`, _)
                        },
                        setup: [
                            {
                                name: 'debug',
                                selector: { id: true, value: 'debug' },
                                actions: [
                                    {
                                        event: 'click',
                                        cmd: (_: WTK_LAYOUT_ELEMENT): void => {
                                            const layout = _.layout as DEBUG_LAYOUT;
                                            layout.select('debug');
                                        }
                                    }
                                ]
                            },
                            {
                                name: 'tests',
                                selector: { id: true, value: 'tests' },
                                actions: [
                                    {
                                        event: 'click',
                                        cmd: (_: WTK_LAYOUT_ELEMENT): void => {
                                            const layout = _.layout as DEBUG_LAYOUT;
                                            layout.select('tests');
                                        }
                                    }
                                ]
                            },
                            {
                                name: 'console',
                                selector: { id: true, value: 'console' },
                                actions: [
                                    {
                                        event: 'click',
                                        cmd: (_: WTK_LAYOUT_ELEMENT): void => {
                                            const layout = _.layout as DEBUG_LAYOUT;
                                            layout.select('console');
                                        }
                                    }
                                ]
                            }
                        ],
                        styles: [

                        ],
                        onReady: async (_: WTK_LAYOUT_ELEMENT) => {               //////////////////
                            (<HTMLElement>_.root).style.overflow = 'hidden';
                            (<HTMLElement>_.root).style.height = '100%';

                            const layout = _.layout as DEBUG_LAYOUT;
                            await layout.select('debug');
                        },
                        slots: [
                            {
                                name: ROUTER,
                                content: WTK_GEN_CONTAINER(WTK_CONTAINER_LOGIC, {
                                    content: [
                                        {
                                            id: 'debug',
                                            ctor: WTK_GEN_VERTICAL_LAYOUT(DEBUG_PANEL as Constructor<WTK_VERTICAL_LAYOUT_LOGIC>, {
                                                element1: WTK_GEN_COMPONENT(DEBUG_TREE,
                                                    {
                                                        html: (_: DEBUG_TREE) => {

                                                            const module = _.module as AppModule;  //

                                                            try {
                                                                _.current;
                                                            }
                                                            catch (e) {
                                                                _.setCurrent(module.inode);
                                                            }

                                                            const ep = module.appTree.get(_.current, Scope.ENTITY).parent;

                                                            const parent = ep ? ep.inode : _.current;

                                                            const items: IDebugTreeItem[] = [];

                                                            items.push({
                                                                target: _.current,
                                                                name: '.',
                                                                isDirectory: true,
                                                                selected: _.selected && _.selected == _.current
                                                            } as IDebugTreeItem);

                                                            if (_.current != parent) {
                                                                items.push({
                                                                    target: parent,
                                                                    name: '..',
                                                                    isDirectory: true,
                                                                    selected: _.selected && _.selected == parent
                                                                } as IDebugTreeItem);
                                                            }

                                                            (module.appTree.get(_.current, Scope.LIST) || []).map((inode: string) => {
                                                                const n = module.appTree.get(inode, Scope.NAME);
                                                                items.push({
                                                                    target: inode,
                                                                    name: (n && n != '') ? n.replace(' ', '_') : inode,
                                                                    isDirectory: !["AppFile"].includes(module.appTree.get(inode, Scope.TYPE)),
                                                                    selected: _.selected && _.selected == inode
                                                                } as IDebugTreeItem)
                                                            })

                                                            return _HTML_(`<div style="font-weight:bold; padding: 5px;">
                                                                              << ${TREE} | ${JSON.stringify({ items })} >>
                                                                           </div>`, _);
                                                        },
                                                        slots: [
                                                            {
                                                                name: TREE,
                                                                content: WTK_GEN_LIST(WTK_LIST_LOGIC, {
                                                                    item: WTK_GEN_COMPONENT(DEBUG_TREE_ITEM, {
                                                                        html: (_: DEBUG_TREE_ITEM) => {
                                                                            const n = _.isDirectory ? _.name.concat('/') : _.name;
                                                                            const s = _.isDirectory ? 'dir' : 'file';
                                                                            const b = _.selected ? 'selected' : '';

                                                                            return _HTML_(`<div --${s}:${b} @item >${n}</div>`, _)
                                                                        },
                                                                        stylesheet: [
                                                                            { name: "file", className: "debug-tree-item-file" },
                                                                            { name: "dir", className: "debug-tree-item-dir" },
                                                                            { name: "selected", className: "debug-tree-item-selected" },
                                                                        ],
                                                                        setup: [
                                                                            {
                                                                                name: 'item',
                                                                                selector: { id: true, value: 'item' },
                                                                                actions: [
                                                                                    {
                                                                                        event: 'dblclick',
                                                                                        cmd: (_: DEBUG_TREE_ITEM): void => {
                                                                                            if (_.isDirectory) {
                                                                                                const t = _.target;
                                                                                                const debugTree = _["parentList"]?.parent as DEBUG_TREE;
                                                                                                console.warn(debugTree?.inode);
                                                                                                if (debugTree) {
                                                                                                    if (_.name != '.') {
                                                                                                        debugTree.setCurrent(t);
                                                                                                    }
                                                                                                    debugTree.render();
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    },
                                                                                    {
                                                                                        event: 'click',
                                                                                        cmd: (_: DEBUG_TREE_ITEM): void => {
                                                                                            const debugTree = _["parentList"]?.parent as DEBUG_TREE;

                                                                                            debugTree.select(_.target);
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        ]
                                                                    }),
                                                                }),
                                                                scope: Scope.LOCAL,
                                                                //access: Scope.GLOBAL
                                                            }
                                                        ],
                                                        onReady: async (_: DEBUG_TREE) => {
                                                            const debugFocus = _.layout.getSlot(WTK_LAYOUT_ELEMENT_2) as DEBUG_FOCUS;
                                                            debugFocus.setFocus(_.selected || _.current);
                                                            await debugFocus.render();
                                                        },
                                                        name: 'Debug Tree',
                                                    }),
                                                element2: WTK_GEN_COMPONENT(DEBUG_FOCUS, {
                                                    html: async (_: DEBUG_FOCUS) => {

                                                        if (_.focus) {
                                                            await _.mount(DETAILS, _.getObjectStore(Scope.LOCAL).get(DebugDetails))
                                                        }
                                                        else {
                                                            await _.umount(DETAILS);
                                                        }

                                                        return _HTML_(`<div style="display:flex; flex-direction:row; background-color: forestgreen;">
                                                                          <div @entity style="padding: 2px; color: snow; font-weight: bold;">[${_.focus || ''}]</div>
                                                                          <div @warnings style="padding: 2px; color: lightgrey;">warnings</div>
                                                                          <div @logs style="padding: 2px; color: lightgrey;">logs</div>
                                                                          <div @errors style="padding: 2px; color: lightgrey;">errors</div>
                                                                       </div>
                                                                       << ${DETAILS} | ${JSON.stringify({ target: _.focus })} >>`, _);
                                                    },
                                                    onReady: async (_: DEBUG_FOCUS) => {              ///////////////
                                                        (<HTMLElement>_.root).style.overflow = 'hidden';
                                                    },
                                                    createLocalStore: true,   //
                                                    name: 'Debug Focus',
                                                    slots: [
                                                        { name: DETAILS }
                                                    ],
                                                }),
                                                scaleRatio: 0.5,
                                                name: 'Debug V Layout',
                                                onCreate: (_: DEBUG_PANEL): void => {
                                                    _.setAppModule(_.___(settings.root));
                                                    //_.module.setFakeRoot(true);
                                                },
                                            })
                                        },
                                        {
                                            id: 'tests',
                                            ctor: WTK_GEN_COMPONENT(TESTS, {
                                                html: (_: TESTS) => {
                                                    return _HTML_(`<div style="height: 100%; display:flex; flex-direction:column;">
                                                                   </div>`, _);
                                                },
                                                onCreate: (_: TESTS): void => {
                                                    _.setAppModule(_.___(settings.root));
                                                    //_.module.setFakeRoot(true);
                                                },
                                            })
                                        },
                                        {
                                            id: 'console',
                                            ctor: WTK_GEN_CONSOLE(DEBUG_CONSOLE, {
                                                interpreter: WTK_GEN_INTERPRETER(WTK_INTERPRETER_LOGIC, {
                                                }),
                                                commandHandler: DebugHandler,
                                                onReset: (_: DEBUG_CONSOLE) => {
                                                    const root = _.module as AppModule;
                                                    root.appTree.setCurrentDir(root.inode);
                                                    _.setAppTree(root.appTree);
                                                    _.setAppName(root.appTree.get(root.inode, Scope.NAME).replace(' ', '_'));

                                                    (<DEBUG_HANDLER>_.commandHandler).setAppTree(root.appTree);                 //
                                                },
                                                root: settings.root,
                                                slowness: 500
                                            })
                                        }
                                    ],
                                    onReady: async (_: WTK_CONTAINER_LOGIC) => {            ///////
                                        (<HTMLElement>_.root).style.maxHeight = '98%';
                                    }
                                }),
                                scope: Scope.GLOBAL
                            }
                        ],
                        ssr: false,
                    }),
                    scaleRatio: settings.scale,
                    name: 'Debug H Layout',
                }),
                scope: Scope.LOCAL
            }
        ],
        createLocalStore: true,
        name: 'Debug View',
        ...settings
    }) as Constructor<WTK_DEBUG_LOGIC>;
}

const _create_debug_ = (debug: WTK_DEBUG_LOGIC, settings: IAppDebug): void => {
    _create_app_module_(debug, settings.module);
}

const _init_debug_ = async (debug: WTK_DEBUG_LOGIC, settings?: IAppDebug) => {

}


/*************************************************************************************/

class DEBUG_FOCUS extends WTK_LAYOUT_ELEMENT {
    focus: string | undefined = undefined;

    setFocus(inode: string) {
        this.focus = inode;
    }
}


/*************************************************************************************/

class DEBUG_DETAILS extends WTK_COMPONENT_LOGIC {
    target: string;

    entityDetails(e: AppEntity): string {
        const module = e.module as AppModule;     ////////////////  
        return _HTML_(`       
            <div> > ${e.inode}/type</div>
            <div style="padding: 5px; background-color: snow;">
               ${module.appTree.get(e.inode, Scope.TYPE)}
            </div>
            <hr>
            <div> > ${e.inode}/name</div>
            <div style="padding: 5px; background-color: snow;">
               ${module.appTree.get(e.inode, Scope.NAME)}
            </div>
            <hr> 
            <div> > ${e.inode}/inode</div>
            <div style="padding: 5px; background-color: snow;">
               ${module.appTree.get(e.inode, Scope.INODE)}
            </div>
            <hr>`, this);
    }

    objectDetails(o: AppObject): string {
        const module = o.module as AppModule;     ////////////////   
        return _HTML_(`     
            <div> > ${o.inode}/memory</div>
            <div style="padding: 5px; background-color: snow;">
               ${module.appTree.get(o.inode, Scope.MEMORY).map(m => `<div>${m}</div>`).join("")}
            </div>
            <div> > ${o.inode}/global</div>
            <div style="padding: 5px; background-color: snow;">
               ${o.getObjectStore(Scope.GLOBAL).inode}
            </div>
            <hr>
            <div> > ${o.inode}/observers</div>
            <div style="padding: 5px; background-color: snow;">
               <div>
                  [GLOBAL] ${o.getObservers(Scope.GLOBAL)
                .map(n => `<div ${o.sends(Scope.GLOBAL).includes(n.event) ? 'style="color: blue; font-weight: bold;"' : ''}>${n.event}: ${JSON.stringify(n.names)}</div>`).join("")}
               </div>
               <hr>
               <div>
                 [LOCAL] ${o.getObservers(Scope.LOCAL)
                .map(n => `<div ${o.sends(Scope.LOCAL).includes(n.event) ? 'style="color: blue; font-weight: bold;"' : ''}>${n.event}: ${JSON.stringify(n.names)}</div>`).join("")}
                </div>
            </div>
            <div> > ${o.inode}/emitters</div>
            <div style="padding: 5px; background-color: snow;">
               <div>
                  [GLOBAL] ${o.getEmitters(Scope.GLOBAL)
                .map(n => `<div ${o.listens(Scope.GLOBAL).includes(n.event) ? 'style="color: blue; font-weight: bold;"' : ''}>${n.event}: ${JSON.stringify(n.names)}</div>`).join("")}
               </div>
               <hr>
               <div>
                  [LOCAL] ${o.getEmitters(Scope.LOCAL)
                .map(n => `<div ${o.listens(Scope.LOCAL).includes(n.event) ? 'style="color: green; font-weight: bold;"' : ''}>${n.event}: ${JSON.stringify(n.names)}</div>`).join("")}
                </div>
            </div>
            <hr>
            <div> > ${o.inode}/sends</div>
            <div style="padding: 5px; background-color: snow;">
               <div>
                 [GLOBAL] ${o.sends(Scope.GLOBAL).map(e => `<div ${o.getObservers(Scope.GLOBAL).map(i => i.event).includes(e) ? 'style="color: blue; font-weight: bold;"' : ''}>${e}</div>`).join("")}
               </div>
               <hr>
               <div>
                 [LOCAL] ${o.sends(Scope.LOCAL).map(e => `<div ${o.getObservers(Scope.LOCAL).map(i => i.event).includes(e) ? 'style="color: blue; font-weight: bold;"' : ''}>${e}</div>`).join("")}
               </div>
            </div>
            <hr>
            <div> > ${o.inode}/listens</div>
            <div style="padding: 5px; background-color: snow;">
               <div>
                 [GLOBAL] ${o.listens(Scope.GLOBAL).map(e => `<div ${o.getEmitters(Scope.GLOBAL).map(i => i.event).includes(e) ? 'style="color: blue; font-weight: bold;"' : ''}>${e}</div>`).join("")}
               </div>
               <hr>
               <div>
                 [LOCAL] ${o.listens(Scope.LOCAL).map(e => `<div ${o.getEmitters(Scope.LOCAL).map(i => i.event).includes(e) ? 'style="color: blue; font-weight: bold;"' : ''}>${e}</div>`).join("")}
               </div>
            </div>
            <hr>
        `, this);
    }

    componentDetails(c: WTK_COMPONENT_LOGIC): string {
        const module = c.module as AppModule;     ////////////////

        const slots = c.listSlots().map(s => {
            const n = c.getSlotByTagname(s);
            return {
                name: s,
                state: n?.nodeState,
                contentName: n?.__name__,
                contentInode: n?.inode
            }
        });

        this.setState('slots',
            slots.map(s => {
                return `<div ${s.state?.includes('READY') ? 'style="color: green; font-weight: bold;"' : ''}>
                        ${s.name}:${s.state?.join()}:${s?.contentName || ''}:${s?.contentInode}
                      </div>`
            }));

        return _HTML_(`   
                 <div> > ${c.inode}/flags</div>
                 <div style="padding: 5px; background-color: snow;">
                    <div>${c.nodeState.join(',')}</div> 
                 </div>
                 <hr>
                 <div> > ${c.inode}/slots</div>
                 <div style="padding: 5px; background-color: snow;">
                    <@ for slot in slots : slot @> 
                 </div>
                 <hr>
                 <div> > ${c.inode}/styles</div>
                 <div style="padding: 5px; background-color: snow;">
                    ${module.appTree.get(c.inode, Scope.STYLES)}
                 </div>
                 <hr>`, this);
    }

    fileDetails(f: WTK_FILE_LOGIC): string {
        return _HTML_(`
             <div style="padding: 5px; background-color: snow;">
                ${f.content.map(l => `<div>${l}</div>`).join('')}
             </div>
         `, this);
    }

    memoryDetails(m: MemoryPool): string {

        return _HTML_(`
             <div> > ${m.inode}/data</div>
             <div style="padding: 5px; background-color: snow;">
                ${m.data.map((k, v) => `<div>[${v}] ${k[0]}:${JSON.stringify(k[1])}</div><hr>`).join('')}
             </div>
         `, this);
    }
}

const DebugDetails = WTK_GEN_COMPONENT(DEBUG_DETAILS,
    {
        html: async (_: DEBUG_DETAILS) => {

            if (_.target) {

                const inode = _.target;
                const module = _.module as AppModule;

                const e = module.appTree.get(inode, Scope.ENTITY);

                let output: string = '<div style="padding: 5px; max-height: 100%; overflow: auto;">';

                output += _.entityDetails(e);

                if (e instanceof ObjectLoader) {

                    if (e instanceof AppObject) {
                        output += _.objectDetails(e);

                        if (e instanceof WTK_COMPONENT_LOGIC) {

                            if (e instanceof AppModule) {
                            }
                            if (e instanceof WTK_APP_ROUTER_LOGIC) {
                            }
                            if (e instanceof WTK_APP_PAGE_LOGIC) {
                            }
                            if (e instanceof WTK_CONTAINER_LOGIC) {
                            }
                            if (e instanceof WTK_LIST_LOGIC) {
                            }
                            if (e instanceof WTK_LAYOUT_LOGIC) {
                            }

                            output += _.componentDetails(e);
                        }
                        else {

                            if (e instanceof MemoryPool) {
                                output += _.memoryDetails(e);
                            }
                            if (e instanceof ObjectFactory) {

                                if (e instanceof ComponentFactory) {
                                }
                            }
                        }
                    }

                }
                else {

                    if (e instanceof WTK_FILE_LOGIC) {
                        output += _.fileDetails(e);
                    }
                }

                output += '</div>';

                return output;

            }
            else {
                return '';
            }
        },
        name: 'Debug Details'
    })


/*************************************************************************************/

class TESTS extends WTK_COMPONENT_LOGIC {

}


/*************************************************************************************/



class DEBUG_CONSOLE extends WTK_CONSOLE_LOGIC {
}