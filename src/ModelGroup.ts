import { AppObject } from "./AppObject.js";
import { StateObject } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { LOGGER } from "./Logger.js";
import { ModelProps, ModelPropsFactory } from "./ModelProps.js";
import { ObjectFactory } from "./ObjectFactory.js";
import { ASSERT, MESSAGE } from "./utils.js";

const THIS = "Model Group"
const DEBUG = LOGGER(THIS);
DEBUG.off();

export type ModelType = string;
export type PropsType = string;

interface IModelType<T> {
    type: T
}

export interface IModelGroupItem<M = ModelType> extends IModelType<M> {
    props: ModelProps;
}
export class IModelGroupItem<M> { }

export interface IModelGroupItemProps<M = ModelType> extends IModelType<M> {
    props: Constructor<ModelProps>;
}

export interface IModelGroupItemLogic<P extends ModelProps, M = ModelType> extends IModelType<M> {
    logic: Constructor<ModelLogic<P>>;
}

interface IModelItemSetup<M = ModelType> {
    type: M,
    props: Constructor<ModelProps<M>>,
    logic: Constructor<ModelLogic<ModelProps<M>>>
}

export interface IModelSetup {
    name: string;
    items: IModelItemSetup[]
}


/***********************************************************************************************/


export abstract class ModelGroup<T = ModelType> extends AppObject {
    private _props: Map<T, Constructor<ModelProps<T>>> = new Map();

    getModelPropsCtor(type: T): Constructor<ModelProps<T>> | undefined {
        return this._props.get(type);
    }

    setProps(props: IModelGroupItemProps<T>[]) {
        for (const item of props) {
            this._props.set(item.type, item.props);
        }
    }
}

/***********************************************************************************************/

abstract class Model<K = string> extends StateObject {
    private _key: K | undefined = undefined;

    get key(): K {
        if (!this._key) {
            throw new Error(MESSAGE(this,`model key is not set`));
        }
        return this._key;
    }

    setKey(key: K) {
        this._key = key;
    }
}



/***********************************************************************************************/

export abstract class ModelGroupItem<M = ModelType> extends Model {
    private _data: IModelGroupItem<M> = new IModelGroupItem();
    private _propsFactory: ModelPropsFactory | undefined = undefined;
    private _group: ModelGroup<M> | undefined = undefined;

    onCreate() {
        super.onCreate()
        this.createLocalStore();
    }

    setType(type: M) {
        this._data.type = type;
    }

    setProps(props: ModelProps) {
        this._data.props = props;
    }

    setModelGroup(modelGroup: Constructor<ModelGroup<M>>) {
        this._group = this.___(modelGroup) as ModelGroup<M>;
    }

    initProps(modelType: M | undefined/*, sharedProps: boolean = false*/): void {
        DEBUG.log(`(init props) modelType=${modelType}`);
       // if (!sharedProps) {  //
            this.createLocalStore();
       // }
       // this._propsFactory = sharedProps ? this.___(ModelPropsFactory) : this._localStore.get(ModelPropsFactory);

        this._propsFactory = this._localStore.get(ModelPropsFactory);

        if(modelType) {
            const modelPropsCtor = this.group.getModelPropsCtor(modelType);
            ASSERT(modelPropsCtor, `(${this.constructor.name})(init props) props ctor undefined, modelType=${modelType}`);
            this._data.type = modelType!;
            this._data.props = this.propsFactory.create(modelPropsCtor!) as ModelProps;
            this._data.props.init();
        }
    }

    getData(): IModelGroupItem<M> {
        return this._data;
    }

    public _deserialize(json: string): void {   //
        DEBUG.log(`deszerialize`);
        DEBUG.log(`json=${json}`);
        const state = JSON.parse(json);
        ASSERT(state.type, '');
        ASSERT(state.props, '');
        const ctor = ASSERT(this.group.getModelPropsCtor(state.type), `${state.type}`);
        this._data.type = state.type;
        this._data.props = this.propsFactory.create(ctor!) as ModelProps;
        this._data.props._deserialize(state.props);
    }

    public _serialize(): string {  //
        return this._data?.type ? JSON.stringify({
            type: this._data.type,
            props: this._data.props._serialize()
        }) : "";
    }

    public _initState(data: IModelGroupItem<M>): void {
        ASSERT(data, `(${this.constructor.name})(init state) data undefined`);
        ASSERT(data.type, `(${this.constructor.name})(init state) data typeundefined`)
        this._data.type = data.type;
        const ctor = ASSERT(this.group.getModelPropsCtor(data.type),
            `(${this.constructor.name}) model item ctor undefined, itemType=${data.type}`);
        this._data.props = this.propsFactory.create(ctor!) as ModelProps;
        this._data.props.init();
        if (data.props?.getData()) {
            this._data.props.merge(data.props);
        }
        //DEBUG.log(`props=${JSON.stringify(this._data.props)}`);
    }

    get type() {
        return this._data.type;
    }

    get props() {
        return this._data.props;
    }

    private get group() {
        return ASSERT(this._group, `(${this.constructor.name}) model group undefined`);
    }

    private get propsFactory() {
        return ASSERT(this._propsFactory, `(${this.constructor.name}) props factory undefined`);
    }
}

/***********************************************************************************************/

export abstract class ModelLogic<P extends ModelProps, T = any> extends AppObject {
    protected _model: T | undefined = undefined;

    getModel() {
        return ASSERT(this._model, `(${this.constructor.name}) model undefined`);
    }

    setModel(model: T) {
        this._model = model;
    }

    init(props: P) {
        this.sync(props);
        this.onInit();
    }

    update(props: P) {
        this.sync(props);
        this.onUpdate();
    }

    sync(props: ModelProps): void {
        for (const propertyName of props.keys()) {
            this[`${propertyName}`] = props.get(propertyName);
        }
    }
}

/***********************************************************************************************/

export enum FactoryMode {
    SINGLETON,
    NEW
};


export class WTK_MODEL_GROUP_ITEM_FACTORY_LOGIC<L extends ModelLogic<P>, P extends ModelProps> extends AppObject {
    private _logic: Map<string, Constructor<ModelLogic<P>>> = new Map();

    setLogic(logic: IModelGroupItemLogic<P>[]): void {
        for (const model of logic) {
            this._logic.set(model.type, model.logic);
        }
    }

    get(modelItem: IModelGroupItem, createMode: FactoryMode): L {
        let model: ModelLogic<P> | undefined = undefined;

        const ctor = ASSERT(this._logic.get(modelItem.type), `${modelItem.type}`);
        if (createMode == FactoryMode.SINGLETON) {
            model = this._localStore.get(ctor) as ModelLogic<ModelProps>;
        }
        else if(createMode == FactoryMode.NEW) {
            model = this._localStore.get(ObjectFactory).create(ctor) as ModelLogic<ModelProps>;
        }

        ASSERT(model, `(${this.constructor.name}) create model error '${modelItem.type}'`);

        model!.init(modelItem.props as P);

        return model as L;
    }
}