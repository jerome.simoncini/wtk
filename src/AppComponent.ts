import { _, __ } from "./Compiler.js";
import { LOGGER } from "./Logger.js";
import { ObjectFactory } from "./ObjectFactory.js";
import { ServerPageMeta } from "./server/ServerPage.js";  //
import { Constructor } from "./Constructor.js";
import { Scope } from "./Scope.js";
import { ASSERT, Mask, MESSAGE } from "./utils.js";
import { MemoryPool } from "./MemoryPool.js";
import { StyleMap } from "./StyleMap.js";
import { AppState } from "./AppState.js";
import { ObjectStore } from "./ObjectStore.js";
import { AppObject } from "./AppObject.js";
import { AppEntity, LogLevel } from "./AppEntity.js";

const DEBUG = LOGGER("App Component");
DEBUG.on();

interface ISelector {
    id?: boolean;
    tagname?: boolean
    value: string;
}

interface IEventAction {
    event: string;
    cmd: (component: WTK_COMPONENT_LOGIC, e: any) => void;
}

export interface IElementSetup {
    name: string;
    selector: ISelector;
    actions: IEventAction[];
    auto?: boolean
}

export interface ISlot {
    name: string;
    content?: Constructor<WTK_COMPONENT_LOGIC>;
    scope?: Scope;
    access?: Scope;  //
}

export interface IComponentProperty {
    name: string;
    key: string;
    memoryPool: MemoryPool<string, any>;  //
}

export interface IService {
    name: string;
    ctor: Constructor<AppObject>,
    scope: Scope
};


export enum AppNodeFlag {  //////////////////////////////////////////////
    INITIALIZED,
    MOUNTED,
    READY,
    ERROR
}



export class WTK_COMPONENT_LOGIC extends AppObject {
    private _root: Element | null = null;  //
    private _styles: string[] = [];
    private _stylesheet: Map<string, string> = new Map();
    private _stylemap: StyleMap = new StyleMap();
    private _ssr: boolean = true;
    private _services: IService[] = [];   //
    private _css_map: Map<string, HTMLLinkElement> = new Map();
    private _css_files: string[] = [];


    _: any = {};  // 
    __: any[] = []; //


    setProps(props: any) {
        Object.assign(this, props);
    }

    setStylesheetProperty(name: string, className: string) {
        this.stylesheet.set(name, className);
    }

    setThemeProperty(style: number, classNames: string[]) {
        this.stylemap.set(style, classNames);
    }

    setSSR(ssr: boolean) {
        this._ssr = ssr;
    }

    loadState(name: string, ctor: Constructor<AppState>, store: ObjectStore = this.getGlobalStore()!) {
        const s = store.get(ctor);
        s.onInit();
    }

    initCSS(files: string[]) {
        this._css_files = files;
    }

    loadCSS(files: string[] = this._css_files) {
        files.forEach(filename => {
            let css = document.createElement('link');
            css.rel = "stylesheet";
            css.href = filename;
            console.log(css.href);
            document.getElementsByTagName('head')[0].appendChild(css);
            this._css_map.set(filename, css);
        });
    }

    unloadCSS(files: string[] = this._css_files) {                                                //
        files.forEach(filename => {
            if (this._css_map.has(filename)) {
                const css = this._css_map.get(filename);
                if (css != null && css.parentNode != null) {
                    css.parentNode.removeChild(css);
                }
            }
        });
    }

    //////////////////
    setServices(services: IService[]) {
        this._services = services;
    }

    ///////////
    private _startServices(): void {  //
        this._services.forEach(service => {
            const store = (service.scope == Scope.LOCAL) ? this.getObjectStore(Scope.LOCAL) : this.getObjectStore(Scope.GLOBAL);
            if (store) {
                const s = store.get(service.ctor);
                s.onInit();      //
            }
            else {
                // notify error
            }

        });
    }

    ///////////
    stopService(service: Constructor<AppObject>, scope: Scope): void {    ////////
        const store = (scope == Scope.LOCAL) ? this.getObjectStore(Scope.LOCAL) : this.getObjectStore(Scope.GLOBAL);
        console.warn(store);
        if (store) {
            store.release(service);
        }
        else {
            // notify error
        }
    }

    get root(): Element {
        return ASSERT(this._root, "root undefined");
    }

    get stylesheet(): Map<string, string> {
        return ASSERT(this._stylesheet, `style sheet undefined`)
    }

    get stylemap(): StyleMap {
        return ASSERT(this._stylemap, `style map undefined`)
    }

    get styles(): string {
        return this._styles.join(' ')
    }

    get ssr(): boolean {
        return this._ssr;
    }

    get services(): IService[] {
        return this._services;
    }

    get parent(): WTK_COMPONENT_LOGIC | undefined {
        return (this._parent && this._parent instanceof WTK_COMPONENT_LOGIC)    //////
            ? this._parent as WTK_COMPONENT_LOGIC
            : undefined;
    }

    setRoot(root: Element) {
        this._root = root;
    }

    setRootTag() {
        // ASSERT(document, `${this.__name__}: (setRootTag) server side`);
        const element = document.getElementsByTagName(this.tagname)[0];      //
        if (element) {
            this._root = element;
        }
        else {
            throw MESSAGE(this, `[FATAL](setRootTag) unknown tagname "${this.tagname}"`);
        }
    }

    _setup(setup: IElementSetup[]) {
        this.__ = [];
        for (const e of setup) {
            //console.warn(this._inode);
            const value = e.selector.tagname ? this.getTagname(e.selector.value) : this.getId(e.selector.value);
            this.__.push({
                name: e.name,
                sel: `${e.selector.id ? '#' : ''}${value}`,
                actions: e.actions.map((a) => {
                    return {
                        event: a.event,
                        cb: (evt) => a.cmd(this, evt)
                    }
                }),
                auto: e.auto
            })
        }
    }

    _loadSetup() { // TODO: event delegation
        // console.warn(MESSAGE(this, `[load setup] ${JSON.stringify(this.__)}`));     //
        for (const att of this.__) {
            if (!att._loaded) {
                //console.warn(att);
                const el = _(att.sel);
                this._[att.name] = el;
                if (att.auto) this.autoUpdate(att.name);
                for (const action of att.actions) {
                    __(el, action.event, action.cb);
                }
                att._loaded = true;
            }
        }
    }

    autoUpdate(name: string, active = true) {  //
        if (this._[name]) {
            let att = this.__.find((att) => (att.name == name));
            if (!att.autoupdate && active) {
                att.autoupdate = true;
                __(this._[name], 'input', (e) => { if (att.autoupdate) { this._$[name] = e.target.value; } });
            } else if (att.autoupdate && !active) att.autoupdate = false;
        }
    }

    getElement(name: string) {    //
        return ASSERT(this._[name], `(${this.__name__}) '${name}' element not found`);
    }

    addStyles(styles: string[]) {
        this._styles = styles;
    }


    getMetas(...names: string[]): string[] {
        const metas: string[] = [];
        for (const name of names) {
            const meta = ASSERT(document.head.querySelector(`meta[name=${name}]`), `meta not found '${name}'`);
            metas.push(ASSERT(meta.getAttribute('content'), `meta content undefined '${name}'`));
        }
        return metas;
    }


    setMeta(name: string, value: string) {
        document.head.querySelector(`meta[name=${name}]`)?.setAttribute('content',value);
    }






    async onShow(...args: any[]): Promise<void> {
        //console.warn(MESSAGE(this, `onShow`));
        this.extensions.execute('beforeOnShow', this);
        this.startNotifier(this._notifierActions.get(Scope.LOCAL) || [], Scope.LOCAL);
        this.startObserver(this._observerActions.get(Scope.LOCAL) || [], Scope.LOCAL);
        this.startNotifier(this._notifierActions.get(Scope.GLOBAL) || [], Scope.GLOBAL);
        this.startObserver(this._observerActions.get(Scope.GLOBAL) || [], Scope.GLOBAL);
        this._startServices();                         //////////////////////////
        this.loadCSS();
        await this._onShow(this);
        this.extensions.execute('afterOnShow', this);
    }

    async onHide(...args: any[]): Promise<void> {
        //console.warn(MESSAGE(this, `onHide`));
        this.extensions.execute('beforeOnHide', this);
        this.stopNotifier(this._notifierActions.get(Scope.LOCAL) || [], Scope.LOCAL);
        this.stopObserver(this._observerActions.get(Scope.LOCAL) || [], Scope.LOCAL);
        this.stopNotifier(this._notifierActions.get(Scope.GLOBAL) || [], Scope.GLOBAL);
        this.stopObserver(this._observerActions.get(Scope.GLOBAL) || [], Scope.GLOBAL);
        //this._stopServices();                       ///////////////////////////
        this.unloadCSS();
        await this._onHide(this);
        this.extensions.execute('afterOnHide', this);
    }


    protected async html(): Promise<string> {      /////
        return this._html(this)
    };

    protected async onprerender(): Promise<void> {    /////
        // console.warn(MESSAGE(this, 'on pre render'));
        await this._onprerender(this);
    };

    protected async onpostrender(): Promise<void> {    /////
        // console.warn(MESSAGE(this, 'on post render'));
        await this._onpostrender(this);
        this._loadSetup();
    };




    async render(props: {} = {}): Promise<void> {                    ////////////////////////////////
        // console.warn(MESSAGE(this, `(render) flags=${this.nodeState}`));

        this.getModule(AppEntity)?.log(this.inode, LogLevel.INFO, `${this.inode}: render`);

        await this._onrenderstart();                      ////////////////////////////////
        await this.__render(props);
        await this._onrenderend();                        ///////////////////////////////
    }

    async _onrenderstart(): Promise<void> {
        // console.warn(MESSAGE(this, `(_onrenderstart) flags=${this.nodeState}`));
        if (!this.nodeflags.has(AppNodeFlag.INITIALIZED)) {
            this.nodeflags.setBits(AppNodeFlag.INITIALIZED);
            await this.onInit();
            await this.onFirstRender();        //////////////
        }

        if (!this.nodeflags.has(AppNodeFlag.READY)) {     ////
            await this.onShow();
        } else {
            this.nodeflags.unset(AppNodeFlag.READY);
        }



        for (const att of this.__) {
            att._loaded = false;
        }

        await this.propagate(async (component: WTK_COMPONENT_LOGIC) => {
            await component._onrenderstart();
        });
    }

    async _onrenderend(): Promise<void> {
        await this.propagate(async (component: WTK_COMPONENT_LOGIC) => {
            await component._onrenderend();
        });

        if (!this.nodeflags.hasOne(AppNodeFlag.ERROR)) {

            for (const className of this._styles) {
                this.root.classList.add(className);
            }

            await this.onReady();
        }
        else {
            // notify error
            await this.onHide();
        }

        //console.warn(MESSAGE(this, `(_onrenderend) flags=${this.nodeState}`));
    }

    private async _render(): Promise<void> {       ////////////////////////////////////////

        this.getModule(AppEntity)?.log(this.inode, LogLevel.INFO, `${this.inode}: _render`);



        //console.warn(MESSAGE(this, `(_render) flags=${this.nodeState}`));

        try {
            await this.onprerender();
            this.root.innerHTML = await this.html();
            console.warn(this.root.innerHTML);
            await this.onpostrender();
        }
        catch (error) {
            console.error(error);
            this.nodeflags.setBits(AppNodeFlag.ERROR);
        }
    }


    private async __render(props: {} = {}): Promise<void> {        ////////////////////////////////////////
        // console.warn(MESSAGE(this, `(__render) flags=${this.nodeState}`));
        this.extensions.execute('beforeOnRender', this);
        if (this.nodeflags.has(AppNodeFlag.MOUNTED)) {   //
            this.setProps(props);               ///////////
            this.setRootTag(); // 
            if (this._root) {
                await this._render();
                ASSERT(!this.nodeflags.has(AppNodeFlag.ERROR), "node error");    ////////////////
                DEBUG.log(JSON.stringify(this._slots));

                await this.propagate(async (component: WTK_COMPONENT_LOGIC, tagname: string) => {
                    if (this.getModule(AppEntity)) {
                        component.setAppModule(this.module);
                    }
                    component.setTagname(tagname);
                    await component.__render();     //
                });
            }
            else {
                // notify error
            }
        }
        this.extensions.execute('afterOnRender', this);
    }

    async renderStatic(metas: ServerPageMeta[]): Promise<string> {   //////////////////////////////////////////
        const slotsContent: Map<string, string> = new Map();

        await this.onprerender();  //

        await this.propagate(async (component: WTK_COMPONENT_LOGIC, tagname: string) => {     ///////////
            if (component.ssr) {
                await component.onInit();
                await component.initSSR(metas);
                slotsContent.set(tagname, `<${tagname} class="${component._styles.join('')}">${await component.renderStatic(metas)}</${tagname}>`)
            }
        })

        let html = await this.html();

        for (const content of slotsContent.entries()) {
            html = html.replace(`<${content[0]} ></${content[0]}>`, content[1]);   //
        }

        return html;
    }


    async _finishSSR(root: string): Promise<void> {        //////////////////////////////////////////////
        this.getModule(AppEntity)?.log(this.inode, LogLevel.INFO, `${this.inode}: finish SSR`);

        this.extensions.execute('beforeFinishSSR', this);

        console.warn('test 0');
        await this.onInit();
        await this.onShow();
        await this.onFirstRender();        /////////

        console.warn('test 1');
        this.setTagname(root);
        this.setRootTag();
        console.warn('test 2');

        await this.propagate(async (component: WTK_COMPONENT_LOGIC, tagname: string) => {
            if (this.getModule(AppEntity)) {
                component.setAppModule(this.module);
            }
            if (component.ssr) {
                await component._finishSSR(tagname);                  //////////////////////////////
            }
            else {
                component.setTagname(tagname);
                await component.render();
            }
        });

        await this.onpostrender();
        await this._onrenderend();

        this.extensions.execute('afterFinishSSR', this);
    }

    async propagate(fun: (component: WTK_COMPONENT_LOGIC, tagname?: string) => Promise<void>) {

        for (const [tagname, node] of this._slots.entries()) {
            const component = node as WTK_COMPONENT_LOGIC;
            if (component) {
                await fun(component, tagname);
            }
            else {
                // notify error
            }
        }
    }

    async _ready() {

        console.warn(MESSAGE(this, 'ready'));

        await this.propagate(async (component: WTK_COMPONENT_LOGIC) => {
            if (component && !component.nodeflags.has(AppNodeFlag.ERROR)) {
                await component._ready();
            }
            else {
                // notify error
            }
        })

        await this.onReady();
    }

    onMount(tagname: string): boolean {

        console.warn(`mount ${tagname}`);
        this.setTagname(tagname);

        return true;   /////////////////////
    }

    async onUnmount(): Promise<boolean> {
        for (const [tagname, component] of this._slots.entries()) {
            if (component) {
                this.umountByTagname(tagname);
            }
            else {
                // notify error
            }
        }

        if (this.nodeflags.has(AppNodeFlag.READY)) {
            await this.onHide();
            this.nodeflags.unset(AppNodeFlag.READY);         ////
        }

        this.nodeflags.unset(AppNodeFlag.INITIALIZED);
        this._tagname = null;
        this._root = null;

        return true;       //////////////////
    }


    async onFirstRender(): Promise<void> {                                  ////////////////////////////
        await this._onFirstRender(this);
    }

    async onResize(rect: DOMRect | undefined = undefined): Promise<void> {
        //console.warn(MESSAGE(this, '(on resize)'));
        const cbr = rect ?? this.root.getBoundingClientRect();
        //console.warn(cbr);
        await this._onResize(this, cbr);

        for (const [tagname, component] of this._slots.entries()) {
            if (component) {
                await (<WTK_COMPONENT_LOGIC>component).onResize();
            }
            else {
                // notify error
            }
        }
    }

    async onReady(): Promise<void> {
        if (!this.nodeflags.has(AppNodeFlag.READY)) {
            this.extensions.execute('beforeOnReady', this);
            await this._onReady(this);
            this.nodeflags.setBits(AppNodeFlag.READY);       //
            this.extensions.execute('afterOnReady', this);
        }
    }

    async initSSR(metas: ServerPageMeta[]): Promise<void> {
        this.extensions.execute('beforeInitSSR', this);
        await this._initSSR(this, metas);
        this.extensions.execute('afterInitSSR', this);
    }

    async _onShow(_: WTK_COMPONENT_LOGIC): Promise<void> { }
    async _onHide(_: WTK_COMPONENT_LOGIC): Promise<void> { }
    async _onReady(_: WTK_COMPONENT_LOGIC): Promise<void> { }
    async _onFirstRender(_: WTK_COMPONENT_LOGIC): Promise<void> { }
    async _onprerender(_: WTK_COMPONENT_LOGIC): Promise<void> { }
    async _onpostrender(_: WTK_COMPONENT_LOGIC): Promise<void> { }
    async _html(_: WTK_COMPONENT_LOGIC): Promise<string> { return '' }
    async _onResize(_: WTK_COMPONENT_LOGIC, cbr: DOMRect): Promise<void> { }
    async _initSSR(_: WTK_COMPONENT_LOGIC, metas: ServerPageMeta[]): Promise<void> { }

    async sync(): Promise<void> {                           //////////////////////////////////////
        throw "not implemented";
    }


    ///////////////////////////////////////////////////////////////////////




    protected _tagname: string | null = null;   //
    protected _slots: Map<string, WTK_COMPONENT_LOGIC | null> = new Map();
    private _nodeflags: Mask = new Mask();


    createSlots(names: string[]) {
        for (let name of names) {
            this.createSlot(name);  //
        }
    }

    createSlot(slotname: string) {
        DEBUG.log(`create slot '${slotname}'`);
        this._slots.set(this.getTagname(slotname), null);
    }

    destroySlot(slotname: string) {
        const node = this._slots.get(slotname);
        if (node) {
            this.umount(slotname);
        }
        this._slots.delete(slotname);
    }


    getSlot(slotname: string): WTK_COMPONENT_LOGIC {              ////////////////////////////////
        //console.warn(this.getTagname(slotname.trim()));
        return ASSERT(this._slots.get(this.getTagname(slotname.trim())), MESSAGE(this, `${slotname}: slot not found`));
    }

    getSlotByTagname(tagname: string) {
        return this._slots.get(tagname);
    }

    listSlots(): string[] {
        return Array.from(this._slots.keys());
    }

    async mount(slotname: string, node: WTK_COMPONENT_LOGIC): Promise<void> {
        //onsole.warn(MESSAGE(this,`mount '${this.inode}|${slotname}' <- '${node.inode}'`));
        const tagname = this.getTagname(slotname);
        if (this._slots.has(tagname)) {
            if (this._slots.get(tagname)) {
                await this.umount(slotname);
            }
            if (node.onMount(tagname)) {
                //console.warn(MESSAGE(this,`mounted '${this.inode}|${slotname}|${node.inode}'`));
                this._slots.set(tagname, node);
                node._nodeflags.setBits(AppNodeFlag.MOUNTED);
                // console.warn(MESSAGE(this,`flags '${node._objectId}:${JSON.stringify(node._nodeflags.values)}'`));   
            }
            else {
                // notify error
            }
        }
        else {
            // notify error
        }
    }

    async umount(slotname: string): Promise<void> {
        const tagname = this.getTagname(slotname);
        await this.umountByTagname(tagname);
    }

    async umountByTagname(tagname: string): Promise<void> {
        //console.warn(MESSAGE(this,`umount '${tagname}'`));
        const content = this._slots.get(tagname);
        if (content) {
            // if (content._nodeflags.has(AppNodeFlag.MOUNTED)) {   //
            if (await content.onUnmount()) {
                console.warn(MESSAGE(this, `umounted '${tagname}'`));
                content._nodeflags.unset(AppNodeFlag.MOUNTED);
                this._slots.set(tagname, null);
            }
            else {
                // notify error
            }
            // }
        }
        else {
            // notify error
        }
    }

    async onCreated(...args: any[]): Promise<void> { }  //
    async onDestroyed(...args: any[]): Promise<void> { }  //

    getTagname(slotname: string): string {
        slotname = slotname.split(" ").join("");
        return `${slotname}-${this._inode.replaceAll(':', '-')}`;         ///////////////////
    }

    setTagname(tagname: string) {
        this._tagname = tagname;
    }

    resetFlags() {  //
        this._nodeflags = new Mask()
    }

    get tagname(): string {
        return ASSERT(this._tagname, MESSAGE(this, `tagname undefined`));
    }

    get nodeflags(): Mask {
        return this._nodeflags;
    }

    get nodeState(): string[] {
        const state: string[] = [];
        if (this.nodeflags.has(AppNodeFlag.MOUNTED)) {
            state.push("MOUNTED");
        }
        if (this.nodeflags.has(AppNodeFlag.INITIALIZED)) {
            state.push("INITIALIZED");
        }
        if (this.nodeflags.has(AppNodeFlag.READY)) {
            state.push("READY");
        }
        if (this.nodeflags.has(AppNodeFlag.ERROR)) {
            state.push("ERROR");
        }
        return state;
    }
}

export class ComponentFactory extends ObjectFactory<WTK_COMPONENT_LOGIC> { }