import { AppObject } from "./AppObject.js";
import { AppState, AppStateService, ModelGroupItemState, StateVariable } from "./AppState.js";
import { IModelGroupItem, ModelGroupItem } from "./ModelGroup.js";
import { ObjectStore } from "./ObjectStore.js";
import { AppDBStoreDTO, WTK_DB_STORE_LOGIC } from "./AppDBStore.js";
import { ASSERT, MESSAGE } from "./utils.js";
import { Constructor } from "./Constructor.js";
import { WTK_DB_LOGIC } from "./AppDB.js";

// IndexedDB <-> session app state

export interface IAppDAO {
    stateCtor: Constructor<ModelGroupItemState<ModelGroupItem>>,
    stateServiceCtor: Constructor<AppStateService>,
    dbStoreCtor: Constructor<WTK_DB_STORE_LOGIC>,
    dbCtor: Constructor<WTK_DB_LOGIC>,
    appStateCtor: Constructor<AppState>,
    stateName: string,
    filename: string,
}

export class WTK_APP_DAO extends AppObject {
    private _setup: IAppDAO | undefined = undefined;

    private _syncState(response: AppDBStoreDTO[]) {
        const data = response.find(entry=>entry.key == this.setup.filename)?.data;
        if(data) {
            const store = new ObjectStore();
            const state = store.get(this.setup.stateCtor);
            state._deserialize(data)
            this.___(this.setup.stateServiceCtor).reset(state.value);
        }
    }

    load() {
        this.___(this.setup.dbCtor).getStore(this.setup.dbStoreCtor)?.getAll({
            onSuccess: (response: AppDBStoreDTO[]) => this._syncState(response),
            onError: (error)=>console.error(MESSAGE(this,`(load) error=${error}`))
        })
    }

    update(rollback: IModelGroupItem) {
        const state = this.___(this.setup.appStateCtor).getState(this.setup.stateName) as StateVariable<ModelGroupItem>;
        this.___(this.setup.dbCtor).getStore(this.setup.dbStoreCtor)?.create(this.setup.filename, state.value, {
            onSuccess: () => this.___(this.setup.stateServiceCtor).reset(state.value), // notify update
            onError: (error)=>{
                console.error(MESSAGE(this,`(update) error=${error}`));
                this.___(this.setup.stateServiceCtor).reset(rollback);
            }
        })
    }

    setSetup(setup: IAppDAO) {
        this._setup = setup;
    }

    get setup(): IAppDAO {
        return ASSERT(this._setup, MESSAGE(this,`setup undefined`));
    }
}