import { AppObject } from "./AppObject.js";


export class WTK_SERVICE_LOGIC extends AppObject {
    private _refCount: number = 0;

    onStart() {
        this._onStart(this);
    }
    
    onStop() {
        this._onStop(this);
    }

    protected _onStart(service: WTK_SERVICE_LOGIC): void { }
    protected _onStop(service: WTK_SERVICE_LOGIC): void { }

    get unused() {
        return this._refCount == 0;
    }
}