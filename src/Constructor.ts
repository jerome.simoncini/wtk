import { AppObject } from "./AppObject";

type AnyConstructor = new () => any;
type AppObjectConstructor<T extends AppObject> = new (...args: any[]) => T;
type Constructor<T={}> = new (...args: any[]) => T;

export { Constructor, AnyConstructor, AppObjectConstructor };