class AppEvent {
    type: EventType;  
    code: any;
    data: any;    // EventData
}

class EventData {
    message: string;
    notifier: string;
    action: string;
    content?: any
}

enum EventType {
    ERROR,
    INFO,
    ACTION
}

enum AppDbEventCode {
    DATABASE_CONNECT_ERROR,
    DATABASE_ERROR,
    DB_MODIFY_SUCCESS,
    DB_MODIFY_ERROR,
    DB_CREATE_SUCCESS,
    DB_CREATE_ERROR
}

export { AppEvent, AppDbEventCode, EventType };