import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { IRendererSetup, RENDERER } from "./services/renderer/Renderer.js";
import { ASSERT, MESSAGE } from "./utils.js";
import { WTK_INPUT_LOGIC } from "./services/input/InputSystem.js";
import { SCENES_LOGIC } from "./services/renderer/Scenes.js";
import { WTK_CANVAS_ID } from "./_gen_canvas.js";
import { SCENE_LOGIC } from "./services/renderer/scenes/Scene.js";


export enum WtkCanvasContextID {
    WEBGL,
    WEBGL2,
    WEBGPU,
    CANVAS_2D
}

export class WTK_CANVAS extends WTK_COMPONENT_LOGIC {
    private _input: WTK_INPUT_LOGIC | undefined = undefined;
    private _current: SCENE_LOGIC<RENDERER, IRendererSetup> | undefined = undefined;

    setInputManager(input: WTK_INPUT_LOGIC): void {
        this._input = input;
    }

    /////////////
    loadScene(scenes: SCENES_LOGIC, name: string): void {

        const canvas = this.getElement(WTK_CANVAS_ID);
        if (canvas) {
            const contextId = scenes.getContextId(name);
            if (contextId) {
                const context = canvas.getContext(getContextId(contextId), {
                    alpha: false,             /////
                    antialias: true           /////
                }, true);      
                if (context) {
                    /////////////////
                    switch (contextId) {
                        case WtkCanvasContextID.WEBGL2:
                            const ext = context.getExtension("EXT_color_buffer_float");
                            if (!ext) {
                                console.warn('WEBGL2 EXT_color_buffer_float not supported');
                            }
                            break;
                    }
                    const box = this.root.getBoundingClientRect();
                    console.warn(MESSAGE(this, `box=${JSON.stringify(box)}`));
                    context.canvas.width = box.width;
                    context.canvas.height = box.height;
                    const loaded = scenes.load(name, this.input, context);
                    if (loaded) {
                        this._current = scenes.current;
                    }
                    else {
                        // notify error
                    }
                }
                else {
                    // notify error
                }
            }
            else {
                // notify error
            }
        }
        else {
            // notify error
        }
    }

    get input(): WTK_INPUT_LOGIC {
        return ASSERT(this._input, MESSAGE(this, `input system undefined`));
    }

    get current(): SCENE_LOGIC<RENDERER,IRendererSetup>|undefined {
        return this._current;
    }
}



const getContextId = (type: WtkCanvasContextID) => {
    switch (type) {
        case WtkCanvasContextID.WEBGL:
            return "webgl";

        case WtkCanvasContextID.WEBGL2:
            return "webgl2";

        case WtkCanvasContextID.WEBGPU:
            return "webgpu";

        case WtkCanvasContextID.CANVAS_2D:
            return "2d";
    }
}