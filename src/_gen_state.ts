
import { AppState, AppStateService, IModelGroupItemState, IModelState, ModelGroupItemState } from "./AppState.js";
import { Constructor } from "./Constructor.js";
import { ModelGroupItem } from "./ModelGroup.js";
import { IAppObject } from "./_gen_object.js";
import { WTK_GEN_OBJECT } from "./_gen_object.js";
import { __skull__ } from "./_skull.js";

const APP_STATE_TYPE = "AppState";
const MODEL_STATE_TYPE = "ModelType";

export interface IAppState extends IAppObject {
    name: string,
    states: IModelState[],
    onReset?: (appState: AppState) => void;
};

export interface IModelItemState extends IAppObject {
    modelItem: Constructor<ModelGroupItem>;
    events: IModelGroupItemState;  
};

export interface IStateService extends IAppObject {
    stateName: string;
    appState: Constructor<AppState>;
};

export const WTK_GEN_APP_STATE = (settings: IAppState): Constructor<AppState> => {
    const skull = __skull__(AppState,{
        onCreate: _create_app_state_, 
        onInit: _init_app_state_, 
        settings: settings
    }) as Constructor<AppState>;
    return WTK_GEN_OBJECT(skull, settings) as Constructor<AppState>
};

export const WTK_GEN_MODEL_STATE = (settings: IModelItemState): Constructor<ModelGroupItemState<ModelGroupItem>> => {
    const modelStateType = class extends ModelGroupItemState<ModelGroupItem> {}
    const skull = __skull__(modelStateType, {
        onCreate: _create_model_state_, 
        onInit: _init_model_state_, 
        settings: settings
    }) as Constructor<ModelGroupItemState<ModelGroupItem>>;
    return WTK_GEN_OBJECT(skull, settings) as Constructor<ModelGroupItemState<ModelGroupItem>>
};

export const WTK_GEN_MODEL_STATE_SERVICE = (settings: IStateService): Constructor<AppStateService> => {
    const skull = __skull__(AppStateService, {
        onCreate: _create_model_state_service_, 
        onInit: _init_model_state_service_, 
        settings: settings
    }) as Constructor<AppStateService>;
    return WTK_GEN_OBJECT(skull, settings) as Constructor<AppStateService>;
};


const _create_app_state_ = (appState: AppState, settings: IAppState): void => {
    appState.setName(settings.name);
    appState.createStates(settings.states);

    if (settings?.onReset) {
        appState._onReset = settings.onReset;
    }

    appState.setObjectType(APP_STATE_TYPE);
};

const _init_app_state_ = async (appState: AppState, settings?: IAppState) => {};


const _create_model_state_ = (modelState: ModelGroupItemState<ModelGroupItem>, settings: IModelItemState): void => {
    modelState.__init(settings.modelItem);
    modelState.setUpdateEvents(settings.events);

    modelState.setObjectType(MODEL_STATE_TYPE);
};

const _init_model_state_ = async (modelState: ModelGroupItemState<ModelGroupItem>, settings?: IModelItemState) => {};


const _create_model_state_service_ = (service: AppStateService, settings: IStateService): void => {
    service.init(settings.appState, settings.stateName);
};

const _init_model_state_service_ = async (service: AppStateService, settings?: IStateService) => {};

