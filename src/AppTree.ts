import { WTK_COMPONENT_LOGIC } from "./AppComponent.js";
import { AppEntity, LogLevel } from "./AppEntity.js";
import { WTK_FILE_LOGIC } from "./AppFile.js";
import { AppObject } from "./AppObject.js";
import { ObjectLoader } from "./ObjectLoader.js";
import { ObjectStore } from "./ObjectStore.js";
import { Scope } from "./Scope.js";
import { WFSNodeType, WTK_GEN_FS_NODE } from "./_gen_fs_node.js";
import { WTK_GEN_OBJECT } from "./_gen_object.js";
import { ASSERT, MESSAGE } from "./utils.js";

const ROOT = '0';

export class WTK_APP_TREE_LOGIC extends AppObject {
    private _current: string | undefined = ROOT;
    private _fake_root: ObjectLoader | undefined = undefined;

    setRoot(root: ObjectLoader) {
        this._fake_root = root;
    }

    setCurrentDir(inode: string) {
        this._current = inode;
    }

    async changeDir(inode: string): Promise<void> {
        this._current = this.get(inode, Scope.INODE) == inode ? inode : ROOT;           /////////////////////
    }

    async createFile(name: string): Promise<void> {
        const p: AppEntity = this.get(this.current, Scope.ENTITY);
        if (p && p instanceof AppObject) {
            (<AppObject>p).getObjectStore(Scope.LOCAL).get(WTK_GEN_FS_NODE(WTK_FILE_LOGIC, {
                name,
                type: WFSNodeType.FILE
            }));
        }
    }

    getName(inode: string): string | undefined {
        return this.get(inode, Scope.NAME);
    }

    getPath(inode: string): string {
        const p = this.get(inode, Scope.PATH);

        const r = this.get(this._get_fake_root_!.inode, Scope.PATH);

        console.warn(p);
        console.warn(r);

        return this._fake_root_ ? p.replace(r, '') : p;
    }

    get(inode: string, scope: Scope): any {  ///
        let output: any = undefined;
        const m: ObjectLoader | undefined = this.getModule(ObjectLoader);
        if (m) {

            const l = inode.split(':').map(p => +p);

            let error: boolean = false;
            let e: AppEntity = m;
            let s: ObjectStore | undefined = undefined;

            //console.warn(`[e:inode ${e.inode}]`);

            l.concat([scope]).map((p) => {

                //console.warn(p);

                if (!error) {
                    if (p > 0) {
                        if (s) {
                            const c = s.find(p);
                            if (c) {
                                e = c;
                                try {
                                    s = (<ObjectLoader>e).getObjectStore(Scope.LOCAL);
                                }
                                catch (e) {
                                    s = undefined;
                                }
                            }
                            else {
                                // notify error
                            }
                        }
                        else {
                            // notify error
                        }
                    }
                    else {
                        try {

                            switch (p) {
                                case Scope.ROOT:
                                    s = e.fakeRoot ? (<ObjectLoader>e).getObjectStore(Scope.LOCAL) : (<ObjectLoader>e).getObjectStore(Scope.GLOBAL);
                                    break;
                                case Scope.LIST:
                                    output = s?.list() || [];
                                    break;
                                case Scope.MEMORY:
                                    output = (<AppObject>e).memory;
                                    break;
                                case Scope.NAME:
                                    output = (<AppEntity>e).__name__;
                                    break;
                                case Scope.PATH:

                                    let path: string[] = [];
                                    const r = m.inode.split(':').length;
                                    const inode = (<AppEntity>e).inode.split(':');

                                    for (let i = inode.length; i > r + 1; i--) {
                                        const c = inode.slice(0, i).join(':');
                                        const n = this.get(c, Scope.NAME);
                                        path.push(n);
                                        if (!n.endsWith('/')) {
                                            path.push('/');
                                        }
                                    }

                                    output = path.reverse().join("");
                                    break;
                                    
                                case Scope.TYPE:
                                    output = (<AppEntity>e).__type__;
                                    break;
                                case Scope.SLOTS:
                                    output = (<WTK_COMPONENT_LOGIC>e).listSlots();
                                    break;
                                case Scope.FLAGS:
                                    output = (<WTK_COMPONENT_LOGIC>e).nodeState;
                                    break;
                                case Scope.INODE:
                                    output = (<AppEntity>e).inode;
                                    break;
                                case Scope.STYLES:
                                    output = (<WTK_COMPONENT_LOGIC>e).styles;
                                    break;
                                case Scope.ENTITY:
                                    output = e;
                                    break;
                                default:
                                    break;
                            }

                        }
                        catch (error) {
                            this.log(this.module.inode, LogLevel.ERROR, MESSAGE(this, `(get) error`));
                        }

                    }
                }
            });
        }
        //console.warn(`==> ${output} `);
        return output;
    }

    get current(): string {
        return ASSERT(this._current, MESSAGE(this, `current directory undefined`));
    }

    get _fake_root_(): boolean {      //
        return this._fake_root != undefined;
    }

    get _get_fake_root_(): ObjectLoader|undefined {  //
        return this._fake_root;
    }

    log(i: string, level: LogLevel, message: string) {

        const p: string[] = [];
        let n: string[] = i.split(':');
        while (n.length > 1) {
            p.push(n.join(':'));
            n = n.slice(0, n.length - 1);
        }

        for (const inode of p) {
            let file: WTK_FILE_LOGIC | undefined = undefined;
            let s: ObjectStore | undefined = undefined;

            const m = this.get(inode, Scope.ENTITY);

            if (m) {

                if (m instanceof ObjectLoader) {
                    try {
                        s = m.getLocalStore();
                    }
                    catch (e) {
                        this.log(this.module.inode, LogLevel.ERROR, `${inode}: local store undefined`);
                    }
                }
                else if (m instanceof ObjectStore) {
                    s = m;
                }

                if (s) {
                    switch (level) {
                        case LogLevel.INFO:
                            file = s.get(InfoFile);
                            break;
                        case LogLevel.WARNING:
                            file = s.get(WarningFile);
                            break;
                        case LogLevel.ERROR:
                            file = s.get(ErrorFile);
                            break;
                        default:
                            ////
                            break;
                    }
                }

                if (file) {
                    //this.log(this.module.inode, LogLevel.INFO, `${inode}: write log`);
                    file.setAppModule(this.module);   /////////
                    file.writeLine(message);
                }
            }
            else {
                // error
                console.error(MESSAGE(this, `(log) ${inode}: not found`));
            }
        }
    }
}

export const AppTree = WTK_GEN_OBJECT(WTK_APP_TREE_LOGIC, {
    name: "App Tree"
});


/*************************************************************************/


const InfoFile = WTK_GEN_FS_NODE(WTK_FILE_LOGIC, {
    name: "log",
    type: WFSNodeType.FILE
});

const WarningFile = WTK_GEN_FS_NODE(WTK_FILE_LOGIC, {
    name: "warning",
    type: WFSNodeType.FILE
});

const ErrorFile = WTK_GEN_FS_NODE(WTK_FILE_LOGIC, {
    name: "error",
    type: WFSNodeType.FILE
});
